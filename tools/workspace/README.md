This package provides a Cypress component testing preset and custom Cypress commands for Angular Material and other UI elements. It also includes an executor to generate a README.md file for the workspace, listing and describing the projects within the workspace. The Cypress commands simplify testing Angular Material components and tables.

[![npm version](https://img.shields.io/npm/v/workspace?style=flat-square)](https://www.npmjs.com/package/workspace)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/workspace)
![npm](https://img.shields.io/npm/dm/workspace)
![NPM](https://img.shields.io/npm/l/workspace)

- [Installation](#installation)
- [Executors](#executors)
  - [readme](#readme)

# Installation

**Add the package to your workspace:**
```bash
yarn add workspace
```
**Install peer dependencies:**
```bash
yarn add @nx/angular @nx/cypress @nx/devkit @nx/plugin @rxap/plugin-utilities handlebars 
```
# Executors

## readme
> Generate the project readme


