# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](https://gitlab.com/rxap/packages/compare/@rxap/fs@1.0.0-dev.2...@rxap/fs@1.0.0) (2025-03-07)

**Note:** Version bump only for package @rxap/fs

# [1.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/fs@1.0.0-dev.1...@rxap/fs@1.0.0-dev.2) (2025-02-25)

### Bug Fixes

- update package groups ([2b1e357](https://gitlab.com/rxap/packages/commit/2b1e35754f020a89677daf058c00895cfac32e1d))

# 1.0.0-dev.1 (2025-02-25)

**Note:** Version bump only for package @rxap/fs
