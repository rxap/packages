Provides utilities for manipulating TypeScript code using the ts-morph library. It offers a fluent API to add, modify, and remove code elements such as classes, decorators, imports, and properties in both Angular and NestJS projects. This package simplifies common code generation and refactoring tasks.

[![npm version](https://img.shields.io/npm/v/@rxap/ts-morph?style=flat-square)](https://www.npmjs.com/package/@rxap/ts-morph)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ts-morph)
![npm](https://img.shields.io/npm/dm/@rxap/ts-morph)
![NPM](https://img.shields.io/npm/l/@rxap/ts-morph)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ts-morph
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ts-morph:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ts-morph:init
```
