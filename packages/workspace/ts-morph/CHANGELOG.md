# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.1.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.12-dev.0...@rxap/workspace-ts-morph@19.1.12-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.11...@rxap/workspace-ts-morph@19.1.12-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.11-dev.1...@rxap/workspace-ts-morph@19.1.11) (2025-03-07)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.11-dev.0...@rxap/workspace-ts-morph@19.1.11-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10...@rxap/workspace-ts-morph@19.1.11-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10-dev.4...@rxap/workspace-ts-morph@19.1.10) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10-dev.3...@rxap/workspace-ts-morph@19.1.10-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10-dev.2...@rxap/workspace-ts-morph@19.1.10-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10-dev.1...@rxap/workspace-ts-morph@19.1.10-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.10-dev.0...@rxap/workspace-ts-morph@19.1.10-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9...@rxap/workspace-ts-morph@19.1.10-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.13...@rxap/workspace-ts-morph@19.1.9) (2025-02-13)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.12...@rxap/workspace-ts-morph@19.1.9-dev.13) (2025-02-13)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.11...@rxap/workspace-ts-morph@19.1.9-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.10...@rxap/workspace-ts-morph@19.1.9-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.9...@rxap/workspace-ts-morph@19.1.9-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.8...@rxap/workspace-ts-morph@19.1.9-dev.9) (2025-02-07)

### Bug Fixes

- support async callback function ([fca7fab](https://gitlab.com/rxap/packages/commit/fca7fab364c5bec4dae262cddc18720e21bbd5ef))

## [19.1.9-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.7...@rxap/workspace-ts-morph@19.1.9-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.6...@rxap/workspace-ts-morph@19.1.9-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.5...@rxap/workspace-ts-morph@19.1.9-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.4...@rxap/workspace-ts-morph@19.1.9-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.3...@rxap/workspace-ts-morph@19.1.9-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.2...@rxap/workspace-ts-morph@19.1.9-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.1...@rxap/workspace-ts-morph@19.1.9-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.9-dev.0...@rxap/workspace-ts-morph@19.1.9-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.8...@rxap/workspace-ts-morph@19.1.9-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.8-dev.3...@rxap/workspace-ts-morph@19.1.8) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.8-dev.2...@rxap/workspace-ts-morph@19.1.8-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.8-dev.1...@rxap/workspace-ts-morph@19.1.8-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.8-dev.0...@rxap/workspace-ts-morph@19.1.8-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.7...@rxap/workspace-ts-morph@19.1.8-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.7-dev.1...@rxap/workspace-ts-morph@19.1.7) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.7-dev.0...@rxap/workspace-ts-morph@19.1.7-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.6...@rxap/workspace-ts-morph@19.1.7-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.6-dev.3...@rxap/workspace-ts-morph@19.1.6) (2024-10-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.6-dev.2...@rxap/workspace-ts-morph@19.1.6-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.6-dev.1...@rxap/workspace-ts-morph@19.1.6-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.6-dev.0...@rxap/workspace-ts-morph@19.1.6-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.5...@rxap/workspace-ts-morph@19.1.6-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.5-dev.1...@rxap/workspace-ts-morph@19.1.5) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.5-dev.0...@rxap/workspace-ts-morph@19.1.5-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4...@rxap/workspace-ts-morph@19.1.5-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4-dev.4...@rxap/workspace-ts-morph@19.1.4) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4-dev.3...@rxap/workspace-ts-morph@19.1.4-dev.4) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4-dev.2...@rxap/workspace-ts-morph@19.1.4-dev.3) (2024-08-21)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4-dev.1...@rxap/workspace-ts-morph@19.1.4-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.4-dev.0...@rxap/workspace-ts-morph@19.1.4-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3...@rxap/workspace-ts-morph@19.1.4-dev.0) (2024-08-12)

### Bug Fixes

- support custom base path ([885931a](https://gitlab.com/rxap/packages/commit/885931a9738cea074c7c3caac3047537685e206e))

## [19.1.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.8...@rxap/workspace-ts-morph@19.1.3) (2024-07-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.7...@rxap/workspace-ts-morph@19.1.3-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.6...@rxap/workspace-ts-morph@19.1.3-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.5...@rxap/workspace-ts-morph@19.1.3-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.4...@rxap/workspace-ts-morph@19.1.3-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.3...@rxap/workspace-ts-morph@19.1.3-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.2...@rxap/workspace-ts-morph@19.1.3-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.1...@rxap/workspace-ts-morph@19.1.3-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.3-dev.0...@rxap/workspace-ts-morph@19.1.3-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.2...@rxap/workspace-ts-morph@19.1.3-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.2-dev.0...@rxap/workspace-ts-morph@19.1.2) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1...@rxap/workspace-ts-morph@19.1.2-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.7...@rxap/workspace-ts-morph@19.1.1) (2024-06-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.6...@rxap/workspace-ts-morph@19.1.1-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.5...@rxap/workspace-ts-morph@19.1.1-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.4...@rxap/workspace-ts-morph@19.1.1-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.3...@rxap/workspace-ts-morph@19.1.1-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.2...@rxap/workspace-ts-morph@19.1.1-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.1...@rxap/workspace-ts-morph@19.1.1-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.1-dev.0...@rxap/workspace-ts-morph@19.1.1-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0...@rxap/workspace-ts-morph@19.1.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.5...@rxap/workspace-ts-morph@19.1.0) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.4...@rxap/workspace-ts-morph@19.1.0-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.3...@rxap/workspace-ts-morph@19.1.0-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.2...@rxap/workspace-ts-morph@19.1.0-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.1...@rxap/workspace-ts-morph@19.1.0-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.1.0-dev.0...@rxap/workspace-ts-morph@19.1.0-dev.1) (2024-06-05)

### Bug Fixes

- base path creation ([64ae570](https://gitlab.com/rxap/packages/commit/64ae57053062ad41db117c94152b2911e5809bb8))
- use the correct property ([d79a2d9](https://gitlab.com/rxap/packages/commit/d79a2d94ba5d1882556f59dc2d3f93500f668b26))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.0.2...@rxap/workspace-ts-morph@19.1.0-dev.0) (2024-06-04)

### Features

- add OverwriteOptions type ([c354ecf](https://gitlab.com/rxap/packages/commit/c354ecff141870c76302fd4dc490d1364ca32a4e))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.0.2-dev.0...@rxap/workspace-ts-morph@19.0.2) (2024-06-02)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.0.1...@rxap/workspace-ts-morph@19.0.2-dev.0) (2024-06-02)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@19.0.1-dev.0...@rxap/workspace-ts-morph@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@18.0.1...@rxap/workspace-ts-morph@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@18.0.1-dev.0...@rxap/workspace-ts-morph@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.3...@rxap/workspace-ts-morph@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.3-dev.0...@rxap/workspace-ts-morph@16.1.3) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.2...@rxap/workspace-ts-morph@16.1.3-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.2-dev.0...@rxap/workspace-ts-morph@16.1.2) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.1...@rxap/workspace-ts-morph@16.1.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.1-dev.0...@rxap/workspace-ts-morph@16.1.1) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.0...@rxap/workspace-ts-morph@16.1.1-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.1.0-dev.0...@rxap/workspace-ts-morph@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.0.2-dev.2...@rxap/workspace-ts-morph@16.1.0-dev.0) (2024-04-07)

### Features

- add TsMorphProjectTransform function ([d193cb0](https://gitlab.com/rxap/packages/commit/d193cb0d52925a7fe25f09643b89e2a3c40bc2e3))

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.0.2-dev.1...@rxap/workspace-ts-morph@16.0.2-dev.2) (2024-03-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.0.2-dev.0...@rxap/workspace-ts-morph@16.0.2-dev.1) (2024-03-05)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.0.1...@rxap/workspace-ts-morph@16.0.2-dev.0) (2024-02-26)

### Bug Fixes

- support optional project file filtering ([05882fb](https://gitlab.com/rxap/packages/commit/05882fb91b5eaa47c664fc1ae258daf104134ffe))

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@16.0.1-dev.0...@rxap/workspace-ts-morph@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/workspace-ts-morph

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.12...@rxap/workspace-ts-morph@16.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

# [0.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.11...@rxap/workspace-ts-morph@0.1.0-dev.12) (2023-10-16)

### Bug Fixes

- use utility CreateProject function to create a ts-morph Project instance ([78b308f](https://gitlab.com/rxap/packages/commit/78b308fd10747616c7c7f27e81501a4ad5052a77))

# [0.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.10...@rxap/workspace-ts-morph@0.1.0-dev.11) (2023-10-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.10...@rxap/workspace-ts-morph@0.1.0-dev.10) (2023-10-11)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# 0.1.0-dev.10 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Features

- support replace option ([1bb05ff](https://gitlab.com/rxap/packages/commit/1bb05ffb5caf8904bb7833639844b0ae7795a80f))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [0.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.8...@rxap/workspace-ts-morph@0.1.0-dev.9) (2023-09-27)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.7...@rxap/workspace-ts-morph@0.1.0-dev.8) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [0.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.6...@rxap/workspace-ts-morph@0.1.0-dev.7) (2023-09-12)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.5...@rxap/workspace-ts-morph@0.1.0-dev.6) (2023-09-07)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.4...@rxap/workspace-ts-morph@0.1.0-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.3...@rxap/workspace-ts-morph@0.1.0-dev.4) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-ts-morph

# [0.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.2...@rxap/workspace-ts-morph@0.1.0-dev.3) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.1...@rxap/workspace-ts-morph@0.1.0-dev.2) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.1.0-dev.0...@rxap/workspace-ts-morph@0.1.0-dev.1) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [0.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.0.2-dev.1...@rxap/workspace-ts-morph@0.1.0-dev.0) (2023-08-05)

### Features

- support replace option ([50f2ac4](https://gitlab.com/rxap/packages/commit/50f2ac4d89017027c51a51223cd58a466edef1d0))

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-ts-morph@0.0.2-dev.0...@rxap/workspace-ts-morph@0.0.2-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 0.0.2-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/workspace-ts-morph
