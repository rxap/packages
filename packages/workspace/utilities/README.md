Provides utilities for Nx workspaces, including file manipulation, project configuration, and code generation. It offers tools for reading, updating, and managing workspace files like &#x60;package.json&#x60;, &#x60;nx.json&#x60;, and &#x60;tsconfig.json&#x60;, as well as functionalities for project-related tasks such as determining project types, dependencies, and build outputs. This package simplifies common workspace operations and enhances developer productivity.

[![npm version](https://img.shields.io/npm/v/@rxap/workspace-utilities?style=flat-square)](https://www.npmjs.com/package/@rxap/workspace-utilities)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/workspace-utilities)
![npm](https://img.shields.io/npm/dm/@rxap/workspace-utilities)
![NPM](https://img.shields.io/npm/l/@rxap/workspace-utilities)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/workspace-utilities
```
**Execute the init generator:**
```bash
yarn nx g @rxap/workspace-utilities:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/workspace-utilities:init
```
