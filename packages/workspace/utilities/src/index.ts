// region ts-config
export * from './lib/ts-config/compile-on-save-definition';
export * from './lib/ts-config/compiler-options-definition';
export * from './lib/ts-config/exclude-definition';
export * from './lib/ts-config/extends-definition';
export * from './lib/ts-config/files-definition';
export * from './lib/ts-config/include-definition';
export * from './lib/ts-config/index';
export * from './lib/ts-config/interface';
export * from './lib/ts-config/references-definition';
export * from './lib/ts-config/ts-node-definition';
export * from './lib/ts-config/type-acquisition-definition';
// endregion

// region project
export * from './lib/project/foreach-init-project';
// endregion

// region package-json
export * from './lib/package-json/dependency';
export * from './lib/package-json/index';
export * from './lib/package-json/interface';
export * from './lib/package-json/package-exports-entry-object';
export * from './lib/package-json/package-exports-entry-or-fallback';
export * from './lib/package-json/package-exports-entry-path';
export * from './lib/package-json/package-exports-entry';
export * from './lib/package-json/package-exports-fallback';
export * from './lib/package-json/person';
export * from './lib/package-json/scripts-install-after';
export * from './lib/package-json/scripts-publish-after';
export * from './lib/package-json/scripts-restart';
export * from './lib/package-json/scripts-start';
export * from './lib/package-json/scripts-stop';
export * from './lib/package-json/scripts-test';
export * from './lib/package-json/scripts-uninstall-before';
export * from './lib/package-json/scripts-version-before';
// endregion

// region nx-json
export * from './lib/nx-json/index';
// endregion

// region nest
export * from './lib/nest/get-nest-api-prefix';
// endregion

// region deployment
export * from './lib/deployment/utilities';
// endregion

// region collection-json
export * from './lib/collection-json/index';
export * from './lib/collection-json/interface';
// endregion

// region coerce
export * from './lib/coerce/coerce-idea-exclude-folders';
export * from './lib/coerce/coerce-nx-json-cacheable-operation';
export * from './lib/coerce/coerce-nx-json-generators';
export * from './lib/coerce/coerce-nx-json-named-inputs';
export * from './lib/coerce/coerce-nx-plugin';
export * from './lib/coerce/coerce-target-defaults-dependency';
export * from './lib/coerce/coerce-target-defaults-input';
export * from './lib/coerce/coerce-target-defaults-output';
export * from './lib/coerce/coerce-target-defaults';
export * from './lib/coerce/coerce-target';
// endregion

// region builders-json
export * from './lib/builders-json/builder';
export * from './lib/builders-json/index';
export * from './lib/builders-json/interface';
// endregion

// region 
export * from './lib/build-angular-base-path';
export * from './lib/build-nest-base-path';
export * from './lib/build-nest-controller-name';
export * from './lib/coerce-assets';
export * from './lib/coerce-file';
export * from './lib/coerce-files-structure';
export * from './lib/coerce-ignore-pattern';
export * from './lib/coerce-lerna-json';
export * from './lib/coerce-project-tags';
export * from './lib/delete-recursive';
export * from './lib/delete-target';
export * from './lib/for-each-secondary-entry-point';
export * from './lib/generators';
export * from './lib/get-angular-major-version';
export * from './lib/get-build-output-for-project';
export * from './lib/get-default-generator-options';
export * from './lib/get-library-path-alias-name';
export * from './lib/get-nx-version';
export * from './lib/get-project';
export * from './lib/get-root-docker-options';
export * from './lib/get-target-configuration-name-list';
export * from './lib/get-workspace-name';
export * from './lib/get-workspace-scope';
export * from './lib/has-components';
export * from './lib/has-project-feature';
export * from './lib/has-target';
export * from './lib/is-already-executed';
export * from './lib/is-buildable';
export * from './lib/is-jetbrains-project';
export * from './lib/is-project';
export * from './lib/is-publishable';
export * from './lib/is-rxap-repository';
export * from './lib/is-standalone-workspace';
export * from './lib/json-file';
export * from './lib/ng-package-json';
export * from './lib/nx-json-file';
export * from './lib/package-json-file';
export * from './lib/process-build-args';
export * from './lib/project-package-name-mapping';
export * from './lib/project-utilities';
export * from './lib/remove-ignore-pattern';
export * from './lib/remove-target';
export * from './lib/search-file';
export * from './lib/serialized-schematic';
export * from './lib/skip-non-generators-project';
export * from './lib/skip-project';
export * from './lib/tree';
export * from './lib/ts-config-file';
export * from './lib/visit-tree';
export * from './lib/yaml-document';
export * from './lib/yaml-file';
// endregion
