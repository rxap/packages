# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [19.8.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.8.0-dev.0...@rxap/workspace-utilities@19.8.0-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.8.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.7.0...@rxap/workspace-utilities@19.8.0-dev.0) (2025-03-12)

### Features

- **workspace-utilities:** add function to check tsconfig.json existence ([5d11ccd](https://gitlab.com/rxap/packages/commit/5d11ccd12bb82282bce64c619de8cb8457cd35e5))

# [19.7.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.7.0-dev.0...@rxap/workspace-utilities@19.7.0) (2025-03-07)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.7.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.3-dev.0...@rxap/workspace-utilities@19.7.0-dev.0) (2025-02-26)

### Bug Fixes

- ensure the project.name property is set ([5836d0a](https://gitlab.com/rxap/packages/commit/5836d0a6333a3e06a36bd9bda9554d64cdcb5de2))

### Features

- add ngPackage json utilities ([67c91c8](https://gitlab.com/rxap/packages/commit/67c91c8dc10aaa04b7063f1e335d141adec1c67e))

## [19.6.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2...@rxap/workspace-utilities@19.6.3-dev.0) (2025-02-25)

### Bug Fixes

- check buildable project by tsconfig ([260be75](https://gitlab.com/rxap/packages/commit/260be754f55a446fd4df6a6f97d272d0e704ae19))

## [19.6.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2-dev.4...@rxap/workspace-utilities@19.6.2) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2-dev.3...@rxap/workspace-utilities@19.6.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2-dev.2...@rxap/workspace-utilities@19.6.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2-dev.1...@rxap/workspace-utilities@19.6.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.2-dev.0...@rxap/workspace-utilities@19.6.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1...@rxap/workspace-utilities@19.6.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.12...@rxap/workspace-utilities@19.6.1) (2025-02-13)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.11...@rxap/workspace-utilities@19.6.1-dev.12) (2025-02-13)

### Bug Fixes

- use correct params ([ed19ca8](https://gitlab.com/rxap/packages/commit/ed19ca82d46635da2d257c1ea54360d573bc9fba))

## [19.6.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.10...@rxap/workspace-utilities@19.6.1-dev.11) (2025-02-11)

### Bug Fixes

- safe access project sourceRoot property ([16ca874](https://gitlab.com/rxap/packages/commit/16ca8747120876ad90e38c0cc012c175741fda0b))

## [19.6.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.9...@rxap/workspace-utilities@19.6.1-dev.10) (2025-02-11)

### Bug Fixes

- search for project recursive ([5e3edc4](https://gitlab.com/rxap/packages/commit/5e3edc4ef182971bb6e069f52841794c64b37504))
- search for the project recursively ([08b7c8e](https://gitlab.com/rxap/packages/commit/08b7c8eab345d37f1dd503094f45fcf4af438cad))
- streamline implementation ([897b998](https://gitlab.com/rxap/packages/commit/897b99869c5b0574311e656b1d1a3e7cf3d0e44f))

## [19.6.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.8...@rxap/workspace-utilities@19.6.1-dev.9) (2025-02-10)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.7...@rxap/workspace-utilities@19.6.1-dev.8) (2025-02-07)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.6...@rxap/workspace-utilities@19.6.1-dev.7) (2025-01-30)

### Bug Fixes

- missing xml2js dep ([ed3bf87](https://gitlab.com/rxap/packages/commit/ed3bf878cbb3900e483866e065d3110c26839dda))

## [19.6.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.5...@rxap/workspace-utilities@19.6.1-dev.6) (2025-01-29)

### Bug Fixes

- extract root docker options ([ca7863d](https://gitlab.com/rxap/packages/commit/ca7863dc29344b88d1446bf0c1efffedc038921f))

## [19.6.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.4...@rxap/workspace-utilities@19.6.1-dev.5) (2025-01-29)

### Bug Fixes

- add jetbrains ignores ([0b59259](https://gitlab.com/rxap/packages/commit/0b5925911be0429e1634388816b91c0d419156ad))

## [19.6.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.3...@rxap/workspace-utilities@19.6.1-dev.4) (2025-01-29)

### Bug Fixes

- improve input flexibility ([670cda6](https://gitlab.com/rxap/packages/commit/670cda671245c36b459fe50c09fcd3014347403c))

## [19.6.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.2...@rxap/workspace-utilities@19.6.1-dev.3) (2025-01-28)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))

## [19.6.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.1...@rxap/workspace-utilities@19.6.1-dev.2) (2025-01-28)

### Bug Fixes

- support minimal interface ([cbbea4b](https://gitlab.com/rxap/packages/commit/cbbea4b6ff07c99bb983887c7dfdb7f07a359e40))

## [19.6.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.1-dev.0...@rxap/workspace-utilities@19.6.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.6.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.0...@rxap/workspace-utilities@19.6.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.6.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.0-dev.1...@rxap/workspace-utilities@19.6.0) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.6.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.6.0-dev.0...@rxap/workspace-utilities@19.6.0-dev.1) (2025-01-04)

### Bug Fixes

- remove dependency to angular ([937bf19](https://gitlab.com/rxap/packages/commit/937bf198fea95fba32384c9842bbac46dbf50d38))

# [19.6.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.5.1-dev.1...@rxap/workspace-utilities@19.6.0-dev.0) (2025-01-03)

### Bug Fixes

- check with full project path ([cf85542](https://gitlab.com/rxap/packages/commit/cf85542c51935955cca6d8038a0564a27e27014d))

### Features

- add CoerceNxPlugin function ([3065108](https://gitlab.com/rxap/packages/commit/30651084998f863b0dbd0417eeefd6675e027f1b))
- add ForeachInitProject function ([adc243c](https://gitlab.com/rxap/packages/commit/adc243cdfad10c2fba75a877229b415b6fec1a1a))
- add FsTree class ([8647222](https://gitlab.com/rxap/packages/commit/8647222bb98669b0720950c9c56a631f60e86197))
- add is n8n project function ([c3666f3](https://gitlab.com/rxap/packages/commit/c3666f37dc37569d936714acc36a1d87d04491a7))
- support nx plugins ([d8ca485](https://gitlab.com/rxap/packages/commit/d8ca4858aa1fd1f8529a75440d4a283a0f03a01e))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

## [19.5.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.5.1-dev.0...@rxap/workspace-utilities@19.5.1-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.5.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.5.0...@rxap/workspace-utilities@19.5.1-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.5.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.5.0-dev.0...@rxap/workspace-utilities@19.5.0) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.5.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.4-dev.0...@rxap/workspace-utilities@19.5.0-dev.0) (2024-11-05)

### Features

- add yaml document updater utility ([d45c4d3](https://gitlab.com/rxap/packages/commit/d45c4d37b280c98ff8c159924745793c7302587a))

## [19.4.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.3...@rxap/workspace-utilities@19.4.4-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.3-dev.3...@rxap/workspace-utilities@19.4.3) (2024-10-28)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.3-dev.2...@rxap/workspace-utilities@19.4.3-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.3-dev.1...@rxap/workspace-utilities@19.4.3-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.3-dev.0...@rxap/workspace-utilities@19.4.3-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.2...@rxap/workspace-utilities@19.4.3-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.2-dev.1...@rxap/workspace-utilities@19.4.2) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.2-dev.0...@rxap/workspace-utilities@19.4.2-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.1...@rxap/workspace-utilities@19.4.2-dev.0) (2024-08-30)

### Bug Fixes

- fallback to manuel search if not found with nx tools ([1018c59](https://gitlab.com/rxap/packages/commit/1018c593ca062b48523221818b75f0eaaf646690))

## [19.4.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.1-dev.1...@rxap/workspace-utilities@19.4.1) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.1-dev.0...@rxap/workspace-utilities@19.4.1-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.4.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0...@rxap/workspace-utilities@19.4.1-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.4.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.6...@rxap/workspace-utilities@19.4.0) (2024-07-30)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.4.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.5...@rxap/workspace-utilities@19.4.0-dev.6) (2024-07-09)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.4.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.4...@rxap/workspace-utilities@19.4.0-dev.5) (2024-07-03)

### Bug Fixes

- check if build target exists ([1df2bd1](https://gitlab.com/rxap/packages/commit/1df2bd1c6e8b885ad8efc2831c621e502f82c41c))

# [19.4.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.3...@rxap/workspace-utilities@19.4.0-dev.4) (2024-07-03)

### Bug Fixes

- support plugin based build targets ([d18058f](https://gitlab.com/rxap/packages/commit/d18058f9673cf3f61abe59ccad18e8370d79c636))

# [19.4.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.2...@rxap/workspace-utilities@19.4.0-dev.3) (2024-07-03)

### Bug Fixes

- ensure the path prefix has the correct prefix ([7245197](https://gitlab.com/rxap/packages/commit/7245197af9286e5ff573eac0e5dbad078e7f4f35))

# [19.4.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.1...@rxap/workspace-utilities@19.4.0-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.4.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.4.0-dev.0...@rxap/workspace-utilities@19.4.0-dev.1) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.4.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.2-dev.0...@rxap/workspace-utilities@19.4.0-dev.0) (2024-07-02)

### Features

- add ForEachSecondaryEntryPoint Generator ([f10f7dc](https://gitlab.com/rxap/packages/commit/f10f7dc9ba02e763b152e259a2b03527ed7af842))

## [19.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.1...@rxap/workspace-utilities@19.3.2-dev.0) (2024-07-01)

### Bug Fixes

- correctly support pre release versions ([d97e9be](https://gitlab.com/rxap/packages/commit/d97e9becc88c8667443474600a7f2ead00f920ce))

## [19.3.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.1-dev.0...@rxap/workspace-utilities@19.3.1) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.0...@rxap/workspace-utilities@19.3.1-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.3.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.0-dev.3...@rxap/workspace-utilities@19.3.0) (2024-06-28)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.3.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.0-dev.2...@rxap/workspace-utilities@19.3.0-dev.3) (2024-06-25)

### Bug Fixes

- return latest if package is not installed ([a72b49b](https://gitlab.com/rxap/packages/commit/a72b49b3b1941b518d35dda14942885ca34333b0))

# [19.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.0-dev.1...@rxap/workspace-utilities@19.3.0-dev.2) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

# [19.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.3.0-dev.0...@rxap/workspace-utilities@19.3.0-dev.1) (2024-06-21)

### Bug Fixes

- check if generators property is defined ([8ff4c36](https://gitlab.com/rxap/packages/commit/8ff4c366d957a93306c240eae7404f1625dbcc5b))
- check if tree like is valid ([2a6a6a6](https://gitlab.com/rxap/packages/commit/2a6a6a699e88b3e761b061308550121050b5808d))
- include the schematics property ([3d86ee1](https://gitlab.com/rxap/packages/commit/3d86ee11c3f39b0c853b1d92c77b7b79278dbaf8))
- normalize paths ([2d92fe1](https://gitlab.com/rxap/packages/commit/2d92fe1207676fff7acc957a56f290157200d37e))

### Features

- add HasGenerator function ([200a944](https://gitlab.com/rxap/packages/commit/200a944ce3d34901d073411c6f7e92314b745907))
- add run once guard ([92136b9](https://gitlab.com/rxap/packages/commit/92136b9a0bfac5f78ed5217a724576542d01b867))

### Performance Improvements

- improve project json file search ([0e6de0d](https://gitlab.com/rxap/packages/commit/0e6de0de54e2bad4d0dfca9c0ee7b9cb1f3ef6f6))

# [19.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.1-dev.0...@rxap/workspace-utilities@19.3.0-dev.0) (2024-06-20)

### Features

- add ngPackageJson utilities ([3433419](https://gitlab.com/rxap/packages/commit/343341938f3b59b33bfcafbaea7ec2da3aaa78e9))

## [19.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0...@rxap/workspace-utilities@19.2.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.9...@rxap/workspace-utilities@19.2.0) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.8...@rxap/workspace-utilities@19.2.0-dev.9) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.7...@rxap/workspace-utilities@19.2.0-dev.8) (2024-06-18)

### Bug Fixes

- ensure only valid options are stored ([c754404](https://gitlab.com/rxap/packages/commit/c75440438fe03e550dad208b46b378aee888a995))

# [19.2.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.6...@rxap/workspace-utilities@19.2.0-dev.7) (2024-06-17)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.5...@rxap/workspace-utilities@19.2.0-dev.6) (2024-06-17)

### Bug Fixes

- cleanup object properties ([750629a](https://gitlab.com/rxap/packages/commit/750629aa1ef97398573982fdabc44252675cdba5))

### Features

- add get major version functions ([dfb514a](https://gitlab.com/rxap/packages/commit/dfb514ae53c9b603cfd57b425f6d32d9e8f12ead))
- add IsPresetProject function ([ef2976a](https://gitlab.com/rxap/packages/commit/ef2976a55085a7cfd56faf17fe8e28d0857ae7e3))

# [19.2.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.4...@rxap/workspace-utilities@19.2.0-dev.5) (2024-06-17)

### Bug Fixes

- only write if changed ([ef80d39](https://gitlab.com/rxap/packages/commit/ef80d39ecd135712ce393c4d1260efc8e795cdcc))
- resolve small issues ([b561b2e](https://gitlab.com/rxap/packages/commit/b561b2eef6e9b3b769319a3b68369d2c7f706375))
- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

# [19.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.3...@rxap/workspace-utilities@19.2.0-dev.4) (2024-06-10)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.2...@rxap/workspace-utilities@19.2.0-dev.3) (2024-06-05)

### Bug Fixes

- base path creation ([64ae570](https://gitlab.com/rxap/packages/commit/64ae57053062ad41db117c94152b2911e5809bb8))
- pass backend option ([d8fb481](https://gitlab.com/rxap/packages/commit/d8fb481cf2f1f8baea2aee52ab438cf9d43211dd))
- propose dynamic service url ([3d2440c](https://gitlab.com/rxap/packages/commit/3d2440ca3a0ca1d1a20dd1daeb469bd3babe6c1e))
- use the correct property ([d79a2d9](https://gitlab.com/rxap/packages/commit/d79a2d94ba5d1882556f59dc2d3f93500f668b26))

### Features

- improve table action processing ([22ef86f](https://gitlab.com/rxap/packages/commit/22ef86f6d10fd8b9160fab64f31479055b56a69a))

# [19.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.1...@rxap/workspace-utilities@19.2.0-dev.2) (2024-06-04)

### Features

- add GetLibraryPathAliasName ([6587bec](https://gitlab.com/rxap/packages/commit/6587bec16c1766d67ebece7af735468ac94027e6))

# [19.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.2.0-dev.0...@rxap/workspace-utilities@19.2.0-dev.1) (2024-06-03)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.1.0...@rxap/workspace-utilities@19.2.0-dev.0) (2024-06-02)

### Features

- add RemoveTarget function ([e6ca1c0](https://gitlab.com/rxap/packages/commit/e6ca1c0d13e75bbb43e2d42fb2146468d852c031))

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.1.0-dev.1...@rxap/workspace-utilities@19.1.0) (2024-06-02)

**Note:** Version bump only for package @rxap/workspace-utilities

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.0.1...@rxap/workspace-utilities@19.1.0-dev.1) (2024-06-02)

### Features

- add HasMigration function ([c385e21](https://gitlab.com/rxap/packages/commit/c385e2139d27f839fcf97adbeb2ce85649396e50))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.0.1...@rxap/workspace-utilities@19.1.0-dev.0) (2024-05-31)

### Features

- add HasMigration function ([4ba111b](https://gitlab.com/rxap/packages/commit/4ba111b4cdc9729fcfb423e415f0b836cd4bcfca))

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@19.0.1-dev.0...@rxap/workspace-utilities@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/workspace-utilities

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@18.0.1...@rxap/workspace-utilities@19.0.1-dev.0) (2024-05-30)

### Bug Fixes

- exclude ignored folder from tree visit ([773732c](https://gitlab.com/rxap/packages/commit/773732caa461a41d6114af5abc0f9d230d33ebe5))

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@18.0.1-dev.0...@rxap/workspace-utilities@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-utilities

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.2...@rxap/workspace-utilities@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.3.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.2-dev.0...@rxap/workspace-utilities@16.3.2) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.1...@rxap/workspace-utilities@16.3.2-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.3.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.1-dev.0...@rxap/workspace-utilities@16.3.1) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.0...@rxap/workspace-utilities@16.3.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.3.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.0-dev.2...@rxap/workspace-utilities@16.3.0) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.0-dev.1...@rxap/workspace-utilities@16.3.0-dev.2) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.3.0-dev.0...@rxap/workspace-utilities@16.3.0-dev.1) (2024-05-16)

### Bug Fixes

- default the api prefix to an empty string ([783a551](https://gitlab.com/rxap/packages/commit/783a551674bb308a55105c9ad1bc510dd9ece435))

# [16.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.2.0...@rxap/workspace-utilities@16.3.0-dev.0) (2024-05-07)

### Features

- add RemoveIgnorePattern function ([ded572b](https://gitlab.com/rxap/packages/commit/ded572b64466a022844e882c254b1b6eb89d6a5c))

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.2.0-dev.0...@rxap/workspace-utilities@16.2.0) (2024-04-18)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0...@rxap/workspace-utilities@16.2.0-dev.0) (2024-04-17)

### Features

- add yaml update functions ([5b1f8c3](https://gitlab.com/rxap/packages/commit/5b1f8c3821a116612e35355a0ffaa50ca225482a))

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.16...@rxap/workspace-utilities@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.1.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.15...@rxap/workspace-utilities@16.1.0-dev.16) (2024-04-16)

### Bug Fixes

- not ignore yaml file changes ([e14c024](https://gitlab.com/rxap/packages/commit/e14c024dbdfda3b887920d439c51b246e108da3c))

# [16.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.14...@rxap/workspace-utilities@16.1.0-dev.15) (2024-04-16)

### Bug Fixes

- update package and tsConfig json type definitions ([0dba533](https://gitlab.com/rxap/packages/commit/0dba53340fb13188821fa64fbb197b7194bf046b))

# [16.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.13...@rxap/workspace-utilities@16.1.0-dev.14) (2024-04-15)

### Bug Fixes

- check if package is private or not ([7048367](https://gitlab.com/rxap/packages/commit/70483672efbe9fb8107c39003d61abd52e274a01))
- support custom type ([4732641](https://gitlab.com/rxap/packages/commit/473264194598852abb88025165018cf2bb146592))

# [16.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.12...@rxap/workspace-utilities@16.1.0-dev.13) (2024-04-12)

### Bug Fixes

- cleanup serialized options ([2adf5ae](https://gitlab.com/rxap/packages/commit/2adf5ae041f13aa5798778307ef5e173cbf6ca0c))
- skip tslib dep ([e913d35](https://gitlab.com/rxap/packages/commit/e913d353f53bbbf085d3c5125ca00e0adff68ca0))
- support .template files ([c168ad2](https://gitlab.com/rxap/packages/commit/c168ad265b06e301311c01c0392a81056577fcdd))
- use json update function ([252d59f](https://gitlab.com/rxap/packages/commit/252d59fc94f886a52559560943cab224ebd2365c))

### Features

- add DeleteTarget function ([a63e68d](https://gitlab.com/rxap/packages/commit/a63e68d940fc179f702615bda1643427b802e12e))
- add init-component generator ([a023d64](https://gitlab.com/rxap/packages/commit/a023d6402b09cf049b82f70a4a1a1e0c2a1671ae))
- add UpdateTsConfigPaths function ([b33f327](https://gitlab.com/rxap/packages/commit/b33f3279bf77c08c5980daa688587c6da8383193))

# [16.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.11...@rxap/workspace-utilities@16.1.0-dev.12) (2024-04-10)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.10...@rxap/workspace-utilities@16.1.0-dev.11) (2024-04-10)

### Bug Fixes

- format json files ([59c8c59](https://gitlab.com/rxap/packages/commit/59c8c59bc129116e28a753cdb814033f19b49983))

### Features

- add GetDefaultGeneratorOptions function ([bbe3a05](https://gitlab.com/rxap/packages/commit/bbe3a050d1e7f8d7d4a0bae0f408273c66da4e92))

# [16.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.9...@rxap/workspace-utilities@16.1.0-dev.10) (2024-04-09)

### Bug Fixes

- set proprs ([1075c59](https://gitlab.com/rxap/packages/commit/1075c599fe83066813c4e9fc7d8703f6e885789f))

### Features

- add schematic serialize functions ([9abea76](https://gitlab.com/rxap/packages/commit/9abea76e1d1471c1955c3ad7d47d4c0f247464a1))

# [16.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.8...@rxap/workspace-utilities@16.1.0-dev.9) (2024-04-07)

### Bug Fixes

- mv SkipNonAngularProject to utilities package ([abcbaa6](https://gitlab.com/rxap/packages/commit/abcbaa6e407714a9f2f426e6635974c10353bb3c))

# [16.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.7...@rxap/workspace-utilities@16.1.0-dev.8) (2024-04-03)

### Bug Fixes

- only return the scope ([bd17c40](https://gitlab.com/rxap/packages/commit/bd17c405fab6311f738f9658019ec97ac70f142b))

### Features

- add buildNestProjectDirectoryPath function ([c3ec499](https://gitlab.com/rxap/packages/commit/c3ec4991be78d19f1d6b69262da53588af609e46))

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.6...@rxap/workspace-utilities@16.1.0-dev.7) (2024-04-01)

**Note:** Version bump only for package @rxap/workspace-utilities

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.5...@rxap/workspace-utilities@16.1.0-dev.6) (2024-03-31)

### Features

- add function IsStandaloneWorkspace ([2170768](https://gitlab.com/rxap/packages/commit/21707681de04678366f611676abe6e206c1ffd38))

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.4...@rxap/workspace-utilities@16.1.0-dev.5) (2024-03-31)

### Bug Fixes

- ensure dependencies are promoted and only added once ([3dc8c72](https://gitlab.com/rxap/packages/commit/3dc8c72fb6c8368f20e1afb06edd632dc0a14101))

### Features

- add function GetWorkspaceName ([54784c6](https://gitlab.com/rxap/packages/commit/54784c64c7d3d03e5934c5217648a1fd6532dec8))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.3...@rxap/workspace-utilities@16.1.0-dev.4) (2024-03-23)

### Bug Fixes

- skip merge if current file is empty ([b6c983c](https://gitlab.com/rxap/packages/commit/b6c983ce59523af571a7671a20382d4717f02bc2))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.2...@rxap/workspace-utilities@16.1.0-dev.3) (2024-03-14)

### Features

- add CoerceTargetDefaults function ([793f347](https://gitlab.com/rxap/packages/commit/793f3476cff8afce23353b28d1472865fac64886))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.1...@rxap/workspace-utilities@16.1.0-dev.2) (2024-03-11)

### Features

- add GetProjectByPackageName function ([04e0534](https://gitlab.com/rxap/packages/commit/04e053436d3d1b7e74da909bdadda837ba871ad4))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.1.0-dev.0...@rxap/workspace-utilities@16.1.0-dev.1) (2024-03-11)

### Bug Fixes

- automatically install all peer dependencies ([cfe3f9e](https://gitlab.com/rxap/packages/commit/cfe3f9ed42382b0ec87e066a6863516bfce45c39))

### Features

- auto install peer dependencies ([0a7f45b](https://gitlab.com/rxap/packages/commit/0a7f45bf5f7b66449cd275cba77e51333f2c548d))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.0.2-dev.0...@rxap/workspace-utilities@16.1.0-dev.0) (2024-03-11)

### Features

- support service without api prefix ([84f1f3c](https://gitlab.com/rxap/packages/commit/84f1f3cafe80d20768868ff07f643c96706adfe1))

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.0.1...@rxap/workspace-utilities@16.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@16.0.1-dev.0...@rxap/workspace-utilities@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/workspace-utilities

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.27...@rxap/workspace-utilities@16.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

# [0.1.0-dev.27](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.26...@rxap/workspace-utilities@0.1.0-dev.27) (2024-02-05)

### Features

- add GetNxVersion function ([73b1bf0](https://gitlab.com/rxap/packages/commit/73b1bf0be0f7bc8550027c0e1d7b4fa5f3ba32d8))

# [0.1.0-dev.26](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.25...@rxap/workspace-utilities@0.1.0-dev.26) (2023-10-27)

### Bug Fixes

- support env and runtime inputs ([12a26c8](https://gitlab.com/rxap/packages/commit/12a26c8845910c440d8ba0a352da6cdbd05bfcd1))

# [0.1.0-dev.25](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.24...@rxap/workspace-utilities@0.1.0-dev.25) (2023-10-25)

### Features

- introduce yaml and json merge strategy ([d246c8f](https://gitlab.com/rxap/packages/commit/d246c8fde4fe9f53c54b5b9bc06ce433846261ba))

# [0.1.0-dev.24](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.23...@rxap/workspace-utilities@0.1.0-dev.24) (2023-10-16)

### Bug Fixes

- **deps:** update semver package version ([6f70c8a](https://gitlab.com/rxap/packages/commit/6f70c8a9810196350abdc8535db898e2cf175068))

# [0.1.0-dev.23](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.22...@rxap/workspace-utilities@0.1.0-dev.23) (2023-10-11)

**Note:** Version bump only for package @rxap/workspace-utilities

# 0.1.0-dev.22 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- align duplicated implementation ([ba2b218](https://gitlab.com/rxap/packages/commit/ba2b218b143b662bd72ac4bc392e761dd1b1eaa3))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- enforce that the production configuration is the default configuration ([00ac30e](https://gitlab.com/rxap/packages/commit/00ac30e65dbe1008bff6d4f149631405fc81c200))
- ensure new line ([f24d277](https://gitlab.com/rxap/packages/commit/f24d277ab2003620486c5f2541c59c4051512cf9))
- ensure script are ordered to the top ([c9d5007](https://gitlab.com/rxap/packages/commit/c9d5007766bf1cb00e6442c6a3df452ecb13c211))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- ensure the project root is always defined ([f3cdb92](https://gitlab.com/rxap/packages/commit/f3cdb92f7be3fbd24f5976faa6e2fb66ea08e7f5))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- introduce Is\*Project functions ([3c9f251](https://gitlab.com/rxap/packages/commit/3c9f251f1d7be46ca366171e79e86ef2764fa3b0))
- introduce more Is\*Project functions ([41a3713](https://gitlab.com/rxap/packages/commit/41a3713e2965f46900e80902a455b62e08686989))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- remove logs ([52c630a](https://gitlab.com/rxap/packages/commit/52c630a5caaa17141bab3996838758f9ccb8f43d))
- set default target value ([98db684](https://gitlab.com/rxap/packages/commit/98db68485308a9efbf6f85bb04550f1601752983))
- skip package version update if not changed ([a5a377e](https://gitlab.com/rxap/packages/commit/a5a377e56fa53f0aebff1a87f0482954e2944f08))
- support project configuration and nxjson ([cf7c3a9](https://gitlab.com/rxap/packages/commit/cf7c3a959927e777f9491a1df5de73a61361ce6c))
- support target configurations ([18eba90](https://gitlab.com/rxap/packages/commit/18eba90ec62981382217ddb5951be0c3da44ae2e))

### Features

- add CleanupPackageJson function ([08ad46c](https://gitlab.com/rxap/packages/commit/08ad46c1fe249aa3ab2ad349e856a5331d5b5d3f))
- add CoerceFilesStructure function ([89fdcca](https://gitlab.com/rxap/packages/commit/89fdccaf003b064b962423d5c7c7449dfd359c28))
- add CoerceLernaJson function ([2143ddc](https://gitlab.com/rxap/packages/commit/2143ddc52b11df0f7439029c02c78656159a5473))
- add function CoerceNxJsonCacheableOperation ([26a36a0](https://gitlab.com/rxap/packages/commit/26a36a0de1ed9edb93af4f34ef01188b95d20a59))
- add GetNestApiPrefix function ([e1f3de3](https://gitlab.com/rxap/packages/commit/e1f3de3c3dbceac65b3fc1d20f61b30715ea426b))
- add GetRootDockerOptions function ([36cf5a4](https://gitlab.com/rxap/packages/commit/36cf5a4273b5edf0331f46b0f671906ca49f7720))
- add IsRxapRepository function ([fccf69a](https://gitlab.com/rxap/packages/commit/fccf69ad2b42fea8e2aa640b185c29daaa7e5faa))
- add project target utilities ([f0075b7](https://gitlab.com/rxap/packages/commit/f0075b70fdbad46877379bfe417f6bb5a4264b78))
- add the DeleteRecursive function ([928e3e2](https://gitlab.com/rxap/packages/commit/928e3e2115d2eed67b77dc083d308fc4c89cb83b))
- **coerce:** add function CoerceTargetDefaultsDependency ([1fba1c8](https://gitlab.com/rxap/packages/commit/1fba1c88909e7874d4d172067f6f61905f789e1c))
- support angular library entrypoints ([29ceeb0](https://gitlab.com/rxap/packages/commit/29ceeb0e99fe374d1a51a74ca1ed72d8487e999c))
- support tree as input ([e920f75](https://gitlab.com/rxap/packages/commit/e920f75706d676ee262a77dd4d12ac65cf03e2cb))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [0.1.0-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.20...@rxap/workspace-utilities@0.1.0-dev.21) (2023-10-10)

### Features

- add GetNestApiPrefix function ([63489ea](https://gitlab.com/rxap/packages/commit/63489ea3a843d8f1810cd25c16dc93a5e769dc8f))

# [0.1.0-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.19...@rxap/workspace-utilities@0.1.0-dev.20) (2023-10-02)

### Bug Fixes

- support project configuration and nxjson ([c1e0836](https://gitlab.com/rxap/packages/commit/c1e0836cc86584cb2385409ff1d66baa9351e0eb))

### Features

- add function CoerceNxJsonCacheableOperation ([7db34b4](https://gitlab.com/rxap/packages/commit/7db34b4d282fbeb56bb0607b4a59ceb534741e8e))
- support tree as input ([bb18922](https://gitlab.com/rxap/packages/commit/bb18922994e4c95c15e8ea9b35775c78f89158db))

# [0.1.0-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.18...@rxap/workspace-utilities@0.1.0-dev.19) (2023-10-02)

### Bug Fixes

- introduce Is\*Project functions ([0f4a53a](https://gitlab.com/rxap/packages/commit/0f4a53a2a68c7f854d819c005a30957d8b1cb3c6))
- introduce more Is\*Project functions ([8d37211](https://gitlab.com/rxap/packages/commit/8d37211fb1906f90d7176cfcfe43f755f04a0fa6))

# [0.1.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.17...@rxap/workspace-utilities@0.1.0-dev.18) (2023-10-02)

### Bug Fixes

- align duplicated implementation ([801aee5](https://gitlab.com/rxap/packages/commit/801aee528f70db9f4736934b0186029d5650ed03))

# [0.1.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.16...@rxap/workspace-utilities@0.1.0-dev.17) (2023-09-27)

### Features

- add CoerceFilesStructure function ([feaaa06](https://gitlab.com/rxap/packages/commit/feaaa06b94509802f5c19f91546df41626d2b4dc))
- add CoerceLernaJson function ([2d41629](https://gitlab.com/rxap/packages/commit/2d41629e8424507a32f1984b0aec6c635f4fc72d))

# [0.1.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.15...@rxap/workspace-utilities@0.1.0-dev.16) (2023-09-19)

### Bug Fixes

- skip package version update if not changed ([c88491c](https://gitlab.com/rxap/packages/commit/c88491c830f31fcb61372d1f6f4db50d24d24340))

# [0.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.14...@rxap/workspace-utilities@0.1.0-dev.15) (2023-09-18)

### Bug Fixes

- set default target value ([a5e3e58](https://gitlab.com/rxap/packages/commit/a5e3e580411a504c4932f03a9d3bb398687a0ecf))

# [0.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.13...@rxap/workspace-utilities@0.1.0-dev.14) (2023-09-15)

### Bug Fixes

- remove logs ([8db6df7](https://gitlab.com/rxap/packages/commit/8db6df70e6426c4bb58422aa95bd21ac0e63c962))

# [0.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.12...@rxap/workspace-utilities@0.1.0-dev.13) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [0.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.11...@rxap/workspace-utilities@0.1.0-dev.12) (2023-09-12)

### Bug Fixes

- ensure script are ordered to the top ([9911a67](https://gitlab.com/rxap/packages/commit/9911a67967e87fe3aec5509d8df0ef174b2c9d30))

### Features

- add CleanupPackageJson function ([0b5c0ce](https://gitlab.com/rxap/packages/commit/0b5c0cee7d080b9948953dc856af20e2516cf024))

# [0.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.10...@rxap/workspace-utilities@0.1.0-dev.11) (2023-09-07)

**Note:** Version bump only for package @rxap/workspace-utilities

# [0.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.9...@rxap/workspace-utilities@0.1.0-dev.10) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-utilities

# [0.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.8...@rxap/workspace-utilities@0.1.0-dev.9) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-utilities

# [0.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.7...@rxap/workspace-utilities@0.1.0-dev.8) (2023-09-03)

### Features

- add GetRootDockerOptions function ([994ca21](https://gitlab.com/rxap/packages/commit/994ca2163e8b2e072cda55d9c86e0ebd539868c3))

# [0.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.6...@rxap/workspace-utilities@0.1.0-dev.7) (2023-09-02)

### Bug Fixes

- ensure the project root is always defined ([22c56fa](https://gitlab.com/rxap/packages/commit/22c56faf7157806170d2757636bd0d9d231ea7a3))

### Features

- add the DeleteRecursive function ([69c01e9](https://gitlab.com/rxap/packages/commit/69c01e90ba223be40ce178d226af2391550b8024))
- support angular library entrypoints ([af9fd2f](https://gitlab.com/rxap/packages/commit/af9fd2f9e03393b0f38b63f2c94a90b3f04802b9))

# [0.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.5...@rxap/workspace-utilities@0.1.0-dev.6) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [0.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.4...@rxap/workspace-utilities@0.1.0-dev.5) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [0.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.3...@rxap/workspace-utilities@0.1.0-dev.4) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [0.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.2...@rxap/workspace-utilities@0.1.0-dev.3) (2023-08-06)

### Bug Fixes

- ensure new line ([f98b261](https://gitlab.com/rxap/packages/commit/f98b26143521240ef649932d97db0054e32c0491))

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.1...@rxap/workspace-utilities@0.1.0-dev.2) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- enforce that the production configuration is the default configuration ([6e9c3b7](https://gitlab.com/rxap/packages/commit/6e9c3b7a58e92bcb5a1b9b772a34153b44acc8f9))

### Features

- add IsRxapRepository function ([4f3a8dd](https://gitlab.com/rxap/packages/commit/4f3a8dd9752c65139111494d2671d911290a816c))

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-utilities@0.1.0-dev.0...@rxap/workspace-utilities@0.1.0-dev.1) (2023-08-03)

### Bug Fixes

- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))
- support target configurations ([1250277](https://gitlab.com/rxap/packages/commit/125027798926c8a4bb534db67909c32bf4a9390d))

### Features

- add project target utilities ([660ac0e](https://gitlab.com/rxap/packages/commit/660ac0e7224d64533e64d54c02ed9fe47278aaa7))

# 0.1.0-dev.0 (2023-08-01)

### Features

- **coerce:** add function CoerceTargetDefaultsDependency ([e1b37c7](https://gitlab.com/rxap/packages/commit/e1b37c724a90db1bf8f3c82339b4c2838b66c385))
