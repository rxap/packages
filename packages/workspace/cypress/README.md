This package provides custom Cypress commands for Angular Material components, particularly for interacting with form fields and handling errors. It also includes an init generator that manages peer dependencies and runs init generators for those dependencies. The package aims to simplify testing Angular Material-based applications with Cypress.

[![npm version](https://img.shields.io/npm/v/@rxap/workspace-cypress?style=flat-square)](https://www.npmjs.com/package/@rxap/workspace-cypress)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/workspace-cypress)
![npm](https://img.shields.io/npm/dm/@rxap/workspace-cypress)
![NPM](https://img.shields.io/npm/l/@rxap/workspace-cypress)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/workspace-cypress
```
**Execute the init generator:**
```bash
yarn nx g @rxap/workspace-cypress:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/workspace-cypress:init
```
