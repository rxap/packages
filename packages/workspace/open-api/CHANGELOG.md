# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.14-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.14-dev.0...@rxap/workspace-open-api@19.0.14-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.14-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.13...@rxap/workspace-open-api@19.0.14-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.13](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.13-dev.1...@rxap/workspace-open-api@19.0.13) (2025-03-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.13-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.13-dev.0...@rxap/workspace-open-api@19.0.13-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.13-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12...@rxap/workspace-open-api@19.0.13-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.5...@rxap/workspace-open-api@19.0.12) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.4...@rxap/workspace-open-api@19.0.12-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.3...@rxap/workspace-open-api@19.0.12-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.2...@rxap/workspace-open-api@19.0.12-dev.3) (2025-02-19)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.1...@rxap/workspace-open-api@19.0.12-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.12-dev.0...@rxap/workspace-open-api@19.0.12-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11...@rxap/workspace-open-api@19.0.12-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.12...@rxap/workspace-open-api@19.0.11) (2025-02-13)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.11...@rxap/workspace-open-api@19.0.11-dev.12) (2025-02-13)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.10...@rxap/workspace-open-api@19.0.11-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.9...@rxap/workspace-open-api@19.0.11-dev.10) (2025-02-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.8...@rxap/workspace-open-api@19.0.11-dev.9) (2025-02-10)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.7...@rxap/workspace-open-api@19.0.11-dev.8) (2025-02-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.6...@rxap/workspace-open-api@19.0.11-dev.7) (2025-01-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.5...@rxap/workspace-open-api@19.0.11-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.4...@rxap/workspace-open-api@19.0.11-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.3...@rxap/workspace-open-api@19.0.11-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.2...@rxap/workspace-open-api@19.0.11-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.1...@rxap/workspace-open-api@19.0.11-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.11-dev.0...@rxap/workspace-open-api@19.0.11-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.10...@rxap/workspace-open-api@19.0.11-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.10-dev.3...@rxap/workspace-open-api@19.0.10) (2025-01-08)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.10-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.10-dev.2...@rxap/workspace-open-api@19.0.10-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.10-dev.1...@rxap/workspace-open-api@19.0.10-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.10-dev.0...@rxap/workspace-open-api@19.0.10-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9...@rxap/workspace-open-api@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9-dev.4...@rxap/workspace-open-api@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9-dev.3...@rxap/workspace-open-api@19.0.9-dev.4) (2024-11-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9-dev.2...@rxap/workspace-open-api@19.0.9-dev.3) (2024-11-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9-dev.1...@rxap/workspace-open-api@19.0.9-dev.2) (2024-11-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.9-dev.0...@rxap/workspace-open-api@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8...@rxap/workspace-open-api@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8-dev.4...@rxap/workspace-open-api@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8-dev.3...@rxap/workspace-open-api@19.0.8-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8-dev.2...@rxap/workspace-open-api@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8-dev.1...@rxap/workspace-open-api@19.0.8-dev.2) (2024-10-23)

### Bug Fixes

- support other content types then json ([0cf8732](https://gitlab.com/rxap/packages/commit/0cf8732426bec3b9231e38b78430fabe1c335ec5))

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.8-dev.0...@rxap/workspace-open-api@19.0.8-dev.1) (2024-10-04)

### Bug Fixes

- remove dep to angular package ([ba3fc6a](https://gitlab.com/rxap/packages/commit/ba3fc6acf51a9231587206fa54808cf8f206806b))

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.7...@rxap/workspace-open-api@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.7-dev.1...@rxap/workspace-open-api@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.7-dev.0...@rxap/workspace-open-api@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.6...@rxap/workspace-open-api@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.6-dev.1...@rxap/workspace-open-api@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.6-dev.0...@rxap/workspace-open-api@19.0.6-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5...@rxap/workspace-open-api@19.0.6-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.7...@rxap/workspace-open-api@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.6...@rxap/workspace-open-api@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.5...@rxap/workspace-open-api@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.4...@rxap/workspace-open-api@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.3...@rxap/workspace-open-api@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.2...@rxap/workspace-open-api@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.1...@rxap/workspace-open-api@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.5-dev.0...@rxap/workspace-open-api@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.4...@rxap/workspace-open-api@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.4-dev.0...@rxap/workspace-open-api@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3...@rxap/workspace-open-api@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3-dev.4...@rxap/workspace-open-api@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3-dev.3...@rxap/workspace-open-api@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3-dev.2...@rxap/workspace-open-api@19.0.3-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3-dev.1...@rxap/workspace-open-api@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.3-dev.0...@rxap/workspace-open-api@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2...@rxap/workspace-open-api@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2-dev.4...@rxap/workspace-open-api@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2-dev.3...@rxap/workspace-open-api@19.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2-dev.2...@rxap/workspace-open-api@19.0.2-dev.3) (2024-06-18)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2-dev.1...@rxap/workspace-open-api@19.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.2-dev.0...@rxap/workspace-open-api@19.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.1...@rxap/workspace-open-api@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@19.0.1-dev.0...@rxap/workspace-open-api@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@18.0.1...@rxap/workspace-open-api@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/workspace-open-api

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@18.0.1-dev.0...@rxap/workspace-open-api@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.2...@rxap/workspace-open-api@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.2-dev.0...@rxap/workspace-open-api@16.1.2) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.1...@rxap/workspace-open-api@16.1.2-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.1-dev.0...@rxap/workspace-open-api@16.1.1) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.0...@rxap/workspace-open-api@16.1.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/workspace-open-api

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.0-dev.1...@rxap/workspace-open-api@16.1.0) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-open-api

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.1.0-dev.0...@rxap/workspace-open-api@16.1.0-dev.1) (2024-05-27)

**Note:** Version bump only for package @rxap/workspace-open-api

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.2...@rxap/workspace-open-api@16.1.0-dev.0) (2024-04-18)

### Features

- support yaml files ([b8b7daa](https://gitlab.com/rxap/packages/commit/b8b7daae5571268f9043873c6d19d5c22b589beb))

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.2-dev.3...@rxap/workspace-open-api@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.2-dev.2...@rxap/workspace-open-api@16.0.2-dev.3) (2024-04-09)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.2-dev.1...@rxap/workspace-open-api@16.0.2-dev.2) (2024-03-26)

### Bug Fixes

- reduce the code duplication when generating the open api client sdk files ([1ea4ac1](https://gitlab.com/rxap/packages/commit/1ea4ac1beb38ddf9d404c72698d1a582bb8d9837)), closes [#36](https://gitlab.com/rxap/packages/issues/36)

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.2-dev.0...@rxap/workspace-open-api@16.0.2-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.1...@rxap/workspace-open-api@16.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@16.0.1-dev.0...@rxap/workspace-open-api@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.10...@rxap/workspace-open-api@16.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

## [0.0.2-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.9...@rxap/workspace-open-api@0.0.2-dev.10) (2023-10-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## [0.0.2-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.9...@rxap/workspace-open-api@0.0.2-dev.9) (2023-10-11)

**Note:** Version bump only for package @rxap/workspace-open-api

## 0.0.2-dev.9 (2023-10-11)

### Bug Fixes

- generate correct type for additionallyProperties set to true ([8c9641a](https://gitlab.com/rxap/packages/commit/8c9641a7a5343cd93ba95579f161563fc7328bab))
- generate generic unknown type ([e9068ba](https://gitlab.com/rxap/packages/commit/e9068bab79d42e7f8b279dde6e539d563d21d0ab))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- use correct import path for remote methods ([86ccd54](https://gitlab.com/rxap/packages/commit/86ccd54838fcd8432ecd9d0d93c3ab9dab74d2ca))
- write operation data with multi line ([f5d5a2c](https://gitlab.com/rxap/packages/commit/f5d5a2c6085181b4885f36156f7c6cd5b8170ea1))

## [0.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.7...@rxap/workspace-open-api@0.0.2-dev.8) (2023-09-27)

**Note:** Version bump only for package @rxap/workspace-open-api

## [0.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.6...@rxap/workspace-open-api@0.0.2-dev.7) (2023-09-19)

### Bug Fixes

- generate correct type for additionallyProperties set to true ([a2976ce](https://gitlab.com/rxap/packages/commit/a2976ce37c224db87d5fdbbd39d0afcd9652c36d))
- generate generic unknown type ([cad13a4](https://gitlab.com/rxap/packages/commit/cad13a4c32ae120adea3c31329f5d10d97f6b1cb))

## [0.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.5...@rxap/workspace-open-api@0.0.2-dev.6) (2023-09-16)

### Bug Fixes

- write operation data with multi line ([c3c59b6](https://gitlab.com/rxap/packages/commit/c3c59b61ea4880db2442bae0d0d11b7eb94e5d22))

## [0.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.4...@rxap/workspace-open-api@0.0.2-dev.5) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [0.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.3...@rxap/workspace-open-api@0.0.2-dev.4) (2023-09-12)

**Note:** Version bump only for package @rxap/workspace-open-api

## [0.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.2...@rxap/workspace-open-api@0.0.2-dev.3) (2023-09-07)

**Note:** Version bump only for package @rxap/workspace-open-api

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.1...@rxap/workspace-open-api@0.0.2-dev.2) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/workspace-open-api@0.0.2-dev.0...@rxap/workspace-open-api@0.0.2-dev.1) (2023-09-03)

**Note:** Version bump only for package @rxap/workspace-open-api

## 0.0.2-dev.0 (2023-09-02)

### Bug Fixes

- use correct import path for remote methods ([704dc70](https://gitlab.com/rxap/packages/commit/704dc7054c6083e6c450a2988010d8c7651b701e))
