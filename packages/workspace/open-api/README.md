This package provides utilities and generators for working with OpenAPI specifications in a workspace. It includes functionality for generating TypeScript interfaces, data sources, directives, remote methods, and operation commands based on an OpenAPI schema. The package also offers tools for loading OpenAPI configurations and resolving references within the schema.

[![npm version](https://img.shields.io/npm/v/@rxap/workspace-open-api?style=flat-square)](https://www.npmjs.com/package/@rxap/workspace-open-api)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/workspace-open-api)
![npm](https://img.shields.io/npm/dm/@rxap/workspace-open-api)
![NPM](https://img.shields.io/npm/l/@rxap/workspace-open-api)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/workspace-open-api
```
**Execute the init generator:**
```bash
yarn nx g @rxap/workspace-open-api:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/workspace-open-api:init
```
