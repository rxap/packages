# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.2.0...@rxap/n8n-utilities@1.2.1-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.2.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.2.0-dev.0...@rxap/n8n-utilities@1.2.0) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.1-dev.0...@rxap/n8n-utilities@1.2.0-dev.0) (2025-03-05)

### Features

- **open-api-node:** add dynamic authentication options and baseURL support ([18139ae](https://gitlab.com/rxap/packages/commit/18139ae519a8190f4edc5f91ba96a8a4ed2939bf))

## [1.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0...@rxap/n8n-utilities@1.1.1-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.5...@rxap/n8n-utilities@1.1.0) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.4...@rxap/n8n-utilities@1.1.0-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.3...@rxap/n8n-utilities@1.1.0-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.2...@rxap/n8n-utilities@1.1.0-dev.3) (2025-02-22)

### Bug Fixes

- add cache key to cached response ([e21cd62](https://gitlab.com/rxap/packages/commit/e21cd6281009bf0b5763d65197bc185b345a969b))
- use sha256 hash as cache key ([2eb1c4d](https://gitlab.com/rxap/packages/commit/2eb1c4d9a743ec39fe7f6d7a40d30d12fb946221))

### Features

- add cache utility function ([9b0823a](https://gitlab.com/rxap/packages/commit/9b0823a35d96f77dea13dc3edbef863ba9dccb84))
- add forEachItem function ([a127b98](https://gitlab.com/rxap/packages/commit/a127b98fafc144018e555cc51e47c6225f59cf7c))

# [1.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.1...@rxap/n8n-utilities@1.1.0-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.1.0-dev.0...@rxap/n8n-utilities@1.1.0-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-utilities

# [1.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-utilities@1.0.1-dev.0...@rxap/n8n-utilities@1.1.0-dev.0) (2025-02-17)

### Features

- add open api abstract classes ([b9726dc](https://gitlab.com/rxap/packages/commit/b9726dc01f4b11d33ac0d8214aebd89e0f090518))

## 1.0.1-dev.0 (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-utilities
