This package provides a cache node for n8n workflows, allowing users to cache items using a MinIO client. It supports both reading from and writing to the cache, with options to disable caching and configure bucket names and keys. The node can improve workflow performance by storing and retrieving data from a cache instead of re-executing expensive operations.

[![npm version](https://img.shields.io/npm/v/@rxap/n8n-nodes-cache?style=flat-square)](https://www.npmjs.com/package/@rxap/n8n-nodes-cache)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/n8n-nodes-cache)
![npm](https://img.shields.io/npm/dm/@rxap/n8n-nodes-cache)
![NPM](https://img.shields.io/npm/l/@rxap/n8n-nodes-cache)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/n8n-nodes-cache
```
**Install peer dependencies:**
```bash
yarn add n8n-workflow 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/n8n-nodes-cache:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/n8n-nodes-cache:init
```
