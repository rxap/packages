# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.6...@rxap/n8n-nodes-yaml@0.1.7-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.6](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.6-dev.0...@rxap/n8n-nodes-yaml@0.1.6) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.5...@rxap/n8n-nodes-yaml@0.1.6-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.5-dev.3...@rxap/n8n-nodes-yaml@0.1.5) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.5-dev.2...@rxap/n8n-nodes-yaml@0.1.5-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.5-dev.1...@rxap/n8n-nodes-yaml@0.1.5-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.5-dev.0...@rxap/n8n-nodes-yaml@0.1.5-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4...@rxap/n8n-nodes-yaml@0.1.5-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.5...@rxap/n8n-nodes-yaml@0.1.4) (2025-02-13)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.4...@rxap/n8n-nodes-yaml@0.1.4-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.3...@rxap/n8n-nodes-yaml@0.1.4-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.2...@rxap/n8n-nodes-yaml@0.1.4-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.1...@rxap/n8n-nodes-yaml@0.1.4-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.4-dev.0...@rxap/n8n-nodes-yaml@0.1.4-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.3...@rxap/n8n-nodes-yaml@0.1.4-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.3-dev.2...@rxap/n8n-nodes-yaml@0.1.3) (2025-01-08)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.3-dev.1...@rxap/n8n-nodes-yaml@0.1.3-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.3-dev.0...@rxap/n8n-nodes-yaml@0.1.3-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.2...@rxap/n8n-nodes-yaml@0.1.3-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.2-dev.0...@rxap/n8n-nodes-yaml@0.1.2) (2024-10-28)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.1...@rxap/n8n-nodes-yaml@0.1.2-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.1-dev.0...@rxap/n8n-nodes-yaml@0.1.1) (2024-08-22)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

## [0.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.0...@rxap/n8n-nodes-yaml@0.1.1-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

# [0.1.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.0-dev.2...@rxap/n8n-nodes-yaml@0.1.0) (2024-07-30)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.0-dev.1...@rxap/n8n-nodes-yaml@0.1.0-dev.2) (2024-07-30)

**Note:** Version bump only for package @rxap/n8n-nodes-yaml

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-yaml@0.1.0-dev.0...@rxap/n8n-nodes-yaml@0.1.0-dev.1) (2024-07-26)

### Features

- support multi set options ([09b4b84](https://gitlab.com/rxap/packages/commit/09b4b8455f8f07caf31886f6f246375e9de85f7d))

# 0.1.0-dev.0 (2024-07-26)

### Features

- add set value option ([b7cbf2b](https://gitlab.com/rxap/packages/commit/b7cbf2b0b26ca258466c1fc4352ebc2df6d3bcc3))
