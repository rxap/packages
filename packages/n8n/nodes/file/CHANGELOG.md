# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.2...@rxap/n8n-nodes-file@1.0.3-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.2-dev.0...@rxap/n8n-nodes-file@1.0.2) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.1...@rxap/n8n-nodes-file@1.0.2-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.1-dev.3...@rxap/n8n-nodes-file@1.0.1) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.1-dev.2...@rxap/n8n-nodes-file@1.0.1-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.1-dev.1...@rxap/n8n-nodes-file@1.0.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## [1.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-file@1.0.1-dev.0...@rxap/n8n-nodes-file@1.0.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-file

## 1.0.1-dev.0 (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-nodes-file
