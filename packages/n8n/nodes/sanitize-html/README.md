Sanitizes HTML content based on specified rules, allowing you to control which tags and attributes are permitted. This package provides a node for n8n that transforms HTML by removing potentially harmful or unwanted elements. It helps ensure that the HTML is safe and conforms to your desired standards.

[![npm version](https://img.shields.io/npm/v/@rxap/n8n-nodes-sanitize-html?style=flat-square)](https://www.npmjs.com/package/@rxap/n8n-nodes-sanitize-html)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/n8n-nodes-sanitize-html)
![npm](https://img.shields.io/npm/dm/@rxap/n8n-nodes-sanitize-html)
![NPM](https://img.shields.io/npm/l/@rxap/n8n-nodes-sanitize-html)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/n8n-nodes-sanitize-html
```
**Install peer dependencies:**
```bash
yarn add n8n-workflow 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/n8n-nodes-sanitize-html:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/n8n-nodes-sanitize-html:init
```
