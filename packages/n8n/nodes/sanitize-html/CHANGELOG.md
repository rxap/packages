# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.1...@rxap/n8n-nodes-sanitize-html@1.1.2-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

## [1.1.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.1-dev.1...@rxap/n8n-nodes-sanitize-html@1.1.1) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

## [1.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.1-dev.0...@rxap/n8n-nodes-sanitize-html@1.1.1-dev.1) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

## [1.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0...@rxap/n8n-nodes-sanitize-html@1.1.1-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.6...@rxap/n8n-nodes-sanitize-html@1.1.0) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.5...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.4...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.3...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.4) (2025-02-22)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.2...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.1...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# [1.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-sanitize-html@1.1.0-dev.0...@rxap/n8n-nodes-sanitize-html@1.1.0-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-nodes-sanitize-html

# 1.1.0-dev.0 (2025-02-17)

### Features

- add SanitizeHtml node ([2c5e93d](https://gitlab.com/rxap/packages/commit/2c5e93d4032c3c6eb06d7fa1ce4024462c24dd5c))
