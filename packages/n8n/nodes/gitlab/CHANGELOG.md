# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-gitlab@0.1.0...@rxap/n8n-nodes-gitlab@0.1.1-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-gitlab

# [0.1.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-gitlab@0.1.0-dev.1...@rxap/n8n-nodes-gitlab@0.1.0) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-gitlab

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-gitlab@0.1.0-dev.0...@rxap/n8n-nodes-gitlab@0.1.0-dev.1) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-gitlab

# 0.1.0-dev.0 (2025-03-05)

### Features

- **gitlab:** add gitlab n8n node ([81096b4](https://gitlab.com/rxap/packages/commit/81096b4866cc16e02f87f0eff17e1c7b18411bae))
