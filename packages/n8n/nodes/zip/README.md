This n8n node package provides functionality to interact with zip files, specifically to extract them. It allows users to extract zip files from a binary property and make the extracted files available as binary data in subsequent nodes.

[![npm version](https://img.shields.io/npm/v/@rxap/n8n-nodes-zip?style=flat-square)](https://www.npmjs.com/package/@rxap/n8n-nodes-zip)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/n8n-nodes-zip)
![npm](https://img.shields.io/npm/dm/@rxap/n8n-nodes-zip)
![NPM](https://img.shields.io/npm/l/@rxap/n8n-nodes-zip)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/n8n-nodes-zip
```
**Install peer dependencies:**
```bash
yarn add n8n-workflow 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/n8n-nodes-zip:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/n8n-nodes-zip:init
```
