# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.3...@rxap/n8n-nodes-open-webui@0.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.3-dev.2...@rxap/n8n-nodes-open-webui@0.0.3) (2025-03-07)

### Bug Fixes

- **open-webui:** update authentication endpoint ([3f1b7da](https://gitlab.com/rxap/packages/commit/3f1b7da8f105212bf2bc50df91dae594c14ebe78))

## [0.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.3-dev.1...@rxap/n8n-nodes-open-webui@0.0.3-dev.2) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.3-dev.0...@rxap/n8n-nodes-open-webui@0.0.3-dev.1) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2...@rxap/n8n-nodes-open-webui@0.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.5...@rxap/n8n-nodes-open-webui@0.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.4...@rxap/n8n-nodes-open-webui@0.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.3...@rxap/n8n-nodes-open-webui@0.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.2...@rxap/n8n-nodes-open-webui@0.0.2-dev.3) (2025-02-22)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.1...@rxap/n8n-nodes-open-webui@0.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-open-webui@0.0.2-dev.0...@rxap/n8n-nodes-open-webui@0.0.2-dev.1) (2025-02-18)

### Bug Fixes

- export utility credentials ([f899192](https://gitlab.com/rxap/packages/commit/f89919221f0206305aa47884893394681a349c52))

## 0.0.2-dev.0 (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-nodes-open-webui
