This package provides an n8n node for interacting with the Open Web UI. It leverages the &#x60;@rxap/n8n-utilities&#x60; package and an OpenAPI definition to simplify the integration. The node allows users to connect and utilize the functionalities exposed by the Open Web UI.

[![npm version](https://img.shields.io/npm/v/@rxap/n8n-nodes-open-webui?style=flat-square)](https://www.npmjs.com/package/@rxap/n8n-nodes-open-webui)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/n8n-nodes-open-webui)
![npm](https://img.shields.io/npm/dm/@rxap/n8n-nodes-open-webui)
![NPM](https://img.shields.io/npm/l/@rxap/n8n-nodes-open-webui)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/n8n-nodes-open-webui
```
**Install peer dependencies:**
```bash
yarn add n8n-workflow 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/n8n-nodes-open-webui:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/n8n-nodes-open-webui:init
```
