# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.4...@rxap/n8n-nodes-cqrs@0.0.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.4-dev.1...@rxap/n8n-nodes-cqrs@0.0.4) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.4-dev.0...@rxap/n8n-nodes-cqrs@0.0.4-dev.1) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.3...@rxap/n8n-nodes-cqrs@0.0.4-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2...@rxap/n8n-nodes-cqrs@0.0.3) (2025-02-23)

### Bug Fixes

- use unique queue per trigger instance ([27b8853](https://gitlab.com/rxap/packages/commit/27b88531c1370c35c29003e549472c49ca83c209))

## [0.0.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2-dev.4...@rxap/n8n-nodes-cqrs@0.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2-dev.3...@rxap/n8n-nodes-cqrs@0.0.2-dev.4) (2025-02-23)

### Bug Fixes

- add missing property ([c327530](https://gitlab.com/rxap/packages/commit/c3275307587cef8890ea5b92e3d9a76f3f95d1d8))
- always forward replay payload ([c45fe84](https://gitlab.com/rxap/packages/commit/c45fe843b26a824dd064a161b3efe7999cc494ff))
- prevent dabble conversation ([6bb0a4f](https://gitlab.com/rxap/packages/commit/6bb0a4f4622ac2a2ae619d7d8e3960bdaaf3b396))
- send correlation id in response ([064331b](https://gitlab.com/rxap/packages/commit/064331b3a478a4fe50f594160b219303fb2d4e02))
- support new cqrs packages ([d399bb0](https://gitlab.com/rxap/packages/commit/d399bb044b7f9d169528d3ca5bb2f7ef18eb72af))

## [0.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2-dev.2...@rxap/n8n-nodes-cqrs@0.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2-dev.1...@rxap/n8n-nodes-cqrs@0.0.2-dev.2) (2025-02-22)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-cqrs@0.0.2-dev.0...@rxap/n8n-nodes-cqrs@0.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs

## 0.0.2-dev.0 (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-cqrs
