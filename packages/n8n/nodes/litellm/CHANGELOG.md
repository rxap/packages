# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-litellm@0.0.2...@rxap/n8n-nodes-litellm@0.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-litellm

## [0.0.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-litellm@0.0.2-dev.2...@rxap/n8n-nodes-litellm@0.0.2) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-litellm

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-litellm@0.0.2-dev.1...@rxap/n8n-nodes-litellm@0.0.2-dev.2) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-litellm

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-litellm@0.0.2-dev.0...@rxap/n8n-nodes-litellm@0.0.2-dev.1) (2025-02-25)

### Bug Fixes

- update package groups ([d426886](https://gitlab.com/rxap/packages/commit/d426886c1c11eb47529cebaca71df10cca9abb5b))

## 0.0.2-dev.0 (2025-02-25)

**Note:** Version bump only for package @rxap/n8n-nodes-litellm
