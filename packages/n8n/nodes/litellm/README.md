
[![npm version](https://img.shields.io/npm/v/@rxap/n8n-nodes-litellm?style=flat-square)](https://www.npmjs.com/package/@rxap/n8n-nodes-litellm)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/n8n-nodes-litellm)
![npm](https://img.shields.io/npm/dm/@rxap/n8n-nodes-litellm)
![NPM](https://img.shields.io/npm/l/@rxap/n8n-nodes-litellm)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/n8n-nodes-litellm
```
**Install peer dependencies:**
```bash
yarn add n8n-workflow 
```
