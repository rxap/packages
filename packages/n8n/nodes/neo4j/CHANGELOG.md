# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.7...@rxap/n8n-nodes-neo4j@0.0.8-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.7](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.7-dev.0...@rxap/n8n-nodes-neo4j@0.0.7) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.6...@rxap/n8n-nodes-neo4j@0.0.7-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.6](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.6-dev.3...@rxap/n8n-nodes-neo4j@0.0.6) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.6-dev.2...@rxap/n8n-nodes-neo4j@0.0.6-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.6-dev.1...@rxap/n8n-nodes-neo4j@0.0.6-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.6-dev.0...@rxap/n8n-nodes-neo4j@0.0.6-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5...@rxap/n8n-nodes-neo4j@0.0.6-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.5...@rxap/n8n-nodes-neo4j@0.0.5) (2025-02-13)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.4...@rxap/n8n-nodes-neo4j@0.0.5-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.3...@rxap/n8n-nodes-neo4j@0.0.5-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.2...@rxap/n8n-nodes-neo4j@0.0.5-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.1...@rxap/n8n-nodes-neo4j@0.0.5-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.5-dev.0...@rxap/n8n-nodes-neo4j@0.0.5-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.4...@rxap/n8n-nodes-neo4j@0.0.5-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.4](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.4-dev.2...@rxap/n8n-nodes-neo4j@0.0.4) (2025-01-08)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.4-dev.1...@rxap/n8n-nodes-neo4j@0.0.4-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.4-dev.0...@rxap/n8n-nodes-neo4j@0.0.4-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.3...@rxap/n8n-nodes-neo4j@0.0.4-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.3-dev.0...@rxap/n8n-nodes-neo4j@0.0.3) (2024-10-28)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.2...@rxap/n8n-nodes-neo4j@0.0.3-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.2-dev.1...@rxap/n8n-nodes-neo4j@0.0.2) (2024-08-22)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-neo4j@0.0.2-dev.0...@rxap/n8n-nodes-neo4j@0.0.2-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j

## 0.0.2-dev.0 (2024-08-07)

**Note:** Version bump only for package @rxap/n8n-nodes-neo4j
