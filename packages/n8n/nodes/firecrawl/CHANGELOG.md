# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.2.1-dev.0...@rxap/n8n-nodes-firecrawl@0.2.1-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

## [0.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.2.0...@rxap/n8n-nodes-firecrawl@0.2.1-dev.0) (2025-03-10)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

# [0.2.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.1...@rxap/n8n-nodes-firecrawl@0.2.0) (2025-03-07)

### Features

- **firecrawl:** add ToolFirecrawl node for advanced web scraping ([1a60d58](https://gitlab.com/rxap/packages/commit/1a60d5832f5b82bd8540196acbb157c8d26b3f49))

## [0.1.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.1-dev.1...@rxap/n8n-nodes-firecrawl@0.1.1) (2025-03-07)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

## [0.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.1-dev.0...@rxap/n8n-nodes-firecrawl@0.1.1-dev.1) (2025-03-05)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

## [0.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.0...@rxap/n8n-nodes-firecrawl@0.1.1-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

# [0.1.0](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.0-dev.3...@rxap/n8n-nodes-firecrawl@0.1.0) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

# [0.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.0-dev.2...@rxap/n8n-nodes-firecrawl@0.1.0-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.0-dev.1...@rxap/n8n-nodes-firecrawl@0.1.0-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/n8n-nodes-firecrawl

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/n8n-nodes-firecrawl@0.1.0-dev.0...@rxap/n8n-nodes-firecrawl@0.1.0-dev.1) (2025-02-22)

### Bug Fixes

- update package groups ([8ec6303](https://gitlab.com/rxap/packages/commit/8ec6303e186dad3e922780c042621a97c4153e69))

# 0.1.0-dev.0 (2025-02-22)

### Bug Fixes

- support other formats ([83520d4](https://gitlab.com/rxap/packages/commit/83520d427a8eb1af2737f9b163059362557519c2))

### Features

- add scrape url support ([c515ef8](https://gitlab.com/rxap/packages/commit/c515ef8e0a748ccbec5c8023bce4e77a3e67bda9))
- support cached requests ([83f7be9](https://gitlab.com/rxap/packages/commit/83f7be9f4cfb4cf6f5fa7c866df5f4864a4174d5))
