# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.3...@rxap/ngx-changelog@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.3-dev.1...@rxap/ngx-changelog@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.3-dev.0...@rxap/ngx-changelog@19.0.3-dev.1) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.2...@rxap/ngx-changelog@19.0.3-dev.0) (2025-02-26)

### Bug Fixes

- update package groups ([5810ff6](https://gitlab.com/rxap/packages/commit/5810ff609fe867842a6c9fc8531511dc1e97587a))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.2-dev.3...@rxap/ngx-changelog@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.2-dev.2...@rxap/ngx-changelog@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.2-dev.1...@rxap/ngx-changelog@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.2-dev.0...@rxap/ngx-changelog@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1...@rxap/ngx-changelog@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.6...@rxap/ngx-changelog@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.5...@rxap/ngx-changelog@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.4...@rxap/ngx-changelog@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.3...@rxap/ngx-changelog@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.2...@rxap/ngx-changelog@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.1...@rxap/ngx-changelog@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.1-dev.0...@rxap/ngx-changelog@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-changelog

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.0...@rxap/ngx-changelog@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-changelog

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.0-dev.2...@rxap/ngx-changelog@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-changelog

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@19.0.0-dev.1...@rxap/ngx-changelog@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-changelog

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.9-dev.0...@rxap/ngx-changelog@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.8...@rxap/ngx-changelog@18.0.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.8-dev.0...@rxap/ngx-changelog@18.0.8) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.7...@rxap/ngx-changelog@18.0.8-dev.0) (2024-12-03)

### Bug Fixes

- ensure the proper theme color is used ([f6f0a9f](https://gitlab.com/rxap/packages/commit/f6f0a9f5ff667cf5eff57d9fded13b89ccd8ffc7))

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.7-dev.0...@rxap/ngx-changelog@18.0.7) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.6...@rxap/ngx-changelog@18.0.7-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.6-dev.0...@rxap/ngx-changelog@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.5...@rxap/ngx-changelog@18.0.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.5-dev.1...@rxap/ngx-changelog@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.5-dev.0...@rxap/ngx-changelog@18.0.5-dev.1) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.4...@rxap/ngx-changelog@18.0.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.4-dev.0...@rxap/ngx-changelog@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.3...@rxap/ngx-changelog@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.3-dev.1...@rxap/ngx-changelog@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.3-dev.0...@rxap/ngx-changelog@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.2...@rxap/ngx-changelog@18.0.3-dev.0) (2024-06-20)

### Bug Fixes

- support custom last version store ([1e5c99e](https://gitlab.com/rxap/packages/commit/1e5c99e6b45712326d740f2f521d9ee409a0c0f5))

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.2-dev.2...@rxap/ngx-changelog@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.2-dev.1...@rxap/ngx-changelog@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.2-dev.0...@rxap/ngx-changelog@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.1...@rxap/ngx-changelog@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@18.0.1-dev.0...@rxap/ngx-changelog@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-changelog

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@17.0.1...@rxap/ngx-changelog@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-changelog

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@17.0.1...@rxap/ngx-changelog@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-changelog

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@17.0.1...@rxap/ngx-changelog@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-changelog

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@17.0.1-dev.0...@rxap/ngx-changelog@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-changelog

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.3...@rxap/ngx-changelog@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.3-dev.0...@rxap/ngx-changelog@16.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.2...@rxap/ngx-changelog@16.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.2-dev.0...@rxap/ngx-changelog@16.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.1...@rxap/ngx-changelog@16.1.2-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.1-dev.1...@rxap/ngx-changelog@16.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.1-dev.0...@rxap/ngx-changelog@16.1.1-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-changelog

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0...@rxap/ngx-changelog@16.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-changelog

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.7...@rxap/ngx-changelog@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-changelog

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.6...@rxap/ngx-changelog@16.1.0-dev.7) (2023-11-12)

### Bug Fixes

- check if changelog is disabled ([9aa1d85](https://gitlab.com/rxap/packages/commit/9aa1d856a938e412051297e85f980e50066239fb))

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.5...@rxap/ngx-changelog@16.1.0-dev.6) (2023-10-30)

**Note:** Version bump only for package @rxap/ngx-changelog

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.4...@rxap/ngx-changelog@16.1.0-dev.5) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-changelog

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.4...@rxap/ngx-changelog@16.1.0-dev.4) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-changelog

# 16.1.0-dev.4 (2023-10-11)

### Bug Fixes

- handle api error responses ([aea5ae6](https://gitlab.com/rxap/packages/commit/aea5ae6f03cda4812751ff8d5e56d81e9058d24d))
- move remember check to hook function ([a5b5fbe](https://gitlab.com/rxap/packages/commit/a5b5fbeb5695825a366ee0eba9c5bfcd08afcff9))
- use generate client sdk code ([18a64ba](https://gitlab.com/rxap/packages/commit/18a64baf9e8e2195fe6ab2fbac158a307a9175ca))

### Features

- add changelog dialog service ([3bc7d8e](https://gitlab.com/rxap/packages/commit/3bc7d8ed9e44eeb9c094e9b4abb42b9eb59c26c8))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.2...@rxap/ngx-changelog@16.1.0-dev.3) (2023-10-01)

### Bug Fixes

- use generate client sdk code ([a4dd413](https://gitlab.com/rxap/packages/commit/a4dd4134cbc7261bf5ee279d3f1a4c78974c3f12))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.1...@rxap/ngx-changelog@16.1.0-dev.2) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-changelog

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-changelog@16.1.0-dev.0...@rxap/ngx-changelog@16.1.0-dev.1) (2023-09-17)

### Bug Fixes

- move remember check to hook function ([9185ea0](https://gitlab.com/rxap/packages/commit/9185ea021ec2a6be2f812fd548b290635bfb4be4))

# 16.1.0-dev.0 (2023-09-15)

### Bug Fixes

- handle api error responses ([ce6f449](https://gitlab.com/rxap/packages/commit/ce6f4495e6e3860e706646371ee369cc40e3d122))

### Features

- add changelog dialog service ([83b7684](https://gitlab.com/rxap/packages/commit/83b768495a70d6c15d10a9f7d66219bbd735070e))
