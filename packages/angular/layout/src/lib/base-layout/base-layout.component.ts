import {
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';
import { FooterComponent } from '../footer/footer.component';
import { HeaderComponent } from '../header/header.component';

@Component({
    selector: 'rxap-base-layout',
    imports: [
        HeaderComponent,
        FooterComponent,
    ],
    templateUrl: './base-layout.component.html',
    styleUrl: './base-layout.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BaseLayoutComponent {}
