# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.3...@rxap/ngx-error@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.3-dev.1...@rxap/ngx-error@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.3-dev.0...@rxap/ngx-error@19.0.3-dev.1) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.2...@rxap/ngx-error@19.0.3-dev.0) (2025-02-26)

### Bug Fixes

- update package groups ([5810ff6](https://gitlab.com/rxap/packages/commit/5810ff609fe867842a6c9fc8531511dc1e97587a))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.2-dev.3...@rxap/ngx-error@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.2-dev.2...@rxap/ngx-error@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.2-dev.1...@rxap/ngx-error@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.2-dev.0...@rxap/ngx-error@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1...@rxap/ngx-error@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.6...@rxap/ngx-error@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.5...@rxap/ngx-error@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.4...@rxap/ngx-error@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.3...@rxap/ngx-error@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.2...@rxap/ngx-error@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.1...@rxap/ngx-error@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.1-dev.0...@rxap/ngx-error@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-error

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.0...@rxap/ngx-error@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-error

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.0-dev.2...@rxap/ngx-error@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-error

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@19.0.0-dev.1...@rxap/ngx-error@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-error

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.3-dev.0...@rxap/ngx-error@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-error

## [18.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.2...@rxap/ngx-error@18.1.3-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-error

## [18.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.2-dev.0...@rxap/ngx-error@18.1.2) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-error

## [18.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.1...@rxap/ngx-error@18.1.2-dev.0) (2024-12-03)

### Bug Fixes

- ensure the proper theme color is used ([f6f0a9f](https://gitlab.com/rxap/packages/commit/f6f0a9f5ff667cf5eff57d9fded13b89ccd8ffc7))

## [18.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.1-dev.0...@rxap/ngx-error@18.1.1) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-error

## [18.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.0...@rxap/ngx-error@18.1.1-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-error

# [18.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.1.0-dev.0...@rxap/ngx-error@18.1.0) (2024-09-18)

**Note:** Version bump only for package @rxap/ngx-error

# [18.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.6...@rxap/ngx-error@18.1.0-dev.0) (2024-08-27)

### Features

- add open api http response error handler ([542efd2](https://gitlab.com/rxap/packages/commit/542efd24c4965d0fdbe3b3d12dcf8e6ac6aa7ebb))

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.6-dev.0...@rxap/ngx-error@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.5...@rxap/ngx-error@18.0.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.5-dev.2...@rxap/ngx-error@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.5-dev.1...@rxap/ngx-error@18.0.5-dev.2) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.5-dev.0...@rxap/ngx-error@18.0.5-dev.1) (2024-07-22)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.4...@rxap/ngx-error@18.0.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.4-dev.0...@rxap/ngx-error@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.3...@rxap/ngx-error@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.3-dev.1...@rxap/ngx-error@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.3-dev.0...@rxap/ngx-error@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2...@rxap/ngx-error@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2-dev.4...@rxap/ngx-error@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2-dev.3...@rxap/ngx-error@18.0.2-dev.4) (2024-06-17)

### Bug Fixes

- update to latest sentry version ([9caf6ce](https://gitlab.com/rxap/packages/commit/9caf6ce42f72b053d1ea779a457fe73dd46dea8b))

## [18.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2-dev.2...@rxap/ngx-error@18.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2-dev.1...@rxap/ngx-error@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.2-dev.0...@rxap/ngx-error@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.1...@rxap/ngx-error@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@18.0.1-dev.0...@rxap/ngx-error@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-error

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@17.0.1...@rxap/ngx-error@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-error

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@17.0.1...@rxap/ngx-error@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-error

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@17.0.1...@rxap/ngx-error@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-error

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@17.0.1-dev.0...@rxap/ngx-error@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-error

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.3...@rxap/ngx-error@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.3-dev.0...@rxap/ngx-error@16.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.2...@rxap/ngx-error@16.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.2-dev.0...@rxap/ngx-error@16.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.1...@rxap/ngx-error@16.1.2-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.1-dev.3...@rxap/ngx-error@16.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.1-dev.2...@rxap/ngx-error@16.1.1-dev.3) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.1-dev.1...@rxap/ngx-error@16.1.1-dev.2) (2024-03-11)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.1-dev.0...@rxap/ngx-error@16.1.1-dev.1) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-error

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0...@rxap/ngx-error@16.1.1-dev.0) (2024-02-09)

### Bug Fixes

- remove FlexLayout dependency ([1fea895](https://gitlab.com/rxap/packages/commit/1fea895ea326d64e3ca230386175d1cd71d25ace))

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.7...@rxap/ngx-error@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-error

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.6...@rxap/ngx-error@16.1.0-dev.7) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-error

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.6...@rxap/ngx-error@16.1.0-dev.6) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-error

# 16.1.0-dev.6 (2023-10-11)

### Bug Fixes

- add missing padding ([23553a3](https://gitlab.com/rxap/packages/commit/23553a39fbb3dd7e9f810c96fe8e242c75bdb09f))
- ensure the error object is defined ([4b32def](https://gitlab.com/rxap/packages/commit/4b32defd3697a1f3c4ed421c3ff875f1c871cd5e))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- remove debug log ([3d23ce3](https://gitlab.com/rxap/packages/commit/3d23ce3b3e18067a1ed0e5c5182164c06754bda4))
- support small displaces ([1c62ebe](https://gitlab.com/rxap/packages/commit/1c62ebe496aa526ab2b923764475b370715a26d7))
- update token description ([284ccd6](https://gitlab.com/rxap/packages/commit/284ccd658576cdb4d8dbf2f2a7c10717de5c1e16))
- use correct unit ([838d4cc](https://gitlab.com/rxap/packages/commit/838d4cc8c1e1399f4bd1945ad7e0f935fdbe1c3c))

### Features

- add angular error dialog ([7fb2307](https://gitlab.com/rxap/packages/commit/7fb230723cfe69fb5bf0d0be7a223c895ca0bbf2))

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.4...@rxap/ngx-error@16.1.0-dev.5) (2023-09-30)

### Bug Fixes

- use correct unit ([7ddbf4f](https://gitlab.com/rxap/packages/commit/7ddbf4fbbdfff6399528feef6ca20c0125bbbb93))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.3...@rxap/ngx-error@16.1.0-dev.4) (2023-09-30)

### Bug Fixes

- support small displaces ([6465343](https://gitlab.com/rxap/packages/commit/64653433b5bf9f22c3ea1ad8060c418b29afd5f2))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.2...@rxap/ngx-error@16.1.0-dev.3) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-error

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.1...@rxap/ngx-error@16.1.0-dev.2) (2023-09-22)

### Bug Fixes

- remove debug log ([d3b39f5](https://gitlab.com/rxap/packages/commit/d3b39f5d7aeba6810a1c23d61240a35aa07268e1))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.1.0-dev.0...@rxap/ngx-error@16.1.0-dev.1) (2023-09-22)

### Bug Fixes

- update token description ([478e773](https://gitlab.com/rxap/packages/commit/478e7735b345ab8a9a205e266e109da3f1eeecd1))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.0.1-dev.4...@rxap/ngx-error@16.1.0-dev.0) (2023-09-21)

### Features

- add angular error dialog ([f7d3a36](https://gitlab.com/rxap/packages/commit/f7d3a369bfbd74f7a02d1ad46bfface528a035c4))

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.0.1-dev.3...@rxap/ngx-error@16.0.1-dev.4) (2023-09-15)

### Bug Fixes

- ensure the error object is defined ([a75e8a6](https://gitlab.com/rxap/packages/commit/a75e8a640afe26ab2d8179b1c3a6432d110666c3))

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.0.1-dev.2...@rxap/ngx-error@16.0.1-dev.3) (2023-09-14)

### Bug Fixes

- add missing padding ([30dd513](https://gitlab.com/rxap/packages/commit/30dd51326d1b8f938d0ff543c3cd2b4191b8d556))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.0.1-dev.1...@rxap/ngx-error@16.0.1-dev.2) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-error@16.0.1-dev.0...@rxap/ngx-error@16.0.1-dev.1) (2023-09-12)

**Note:** Version bump only for package @rxap/ngx-error

## 16.0.1-dev.0 (2023-09-08)

**Note:** Version bump only for package @rxap/ngx-error
