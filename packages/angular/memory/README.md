This package provides Angular services and decorators for interacting with web storage (localStorage and sessionStorage) and in-memory storage. It offers a simple way to manage data persistence with expiration options and provides decorators for binding class properties directly to storage values. The package includes services for local storage, session storage, and ephemeral (in-memory) storage.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-memory?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-memory)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-memory)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-memory)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-memory)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-memory
```
**Install peer dependencies:**
```bash
yarn add @angular/core 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-memory:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-memory:init
```
