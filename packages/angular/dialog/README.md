This package provides Angular Material-based message and confirm dialog components and services. It offers customizable dialogs for displaying messages and confirmations to users. The package includes components for rendering the dialogs and services for easily opening them with configurable options.

[![npm version](https://img.shields.io/npm/v/@rxap/dialog?style=flat-square)](https://www.npmjs.com/package/@rxap/dialog)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/dialog)
![npm](https://img.shields.io/npm/dm/@rxap/dialog)
![NPM](https://img.shields.io/npm/l/@rxap/dialog)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/dialog
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/material @rxap/pipes rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/dialog:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/dialog:init
```
