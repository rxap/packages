# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.3...@rxap/ngx-pub-sub@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.3-dev.0...@rxap/ngx-pub-sub@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.2...@rxap/ngx-pub-sub@19.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.2-dev.1...@rxap/ngx-pub-sub@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.2-dev.0...@rxap/ngx-pub-sub@19.0.2-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1...@rxap/ngx-pub-sub@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.6...@rxap/ngx-pub-sub@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.5...@rxap/ngx-pub-sub@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.4...@rxap/ngx-pub-sub@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.3...@rxap/ngx-pub-sub@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.2...@rxap/ngx-pub-sub@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.1...@rxap/ngx-pub-sub@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.1-dev.0...@rxap/ngx-pub-sub@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.0...@rxap/ngx-pub-sub@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.0-dev.2...@rxap/ngx-pub-sub@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@19.0.0-dev.1...@rxap/ngx-pub-sub@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.3-dev.0...@rxap/ngx-pub-sub@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.2...@rxap/ngx-pub-sub@18.1.3-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.2-dev.0...@rxap/ngx-pub-sub@18.1.2) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.1...@rxap/ngx-pub-sub@18.1.2-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.1-dev.0...@rxap/ngx-pub-sub@18.1.1) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.0...@rxap/ngx-pub-sub@18.1.1-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [18.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.0-dev.2...@rxap/ngx-pub-sub@18.1.0) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [18.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.0-dev.1...@rxap/ngx-pub-sub@18.1.0-dev.2) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-pub-sub

# [18.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.1.0-dev.0...@rxap/ngx-pub-sub@18.1.0-dev.1) (2024-07-09)

### Bug Fixes

- use static pub sub topics ([780d3f9](https://gitlab.com/rxap/packages/commit/780d3f9aae3b272713ad9946e7938f1f13dd91a4))

# [18.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.4...@rxap/ngx-pub-sub@18.1.0-dev.0) (2024-07-03)

### Features

- add topics object ([97b9bed](https://gitlab.com/rxap/packages/commit/97b9bed76aadf6871a14b0d2ef5ac22329125283))

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.4-dev.0...@rxap/ngx-pub-sub@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.3...@rxap/ngx-pub-sub@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.3-dev.2...@rxap/ngx-pub-sub@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.3-dev.1...@rxap/ngx-pub-sub@18.0.3-dev.2) (2024-06-26)

### Bug Fixes

- auto init the garbage collation ([58f2da8](https://gitlab.com/rxap/packages/commit/58f2da80e4f5a5b9f163fa20847c4dc210fda38b))

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.3-dev.0...@rxap/ngx-pub-sub@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.2...@rxap/ngx-pub-sub@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.2-dev.1...@rxap/ngx-pub-sub@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.2-dev.0...@rxap/ngx-pub-sub@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.1...@rxap/ngx-pub-sub@18.0.2-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@18.0.1-dev.0...@rxap/ngx-pub-sub@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@17.0.1...@rxap/ngx-pub-sub@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@17.0.1...@rxap/ngx-pub-sub@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@17.0.1...@rxap/ngx-pub-sub@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@17.0.1-dev.0...@rxap/ngx-pub-sub@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.4...@rxap/ngx-pub-sub@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.4-dev.0...@rxap/ngx-pub-sub@16.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.3...@rxap/ngx-pub-sub@16.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.3-dev.0...@rxap/ngx-pub-sub@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.2...@rxap/ngx-pub-sub@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.2-dev.1...@rxap/ngx-pub-sub@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.2-dev.0...@rxap/ngx-pub-sub@16.0.2-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.1...@rxap/ngx-pub-sub@16.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.1-dev.2...@rxap/ngx-pub-sub@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.1-dev.1...@rxap/ngx-pub-sub@16.0.1-dev.2) (2023-10-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-pub-sub@16.0.1-dev.1...@rxap/ngx-pub-sub@16.0.1-dev.1) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## 16.0.1-dev.1 (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-pub-sub

## 16.0.1-dev.0 (2023-09-30)

**Note:** Version bump only for package @rxap/ngx-pub-sub
