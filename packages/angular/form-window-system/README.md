Provides directives and services to simplify opening forms in windows within an Angular application. It includes features for submitting forms, managing window footers, and opening forms using methods. This package integrates with @rxap/forms and @rxap/window-system to streamline form-based workflows in a windowed environment.

[![npm version](https://img.shields.io/npm/v/@rxap/form-window-system?style=flat-square)](https://www.npmjs.com/package/@rxap/form-window-system)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/form-window-system)
![npm](https://img.shields.io/npm/dm/@rxap/form-window-system)
![NPM](https://img.shields.io/npm/l/@rxap/form-window-system)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/form-window-system
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/core @rxap/directives @rxap/form-system @rxap/forms @rxap/pattern @rxap/reflect-metadata @rxap/rxjs @rxap/utilities @rxap/window-system rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/form-window-system:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/form-window-system:init
```
