# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.3...@rxap/ngx-theme@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.3-dev.1...@rxap/ngx-theme@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.3-dev.0...@rxap/ngx-theme@19.0.3-dev.1) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.2...@rxap/ngx-theme@19.0.3-dev.0) (2025-02-25)

### Bug Fixes

- update package groups ([7060546](https://gitlab.com/rxap/packages/commit/7060546f6e8c0c7644bd18f5ad6b1ed62c672fc2))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.2-dev.3...@rxap/ngx-theme@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.2-dev.2...@rxap/ngx-theme@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.2-dev.1...@rxap/ngx-theme@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.2-dev.0...@rxap/ngx-theme@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1...@rxap/ngx-theme@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.6...@rxap/ngx-theme@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.5...@rxap/ngx-theme@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.4...@rxap/ngx-theme@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.3...@rxap/ngx-theme@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.2...@rxap/ngx-theme@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.1...@rxap/ngx-theme@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.1-dev.0...@rxap/ngx-theme@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-theme

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.0...@rxap/ngx-theme@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-theme

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.0-dev.2...@rxap/ngx-theme@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-theme

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@19.0.0-dev.1...@rxap/ngx-theme@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-theme

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.3-dev.0...@rxap/ngx-theme@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.2...@rxap/ngx-theme@18.1.3-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.2-dev.0...@rxap/ngx-theme@18.1.2) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.1...@rxap/ngx-theme@18.1.2-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.1-dev.0...@rxap/ngx-theme@18.1.1) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.0...@rxap/ngx-theme@18.1.1-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-theme

# [18.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.0-dev.2...@rxap/ngx-theme@18.1.0) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-theme

# [18.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.0-dev.1...@rxap/ngx-theme@18.1.0-dev.2) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-theme

# [18.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.1.0-dev.0...@rxap/ngx-theme@18.1.0-dev.1) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-theme

# [18.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.4...@rxap/ngx-theme@18.1.0-dev.0) (2024-07-03)

### Bug Fixes

- add publish option ([62559d6](https://gitlab.com/rxap/packages/commit/62559d613e53b9647e9ef849727067f72fcebe2e))

### Features

- use pubSub to update theme ([f400f0c](https://gitlab.com/rxap/packages/commit/f400f0c434c2c75e31f4bbb2d25e6f4c13e6704a))

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.4-dev.0...@rxap/ngx-theme@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.3...@rxap/ngx-theme@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.3-dev.1...@rxap/ngx-theme@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.3-dev.0...@rxap/ngx-theme@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.2...@rxap/ngx-theme@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.2-dev.2...@rxap/ngx-theme@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.2-dev.1...@rxap/ngx-theme@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.2-dev.0...@rxap/ngx-theme@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.1...@rxap/ngx-theme@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@18.0.1-dev.0...@rxap/ngx-theme@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-theme

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@17.0.1...@rxap/ngx-theme@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-theme

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@17.0.1...@rxap/ngx-theme@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-theme

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@17.0.1...@rxap/ngx-theme@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-theme

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@17.0.1-dev.0...@rxap/ngx-theme@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-theme

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.3...@rxap/ngx-theme@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.3-dev.0...@rxap/ngx-theme@16.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.2...@rxap/ngx-theme@16.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.2-dev.1...@rxap/ngx-theme@16.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.2-dev.0...@rxap/ngx-theme@16.1.2-dev.1) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.1...@rxap/ngx-theme@16.1.2-dev.0) (2024-04-29)

### Bug Fixes

- return null if explicit set to false ([285b99c](https://gitlab.com/rxap/packages/commit/285b99c01c6cd051238747150f0c11f0764239c2))

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.1-dev.1...@rxap/ngx-theme@16.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.1-dev.0...@rxap/ngx-theme@16.1.1-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-theme

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0...@rxap/ngx-theme@16.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-theme

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0-dev.4...@rxap/ngx-theme@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-theme

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0-dev.3...@rxap/ngx-theme@16.1.0-dev.4) (2023-10-30)

**Note:** Version bump only for package @rxap/ngx-theme

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0-dev.3...@rxap/ngx-theme@16.1.0-dev.3) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-theme

# 16.1.0-dev.3 (2023-10-11)

### Bug Fixes

- move restore logic in separate function ([f08facb](https://gitlab.com/rxap/packages/commit/f08facbe8e4e3bcf6741e5e1968d3a7ee35d7e85))
- move theme related code to the project angular-theme ([c0fd3bc](https://gitlab.com/rxap/packages/commit/c0fd3bc6de2b1b43ddafa0743bc9efe3e144ea72))
- only use media query to get prefers-color-scheme if dark mode is not defined in local storage ([99968a1](https://gitlab.com/rxap/packages/commit/99968a12b7fd46ef81a6d4e53f2f138ff357f249))
- use new angular-theme project ([67e9290](https://gitlab.com/rxap/packages/commit/67e9290c693b0f1c5ac087453897c0ee3d43521e))

### Features

- emit theme changes to pub-sub ([1b9e55b](https://gitlab.com/rxap/packages/commit/1b9e55ba0a4a2cc0c39483cbb195c4a13df796e3))
- restore theme settings from local storage ([89383c2](https://gitlab.com/rxap/packages/commit/89383c24a4dde3e7b0a43795995fd4badba97801))
- restore theme settings from user settings ([fd32708](https://gitlab.com/rxap/packages/commit/fd3270834279958e6650c099e55a812bde9c1c19))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0-dev.1...@rxap/ngx-theme@16.1.0-dev.2) (2023-10-01)

### Bug Fixes

- move restore logic in separate function ([e20fae2](https://gitlab.com/rxap/packages/commit/e20fae29d149245c7e629a764c7bdd3a10db6364))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.1.0-dev.0...@rxap/ngx-theme@16.1.0-dev.1) (2023-09-30)

### Bug Fixes

- only use media query to get prefers-color-scheme if dark mode is not defined in local storage ([f57380b](https://gitlab.com/rxap/packages/commit/f57380be1e0ff7783345d88d0671fc4bd7a469f2))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-theme@16.0.1-dev.0...@rxap/ngx-theme@16.1.0-dev.0) (2023-09-30)

### Features

- emit theme changes to pub-sub ([6bb2e41](https://gitlab.com/rxap/packages/commit/6bb2e414b8139f1f343d215d8ef79282fdf1d83c))
- restore theme settings from local storage ([b6782c5](https://gitlab.com/rxap/packages/commit/b6782c502f81dc88042f7172dec6b18a8cb42c4f))
- restore theme settings from user settings ([35d2fef](https://gitlab.com/rxap/packages/commit/35d2fefa3e5cd1c63962a6d9a1ba854679ee60c2))

## 16.0.1-dev.0 (2023-09-29)

### Bug Fixes

- move theme related code to the project angular-theme ([dff497a](https://gitlab.com/rxap/packages/commit/dff497a36e7dab5de535050c5ddafa7082bfa399))
- use new angular-theme project ([4d99def](https://gitlab.com/rxap/packages/commit/4d99def8e1b70a5c27e19bc5f5452ee1d5eadfa8))
