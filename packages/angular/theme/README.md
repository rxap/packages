This package provides an Angular theme service that allows you to manage and customize the look and feel of your application. It includes features such as dark mode support, theme density control, typography settings, and color palette management. The service also provides utilities for computing color palettes and observing theme density changes.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-theme?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-theme)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-theme)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-theme)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-theme)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-theme
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/core @rxap/config @rxap/ngx-pub-sub @rxap/rxjs @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-theme:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-theme:init
```
