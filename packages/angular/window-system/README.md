A module for creating and managing windows within an Angular application. It provides components for window containers, toolbars, resizers, action bars, and task bars, along with services for managing window instances and configurations. This library allows developers to create a desktop-like experience within their web applications.

[![npm version](https://img.shields.io/npm/v/@rxap/window-system?style=flat-square)](https://www.npmjs.com/package/@rxap/window-system)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/window-system)
![npm](https://img.shields.io/npm/dm/@rxap/window-system)
![NPM](https://img.shields.io/npm/l/@rxap/window-system)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/window-system
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/material @rxap/directives @rxap/material-directives @rxap/rxjs @rxap/services @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/window-system:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/window-system:init
```
