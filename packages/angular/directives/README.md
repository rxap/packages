This package provides a set of Angular directives to simplify common tasks such as setting background images, confirming clicks, making elements contenteditable, and more. It includes directives for handling UI interactions, applying styles, and managing asynchronous operations. These directives aim to reduce boilerplate code and improve the reusability of components in Angular applications.

[![npm version](https://img.shields.io/npm/v/@rxap/directives?style=flat-square)](https://www.npmjs.com/package/@rxap/directives)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/directives)
![npm](https://img.shields.io/npm/dm/@rxap/directives)
![NPM](https://img.shields.io/npm/l/@rxap/directives)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/directives
```
**Install peer dependencies:**
```bash
yarn add @angular/core @rxap/pattern @rxap/rxjs @rxap/services @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/directives:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/directives:init
```
