# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.3...@rxap/ngx-sentry@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.3-dev.0...@rxap/ngx-sentry@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.2...@rxap/ngx-sentry@19.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.2-dev.3...@rxap/ngx-sentry@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.2-dev.2...@rxap/ngx-sentry@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.2-dev.1...@rxap/ngx-sentry@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.2-dev.0...@rxap/ngx-sentry@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1...@rxap/ngx-sentry@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.6...@rxap/ngx-sentry@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.5...@rxap/ngx-sentry@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.4...@rxap/ngx-sentry@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.3...@rxap/ngx-sentry@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.2...@rxap/ngx-sentry@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.1...@rxap/ngx-sentry@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.1-dev.0...@rxap/ngx-sentry@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-sentry

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.0...@rxap/ngx-sentry@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-sentry

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.0-dev.2...@rxap/ngx-sentry@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-sentry

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@19.0.0-dev.1...@rxap/ngx-sentry@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-sentry

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.8-dev.0...@rxap/ngx-sentry@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.7...@rxap/ngx-sentry@18.0.8-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.7-dev.0...@rxap/ngx-sentry@18.0.7) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.6...@rxap/ngx-sentry@18.0.7-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.6-dev.0...@rxap/ngx-sentry@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5...@rxap/ngx-sentry@18.0.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5-dev.4...@rxap/ngx-sentry@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5-dev.3...@rxap/ngx-sentry@18.0.5-dev.4) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5-dev.2...@rxap/ngx-sentry@18.0.5-dev.3) (2024-07-22)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5-dev.1...@rxap/ngx-sentry@18.0.5-dev.2) (2024-07-18)

### Bug Fixes

- add missing dynamic configuration options ([1dc8a4f](https://gitlab.com/rxap/packages/commit/1dc8a4f2a33aaa508804bd919456a0cd784c8c94))

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.5-dev.0...@rxap/ngx-sentry@18.0.5-dev.1) (2024-07-18)

### Bug Fixes

- remove sentry tunnel default ([9f4af29](https://gitlab.com/rxap/packages/commit/9f4af29bb772cf5bbb3f91d258ce52ca1adae9bb))

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.4...@rxap/ngx-sentry@18.0.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.4-dev.0...@rxap/ngx-sentry@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.3...@rxap/ngx-sentry@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.3-dev.1...@rxap/ngx-sentry@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.3-dev.0...@rxap/ngx-sentry@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.2...@rxap/ngx-sentry@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.2-dev.3...@rxap/ngx-sentry@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.2-dev.2...@rxap/ngx-sentry@18.0.2-dev.3) (2024-06-17)

### Bug Fixes

- update to latest sentry version ([9caf6ce](https://gitlab.com/rxap/packages/commit/9caf6ce42f72b053d1ea779a457fe73dd46dea8b))

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.2-dev.1...@rxap/ngx-sentry@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.2-dev.0...@rxap/ngx-sentry@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.1...@rxap/ngx-sentry@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@18.0.1-dev.0...@rxap/ngx-sentry@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-sentry

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@17.0.1...@rxap/ngx-sentry@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-sentry

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@17.0.1...@rxap/ngx-sentry@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-sentry

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@17.0.1...@rxap/ngx-sentry@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-sentry

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@17.0.1-dev.0...@rxap/ngx-sentry@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-sentry

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.4...@rxap/ngx-sentry@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.4-dev.0...@rxap/ngx-sentry@16.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.3...@rxap/ngx-sentry@16.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.3-dev.0...@rxap/ngx-sentry@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.2...@rxap/ngx-sentry@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.2-dev.1...@rxap/ngx-sentry@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.2-dev.0...@rxap/ngx-sentry@16.0.2-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1...@rxap/ngx-sentry@16.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.12...@rxap/ngx-sentry@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.11...@rxap/ngx-sentry@16.0.1-dev.12) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.11...@rxap/ngx-sentry@16.0.1-dev.11) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-sentry

## 16.0.1-dev.11 (2023-10-11)

### Bug Fixes

- add browser-tailwind as imp dep if project has tailwind configuration ([3d90660](https://gitlab.com/rxap/packages/commit/3d906604470f4f26d157f4683afe72b3dd8baae3))
- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add tailwind bundle build target and configurations ([de3825a](https://gitlab.com/rxap/packages/commit/de3825a0e2977389f81cc4ce63e510767ca25810))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

## [16.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.9...@rxap/ngx-sentry@16.0.1-dev.10) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.8...@rxap/ngx-sentry@16.0.1-dev.9) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.7...@rxap/ngx-sentry@16.0.1-dev.8) (2023-09-12)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.6...@rxap/ngx-sentry@16.0.1-dev.7) (2023-09-07)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.5...@rxap/ngx-sentry@16.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.4...@rxap/ngx-sentry@16.0.1-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.3...@rxap/ngx-sentry@16.0.1-dev.4) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.2...@rxap/ngx-sentry@16.0.1-dev.3) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.1...@rxap/ngx-sentry@16.0.1-dev.2) (2023-08-03)

**Note:** Version bump only for package @rxap/ngx-sentry

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-sentry@16.0.1-dev.0...@rxap/ngx-sentry@16.0.1-dev.1) (2023-08-01)

### Bug Fixes

- add browser-tailwind as imp dep if project has tailwind configuration ([6ea13c5](https://gitlab.com/rxap/packages/commit/6ea13c5f9b4e652436bf1da879b564d1ed7b8061))
- add tailwind bundle build target and configurations ([bec6b96](https://gitlab.com/rxap/packages/commit/bec6b96be15bbc11ad072ccefdcaf7df9e8fea52))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 16.0.1-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/ngx-sentry
