Provides services and components to simplify the management of service worker updates in Angular applications. It includes features such as automatic updates, dialog prompts for updates, and logging of update events. The package also offers utilities to unregister service workers and initialize peer dependencies.

[![npm version](https://img.shields.io/npm/v/@rxap/service-worker?style=flat-square)](https://www.npmjs.com/package/@rxap/service-worker)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/service-worker)
![npm](https://img.shields.io/npm/dm/@rxap/service-worker)
![NPM](https://img.shields.io/npm/l/@rxap/service-worker)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/service-worker
```
**Install peer dependencies:**
```bash
yarn add @angular/common @angular/core @angular/service-worker @rxap/environment @rxap/life-cycle rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/service-worker:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/service-worker:init
```
