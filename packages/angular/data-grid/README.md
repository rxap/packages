Provides a data grid component for Angular applications. It supports plain and form modes, custom header and cell templates, and integration with Rxap Forms and Data Sources. The component allows for displaying and editing data in a tabular format with features like edit mode, validation, and data refresh.

[![npm version](https://img.shields.io/npm/v/@rxap/data-grid?style=flat-square)](https://www.npmjs.com/package/@rxap/data-grid)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/data-grid)
![npm](https://img.shields.io/npm/dm/@rxap/data-grid)
![NPM](https://img.shields.io/npm/l/@rxap/data-grid)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/data-grid
```
**Install peer dependencies:**
```bash
yarn add @angular/common @angular/core @angular/material @angular/router @rxap/forms @rxap/pattern @rxap/pipes @rxap/rxjs @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/data-grid:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/data-grid:init
```
