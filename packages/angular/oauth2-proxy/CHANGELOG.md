# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.3...@rxap/ngx-oauth2-proxy@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.3-dev.1...@rxap/ngx-oauth2-proxy@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.3-dev.0...@rxap/ngx-oauth2-proxy@19.0.3-dev.1) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2...@rxap/ngx-oauth2-proxy@19.0.3-dev.0) (2025-02-26)

### Bug Fixes

- update package groups ([5810ff6](https://gitlab.com/rxap/packages/commit/5810ff609fe867842a6c9fc8531511dc1e97587a))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2-dev.4...@rxap/ngx-oauth2-proxy@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2-dev.3...@rxap/ngx-oauth2-proxy@19.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2-dev.2...@rxap/ngx-oauth2-proxy@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2-dev.1...@rxap/ngx-oauth2-proxy@19.0.2-dev.2) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.2-dev.0...@rxap/ngx-oauth2-proxy@19.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1...@rxap/ngx-oauth2-proxy@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.6...@rxap/ngx-oauth2-proxy@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.5...@rxap/ngx-oauth2-proxy@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.4...@rxap/ngx-oauth2-proxy@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.3...@rxap/ngx-oauth2-proxy@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.2...@rxap/ngx-oauth2-proxy@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.1...@rxap/ngx-oauth2-proxy@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.1-dev.0...@rxap/ngx-oauth2-proxy@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.0...@rxap/ngx-oauth2-proxy@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.0-dev.2...@rxap/ngx-oauth2-proxy@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@19.0.0-dev.1...@rxap/ngx-oauth2-proxy@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.2-dev.0...@rxap/ngx-oauth2-proxy@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [18.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.1...@rxap/ngx-oauth2-proxy@18.1.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [18.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.1-dev.0...@rxap/ngx-oauth2-proxy@18.1.1) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [18.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.0...@rxap/ngx-oauth2-proxy@18.1.1-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [18.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.0-dev.2...@rxap/ngx-oauth2-proxy@18.1.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [18.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.0-dev.1...@rxap/ngx-oauth2-proxy@18.1.0-dev.2) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [18.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.1.0-dev.0...@rxap/ngx-oauth2-proxy@18.1.0-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# [18.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.1...@rxap/ngx-oauth2-proxy@18.1.0-dev.0) (2024-08-12)

### Features

- add user profile method and data source ([ff9b72b](https://gitlab.com/rxap/packages/commit/ff9b72b8977bffb4492ac0964486c68432c09ac6))

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.1-dev.2...@rxap/ngx-oauth2-proxy@18.0.1) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [18.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.1-dev.1...@rxap/ngx-oauth2-proxy@18.0.1-dev.2) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

## [18.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.1-dev.0...@rxap/ngx-oauth2-proxy@18.0.1-dev.1) (2024-07-18)

### Bug Fixes

- delay logout handling ([5ba4835](https://gitlab.com/rxap/packages/commit/5ba48357ec45b3d38ace8e279d76337c4e8f6b46))

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.0...@rxap/ngx-oauth2-proxy@18.0.1-dev.0) (2024-07-09)

### Bug Fixes

- use static pub sub topics ([780d3f9](https://gitlab.com/rxap/packages/commit/780d3f9aae3b272713ad9946e7938f1f13dd91a4))

# [18.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-oauth2-proxy@18.0.0-dev.1...@rxap/ngx-oauth2-proxy@18.0.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy

# 18.0.0-dev.1 (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-oauth2-proxy
