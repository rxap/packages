# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.3...@rxap/ngx-marker-io@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.3-dev.0...@rxap/ngx-marker-io@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2...@rxap/ngx-marker-io@19.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2-dev.4...@rxap/ngx-marker-io@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2-dev.3...@rxap/ngx-marker-io@19.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2-dev.2...@rxap/ngx-marker-io@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2-dev.1...@rxap/ngx-marker-io@19.0.2-dev.2) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.2-dev.0...@rxap/ngx-marker-io@19.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1...@rxap/ngx-marker-io@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.6...@rxap/ngx-marker-io@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.5...@rxap/ngx-marker-io@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.4...@rxap/ngx-marker-io@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.3...@rxap/ngx-marker-io@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.2...@rxap/ngx-marker-io@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.1...@rxap/ngx-marker-io@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.1-dev.0...@rxap/ngx-marker-io@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.0...@rxap/ngx-marker-io@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-marker-io

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.0-dev.2...@rxap/ngx-marker-io@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-marker-io

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@19.0.0-dev.1...@rxap/ngx-marker-io@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-marker-io

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@18.0.2-dev.0...@rxap/ngx-marker-io@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@18.0.1...@rxap/ngx-marker-io@18.0.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@18.0.1-dev.0...@rxap/ngx-marker-io@18.0.1) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-marker-io

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@18.0.0...@rxap/ngx-marker-io@18.0.1-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-marker-io

# [18.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-marker-io@18.0.0-dev.1...@rxap/ngx-marker-io@18.0.0) (2024-09-18)

**Note:** Version bump only for package @rxap/ngx-marker-io

# 18.0.0-dev.1 (2024-09-10)

**Note:** Version bump only for package @rxap/ngx-marker-io
