# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.1.0-dev.2...@rxap/ngx-localize@19.1.0-dev.3) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-localize

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.1.0-dev.1...@rxap/ngx-localize@19.1.0-dev.2) (2025-03-10)

### Bug Fixes

- **localize:** validate XLIFF files before processing ([84750ab](https://gitlab.com/rxap/packages/commit/84750ab2fcf4d2305939dc573b5e7b18d07e0f3a))

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.1.0-dev.0...@rxap/ngx-localize@19.1.0-dev.1) (2025-03-10)

### Features

- **localize:** add i18n bootstrap hook for dynamic locale loading ([22ab76c](https://gitlab.com/rxap/packages/commit/22ab76c57c7b86215b2075921124c3d6dff8e545))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.3...@rxap/ngx-localize@19.1.0-dev.0) (2025-03-10)

### Features

- **localize:** add xliff support for dynamic translations ([f6b4dda](https://gitlab.com/rxap/packages/commit/f6b4dda9e4a08817ac8b329f016ae15fed947529))

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.3-dev.0...@rxap/ngx-localize@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.2...@rxap/ngx-localize@19.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.2-dev.3...@rxap/ngx-localize@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.2-dev.2...@rxap/ngx-localize@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.2-dev.1...@rxap/ngx-localize@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.2-dev.0...@rxap/ngx-localize@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1...@rxap/ngx-localize@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.6...@rxap/ngx-localize@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.5...@rxap/ngx-localize@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.4...@rxap/ngx-localize@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.3...@rxap/ngx-localize@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.2...@rxap/ngx-localize@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.1...@rxap/ngx-localize@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.1-dev.0...@rxap/ngx-localize@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-localize

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.0...@rxap/ngx-localize@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-localize

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.0-dev.2...@rxap/ngx-localize@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-localize

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@19.0.0-dev.1...@rxap/ngx-localize@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-localize

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.8-dev.0...@rxap/ngx-localize@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.7...@rxap/ngx-localize@18.0.8-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.7-dev.0...@rxap/ngx-localize@18.0.7) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.6...@rxap/ngx-localize@18.0.7-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.6-dev.0...@rxap/ngx-localize@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.5...@rxap/ngx-localize@18.0.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.5-dev.1...@rxap/ngx-localize@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.5-dev.0...@rxap/ngx-localize@18.0.5-dev.1) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.4...@rxap/ngx-localize@18.0.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.4-dev.0...@rxap/ngx-localize@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.3...@rxap/ngx-localize@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.3-dev.3...@rxap/ngx-localize@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.3-dev.2...@rxap/ngx-localize@18.0.3-dev.3) (2024-06-27)

### Bug Fixes

- set the current language after the redirect is executed ([56b9caa](https://gitlab.com/rxap/packages/commit/56b9caa1004b5fc35dc985584399142a57f1f125))

## [18.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.3-dev.1...@rxap/ngx-localize@18.0.3-dev.2) (2024-06-24)

### Bug Fixes

- move language selector ([8cfbe0d](https://gitlab.com/rxap/packages/commit/8cfbe0ddc8417caa955dbe22dd5e85976149de51))

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.3-dev.0...@rxap/ngx-localize@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.2...@rxap/ngx-localize@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.2-dev.2...@rxap/ngx-localize@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.2-dev.1...@rxap/ngx-localize@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.2-dev.0...@rxap/ngx-localize@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.1...@rxap/ngx-localize@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@18.0.1-dev.0...@rxap/ngx-localize@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-localize

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@17.0.1...@rxap/ngx-localize@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-localize

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@17.0.1...@rxap/ngx-localize@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-localize

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@17.0.1...@rxap/ngx-localize@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-localize

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@17.0.1-dev.0...@rxap/ngx-localize@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-localize

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.3...@rxap/ngx-localize@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.3-dev.0...@rxap/ngx-localize@16.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.2...@rxap/ngx-localize@16.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.2-dev.0...@rxap/ngx-localize@16.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.1...@rxap/ngx-localize@16.1.2-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.1-dev.1...@rxap/ngx-localize@16.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.1-dev.0...@rxap/ngx-localize@16.1.1-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-localize

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0...@rxap/ngx-localize@16.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-localize

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0-dev.4...@rxap/ngx-localize@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-localize

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0-dev.3...@rxap/ngx-localize@16.1.0-dev.4) (2023-11-23)

### Bug Fixes

- always show the selected language ([78d02b7](https://gitlab.com/rxap/packages/commit/78d02b7c9918104e9cdf750b7d29a04e60a68187))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0-dev.2...@rxap/ngx-localize@16.1.0-dev.3) (2023-10-30)

**Note:** Version bump only for package @rxap/ngx-localize

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0-dev.2...@rxap/ngx-localize@16.1.0-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-localize

# 16.1.0-dev.2 (2023-10-11)

### Features

- add language utility services ([4c248be](https://gitlab.com/rxap/packages/commit/4c248be9cf00e8e96c859ffa86bff2bd19d4c9f6))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.1.0-dev.0...@rxap/ngx-localize@16.1.0-dev.1) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-localize

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-localize@16.0.1-dev.0...@rxap/ngx-localize@16.1.0-dev.0) (2023-09-19)

### Features

- add language utility services ([96e6785](https://gitlab.com/rxap/packages/commit/96e67855bfcb27f274393526d1ab81f6d7c2f24a))

## 16.0.1-dev.0 (2023-09-18)

**Note:** Version bump only for package @rxap/ngx-localize
