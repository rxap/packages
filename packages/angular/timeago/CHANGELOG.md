# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.3...@rxap/ngx-timeago@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.3-dev.0...@rxap/ngx-timeago@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.2...@rxap/ngx-timeago@19.0.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.2-dev.3...@rxap/ngx-timeago@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.2-dev.2...@rxap/ngx-timeago@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.2-dev.1...@rxap/ngx-timeago@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.2-dev.0...@rxap/ngx-timeago@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1...@rxap/ngx-timeago@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.6...@rxap/ngx-timeago@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.5...@rxap/ngx-timeago@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.4...@rxap/ngx-timeago@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.3...@rxap/ngx-timeago@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.2...@rxap/ngx-timeago@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.1...@rxap/ngx-timeago@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.1-dev.0...@rxap/ngx-timeago@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-timeago

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.0...@rxap/ngx-timeago@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-timeago

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.0-dev.2...@rxap/ngx-timeago@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-timeago

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@19.0.0-dev.1...@rxap/ngx-timeago@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-timeago

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.8-dev.0...@rxap/ngx-timeago@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.7...@rxap/ngx-timeago@18.0.8-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.7-dev.0...@rxap/ngx-timeago@18.0.7) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.6...@rxap/ngx-timeago@18.0.7-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.6-dev.0...@rxap/ngx-timeago@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.5...@rxap/ngx-timeago@18.0.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.5-dev.1...@rxap/ngx-timeago@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.5-dev.0...@rxap/ngx-timeago@18.0.5-dev.1) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.4...@rxap/ngx-timeago@18.0.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.4-dev.0...@rxap/ngx-timeago@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.3...@rxap/ngx-timeago@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.3-dev.1...@rxap/ngx-timeago@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.3-dev.0...@rxap/ngx-timeago@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.2...@rxap/ngx-timeago@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.2-dev.2...@rxap/ngx-timeago@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.2-dev.1...@rxap/ngx-timeago@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.2-dev.0...@rxap/ngx-timeago@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.1...@rxap/ngx-timeago@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@18.0.1-dev.0...@rxap/ngx-timeago@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-timeago

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@17.0.1...@rxap/ngx-timeago@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-timeago

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@17.0.1...@rxap/ngx-timeago@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-timeago

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@17.0.1...@rxap/ngx-timeago@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-timeago

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@17.0.1-dev.0...@rxap/ngx-timeago@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-timeago

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.4...@rxap/ngx-timeago@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.4-dev.0...@rxap/ngx-timeago@16.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.3...@rxap/ngx-timeago@16.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.3-dev.0...@rxap/ngx-timeago@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.2...@rxap/ngx-timeago@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.2-dev.2...@rxap/ngx-timeago@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.2-dev.1...@rxap/ngx-timeago@16.0.2-dev.2) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.2-dev.0...@rxap/ngx-timeago@16.0.2-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1...@rxap/ngx-timeago@16.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.8...@rxap/ngx-timeago@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.7...@rxap/ngx-timeago@16.0.1-dev.8) (2023-10-12)

### Bug Fixes

- ensure $localize is not directly imported ([2c8880e](https://gitlab.com/rxap/packages/commit/2c8880e41bc17b8c72354d89e605756806069e8e))

## [16.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.6...@rxap/ngx-timeago@16.0.1-dev.7) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.5...@rxap/ngx-timeago@16.0.1-dev.6) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-timeago

## 16.0.1-dev.5 (2023-10-11)

### Bug Fixes

- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- split live and static pipe ([b217ac1](https://gitlab.com/rxap/packages/commit/b217ac1ba9f0823e16ac9adf2cfd0cfa59abd3f0))

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.3...@rxap/ngx-timeago@16.0.1-dev.4) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.2...@rxap/ngx-timeago@16.0.1-dev.3) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.1...@rxap/ngx-timeago@16.0.1-dev.2) (2023-09-12)

**Note:** Version bump only for package @rxap/ngx-timeago

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-timeago@16.0.1-dev.0...@rxap/ngx-timeago@16.0.1-dev.1) (2023-09-09)

### Bug Fixes

- split live and static pipe ([f413dfa](https://gitlab.com/rxap/packages/commit/f413dfa9df8cf99b5a3f079cbb682a20d57e2f30))

## 16.0.1-dev.0 (2023-09-08)

**Note:** Version bump only for package @rxap/ngx-timeago
