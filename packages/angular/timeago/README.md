This package provides Angular pipes and a directive for displaying dates and times in a human-readable &quot;time ago&quot; format. It includes a live pipe that updates automatically. The package also offers a formatter for converting dates into time ago strings.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-timeago?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-timeago)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-timeago)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-timeago)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-timeago)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-timeago
```
**Install peer dependencies:**
```bash
yarn add @angular/core @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-timeago:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-timeago:init
```
