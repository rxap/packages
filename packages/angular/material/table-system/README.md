This package provides a set of Angular directives, components, and services to enhance and customize Angular Material tables. It includes features such as row selection, column filtering, expandable rows, table actions, and more. The goal is to simplify common table-related tasks and provide a more flexible and feature-rich table experience.

[![npm version](https://img.shields.io/npm/v/@rxap/material-table-system?style=flat-square)](https://www.npmjs.com/package/@rxap/material-table-system)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/material-table-system)
![npm](https://img.shields.io/npm/dm/@rxap/material-table-system)
![NPM](https://img.shields.io/npm/l/@rxap/material-table-system)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/material-table-system
```
**Install peer dependencies:**
```bash
yarn add @angular/animations @angular/cdk @angular/common @angular/core @angular/forms @angular/material @angular/router @rxap/components @rxap/data-source @rxap/directives @rxap/forms @rxap/material-directives @rxap/pattern @rxap/reflect-metadata @rxap/rxjs @rxap/utilities @rxap/window-system rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/material-table-system:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/material-table-system:init
```
