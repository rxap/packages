Provides a directive and service to open a table select component in a window. This package allows users to select rows from a table within a window and return the selected data. It integrates with &#x60;@rxap/material-table-system&#x60; and &#x60;@rxap/window-system&#x60; for displaying and managing the table selection in a window.

[![npm version](https://img.shields.io/npm/v/@rxap/material-table-window-system?style=flat-square)](https://www.npmjs.com/package/@rxap/material-table-window-system)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/material-table-window-system)
![npm](https://img.shields.io/npm/dm/@rxap/material-table-window-system)
![NPM](https://img.shields.io/npm/l/@rxap/material-table-window-system)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/material-table-window-system
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/core @angular/material @rxap/data-source @rxap/form-system @rxap/forms @rxap/material-table-system @rxap/mixin @rxap/pattern @rxap/utilities @rxap/window-system rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/material-table-window-system:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/material-table-window-system:init
```
