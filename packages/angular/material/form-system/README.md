Provides directives and a module to enhance Angular Material forms. It includes features such as displaying control errors, clearing input fields, handling required fields, and managing form field visibility based on defined conditions. The package also offers a form controls component with built-in reset, cancel, and submit functionalities.

[![npm version](https://img.shields.io/npm/v/@rxap/material-form-system?style=flat-square)](https://www.npmjs.com/package/@rxap/material-form-system)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/material-form-system)
![npm](https://img.shields.io/npm/dm/@rxap/material-form-system)
![NPM](https://img.shields.io/npm/l/@rxap/material-form-system)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/material-form-system
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/forms @angular/material @angular/router @rxap/form-system @rxap/forms @rxap/mixin @rxap/reflect-metadata @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/material-form-system:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/material-form-system:init
```
