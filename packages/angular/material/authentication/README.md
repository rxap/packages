This package provides Angular Material components and services for user authentication, including login, reset password, and loading functionalities. It offers pre-built forms, methods, and UI components to streamline the implementation of authentication flows in Angular applications. The package also includes an authentication container component and route configurations for easy integration.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-material-authentication?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-material-authentication)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-material-authentication)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-material-authentication)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-material-authentication)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-material-authentication
```
**Install peer dependencies:**
```bash
yarn add @angular/animations @angular/common @angular/core @angular/forms @angular/material @angular/router @rxap/authentication @rxap/config @rxap/forms @rxap/material-form-system @rxap/pattern @rxap/rxjs @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-material-authentication:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-material-authentication:init
```
