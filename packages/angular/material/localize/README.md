Provides components and services to simplify internationalization (i18n) in Angular Material applications. It offers components for language selection and utilities to localize Material components like Datepicker, Paginator and Stepper. This package aims to streamline the process of adapting your Angular Material UI to different languages.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-material-localize?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-material-localize)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-material-localize)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-material-localize)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-material-localize)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-material-localize
```
**Install peer dependencies:**
```bash
yarn add @angular/common @angular/core @angular/forms @angular/material @rxap/directives @rxap/ngx-localize rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-material-localize:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-material-localize:init
```
