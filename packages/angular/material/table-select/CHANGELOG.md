# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.3...@rxap/ngx-material-table-select@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.3-dev.2...@rxap/ngx-material-table-select@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.3-dev.1...@rxap/ngx-material-table-select@19.0.3-dev.2) (2025-02-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.3-dev.0...@rxap/ngx-material-table-select@19.0.3-dev.1) (2025-02-26)

### Bug Fixes

- update package groups ([5810ff6](https://gitlab.com/rxap/packages/commit/5810ff609fe867842a6c9fc8531511dc1e97587a))

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.2...@rxap/ngx-material-table-select@19.0.3-dev.0) (2025-02-25)

### Bug Fixes

- update package groups ([7060546](https://gitlab.com/rxap/packages/commit/7060546f6e8c0c7644bd18f5ad6b1ed62c672fc2))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.2-dev.3...@rxap/ngx-material-table-select@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.2-dev.2...@rxap/ngx-material-table-select@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.2-dev.1...@rxap/ngx-material-table-select@19.0.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.2-dev.0...@rxap/ngx-material-table-select@19.0.2-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1...@rxap/ngx-material-table-select@19.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.6...@rxap/ngx-material-table-select@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.5...@rxap/ngx-material-table-select@19.0.1-dev.6) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.4...@rxap/ngx-material-table-select@19.0.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.3...@rxap/ngx-material-table-select@19.0.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.2...@rxap/ngx-material-table-select@19.0.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.1...@rxap/ngx-material-table-select@19.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.1-dev.0...@rxap/ngx-material-table-select@19.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.0...@rxap/ngx-material-table-select@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.0-dev.2...@rxap/ngx-material-table-select@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@19.0.0-dev.1...@rxap/ngx-material-table-select@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.9-dev.0...@rxap/ngx-material-table-select@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.8...@rxap/ngx-material-table-select@18.0.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.8-dev.0...@rxap/ngx-material-table-select@18.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.7...@rxap/ngx-material-table-select@18.0.8-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.7-dev.0...@rxap/ngx-material-table-select@18.0.7) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.6...@rxap/ngx-material-table-select@18.0.7-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.6-dev.1...@rxap/ngx-material-table-select@18.0.6) (2024-07-30)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.6-dev.0...@rxap/ngx-material-table-select@18.0.6-dev.1) (2024-07-25)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.5...@rxap/ngx-material-table-select@18.0.6-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.5-dev.0...@rxap/ngx-material-table-select@18.0.5) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.4...@rxap/ngx-material-table-select@18.0.5-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.3...@rxap/ngx-material-table-select@18.0.4) (2024-06-28)

### Bug Fixes

- add default compare with function ([16e35ef](https://gitlab.com/rxap/packages/commit/16e35efbfdeaa58ce4524cd470525e3e52765aec))

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.3-dev.2...@rxap/ngx-material-table-select@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.3-dev.1...@rxap/ngx-material-table-select@18.0.3-dev.2) (2024-06-27)

### Bug Fixes

- support multi select ([1ee6773](https://gitlab.com/rxap/packages/commit/1ee67733665a51db7b536ad6f71e1973c5973b97))

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.3-dev.0...@rxap/ngx-material-table-select@18.0.3-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.2...@rxap/ngx-material-table-select@18.0.3-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.2-dev.2...@rxap/ngx-material-table-select@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.2-dev.1...@rxap/ngx-material-table-select@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.2-dev.0...@rxap/ngx-material-table-select@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.1...@rxap/ngx-material-table-select@18.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@18.0.1-dev.0...@rxap/ngx-material-table-select@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@17.0.1...@rxap/ngx-material-table-select@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@17.0.1...@rxap/ngx-material-table-select@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@17.0.1...@rxap/ngx-material-table-select@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@17.0.1-dev.0...@rxap/ngx-material-table-select@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.2...@rxap/ngx-material-table-select@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.2-dev.0...@rxap/ngx-material-table-select@16.1.2) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.1...@rxap/ngx-material-table-select@16.1.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.1-dev.0...@rxap/ngx-material-table-select@16.1.1) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0...@rxap/ngx-material-table-select@16.1.1-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.8...@rxap/ngx-material-table-select@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.7...@rxap/ngx-material-table-select@16.1.0-dev.8) (2024-04-15)

### Bug Fixes

- filter the rxap from control ([329eb2e](https://gitlab.com/rxap/packages/commit/329eb2eec1c651dbfa1b45c0d53c24c7e2160c8c))

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.6...@rxap/ngx-material-table-select@16.1.0-dev.7) (2024-04-10)

### Bug Fixes

- add control null checks ([c033790](https://gitlab.com/rxap/packages/commit/c03379044b6de8100f7d899dd0053a7c75481816))

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.5...@rxap/ngx-material-table-select@16.1.0-dev.6) (2024-04-09)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.4...@rxap/ngx-material-table-select@16.1.0-dev.5) (2024-04-04)

### Bug Fixes

- apply external changes ([d64b468](https://gitlab.com/rxap/packages/commit/d64b468c606e5c905a0940138a482c2e8e1b602e))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.3...@rxap/ngx-material-table-select@16.1.0-dev.4) (2024-03-31)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.2...@rxap/ngx-material-table-select@16.1.0-dev.3) (2024-03-14)

### Bug Fixes

- ensure selected var is defined ([f04b317](https://gitlab.com/rxap/packages/commit/f04b317bf84ed77d101bb4697d98acc55de88052))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.1...@rxap/ngx-material-table-select@16.1.0-dev.2) (2024-03-11)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.1.0-dev.0...@rxap/ngx-material-table-select@16.1.0-dev.1) (2024-03-05)

**Note:** Version bump only for package @rxap/ngx-material-table-select

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.2-dev.0...@rxap/ngx-material-table-select@16.1.0-dev.0) (2024-02-29)

### Features

- add UseTableSelectMethod function ([53ddb3a](https://gitlab.com/rxap/packages/commit/53ddb3aaab58d2671bcd366e7ddf459b02b5fae2))

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1...@rxap/ngx-material-table-select@16.0.2-dev.0) (2024-02-09)

### Bug Fixes

- remove FlexLayout dependency ([1fea895](https://gitlab.com/rxap/packages/commit/1fea895ea326d64e3ca230386175d1cd71d25ace))

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.17...@rxap/ngx-material-table-select@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.16...@rxap/ngx-material-table-select@16.0.1-dev.17) (2023-10-18)

### Bug Fixes

- add missing type attr to button elements ([7e1ec23](https://gitlab.com/rxap/packages/commit/7e1ec238905a8bbdfcefc742f4147dc90725d8c7))

## [16.0.1-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.15...@rxap/ngx-material-table-select@16.0.1-dev.16) (2023-10-12)

### Bug Fixes

- ensure $localize is not directly imported ([2c8880e](https://gitlab.com/rxap/packages/commit/2c8880e41bc17b8c72354d89e605756806069e8e))

## [16.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.14...@rxap/ngx-material-table-select@16.0.1-dev.15) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.14...@rxap/ngx-material-table-select@16.0.1-dev.14) (2023-10-11)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## 16.0.1-dev.14 (2023-10-11)

### Bug Fixes

- add browser-tailwind as imp dep if project has tailwind configuration ([3d90660](https://gitlab.com/rxap/packages/commit/3d906604470f4f26d157f4683afe72b3dd8baae3))
- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add tailwind bundle build target and configurations ([de3825a](https://gitlab.com/rxap/packages/commit/de3825a0e2977389f81cc4ce63e510767ca25810))
- destroy window instance if directive is destroyed ([6b76028](https://gitlab.com/rxap/packages/commit/6b760280e608211460b92080f1083bbf4f81a0a2))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- inject any button type instance ([112d38d](https://gitlab.com/rxap/packages/commit/112d38d0c1c7bb8274864ccab371e0df5cabfd03))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

## [16.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.12...@rxap/ngx-material-table-select@16.0.1-dev.13) (2023-09-27)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.11...@rxap/ngx-material-table-select@16.0.1-dev.12) (2023-09-21)

### Bug Fixes

- destroy window instance if directive is destroyed ([908fa43](https://gitlab.com/rxap/packages/commit/908fa437aaa035f324551f866304939124bec2f7))

## [16.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.10...@rxap/ngx-material-table-select@16.0.1-dev.11) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.9...@rxap/ngx-material-table-select@16.0.1-dev.10) (2023-09-12)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.8...@rxap/ngx-material-table-select@16.0.1-dev.9) (2023-09-07)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.7...@rxap/ngx-material-table-select@16.0.1-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.6...@rxap/ngx-material-table-select@16.0.1-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.5...@rxap/ngx-material-table-select@16.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.4...@rxap/ngx-material-table-select@16.0.1-dev.5) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.3...@rxap/ngx-material-table-select@16.0.1-dev.4) (2023-08-14)

### Bug Fixes

- inject any button type instance ([6f1965a](https://gitlab.com/rxap/packages/commit/6f1965a1478e50ad6d24d637d496b4dea5478055))

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.2...@rxap/ngx-material-table-select@16.0.1-dev.3) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.1...@rxap/ngx-material-table-select@16.0.1-dev.2) (2023-08-03)

**Note:** Version bump only for package @rxap/ngx-material-table-select

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/ngx-material-table-select@16.0.1-dev.0...@rxap/ngx-material-table-select@16.0.1-dev.1) (2023-08-01)

### Bug Fixes

- add browser-tailwind as imp dep if project has tailwind configuration ([6ea13c5](https://gitlab.com/rxap/packages/commit/6ea13c5f9b4e652436bf1da879b564d1ed7b8061))
- add tailwind bundle build target and configurations ([bec6b96](https://gitlab.com/rxap/packages/commit/bec6b96be15bbc11ad072ccefdcaf7df9e8fea52))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 16.0.1-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/ngx-material-table-select
