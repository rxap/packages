import { TableSelectWindowComponent } from './table-select-window.component';

export default {
  title: 'TableSelectWindowComponent',
  component: TableSelectWindowComponent,
};

export const basic = () => ({
  component: TableSelectWindowComponent,
  props: {},
});
