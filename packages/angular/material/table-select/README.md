Provides components and directives to integrate a table selection window into Angular Material forms. It includes features for data binding, filtering, and display customization, making it easier to select data from a table within a form context. This package also offers decorators and mixins to streamline the configuration and extraction of data for table selection.

[![npm version](https://img.shields.io/npm/v/@rxap/ngx-material-table-select?style=flat-square)](https://www.npmjs.com/package/@rxap/ngx-material-table-select)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/ngx-material-table-select)
![npm](https://img.shields.io/npm/dm/@rxap/ngx-material-table-select)
![NPM](https://img.shields.io/npm/l/@rxap/ngx-material-table-select)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/ngx-material-table-select
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/forms @angular/material @rxap/data-source @rxap/form-system @rxap/forms @rxap/material-directives @rxap/material-form-system @rxap/material-table-system @rxap/material-table-window-system @rxap/mixin @rxap/pattern @rxap/pipes @rxap/reflect-metadata @rxap/rxjs @rxap/utilities @rxap/window-system rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/ngx-material-table-select:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/ngx-material-table-select:init
```
