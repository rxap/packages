This package provides a tree component and data source for Angular applications. It includes features such as searching, filtering, and displaying hierarchical data. The package also offers directives for customizing the content of tree nodes.

[![npm version](https://img.shields.io/npm/v/@rxap/tree?style=flat-square)](https://www.npmjs.com/package/@rxap/tree)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/tree)
![npm](https://img.shields.io/npm/dm/@rxap/tree)
![NPM](https://img.shields.io/npm/l/@rxap/tree)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/tree
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/material @rxap/contenteditable @rxap/data-source @rxap/data-structure-tree @rxap/forms @rxap/material-directives @rxap/pattern @rxap/rxjs @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/tree:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/tree:init
```
