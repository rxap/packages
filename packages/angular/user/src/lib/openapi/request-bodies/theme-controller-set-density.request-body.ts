export interface ThemeControllerSetDensityRequestBody {
  value: number;
}
