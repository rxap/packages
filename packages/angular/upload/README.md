A Material Design upload button component for Angular that supports file selection, display, and download. It integrates with Angular Forms and provides visual feedback. It also includes a pipe to read files as data URLs.

[![npm version](https://img.shields.io/npm/v/@rxap/upload?style=flat-square)](https://www.npmjs.com/package/@rxap/upload)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/upload)
![npm](https://img.shields.io/npm/dm/@rxap/upload)
![NPM](https://img.shields.io/npm/l/@rxap/upload)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/upload
```
**Install peer dependencies:**
```bash
yarn add @angular/cdk @angular/common @angular/core @angular/forms @angular/material @rxap/directives @rxap/pattern rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/upload:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/upload:init
```
