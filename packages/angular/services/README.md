Provides Angular services for common tasks such as loading images, managing loading indicators, resetting data, and managing components in a window container sidenav. It also includes a service to generate avatar images. This package offers reusable services to simplify development in Angular applications.

[![npm version](https://img.shields.io/npm/v/@rxap/services?style=flat-square)](https://www.npmjs.com/package/@rxap/services)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/services)
![npm](https://img.shields.io/npm/dm/@rxap/services)
![NPM](https://img.shields.io/npm/l/@rxap/services)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/services
```
**Install peer dependencies:**
```bash
yarn add @angular/common @angular/core @rxap/rxjs @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/services:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/services:init
```
