This package provides a schematic to compose and execute other schematics based on configuration files. It allows executing schematics for a specific project, feature, or the entire workspace. The configuration files can be in JSON or YAML format, defining the schematics to be executed and their options.

[![npm version](https://img.shields.io/npm/v/@rxap/schematic-composer?style=flat-square)](https://www.npmjs.com/package/@rxap/schematic-composer)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematic-composer)
![npm](https://img.shields.io/npm/dm/@rxap/schematic-composer)
![NPM](https://img.shields.io/npm/l/@rxap/schematic-composer)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematic-composer
```
