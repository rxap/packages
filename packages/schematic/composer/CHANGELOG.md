# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.4-dev.0...@rxap/schematic-composer@19.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.3...@rxap/schematic-composer@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.3-dev.1...@rxap/schematic-composer@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.3-dev.0...@rxap/schematic-composer@19.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2...@rxap/schematic-composer@19.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2-dev.4...@rxap/schematic-composer@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2-dev.3...@rxap/schematic-composer@19.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2-dev.2...@rxap/schematic-composer@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2-dev.1...@rxap/schematic-composer@19.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.2-dev.0...@rxap/schematic-composer@19.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1...@rxap/schematic-composer@19.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.14...@rxap/schematic-composer@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.13...@rxap/schematic-composer@19.0.1-dev.14) (2025-02-13)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.12...@rxap/schematic-composer@19.0.1-dev.13) (2025-02-11)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.11...@rxap/schematic-composer@19.0.1-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.10...@rxap/schematic-composer@19.0.1-dev.11) (2025-02-10)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.9...@rxap/schematic-composer@19.0.1-dev.10) (2025-02-07)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.8...@rxap/schematic-composer@19.0.1-dev.9) (2025-02-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.7...@rxap/schematic-composer@19.0.1-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.6...@rxap/schematic-composer@19.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.5...@rxap/schematic-composer@19.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.4...@rxap/schematic-composer@19.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.3...@rxap/schematic-composer@19.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.2...@rxap/schematic-composer@19.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.1...@rxap/schematic-composer@19.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.1-dev.0...@rxap/schematic-composer@19.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/schematic-composer

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.0...@rxap/schematic-composer@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/schematic-composer

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.0-dev.3...@rxap/schematic-composer@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/schematic-composer

# [19.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.0-dev.2...@rxap/schematic-composer@19.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/schematic-composer

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@19.0.0-dev.1...@rxap/schematic-composer@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/schematic-composer

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.10-dev.0...@rxap/schematic-composer@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.9...@rxap/schematic-composer@18.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.9-dev.1...@rxap/schematic-composer@18.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.9-dev.0...@rxap/schematic-composer@18.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.8...@rxap/schematic-composer@18.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.8-dev.3...@rxap/schematic-composer@18.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.8-dev.2...@rxap/schematic-composer@18.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.8-dev.1...@rxap/schematic-composer@18.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.8-dev.0...@rxap/schematic-composer@18.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.7...@rxap/schematic-composer@18.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.7-dev.1...@rxap/schematic-composer@18.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.7-dev.0...@rxap/schematic-composer@18.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6...@rxap/schematic-composer@18.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.6...@rxap/schematic-composer@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.5...@rxap/schematic-composer@18.0.6-dev.6) (2024-08-22)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.4...@rxap/schematic-composer@18.0.6-dev.5) (2024-08-21)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.3...@rxap/schematic-composer@18.0.6-dev.4) (2024-08-21)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.2...@rxap/schematic-composer@18.0.6-dev.3) (2024-08-15)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.1...@rxap/schematic-composer@18.0.6-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.6-dev.0...@rxap/schematic-composer@18.0.6-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5...@rxap/schematic-composer@18.0.6-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.8...@rxap/schematic-composer@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.7...@rxap/schematic-composer@18.0.5-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.6...@rxap/schematic-composer@18.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.5...@rxap/schematic-composer@18.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.4...@rxap/schematic-composer@18.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.3...@rxap/schematic-composer@18.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.2...@rxap/schematic-composer@18.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.1...@rxap/schematic-composer@18.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.5-dev.0...@rxap/schematic-composer@18.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.4...@rxap/schematic-composer@18.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.4-dev.0...@rxap/schematic-composer@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3...@rxap/schematic-composer@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.7...@rxap/schematic-composer@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.6...@rxap/schematic-composer@18.0.3-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.5...@rxap/schematic-composer@18.0.3-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.4...@rxap/schematic-composer@18.0.3-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.3...@rxap/schematic-composer@18.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.2...@rxap/schematic-composer@18.0.3-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.1...@rxap/schematic-composer@18.0.3-dev.2) (2024-06-21)

### Performance Improvements

- improve project json file search ([0e6de0d](https://gitlab.com/rxap/packages/commit/0e6de0de54e2bad4d0dfca9c0ee7b9cb1f3ef6f6))
- use tree visit generator ([2dd66d1](https://gitlab.com/rxap/packages/commit/2dd66d103e06ff5cb830e1f9a88c1878240fc672))

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.3-dev.0...@rxap/schematic-composer@18.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2...@rxap/schematic-composer@18.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.5...@rxap/schematic-composer@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.4...@rxap/schematic-composer@18.0.2-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.3...@rxap/schematic-composer@18.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.2...@rxap/schematic-composer@18.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.1...@rxap/schematic-composer@18.0.2-dev.2) (2024-06-17)

### Bug Fixes

- support directory parameters ([cb6bf35](https://gitlab.com/rxap/packages/commit/cb6bf358d399923903942de9c86274278c448327))

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.2-dev.0...@rxap/schematic-composer@18.0.2-dev.1) (2024-06-17)

### Bug Fixes

- omit the lib prefix for library projects ([3622bb8](https://gitlab.com/rxap/packages/commit/3622bb88c7d1032fe695bfb777d20b377b3ba07f))

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.1...@rxap/schematic-composer@18.0.2-dev.0) (2024-06-05)

### Bug Fixes

- check schematic input file ([ccfb427](https://gitlab.com/rxap/packages/commit/ccfb42774950af2795457cf5da2812c1aca246cf))
- validate input objects ([aae4139](https://gitlab.com/rxap/packages/commit/aae413949c03b6442f9590c83a5e3b0909eac2d5))

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@18.0.1-dev.0...@rxap/schematic-composer@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@17.0.1...@rxap/schematic-composer@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/schematic-composer

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@17.0.1...@rxap/schematic-composer@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@17.0.1...@rxap/schematic-composer@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@17.0.1-dev.0...@rxap/schematic-composer@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-composer

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.2.1...@rxap/schematic-composer@17.0.1-dev.0) (2024-05-29)

### Bug Fixes

- skip ignored folders ([c431c4b](https://gitlab.com/rxap/packages/commit/c431c4b4e2d51aaef4fcc1faa26cb894783b7e34))

## [16.2.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.2.1-dev.0...@rxap/schematic-composer@16.2.1) (2024-05-28)

**Note:** Version bump only for package @rxap/schematic-composer

## [16.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.2.0...@rxap/schematic-composer@16.2.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/schematic-composer

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.2.0-dev.1...@rxap/schematic-composer@16.2.0) (2024-05-27)

**Note:** Version bump only for package @rxap/schematic-composer

# [16.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.2.0-dev.0...@rxap/schematic-composer@16.2.0-dev.1) (2024-05-27)

**Note:** Version bump only for package @rxap/schematic-composer

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0...@rxap/schematic-composer@16.2.0-dev.0) (2024-04-18)

### Features

- only run install if dep have changed ([94ffb0c](https://gitlab.com/rxap/packages/commit/94ffb0ce884caae90a7636eb922caca9c7a9dc75))

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.6...@rxap/schematic-composer@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/schematic-composer

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.5...@rxap/schematic-composer@16.1.0-dev.6) (2024-04-09)

**Note:** Version bump only for package @rxap/schematic-composer

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.4...@rxap/schematic-composer@16.1.0-dev.5) (2024-04-04)

### Bug Fixes

- normalize overwrite option if required ([aca6447](https://gitlab.com/rxap/packages/commit/aca6447105b7b9a2ff9fb3db32bce9384cce42c8))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.3...@rxap/schematic-composer@16.1.0-dev.4) (2024-04-03)

### Bug Fixes

- ensure add packages are installed ([a842e7f](https://gitlab.com/rxap/packages/commit/a842e7f60fa16885159ad4cec6f1536a3be8a004))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.2...@rxap/schematic-composer@16.1.0-dev.3) (2024-04-03)

### Bug Fixes

- support schematics without project parameter ([3a054cc](https://gitlab.com/rxap/packages/commit/3a054ccaea181f6bd4f158bbcbdc5ee472546c64))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.1...@rxap/schematic-composer@16.1.0-dev.2) (2024-04-01)

### Bug Fixes

- resolve infinity loop ([d9394be](https://gitlab.com/rxap/packages/commit/d9394be8087e3022ab183ad0d50b8e36ce521898))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.1.0-dev.0...@rxap/schematic-composer@16.1.0-dev.1) (2024-03-31)

### Bug Fixes

- ensure the generator package is installed ([cfd7821](https://gitlab.com/rxap/packages/commit/cfd78219b3896aca9617404342b560ab8ae8e3dc))
- install packages before executing external schematic ([dc72a5c](https://gitlab.com/rxap/packages/commit/dc72a5c60713559de146d000120c75550f9f8440))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.0.2-dev.1...@rxap/schematic-composer@16.1.0-dev.0) (2024-03-14)

### Bug Fixes

- use the project root instead of source root ([21cc75f](https://gitlab.com/rxap/packages/commit/21cc75fb02a0ae5bbe08c2b0b5ff1cd3c14335a0))

### Features

- support project detection ([db4e7b6](https://gitlab.com/rxap/packages/commit/db4e7b66c799adaddea874725aac21607748b2ff))

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.0.2-dev.0...@rxap/schematic-composer@16.0.2-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/schematic-composer

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.0.1...@rxap/schematic-composer@16.0.2-dev.0) (2024-02-25)

**Note:** Version bump only for package @rxap/schematic-composer

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@16.0.1-dev.0...@rxap/schematic-composer@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/schematic-composer

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.5...@rxap/schematic-composer@16.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

# [0.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.4...@rxap/schematic-composer@0.1.0-dev.5) (2023-11-18)

### Bug Fixes

- support yml as file extensions ([60f3d32](https://gitlab.com/rxap/packages/commit/60f3d325be0b6ce9585e3826f892cfe51484c593))

# [0.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.3...@rxap/schematic-composer@0.1.0-dev.4) (2023-11-16)

### Bug Fixes

- support sub directories ([c38f925](https://gitlab.com/rxap/packages/commit/c38f925b2bab59843d5f62f49c69c1dfb68c24b1))
- support yaml files ([353b63e](https://gitlab.com/rxap/packages/commit/353b63e58bab01e8080aea30255ddb77133e4a76))

# [0.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.2...@rxap/schematic-composer@0.1.0-dev.3) (2023-10-11)

**Note:** Version bump only for package @rxap/schematic-composer

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.2...@rxap/schematic-composer@0.1.0-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/schematic-composer

# 0.1.0-dev.2 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- overwrite property schema ([c5f1990](https://gitlab.com/rxap/packages/commit/c5f1990eaa0432d356077674e9dd17817d2953fc))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Features

- support overwrite array ([2696160](https://gitlab.com/rxap/packages/commit/2696160aef2cf384edca3235aa0aaadc3afb97cc))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.1.0-dev.0...@rxap/schematic-composer@0.1.0-dev.1) (2023-10-09)

### Bug Fixes

- overwrite property schema ([0ae174c](https://gitlab.com/rxap/packages/commit/0ae174c4346bc233b863c13909449594a3701032))

# [0.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.9...@rxap/schematic-composer@0.1.0-dev.0) (2023-10-09)

### Features

- support overwrite array ([e528c87](https://gitlab.com/rxap/packages/commit/e528c87fd5fb49898583e0c08c191942e7e2f8e4))

## [0.0.2-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.8...@rxap/schematic-composer@0.0.2-dev.9) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [0.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.7...@rxap/schematic-composer@0.0.2-dev.8) (2023-09-12)

**Note:** Version bump only for package @rxap/schematic-composer

## [0.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.6...@rxap/schematic-composer@0.0.2-dev.7) (2023-09-07)

**Note:** Version bump only for package @rxap/schematic-composer

## [0.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.5...@rxap/schematic-composer@0.0.2-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [0.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.4...@rxap/schematic-composer@0.0.2-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/schematic-composer

## [0.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.3...@rxap/schematic-composer@0.0.2-dev.4) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [0.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.2...@rxap/schematic-composer@0.0.2-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.1...@rxap/schematic-composer@0.0.2-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-composer@0.0.2-dev.0...@rxap/schematic-composer@0.0.2-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 0.0.2-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/schematic-composer
