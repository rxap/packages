This package provides utilities for parsing XML templates within Angular Schematics. It includes functionality to locate templates in a file system, parse them using a specified DOMParser, and register custom elements for parsing. The package also offers elements for defining modules, types, and methods within the XML templates.

[![npm version](https://img.shields.io/npm/v/@rxap/schematics-xml-parser?style=flat-square)](https://www.npmjs.com/package/@rxap/schematics-xml-parser)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematics-xml-parser)
![npm](https://img.shields.io/npm/dm/@rxap/schematics-xml-parser)
![NPM](https://img.shields.io/npm/l/@rxap/schematics-xml-parser)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematics-xml-parser
```
**Execute the init generator:**
```bash
yarn nx g @rxap/schematics-xml-parser:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/schematics-xml-parser:init
```
