# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [19.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.4-dev.0...@rxap/schematic-nestjs@19.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.3...@rxap/schematic-nestjs@19.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.3-dev.1...@rxap/schematic-nestjs@19.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.3-dev.0...@rxap/schematic-nestjs@19.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2...@rxap/schematic-nestjs@19.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2-dev.4...@rxap/schematic-nestjs@19.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2-dev.3...@rxap/schematic-nestjs@19.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2-dev.2...@rxap/schematic-nestjs@19.0.2-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2-dev.1...@rxap/schematic-nestjs@19.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.2-dev.0...@rxap/schematic-nestjs@19.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1...@rxap/schematic-nestjs@19.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.14...@rxap/schematic-nestjs@19.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.13...@rxap/schematic-nestjs@19.0.1-dev.14) (2025-02-13)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.12...@rxap/schematic-nestjs@19.0.1-dev.13) (2025-02-11)

### Bug Fixes

- safe access project sourceRoot property ([16ca874](https://gitlab.com/rxap/packages/commit/16ca8747120876ad90e38c0cc012c175741fda0b))

## [19.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.11...@rxap/schematic-nestjs@19.0.1-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.10...@rxap/schematic-nestjs@19.0.1-dev.11) (2025-02-10)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.9...@rxap/schematic-nestjs@19.0.1-dev.10) (2025-02-07)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.8...@rxap/schematic-nestjs@19.0.1-dev.9) (2025-02-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.7...@rxap/schematic-nestjs@19.0.1-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.6...@rxap/schematic-nestjs@19.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.5...@rxap/schematic-nestjs@19.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.4...@rxap/schematic-nestjs@19.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.3...@rxap/schematic-nestjs@19.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.2...@rxap/schematic-nestjs@19.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.1...@rxap/schematic-nestjs@19.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.1-dev.0...@rxap/schematic-nestjs@19.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.0...@rxap/schematic-nestjs@19.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.0-dev.3...@rxap/schematic-nestjs@19.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [19.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.0-dev.2...@rxap/schematic-nestjs@19.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@19.0.0-dev.1...@rxap/schematic-nestjs@19.0.0-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [19.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.10-dev.0...@rxap/schematic-nestjs@19.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.9...@rxap/schematic-nestjs@18.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.9-dev.1...@rxap/schematic-nestjs@18.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.9-dev.0...@rxap/schematic-nestjs@18.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.8...@rxap/schematic-nestjs@18.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.8-dev.3...@rxap/schematic-nestjs@18.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.8-dev.2...@rxap/schematic-nestjs@18.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.8-dev.1...@rxap/schematic-nestjs@18.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.8-dev.0...@rxap/schematic-nestjs@18.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.7...@rxap/schematic-nestjs@18.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.7-dev.1...@rxap/schematic-nestjs@18.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.7-dev.0...@rxap/schematic-nestjs@18.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6...@rxap/schematic-nestjs@18.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.6...@rxap/schematic-nestjs@18.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.5...@rxap/schematic-nestjs@18.0.6-dev.6) (2024-08-22)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.4...@rxap/schematic-nestjs@18.0.6-dev.5) (2024-08-21)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.3...@rxap/schematic-nestjs@18.0.6-dev.4) (2024-08-21)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.2...@rxap/schematic-nestjs@18.0.6-dev.3) (2024-08-15)

### Bug Fixes

- use typed logger ([76a0fe1](https://gitlab.com/rxap/packages/commit/76a0fe1510c2866d3934a30d56d920bc265ce46d))

## [18.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.1...@rxap/schematic-nestjs@18.0.6-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.6-dev.0...@rxap/schematic-nestjs@18.0.6-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5...@rxap/schematic-nestjs@18.0.6-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.8...@rxap/schematic-nestjs@18.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.7...@rxap/schematic-nestjs@18.0.5-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.6...@rxap/schematic-nestjs@18.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.5...@rxap/schematic-nestjs@18.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.4...@rxap/schematic-nestjs@18.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.3...@rxap/schematic-nestjs@18.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.2...@rxap/schematic-nestjs@18.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.1...@rxap/schematic-nestjs@18.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.5-dev.0...@rxap/schematic-nestjs@18.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.4...@rxap/schematic-nestjs@18.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.4-dev.0...@rxap/schematic-nestjs@18.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3...@rxap/schematic-nestjs@18.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.7...@rxap/schematic-nestjs@18.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.6...@rxap/schematic-nestjs@18.0.3-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.5...@rxap/schematic-nestjs@18.0.3-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.4...@rxap/schematic-nestjs@18.0.3-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.3...@rxap/schematic-nestjs@18.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.2...@rxap/schematic-nestjs@18.0.3-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.1...@rxap/schematic-nestjs@18.0.3-dev.2) (2024-06-21)

### Performance Improvements

- improve project json file search ([d519c90](https://gitlab.com/rxap/packages/commit/d519c907bd8826bd8b6ed5bd1dab9c4f8d594d1b))

## [18.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.3-dev.0...@rxap/schematic-nestjs@18.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2...@rxap/schematic-nestjs@18.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2-dev.4...@rxap/schematic-nestjs@18.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2-dev.3...@rxap/schematic-nestjs@18.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2-dev.2...@rxap/schematic-nestjs@18.0.2-dev.3) (2024-06-18)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2-dev.1...@rxap/schematic-nestjs@18.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.2-dev.0...@rxap/schematic-nestjs@18.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.1...@rxap/schematic-nestjs@18.0.2-dev.0) (2024-06-05)

### Bug Fixes

- pass backend option ([d8fb481](https://gitlab.com/rxap/packages/commit/d8fb481cf2f1f8baea2aee52ab438cf9d43211dd))
- use the correct property ([d79a2d9](https://gitlab.com/rxap/packages/commit/d79a2d94ba5d1882556f59dc2d3f93500f668b26))

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@18.0.1-dev.0...@rxap/schematic-nestjs@18.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@17.0.1...@rxap/schematic-nestjs@18.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@17.0.1...@rxap/schematic-nestjs@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@17.0.1...@rxap/schematic-nestjs@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@17.0.1-dev.0...@rxap/schematic-nestjs@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.4...@rxap/schematic-nestjs@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.4-dev.0...@rxap/schematic-nestjs@16.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.3...@rxap/schematic-nestjs@16.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.3-dev.0...@rxap/schematic-nestjs@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2...@rxap/schematic-nestjs@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.7...@rxap/schematic-nestjs@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.6...@rxap/schematic-nestjs@16.0.2-dev.7) (2024-04-09)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.5...@rxap/schematic-nestjs@16.0.2-dev.6) (2024-03-31)

### Bug Fixes

- use environment.app to get the default sentry server name ([c89f1bd](https://gitlab.com/rxap/packages/commit/c89f1bdc99425aaee1417b51dfe05c17dbb6565e))

## [16.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.4...@rxap/schematic-nestjs@16.0.2-dev.5) (2024-03-31)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.3...@rxap/schematic-nestjs@16.0.2-dev.4) (2024-03-27)

### Bug Fixes

- add correct return type for exists methods ([51ebe6d](https://gitlab.com/rxap/packages/commit/51ebe6d822d7d282b64841809f9cca6f241f76c3))
- skip missing properties in update requests validation ([4a41442](https://gitlab.com/rxap/packages/commit/4a41442fcbeeda6a184f62c76b1a6937d0792a02))

## [16.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.2...@rxap/schematic-nestjs@16.0.2-dev.3) (2024-03-26)

### Bug Fixes

- ensure the correct document id is logged ([8f6eaad](https://gitlab.com/rxap/packages/commit/8f6eaad1e5caa20ad419e44b1f77690369f59f91))

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.1...@rxap/schematic-nestjs@16.0.2-dev.2) (2024-03-26)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.2-dev.0...@rxap/schematic-nestjs@16.0.2-dev.1) (2024-03-11)

### Bug Fixes

- add Overwrite options type ([abf5d68](https://gitlab.com/rxap/packages/commit/abf5d68e01b4883ebf865da740973d9a18c3e319))

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.1...@rxap/schematic-nestjs@16.0.2-dev.0) (2024-03-09)

### Bug Fixes

- support complex data properties ([2294a05](https://gitlab.com/rxap/packages/commit/2294a05260c5d876ebf5ff209fd450592c7ebe4b))

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@16.0.1-dev.0...@rxap/schematic-nestjs@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/schematic-nestjs

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.17...@rxap/schematic-nestjs@16.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

# [0.1.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.16...@rxap/schematic-nestjs@0.1.0-dev.17) (2023-10-16)

### Bug Fixes

- use utility CreateProject function to create a ts-morph Project instance ([78b308f](https://gitlab.com/rxap/packages/commit/78b308fd10747616c7c7f27e81501a4ad5052a77))

# [0.1.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.15...@rxap/schematic-nestjs@0.1.0-dev.16) (2023-10-12)

### Bug Fixes

- ensure the plain text of the nodes is used ([68091ae](https://gitlab.com/rxap/packages/commit/68091ae4d684b4e16c5023ba6a9c36f90319e903))

# [0.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.14...@rxap/schematic-nestjs@0.1.0-dev.15) (2023-10-11)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [0.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.13...@rxap/schematic-nestjs@0.1.0-dev.14) (2023-10-11)

**Note:** Version bump only for package @rxap/schematic-nestjs

# 0.1.0-dev.13 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- controller naming and route ([38054d3](https://gitlab.com/rxap/packages/commit/38054d3ee106e1f50f806e1596da674f268cb4aa))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- init nx json configurations ([4c14f56](https://gitlab.com/rxap/packages/commit/4c14f56da8bae5971165cbff2269d86fa9951629))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- prevent undefined access error ([f39f37f](https://gitlab.com/rxap/packages/commit/f39f37fb4ba1be924c18cbe6a91ca0e0143e0ce9))
- swagger output path ([77e00ef](https://gitlab.com/rxap/packages/commit/77e00efb4d435dce9d2beea5afd52e33db339005))
- update default api configuration file path ([b4c2a61](https://gitlab.com/rxap/packages/commit/b4c2a61450a321a8ef2b6759d33d2968cb854d9a))
- use async factory for ThrottlerModule ([4564625](https://gitlab.com/rxap/packages/commit/45646252262b3c7a4841a2fa959110e06f6d6246))
- use SentryOptionsFactory and GetLogLevels utility function ([2197ae3](https://gitlab.com/rxap/packages/commit/2197ae3207c280a021a998265b2c14a5f0daab19))

### Features

- mv swagger generator to plugin-nestjs ([895cba5](https://gitlab.com/rxap/packages/commit/895cba5d040262ae64f05ff14b604871240a0a4b))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [0.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.11...@rxap/schematic-nestjs@0.1.0-dev.12) (2023-10-02)

### Bug Fixes

- update default api configuration file path ([a87c08c](https://gitlab.com/rxap/packages/commit/a87c08c10d36ff3830db2526bc3759baa83e8efc))

# [0.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.10...@rxap/schematic-nestjs@0.1.0-dev.11) (2023-09-18)

### Bug Fixes

- init nx json configurations ([9b4b023](https://gitlab.com/rxap/packages/commit/9b4b023e849d1c0bf21b14a9e219a0e9cd6ab2f6))

# [0.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.9...@rxap/schematic-nestjs@0.1.0-dev.10) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [0.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.8...@rxap/schematic-nestjs@0.1.0-dev.9) (2023-09-12)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [0.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.7...@rxap/schematic-nestjs@0.1.0-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [0.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.6...@rxap/schematic-nestjs@0.1.0-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [0.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.5...@rxap/schematic-nestjs@0.1.0-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/schematic-nestjs

# [0.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.4...@rxap/schematic-nestjs@0.1.0-dev.5) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [0.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.3...@rxap/schematic-nestjs@0.1.0-dev.4) (2023-08-24)

### Bug Fixes

- use async factory for ThrottlerModule ([57f9732](https://gitlab.com/rxap/packages/commit/57f97329b9508c47903438fb5718d403a9b21ec5))

# [0.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.2...@rxap/schematic-nestjs@0.1.0-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.1...@rxap/schematic-nestjs@0.1.0-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.1.0-dev.0...@rxap/schematic-nestjs@0.1.0-dev.1) (2023-08-14)

### Bug Fixes

- controller naming and route ([6004d02](https://gitlab.com/rxap/packages/commit/6004d02890eb4d9b4d8369a9fa9b7726f7af28e2))
- prevent undefined access error ([9c249a3](https://gitlab.com/rxap/packages/commit/9c249a3f1edf494e50e765700f3d62dcdfe9bcf0))

# [0.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.0.2-dev.1...@rxap/schematic-nestjs@0.1.0-dev.0) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- swagger output path ([8b018be](https://gitlab.com/rxap/packages/commit/8b018be3ce8bd5e06f689b856f4988fa2e64f297))

### Features

- mv swagger generator to plugin-nestjs ([cf2ecbb](https://gitlab.com/rxap/packages/commit/cf2ecbb16b681cb04d392d17bb987b24e8c9224b))

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/schematic-nestjs@0.0.2-dev.0...@rxap/schematic-nestjs@0.0.2-dev.1) (2023-08-03)

### Bug Fixes

- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))
- use SentryOptionsFactory and GetLogLevels utility function ([99d41b5](https://gitlab.com/rxap/packages/commit/99d41b5bfd99a30c908f16f89c74b71f984c6e41))

## 0.0.2-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/schematic-nestjs
