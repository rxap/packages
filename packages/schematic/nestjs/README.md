This package provides schematics for NestJS projects. It includes schematics for generating CRUD services, microservices, health indicators, and other common NestJS features. It also provides utilities for working with TypeScript and NestJS code.

[![npm version](https://img.shields.io/npm/v/@rxap/schematic-nestjs?style=flat-square)](https://www.npmjs.com/package/@rxap/schematic-nestjs)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematic-nestjs)
![npm](https://img.shields.io/npm/dm/@rxap/schematic-nestjs)
![NPM](https://img.shields.io/npm/l/@rxap/schematic-nestjs)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematic-nestjs
```
