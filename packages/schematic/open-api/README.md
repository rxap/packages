This package provides schematics for generating code from OpenAPI specifications. It includes utilities for loading OpenAPI configurations, generating components, parameters, request bodies, and responses, as well as generating Angular directives and NestJS commands based on the OpenAPI schema. It simplifies the process of creating data access layers and UI components from API definitions.

[![npm version](https://img.shields.io/npm/v/@rxap/schematics-open-api?style=flat-square)](https://www.npmjs.com/package/@rxap/schematics-open-api)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematics-open-api)
![npm](https://img.shields.io/npm/dm/@rxap/schematics-open-api)
![NPM](https://img.shields.io/npm/l/@rxap/schematics-open-api)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematics-open-api
```
