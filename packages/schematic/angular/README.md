Provides schematics for generating Angular components, services, and related files, particularly focused on creating tables, forms, data grids, and accordions with NestJS integration. It offers tools for generating UI elements with backend connectivity, including data sources, methods, and form handling. This package aims to streamline the development of data-driven Angular applications with a focus on material design and backend integration.

[![npm version](https://img.shields.io/npm/v/@rxap/schematic-angular?style=flat-square)](https://www.npmjs.com/package/@rxap/schematic-angular)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematic-angular)
![npm](https://img.shields.io/npm/dm/@rxap/schematic-angular)
![NPM](https://img.shields.io/npm/l/@rxap/schematic-angular)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematic-angular
```
