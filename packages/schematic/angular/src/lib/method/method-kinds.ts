export enum MethodKinds {
  DEFAULT = 'default',
  IMPORT = 'import',
  OPEN_API = 'open-api',
}
