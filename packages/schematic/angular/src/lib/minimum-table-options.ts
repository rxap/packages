import {
  DataProperty,
  NormalizeDataProperty,
  NormalizeDataPropertyList,
  NormalizedDataProperty,
  NormalizedUpstreamOptions,
  NormalizeUpstreamOptions,
  UpstreamOptions,
} from '@rxap/ts-morph';
import {
  CoerceArrayItems,
  CoerceSuffix,
  Normalized,
} from '@rxap/utilities';
import {
  AccordionIdentifier,
  NormalizeAccordionIdentifier,
  NormalizedAccordionIdentifier,
} from './accordion-identifier';
import {
  CssClass,
  NormalizeCssClass,
  NormalizedCssClass,
} from './css-class';
import {
  FormControl,
  NormalizedFormControl,
  NormalizeFormControl,
  NormalizeFormControlList,
} from './form/control/form-control';
import {
  NormalizeHeaderButton,
  HeaderButton,
} from './table/table-header-button';
import {
  NormalizedSortable,
  NormalizeSortable,
  Sortable,
} from './table/sortable';
import {
  NormalizedTableAction,
  NormalizeTableActionList,
  TableAction,
} from './table/table-action';
import {
  NormalizedTableColumn,
  NormalizeTableColumnList,
  TableColumn,
} from './table/table-column';
import { ToTitle } from './to-title';

export enum MinimumTableModifiers {
  OVERWRITE = 'overwrite',
  NAVIGATION_BACK_HEADER = 'navigation-back-header',
  WITHOUT_TITLE = 'without-title',
  WITH_HEADER = 'with-header',
}

export function IsMinimumTableModifiers(value: string): value is MinimumTableModifiers {
  return Object.values(MinimumTableModifiers).includes(value as MinimumTableModifiers);
}

export interface MinimumTableOptions {
  headerButton?: HeaderButton;
  columnList: Array<TableColumn>;
  actionList: Array<TableAction>;
  filterList: Array<FormControl>;
  propertyList: Array<string | DataProperty>;
  modifiers?: string[];
  title?: string;
  componentName?: string;
  cssClass?: CssClass;
  identifier?: AccordionIdentifier;
  rowId?: DataProperty;
  hasPaginator?: boolean;
  upstream?: UpstreamOptions;
  sortable?: Sortable;
}

export interface NormalizedMinimumTableOptions<MODIFIER extends string = string>
  extends Readonly<Omit<Normalized<MinimumTableOptions>, 'columnList' | 'actionList' | 'propertyList' | 'modifiers' | 'filterList'>> {
  componentName: string;
  columnList: ReadonlyArray<NormalizedTableColumn>;
  actionList: ReadonlyArray<NormalizedTableAction>;
  filterList: ReadonlyArray<NormalizedFormControl>;
  propertyList: Array<NormalizedDataProperty>;
  modifiers: Array<MODIFIER>;
  cssClass: NormalizedCssClass;
  identifier: NormalizedAccordionIdentifier | null;
  rowId: NormalizedDataProperty | null;
  upstream: NormalizedUpstreamOptions | null;
  withHeader: boolean;
  sortable: NormalizedSortable;
}

export function NormalizeMinimumTableOptions<MODIFIER extends string = string>(
  options: Readonly<MinimumTableOptions>,
  name: string,
  isModifier: (value: string) => value is MODIFIER,
  suffix: string,
): NormalizedMinimumTableOptions<MODIFIER> {
  const componentName = options.componentName ?? CoerceSuffix(name, suffix);
  const actionList = NormalizeTableActionList(options.actionList);
  const sortable = NormalizeSortable(options.sortable);
  for (const column of options.columnList) {
    column.sortable ??= sortable.enabled;
  }
  const filterList = NormalizeFormControlList(options.filterList);
  const columnList = NormalizeTableColumnList(options.columnList);
  if (!sortable.enabled && columnList.some(column => column.sortable)) {
    sortable.enabled = true;
  }
  const propertyList = NormalizeDataPropertyList(options.propertyList);
  const headerButton = NormalizeHeaderButton(options.headerButton, name);
  const modifiers = options.modifiers ?? [];
  if (columnList.some(column => !column.filterControl)) {
    CoerceArrayItems(modifiers, [MinimumTableModifiers.WITH_HEADER]);
  }
  if (sortable.enabled) {
    CoerceArrayItems(modifiers, [MinimumTableModifiers.WITH_HEADER]);
  }
  if (actionList.some(action => action.inHeader)) {
    CoerceArrayItems(modifiers, [MinimumTableModifiers.WITH_HEADER]);
  }
  const title = options.title ?? ToTitle(name);
  if (!modifiers.every(isModifier)) {
    throw new Error(`Invalid table modifier for table: ${ componentName } - [ ${ modifiers.join(', ') } ] with function: ${ isModifier.name }`);
  }
  // TODO : map the columnList to a propertyList
  CoerceArrayItems(propertyList, columnList.filter(column => !column.synthetic), (a, b) => a.name === b.name);
  const identifier = NormalizeAccordionIdentifier(options.identifier);
  if (identifier) {
    CoerceArrayItems(propertyList, [identifier.property], (a, b) => a.name === b.name);
  }
  return Object.freeze({
    filterList,
    componentName,
    actionList,
    columnList,
    headerButton,
    modifiers,
    title,
    propertyList,
    rowId: options.rowId ? NormalizeDataProperty(options.rowId) : null,
    cssClass: NormalizeCssClass(options.cssClass),
    identifier,
    hasPaginator: options.hasPaginator ?? true,
    upstream: NormalizeUpstreamOptions(options.upstream),
    sortable,
    withHeader: modifiers?.includes(MinimumTableModifiers.WITH_HEADER as any) ?? false,
  });
}
