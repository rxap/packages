export enum HeaderButtonKind {
  DEFAULT = 'default',
  FORM = 'form',
  METHOD = 'method',
  NAVIGATION = 'navigation',
}
