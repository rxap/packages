export enum AccordionHeaderKinds {
  Default = 'default',
  Property = 'property',
  Static = 'static',
}
