export enum BackendTypes {
  NONE = 'none',
  NESTJS = 'nestjs',
  OPEN_API = 'open-api',
  LOCAL = 'local',
  DATA_SOURCE = 'data-source',
}
