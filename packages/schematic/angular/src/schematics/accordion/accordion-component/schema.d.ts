import { Accordion } from '../../../lib/accordion/accordion';
import { AngularOptions } from '../../../lib/angular-options';

export type AccordionComponentOptions = Accordion & AngularOptions;
