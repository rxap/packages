import { DateFormControl } from '../../../../lib/form/control/date-form-control';
import { FormControlOptions } from '../../form-control/schema';

export type DateFormControlOptions = FormControlOptions & DateFormControl;
