import { AutocompleteFormControl } from '../../../../lib/form/control/autocomplete-form-control';
import { FormControlOptions } from '../../form-control/schema';

export type AutocompleteFormControlOptions = FormControlOptions & AutocompleteFormControl;
