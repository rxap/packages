import {
  chain,
  Rule,
  SchematicsException,
} from '@angular-devkit/schematics';
import {
  AbstractControl,
  BuildNestControllerName,
  buildOperationId,
  CoerceFormControl,
  CoerceFormDefinitionControl,
  CoerceFormProviderRule,
  CoerceOptionsDataSourceRule,
  CoerceOptionsOperationRule,
  EnforceUseFormControlOrderRule,
} from '@rxap/schematics-ts-morph';
import {
  classify,
  dasherize,
} from '@rxap/schematics-utilities';
import {
  CoerceDecorator,
  CoerceImports,
  OperationIdToClassRemoteMethodImportPath,
  OperationIdToRemoteMethodClassName,
} from '@rxap/ts-morph';
import { Normalized } from '@rxap/utilities';
import { join } from 'path';
import {
  ClassDeclaration,
  SourceFile,
} from 'ts-morph';
import { PrintAngularOptions } from '../../../../lib/angular-options';
import { BackendTypes } from '../../../../lib/backend/backend-types';
import { IsNormalizedImportDataSourceOptions } from '../../../../lib/data-source/data-source-options';
import {
  NormalizedSelectFormControl,
  NormalizeSelectFormControl,
  SelectFormControl,
} from '../../../../lib/form/control/select-form-control';
import {
  NormalizedFormControlOptions,
  NormalizeFormControlOptions,
} from '../../form-control';
import { FormControlOptions } from '../../form-control/schema';
import { SelectFormControlOptions } from './schema';

export type NormalizedSelectFormControlOptions = Readonly<Normalized<Omit<SelectFormControlOptions, keyof SelectFormControl | keyof FormControlOptions>>>
  & NormalizedFormControlOptions & NormalizedSelectFormControl;

export function NormalizeSelectFormControlOptions(
  options: SelectFormControlOptions,
): NormalizedSelectFormControlOptions {
  return Object.freeze({
    ...NormalizeFormControlOptions(options),
    ...NormalizeSelectFormControl(options),
  });
}

function printOptions(options: NormalizedSelectFormControlOptions) {
  PrintAngularOptions('select-form-control', options);
}

function noneBackendOptionsRule(normalizedOptions: NormalizedSelectFormControlOptions): Rule {
  const {
    name,
    project,
    feature,
    directory,
    formName,
    type,
    isArray,
    state,
    isRequired,
    validatorList,
    role,
    isOptional,
    source,
    optionList,
  } = normalizedOptions;
  const optionsDataSourceDirectory = join(directory ?? '', 'data-sources');
  const optionsDataSourceName = classify(
    [ dasherize(name), 'options', 'data-source' ].join('-'),
  );
  const optionsDataSourceImportPath = `./data-sources/${ dasherize(
    name,
  ) }-options.data-source`;
  return chain([
    CoerceOptionsDataSourceRule({
      project,
      feature,
      optionList: optionList ?? [],
      directory: optionsDataSourceDirectory,
      name: [ dasherize(name), 'options' ].join('-'),
    }),
    CoerceFormProviderRule({
      project,
      feature,
      directory,
      providerObject: optionsDataSourceName,
      importStructures: [
        {
          namedImports: [ optionsDataSourceName ],
          moduleSpecifier: optionsDataSourceImportPath,
        },
      ],
    }),
    CoerceFormDefinitionControl({
      role,
      isOptional,
      source,
      project,
      feature,
      directory,
      formName,
      name,
      type,
      isArray,
      state,
      isRequired,
      validatorList,
      coerceFormControl: (
        sourceFile: SourceFile,
        classDeclaration: ClassDeclaration,
        formTypeName: string,
        control: AbstractControl,
      ) => {
        const {
          propertyDeclaration,
          decoratorDeclaration,
        } = CoerceFormControl(sourceFile, classDeclaration, formTypeName, control);

        CoerceDecorator(propertyDeclaration, 'UseOptionsDataSource').set({
          arguments: [ optionsDataSourceName ],
        });
        CoerceImports(sourceFile, {
          namedImports: [ optionsDataSourceName ],
          moduleSpecifier: optionsDataSourceImportPath,
        });
        CoerceImports(sourceFile, {
          namedImports: [ 'UseOptionsDataSource' ],
          moduleSpecifier: '@rxap/form-system',
        });

        return {
          propertyDeclaration,
          decoratorDeclaration,
        };

      },
    })
  ]);
}

function dataSourceBackendOptionsRule(normalizedOptions: NormalizedSelectFormControlOptions): Rule {
  const {
    name,
    project,
    feature,
    directory,
    formName,
    type,
    isArray,
    state,
    isRequired,
    validatorList,
    role,
    isOptional,
    source,
    dataSource,
  } = normalizedOptions;

  if (!dataSource) {
    throw new SchematicsException('The data source is required for the data source backend options rule!');
  }

  if (!IsNormalizedImportDataSourceOptions(dataSource)) {
    throw new SchematicsException('The data source must be an import data source!');
  }

  const optionsDataSourceName = dataSource.import.name;
  const optionsDataSourceImportPath = dataSource.import.moduleSpecifier;

  if (!optionsDataSourceName || !optionsDataSourceImportPath) {
    throw new SchematicsException('The data source name and import path are required for the data source backend options rule!');
  }

  return chain([
    CoerceFormProviderRule({
      project,
      feature,
      directory,
      providerObject: optionsDataSourceName,
      importStructures: [
        {
          namedImports: [ optionsDataSourceName ],
          moduleSpecifier: optionsDataSourceImportPath,
        },
      ],
    }),
    CoerceFormDefinitionControl({
      role,
      isOptional,
      source,
      project,
      feature,
      directory,
      formName,
      name,
      type,
      isArray,
      state,
      isRequired,
      validatorList,
      coerceFormControl: (
        sourceFile: SourceFile,
        classDeclaration: ClassDeclaration,
        formTypeName: string,
        control: AbstractControl,
      ) => {
        const {
          propertyDeclaration,
          decoratorDeclaration,
        } = CoerceFormControl(sourceFile, classDeclaration, formTypeName, control);

        CoerceDecorator(propertyDeclaration, 'UseOptionsDataSource').set({
          arguments: [ optionsDataSourceName ],
        });
        CoerceImports(sourceFile, {
          namedImports: [ optionsDataSourceName ],
          moduleSpecifier: optionsDataSourceImportPath,
        });
        CoerceImports(sourceFile, {
          namedImports: [ 'UseOptionsDataSource' ],
          moduleSpecifier: '@rxap/form-system',
        });

        return {
          propertyDeclaration,
          decoratorDeclaration,
        };

      },
    })
  ]);
}

function nestJsBackendOptionsRule(normalizedOptions: NormalizedSelectFormControlOptions): Rule {
  const {
    name,
    project,
    feature,
    directory,
    formName,
    type,
    isArray,
    state,
    isRequired,
    validatorList,
    nestModule,
    controllerName,
    context,
    scope,
    overwrite,
    role,
    isOptional,
    source,
    upstream,
    backend,
  } = normalizedOptions;
  const optionsOperationPath = [ 'control', dasherize(name), 'options' ].join('/');
  const optionsOperationName = [ 'get', dasherize(name), 'control', 'options' ].join('-');
  const optionsOperationId = buildOperationId(
    normalizedOptions,
    optionsOperationName,
    BuildNestControllerName({
      controllerName,
      nestModule,
    }),
  );
  return chain([
    CoerceOptionsOperationRule({
      project,
      feature,
      nestModule,
      controllerName,
      overwrite,
      operationName: optionsOperationName,
      path: optionsOperationPath,
      control: normalizedOptions,
      context,
      upstream,
      backend,
    }),
    CoerceFormDefinitionControl({
      role,
      isOptional,
      source,
      project,
      feature,
      directory,
      formName,
      name,
      type,
      isArray,
      state,
      isRequired,
      validatorList,
      coerceFormControl: (
        sourceFile: SourceFile,
        classDeclaration: ClassDeclaration,
        formTypeName,
        control: AbstractControl,
      ) => {
        const {
          propertyDeclaration,
          decoratorDeclaration,
        } = CoerceFormControl(sourceFile, classDeclaration, formTypeName, control);

        CoerceDecorator(propertyDeclaration, 'UseOptionsMethod', {
          arguments: [
            OperationIdToRemoteMethodClassName(optionsOperationId),
          ],
        });
        CoerceImports(sourceFile, {
          namedImports: [ OperationIdToRemoteMethodClassName(optionsOperationId) ],
          moduleSpecifier: OperationIdToClassRemoteMethodImportPath(optionsOperationId, scope),
        });
        CoerceImports(sourceFile, {
          namedImports: [ 'UseOptionsMethod' ],
          moduleSpecifier: '@rxap/form-system',
        });

        return {
          propertyDeclaration,
          decoratorDeclaration,
        };

      },
    })
  ]);
}

function openApiBackendOptionsRule(normalizedOptions: NormalizedSelectFormControlOptions): Rule {
  return () => {
    throw new SchematicsException('The open api backend is not supported yet!');
  };
}

function optionsRule(normalizedOptions: NormalizedSelectFormControlOptions): Rule {
  const { backend, dataSource } = normalizedOptions;
  if (dataSource) {
    return dataSourceBackendOptionsRule(normalizedOptions);
  }
  switch (backend.kind) {
    case BackendTypes.LOCAL:
    case BackendTypes.NONE:
      return noneBackendOptionsRule(normalizedOptions);
    case BackendTypes.NESTJS:
      return nestJsBackendOptionsRule(normalizedOptions);
    case BackendTypes.OPEN_API:
      return openApiBackendOptionsRule(normalizedOptions);
    default:
      throw new SchematicsException(`The backend type "${ backend }" is not supported!`);
  }
}

export default function (options: SelectFormControlOptions) {
  const normalizedOptions = NormalizeSelectFormControlOptions(options);
  printOptions(normalizedOptions);
  return () => {
    return chain([
      () => console.group('[@rxap/schematics-angular:select-form-control]'.green),
      optionsRule(normalizedOptions),
      EnforceUseFormControlOrderRule(normalizedOptions),
      () => console.groupEnd(),
    ]);
  };
}
