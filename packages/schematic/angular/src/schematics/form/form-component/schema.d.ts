import { AngularOptions } from '../../../lib/angular-options';
import { FormComponent } from '../../../lib/form/form-component';

export interface FormComponentOptions extends AngularOptions, FormComponent {
}
