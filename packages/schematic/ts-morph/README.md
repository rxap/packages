This package provides utilities for manipulating TypeScript code using ts-morph, particularly for Angular and NestJS projects. It offers functions to add, coerce, and modify code elements like classes, methods, decorators, and imports. The package also includes specialized functions for working with Angular components, NestJS modules, and form definitions.

[![npm version](https://img.shields.io/npm/v/@rxap/schematics-ts-morph?style=flat-square)](https://www.npmjs.com/package/@rxap/schematics-ts-morph)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematics-ts-morph)
![npm](https://img.shields.io/npm/dm/@rxap/schematics-ts-morph)
![NPM](https://img.shields.io/npm/l/@rxap/schematics-ts-morph)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematics-ts-morph
```
**Execute the init generator:**
```bash
yarn nx g @rxap/schematics-ts-morph:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/schematics-ts-morph:init
```
