import { classify } from '@rxap/schematics-utilities';
import {
  CoerceDependencyInjection,
  CoerceImports,
  CoerceMappingClassMethod,
  Module,
  OperationIdToClassRemoteMethodImportPath,
  OperationIdToParameterClassImportPath,
  OperationIdToParameterClassName,
  OperationIdToRemoteMethodClassName,
  OperationIdToRequestBodyClassImportPath,
  OperationIdToRequestBodyClassName,
  OperationIdToResponseClassImportPath,
  OperationIdToResponseClassName,
  ToMappingObjectOptions,
} from '@rxap/ts-morph';
import {
  dasherize,
  noop,
} from '@rxap/utilities';
import {
  Scope,
  StatementStructures,
  WriterFunction,
} from 'ts-morph';
import {
  CoerceTableActionOptions,
  CoerceTableActionRule,
} from './coerce-table-action';

export interface LoadFromTableActionOptions {
  operationId: string;
  scope?: string | null;
  body: boolean | Record<string, any>;
  parameters: boolean | Record<string, any>;
}

export interface CoerceFormTableActionOptions extends CoerceTableActionOptions {
  loadFrom?: LoadFromTableActionOptions | null;
  formInitial?: Record<string, any> | boolean;
  scope?: string | null;
  formComponent: string;
}

const toMappingObjectOptions: ToMappingObjectOptions = {
  aliasFnc: (key: string, value: string) => {
    if ([ 'rowId', '_rowId', '__rowId' ].includes(value)) {
      return '__rowId';
    }
    return value;
  },
};

export function CoerceFormTableActionRule(options: CoerceFormTableActionOptions) {
  const {
    type,
    loadFrom,
    tableName,
    tsMorphTransform = noop,
    scope,
    formInitial,
    formComponent,
  } = options;

  return CoerceTableActionRule({
    ...options,
    tsMorphTransform: (project, sourceFile, classDeclaration) => {

      const tableInterfaceName = `I${ classify(tableName) }`;

      CoerceImports(sourceFile, {
        moduleSpecifier: '@angular/core',
        namedImports: [ 'ChangeDetectorRef', 'Inject', 'INJECTOR', 'Injector' ],
      });
      const openFormWindowMethod = `Open${ classify(formComponent) }WindowMethod`;
      CoerceImports(sourceFile, {
        moduleSpecifier: `../../${ dasherize(formComponent) }/open-${ dasherize(formComponent) }-window.method`,
        namedImports: [ openFormWindowMethod ],
      });
      CoerceImports(sourceFile, {
        namedImports: [ 'firstValueFrom' ],
        moduleSpecifier: 'rxjs',
      });
      if (loadFrom?.operationId) {
        CoerceImports(sourceFile, {
          moduleSpecifier: OperationIdToClassRemoteMethodImportPath(loadFrom.operationId, loadFrom.scope ?? scope),
          namedImports: [ OperationIdToRemoteMethodClassName(loadFrom.operationId) ],
        });
        if (formInitial) {
          CoerceImports(sourceFile, {
            moduleSpecifier: OperationIdToResponseClassImportPath(loadFrom.operationId, loadFrom.scope ?? scope),
            namedImports: [ OperationIdToResponseClassName(loadFrom.operationId) ],
          });
        }
      }

      CoerceDependencyInjection(sourceFile, {
        injectionToken: openFormWindowMethod,
        parameterName: 'openFormWindow',
        scope: Scope.Private,
        module: Module.ANGULAR,
      });
      CoerceDependencyInjection(sourceFile, {
        injectionToken: 'INJECTOR',
        parameterName: 'injector',
        type: 'Injector',
        scope: Scope.Private,
        module: Module.ANGULAR,
      });
      CoerceDependencyInjection(sourceFile, {
        injectionToken: 'ChangeDetectorRef',
        parameterName: 'cdr',
        scope: Scope.Private,
        module: Module.ANGULAR,
      });
      if (loadFrom?.operationId) {
        CoerceDependencyInjection(sourceFile, {
          injectionToken: OperationIdToRemoteMethodClassName(loadFrom.operationId),
          parameterName: 'getInitial',
          scope: Scope.Private,
          module: Module.ANGULAR,
        });
      }
      const statements: (string | WriterFunction | StatementStructures)[] = [];
      statements.push(`console.log(\`action row type: ${ type }\`, parameters);`);
      statements.push(`const { __rowId: rowId } = parameters;`);
      if (loadFrom?.operationId) {
        if (loadFrom.body) {
          CoerceMappingClassMethod(sourceFile, classDeclaration, {
            name: 'getBody',
            parameterType: tableInterfaceName,
            returnType: OperationIdToRequestBodyClassName(loadFrom.operationId),
            mapping: loadFrom.body,
            mappingOptions: toMappingObjectOptions,
          });
          CoerceImports(sourceFile, {
            namedImports: [ OperationIdToRequestBodyClassName(loadFrom.operationId) ],
            moduleSpecifier: OperationIdToRequestBodyClassImportPath(loadFrom.operationId, scope),
          });
          statements.push(`const requestBody = this.getBody(parameters);`);
        }
        if (loadFrom.parameters) {
          CoerceMappingClassMethod(sourceFile, classDeclaration, {
            name: 'getParameters',
            parameterType: tableInterfaceName,
            returnType: OperationIdToParameterClassName(loadFrom.operationId),
            mapping: loadFrom.parameters,
            mappingOptions: toMappingObjectOptions,
          });
          CoerceImports(sourceFile, {
            namedImports: [ OperationIdToParameterClassName(loadFrom.operationId) ],
            moduleSpecifier: OperationIdToParameterClassImportPath(loadFrom.operationId, scope),
          });
          statements.push(`const requestParameters = this.getParameters(parameters);`);
        }

        if (loadFrom.body && loadFrom.parameters) {
          statements.push(
            `const initial = await this.getInitial.call({ parameters: requestParameters, requestBody });`);
        } else if (loadFrom.body) {
          statements.push(`const initial = await this.getInitial.call({ requestBody });`);
        } else if (loadFrom.parameters) {
          statements.push(`const initial = await this.getInitial.call({ parameters: requestParameters });`);
        } else {
          statements.push(`const initial = parameters;`);
        }
      } else {
        if (formInitial) {
          statements.push(`const initial = parameters;`);
        } else {
          statements.push(`const initial = {};`);
        }
      }
      statements.push(`this.cdr.markForCheck();`);
      if (typeof formInitial === 'object') {
        CoerceMappingClassMethod(sourceFile, classDeclaration, {
          name: 'toInitial',
          parameterType: loadFrom?.operationId ? OperationIdToResponseClassName(loadFrom.operationId) :
                         tableInterfaceName,
          mapping: formInitial,
          mappingOptions: toMappingObjectOptions,
          // TODO : use the form interface as return type
          returnType: 'any',
        });
        statements.push(
          `return firstValueFrom(this.openFormWindow.call(this.toInitial(initial), {injector: this.injector, context: { rowId }}));`);
      } else {
        statements.push(
          `return firstValueFrom(this.openFormWindow.call(initial, {injector: this.injector, context: { rowId }}));`);
      }

      return {
        statements,
        isAsync: true,
        scope: Scope.Public,
        returnType: 'Promise<any>',
        ...(tsMorphTransform(project, sourceFile, classDeclaration) ?? {}),
      };
    },
  });

}
