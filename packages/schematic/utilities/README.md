This package provides a set of utilities and helpers for Angular Schematics, including file manipulation, string operations, object manipulation, and Angular-specific JSON file handling. It offers functionalities to simplify common tasks such as updating JSON files, installing dependencies, and managing Angular project configurations. The package also includes generators for initializing schematics.

[![npm version](https://img.shields.io/npm/v/@rxap/schematics-utilities?style=flat-square)](https://www.npmjs.com/package/@rxap/schematics-utilities)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/schematics-utilities)
![npm](https://img.shields.io/npm/dm/@rxap/schematics-utilities)
![NPM](https://img.shields.io/npm/l/@rxap/schematics-utilities)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/schematics-utilities
```
**Execute the init generator:**
```bash
yarn nx g @rxap/schematics-utilities:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/schematics-utilities:init
```
