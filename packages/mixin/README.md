Provides decorators and utilities for applying the mixin pattern in TypeScript. It allows you to compose classes by combining properties and methods from multiple sources. This library includes features for overwriting properties and handling metadata during the mixin process.

[![npm version](https://img.shields.io/npm/v/@rxap/mixin?style=flat-square)](https://www.npmjs.com/package/@rxap/mixin)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/mixin)
![npm](https://img.shields.io/npm/dm/@rxap/mixin)
![NPM](https://img.shields.io/npm/l/@rxap/mixin)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/mixin
```
**Execute the init generator:**
```bash
yarn nx g @rxap/mixin:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/mixin:init
```
