# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.13-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.12...@rxap/browser-tailwind@1.0.13-dev.0) (2025-03-12)

### Reverts

- "chore(deps): upgrade tailwindcss to 4.0.12" ([da8a2c7](https://gitlab.com/rxap/packages/commit/da8a2c79cb35967235bfc33c2f03bde91e1bb772))

## [1.0.12](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.12-dev.1...@rxap/browser-tailwind@1.0.12) (2025-02-23)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.12-dev.0...@rxap/browser-tailwind@1.0.12-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11...@rxap/browser-tailwind@1.0.12-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.5...@rxap/browser-tailwind@1.0.11) (2025-02-13)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.4...@rxap/browser-tailwind@1.0.11-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.3...@rxap/browser-tailwind@1.0.11-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.2...@rxap/browser-tailwind@1.0.11-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.1...@rxap/browser-tailwind@1.0.11-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.11-dev.0...@rxap/browser-tailwind@1.0.11-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.10...@rxap/browser-tailwind@1.0.11-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.10](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.10-dev.2...@rxap/browser-tailwind@1.0.10) (2025-01-08)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.10-dev.1...@rxap/browser-tailwind@1.0.10-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.10-dev.0...@rxap/browser-tailwind@1.0.10-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.9...@rxap/browser-tailwind@1.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.9](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.9-dev.0...@rxap/browser-tailwind@1.0.9) (2024-10-28)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.8...@rxap/browser-tailwind@1.0.9-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.8](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.8-dev.0...@rxap/browser-tailwind@1.0.8) (2024-08-22)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.7...@rxap/browser-tailwind@1.0.8-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.7](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.7-dev.0...@rxap/browser-tailwind@1.0.7) (2024-07-30)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.6...@rxap/browser-tailwind@1.0.7-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.6](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.6-dev.0...@rxap/browser-tailwind@1.0.6) (2024-06-30)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.5...@rxap/browser-tailwind@1.0.6-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.5](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.5-dev.1...@rxap/browser-tailwind@1.0.5) (2024-06-28)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.5-dev.0...@rxap/browser-tailwind@1.0.5-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.4...@rxap/browser-tailwind@1.0.5-dev.0) (2024-06-20)

### Bug Fixes

- add html code coverage reporters ([73da1d8](https://gitlab.com/rxap/packages/commit/73da1d85274686590952a97e202c713718988caa))

## [1.0.4](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.4-dev.2...@rxap/browser-tailwind@1.0.4) (2024-06-18)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.4-dev.1...@rxap/browser-tailwind@1.0.4-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.4-dev.0...@rxap/browser-tailwind@1.0.4-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.3...@rxap/browser-tailwind@1.0.4-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.3](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.3-dev.0...@rxap/browser-tailwind@1.0.3) (2024-05-29)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.2...@rxap/browser-tailwind@1.0.3-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.2-dev.1...@rxap/browser-tailwind@1.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.2-dev.0...@rxap/browser-tailwind@1.0.2-dev.1) (2024-04-12)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.1...@rxap/browser-tailwind@1.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@1.0.1-dev.0...@rxap/browser-tailwind@1.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/browser-tailwind

## [1.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.1.0-dev.4...@rxap/browser-tailwind@1.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

# [0.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.1.0-dev.3...@rxap/browser-tailwind@0.1.0-dev.4) (2023-10-11)

**Note:** Version bump only for package @rxap/browser-tailwind

# 0.1.0-dev.3 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([6223c97](https://gitlab.com/rxap/packages/commit/6223c978078cfa899ca69424b62d2a99cbb290a7))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- support dynamic font families ([37e1140](https://gitlab.com/rxap/packages/commit/37e1140d8edb8d65c0ea9fa18b57aa052ba01ded))

### Features

- add tailwind plugin for markdown ([653dfb3](https://gitlab.com/rxap/packages/commit/653dfb31497634ece9e0eb89528f5f32407ce66b))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [0.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.1.0-dev.1...@rxap/browser-tailwind@0.1.0-dev.2) (2023-09-28)

### Bug Fixes

- support dynamic font families ([47db43d](https://gitlab.com/rxap/packages/commit/47db43d466ac51bded60434dab9131eab56c82a2))

# [0.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.1.0-dev.0...@rxap/browser-tailwind@0.1.0-dev.1) (2023-09-27)

**Note:** Version bump only for package @rxap/browser-tailwind

# [0.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.10...@rxap/browser-tailwind@0.1.0-dev.0) (2023-09-15)

### Features

- add tailwind plugin for markdown ([0ee05f4](https://gitlab.com/rxap/packages/commit/0ee05f47a00f879c0abdd9009b96c539a2b9b76f))

## [0.0.2-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.9...@rxap/browser-tailwind@0.0.2-dev.10) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [0.0.2-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.8...@rxap/browser-tailwind@0.0.2-dev.9) (2023-09-12)

**Note:** Version bump only for package @rxap/browser-tailwind

## [0.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.7...@rxap/browser-tailwind@0.0.2-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/browser-tailwind

## [0.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.6...@rxap/browser-tailwind@0.0.2-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/browser-tailwind

## [0.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.5...@rxap/browser-tailwind@0.0.2-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/browser-tailwind

## [0.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.4...@rxap/browser-tailwind@0.0.2-dev.5) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [0.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.3...@rxap/browser-tailwind@0.0.2-dev.4) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [0.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.2...@rxap/browser-tailwind@0.0.2-dev.3) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([a69aa25](https://gitlab.com/rxap/packages/commit/a69aa25b9824b94613392b3ea42fba18e5eb1168))

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.1...@rxap/browser-tailwind@0.0.2-dev.2) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))

## [0.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/browser-tailwind@0.0.2-dev.0...@rxap/browser-tailwind@0.0.2-dev.1) (2023-08-03)

**Note:** Version bump only for package @rxap/browser-tailwind

## 0.0.2-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/browser-tailwind
