Provides a Tailwind CSS configuration with RXAP-specific theme extensions, including custom color palettes and font families. It also includes an init generator that adds missing peer dependencies to the project&#x27;s package.json and runs init generators for those dependencies.

[![npm version](https://img.shields.io/npm/v/@rxap/browser-tailwind?style=flat-square)](https://www.npmjs.com/package/@rxap/browser-tailwind)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/browser-tailwind)
![npm](https://img.shields.io/npm/dm/@rxap/browser-tailwind)
![NPM](https://img.shields.io/npm/l/@rxap/browser-tailwind)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/browser-tailwind
```
**Execute the init generator:**
```bash
yarn nx g @rxap/browser-tailwind:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/browser-tailwind:init
```
