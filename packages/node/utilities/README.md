Provides a set of utility functions for Node.js development, including file system operations, git integration, and package.json manipulation. It offers functionalities like copying folders, reading JSON files with retry logic, retrieving the current git branch, and fetching package information from npm. This package aims to simplify common tasks and improve developer productivity in Node.js projects.

[![npm version](https://img.shields.io/npm/v/@rxap/node-utilities?style=flat-square)](https://www.npmjs.com/package/@rxap/node-utilities)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/node-utilities)
![npm](https://img.shields.io/npm/dm/@rxap/node-utilities)
![NPM](https://img.shields.io/npm/l/@rxap/node-utilities)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/node-utilities
```
**Execute the init generator:**
```bash
yarn nx g @rxap/node-utilities:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/node-utilities:init
```
