# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.9...@rxap/node-utilities@1.3.10-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.9](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.9-dev.3...@rxap/node-utilities@1.3.9) (2025-02-23)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.9-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.9-dev.2...@rxap/node-utilities@1.3.9-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.9-dev.1...@rxap/node-utilities@1.3.9-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.9-dev.0...@rxap/node-utilities@1.3.9-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8...@rxap/node-utilities@1.3.9-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.5...@rxap/node-utilities@1.3.8) (2025-02-13)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.4...@rxap/node-utilities@1.3.8-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.3...@rxap/node-utilities@1.3.8-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.2...@rxap/node-utilities@1.3.8-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.1...@rxap/node-utilities@1.3.8-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.8-dev.0...@rxap/node-utilities@1.3.8-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.7...@rxap/node-utilities@1.3.8-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.7](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.7-dev.2...@rxap/node-utilities@1.3.7) (2025-01-08)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.7-dev.1...@rxap/node-utilities@1.3.7-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.7-dev.0...@rxap/node-utilities@1.3.7-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.6...@rxap/node-utilities@1.3.7-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.6](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.6-dev.0...@rxap/node-utilities@1.3.6) (2024-12-10)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.5...@rxap/node-utilities@1.3.6-dev.0) (2024-11-04)

### Bug Fixes

- support production fallback in scoped file names ([08db421](https://gitlab.com/rxap/packages/commit/08db4217a5ff374f39baa6c2a29d218899fa3ee2))

## [1.3.5](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.5-dev.0...@rxap/node-utilities@1.3.5) (2024-10-28)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.4...@rxap/node-utilities@1.3.5-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.4](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.4-dev.0...@rxap/node-utilities@1.3.4) (2024-08-22)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.3...@rxap/node-utilities@1.3.4-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.3](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.3-dev.0...@rxap/node-utilities@1.3.3) (2024-07-30)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.2...@rxap/node-utilities@1.3.3-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.2-dev.0...@rxap/node-utilities@1.3.2) (2024-06-30)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.1...@rxap/node-utilities@1.3.2-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.1-dev.1...@rxap/node-utilities@1.3.1) (2024-06-28)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.1-dev.0...@rxap/node-utilities@1.3.1-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/node-utilities

## [1.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.0...@rxap/node-utilities@1.3.1-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/node-utilities

# [1.3.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.0-dev.1...@rxap/node-utilities@1.3.0) (2024-06-18)

**Note:** Version bump only for package @rxap/node-utilities

# [1.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.3.0-dev.0...@rxap/node-utilities@1.3.0-dev.1) (2024-06-18)

### Bug Fixes

- use nx env to local the rxap package ([08d83ab](https://gitlab.com/rxap/packages/commit/08d83ab17a385ce14147df26d0983f49a9601779))
- use nx env to local the rxap package ([06ca11c](https://gitlab.com/rxap/packages/commit/06ca11c3f2f2a431aef323861f012f7c26951cea))

# [1.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.3-dev.0...@rxap/node-utilities@1.3.0-dev.0) (2024-06-18)

### Features

- use the rxap package to resolve package versions ([d499a8a](https://gitlab.com/rxap/packages/commit/d499a8a0e7e6f7ee0b54e9f14bf77509ff7ae910))

## [1.2.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.2...@rxap/node-utilities@1.2.3-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/node-utilities

## [1.2.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.2-dev.0...@rxap/node-utilities@1.2.2) (2024-05-29)

### Bug Fixes

- support async package json reading ([6b8a163](https://gitlab.com/rxap/packages/commit/6b8a16323b25108a81291ccf976ee7c2aaedacc3))

## [1.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.1...@rxap/node-utilities@1.2.2-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/node-utilities

## [1.2.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.1-dev.0...@rxap/node-utilities@1.2.1) (2024-05-28)

**Note:** Version bump only for package @rxap/node-utilities

## [1.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.0...@rxap/node-utilities@1.2.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/node-utilities

# [1.2.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.0-dev.2...@rxap/node-utilities@1.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/node-utilities

# [1.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.0-dev.1...@rxap/node-utilities@1.2.0-dev.2) (2024-04-12)

### Bug Fixes

- throw error if package does no exists ([f925541](https://gitlab.com/rxap/packages/commit/f925541b59470f03c81ca4ae0c49f53b21b52d99))

# [1.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.2.0-dev.0...@rxap/node-utilities@1.2.0-dev.1) (2024-03-31)

**Note:** Version bump only for package @rxap/node-utilities

# [1.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.1-dev.1...@rxap/node-utilities@1.2.0-dev.0) (2024-03-11)

### Bug Fixes

- store the correct cache value ([3b9380d](https://gitlab.com/rxap/packages/commit/3b9380d3d0710b628fc0203591ab63a93a894094))

### Features

- add GetPackageInfo function ([351c305](https://gitlab.com/rxap/packages/commit/351c305401f52c61b96c501410a7f284bfc82700))
- add GetPackagePeerDependencies function ([6fe6c50](https://gitlab.com/rxap/packages/commit/6fe6c5062c2bcdb36bd41f98f122cd66cbdf7fba))

## [1.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.1-dev.0...@rxap/node-utilities@1.1.1-dev.1) (2024-03-05)

**Note:** Version bump only for package @rxap/node-utilities

## [1.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0...@rxap/node-utilities@1.1.1-dev.0) (2024-03-04)

### Bug Fixes

- log composed file path with scope ([62ad174](https://gitlab.com/rxap/packages/commit/62ad174539788e87b10909cb32e1516e57218a06))

# [1.1.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.15...@rxap/node-utilities@1.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.14...@rxap/node-utilities@1.1.0-dev.15) (2023-10-11)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.0.1-dev.1...@rxap/node-utilities@1.1.0-dev.14) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add package version cache ([d53c22b](https://gitlab.com/rxap/packages/commit/d53c22b2879d9ea39a3f05e92dfc3a66c92e22ae))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- improve error message output ([64f4ea2](https://gitlab.com/rxap/packages/commit/64f4ea2c74bc95facf5d51d524c0ddf6351eeb4d))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Features

- add file with scope utility functions ([bf882cf](https://gitlab.com/rxap/packages/commit/bf882cf979c5095f3175c01f9bd68b903dd08d38))
- add function CopyFolderSync ([b140757](https://gitlab.com/rxap/packages/commit/b14075764c3a25c5df6aab16d73a0305443a930a))
- add function GetCurrentBranch ([ea5d168](https://gitlab.com/rxap/packages/commit/ea5d16831594404865bd77eb3b673cfcd687d577))
- add function RemoveDirSync ([e36561d](https://gitlab.com/rxap/packages/commit/e36561d456717a2a3311a2fe9bd35f1d71842609))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [1.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.12...@rxap/node-utilities@1.1.0-dev.13) (2023-10-01)

### Features

- add function CopyFolderSync ([0f2bfbe](https://gitlab.com/rxap/packages/commit/0f2bfbeea982f49278d5142d094c62d84cddc6bf))

# [1.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.11...@rxap/node-utilities@1.1.0-dev.12) (2023-09-29)

### Features

- add function RemoveDirSync ([e0e1271](https://gitlab.com/rxap/packages/commit/e0e127195cbcfebee4d9bda0a77bda92fff8b4af))

# [1.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.10...@rxap/node-utilities@1.1.0-dev.11) (2023-09-27)

### Features

- add file with scope utility functions ([f74e8cb](https://gitlab.com/rxap/packages/commit/f74e8cb8465b43190f222d4a04f644a6eeb66a71))

# [1.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.9...@rxap/node-utilities@1.1.0-dev.10) (2023-09-27)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.8...@rxap/node-utilities@1.1.0-dev.9) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [1.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.7...@rxap/node-utilities@1.1.0-dev.8) (2023-09-12)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.6...@rxap/node-utilities@1.1.0-dev.7) (2023-09-07)

### Bug Fixes

- improve error message output ([663f50b](https://gitlab.com/rxap/packages/commit/663f50b03e8729fba2d76c08cdbe292af438a5da))

# [1.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.5...@rxap/node-utilities@1.1.0-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.4...@rxap/node-utilities@1.1.0-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/node-utilities

# [1.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.3...@rxap/node-utilities@1.1.0-dev.4) (2023-08-31)

### Bug Fixes

- add package version cache ([59444f0](https://gitlab.com/rxap/packages/commit/59444f0a111071d7fc9990afb9fecae051e0c2e3))
- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [1.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.2...@rxap/node-utilities@1.1.0-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [1.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.1...@rxap/node-utilities@1.1.0-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [1.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.1.0-dev.0...@rxap/node-utilities@1.1.0-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))

# [1.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.0.1-dev.2...@rxap/node-utilities@1.1.0-dev.0) (2023-08-03)

### Bug Fixes

- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

### Features

- add function GetCurrentBranch ([89e2431](https://gitlab.com/rxap/packages/commit/89e2431c55b440e44f12f9fc14290fe246e9bc8d))

## 1.0.1-dev.2 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))

## [1.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-utilities@1.0.1-dev.0...@rxap/node-utilities@1.0.1-dev.1) (2023-07-13)

**Note:** Version bump only for package @rxap/node-utilities

## 1.0.1-dev.0 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
