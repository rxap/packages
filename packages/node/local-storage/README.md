Provides a simple local storage implementation for Node.js environments, persisting data to a specified directory. It implements the standard &#x60;Storage&#x60; interface, allowing for easy data storage and retrieval. This package is useful for applications that need a persistent storage mechanism without relying on a database.

[![npm version](https://img.shields.io/npm/v/@rxap/node-local-storage?style=flat-square)](https://www.npmjs.com/package/@rxap/node-local-storage)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/node-local-storage)
![npm](https://img.shields.io/npm/dm/@rxap/node-local-storage)
![NPM](https://img.shields.io/npm/l/@rxap/node-local-storage)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/node-local-storage
```
**Execute the init generator:**
```bash
yarn nx g @rxap/node-local-storage:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/node-local-storage:init
```
