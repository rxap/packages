# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.13-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.12...@rxap/node-local-storage@1.0.13-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.12](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.12-dev.1...@rxap/node-local-storage@1.0.12) (2025-02-23)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.12-dev.0...@rxap/node-local-storage@1.0.12-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11...@rxap/node-local-storage@1.0.12-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.5...@rxap/node-local-storage@1.0.11) (2025-02-13)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.4...@rxap/node-local-storage@1.0.11-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.3...@rxap/node-local-storage@1.0.11-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.2...@rxap/node-local-storage@1.0.11-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.1...@rxap/node-local-storage@1.0.11-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.11-dev.0...@rxap/node-local-storage@1.0.11-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.10...@rxap/node-local-storage@1.0.11-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.10](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.10-dev.2...@rxap/node-local-storage@1.0.10) (2025-01-08)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.10-dev.1...@rxap/node-local-storage@1.0.10-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.10-dev.0...@rxap/node-local-storage@1.0.10-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.9...@rxap/node-local-storage@1.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.9](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.9-dev.0...@rxap/node-local-storage@1.0.9) (2024-10-28)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.8...@rxap/node-local-storage@1.0.9-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.8](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.8-dev.0...@rxap/node-local-storage@1.0.8) (2024-08-22)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.7...@rxap/node-local-storage@1.0.8-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.7](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.7-dev.0...@rxap/node-local-storage@1.0.7) (2024-07-30)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.6...@rxap/node-local-storage@1.0.7-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.6](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.6-dev.0...@rxap/node-local-storage@1.0.6) (2024-06-30)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.5...@rxap/node-local-storage@1.0.6-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.5](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.5-dev.1...@rxap/node-local-storage@1.0.5) (2024-06-28)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.5-dev.0...@rxap/node-local-storage@1.0.5-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.4...@rxap/node-local-storage@1.0.5-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.4](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.4-dev.0...@rxap/node-local-storage@1.0.4) (2024-06-18)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.3...@rxap/node-local-storage@1.0.4-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.3](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.3-dev.0...@rxap/node-local-storage@1.0.3) (2024-05-29)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.2...@rxap/node-local-storage@1.0.3-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.2](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.2-dev.0...@rxap/node-local-storage@1.0.2) (2024-04-17)

### Bug Fixes

- skip local storage tests ([9030c84](https://gitlab.com/rxap/packages/commit/9030c8478fd38d08aab01be3dac38e02101d060e))

## [1.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.1...@rxap/node-local-storage@1.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.1](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@1.0.1-dev.0...@rxap/node-local-storage@1.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/node-local-storage

## [1.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@0.0.2-dev.2...@rxap/node-local-storage@1.0.1-dev.0) (2024-02-07)

### Bug Fixes

- use correct base version ([71e15a4](https://gitlab.com/rxap/packages/commit/71e15a49f9ee249076ae8ae0987a15143fe18836))

## [0.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/node-local-storage@0.0.2-dev.1...@rxap/node-local-storage@0.0.2-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/node-local-storage

## 0.0.2-dev.1 (2023-10-11)

### Bug Fixes

- remove package dependency ([aacf54a](https://gitlab.com/rxap/packages/commit/aacf54a427bf0eddd124b97ac19e634260263b10))

## 0.0.2-dev.0 (2023-09-29)

### Bug Fixes

- remove package dependency ([989a6bf](https://gitlab.com/rxap/packages/commit/989a6bfc0e8aa858e7d6fd552ac0269c546a1e87))
