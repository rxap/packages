# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.4-dev.1...@rxap/preset-workspace@20.0.4-dev.2) (2025-03-12)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.4-dev.0...@rxap/preset-workspace@20.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.3...@rxap/preset-workspace@20.0.4-dev.0) (2025-03-10)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.3-dev.2...@rxap/preset-workspace@20.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.3-dev.1...@rxap/preset-workspace@20.0.3-dev.2) (2025-02-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.3-dev.0...@rxap/preset-workspace@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2...@rxap/preset-workspace@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.6...@rxap/preset-workspace@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.5...@rxap/preset-workspace@20.0.2-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.4...@rxap/preset-workspace@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.3...@rxap/preset-workspace@20.0.2-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.2...@rxap/preset-workspace@20.0.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.1...@rxap/preset-workspace@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.2-dev.0...@rxap/preset-workspace@20.0.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1...@rxap/preset-workspace@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.15...@rxap/preset-workspace@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.14...@rxap/preset-workspace@20.0.1-dev.15) (2025-02-13)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.13...@rxap/preset-workspace@20.0.1-dev.14) (2025-02-11)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.12...@rxap/preset-workspace@20.0.1-dev.13) (2025-02-11)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.11...@rxap/preset-workspace@20.0.1-dev.12) (2025-02-10)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.10...@rxap/preset-workspace@20.0.1-dev.11) (2025-02-07)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.9...@rxap/preset-workspace@20.0.1-dev.10) (2025-01-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.8...@rxap/preset-workspace@20.0.1-dev.9) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.7...@rxap/preset-workspace@20.0.1-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.6...@rxap/preset-workspace@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.5...@rxap/preset-workspace@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.4...@rxap/preset-workspace@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.3...@rxap/preset-workspace@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.2...@rxap/preset-workspace@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.1...@rxap/preset-workspace@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.1-dev.0...@rxap/preset-workspace@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/preset-workspace

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0...@rxap/preset-workspace@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/preset-workspace

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0-dev.5...@rxap/preset-workspace@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/preset-workspace

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0-dev.4...@rxap/preset-workspace@20.0.0-dev.5) (2025-01-04)

**Note:** Version bump only for package @rxap/preset-workspace

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0-dev.3...@rxap/preset-workspace@20.0.0-dev.4) (2025-01-04)

**Note:** Version bump only for package @rxap/preset-workspace

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0-dev.2...@rxap/preset-workspace@20.0.0-dev.3) (2025-01-03)

**Note:** Version bump only for package @rxap/preset-workspace

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@20.0.0-dev.1...@rxap/preset-workspace@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.10-dev.0...@rxap/preset-workspace@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.9...@rxap/preset-workspace@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.9-dev.1...@rxap/preset-workspace@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.9-dev.0...@rxap/preset-workspace@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.8...@rxap/preset-workspace@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.8-dev.3...@rxap/preset-workspace@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.8-dev.2...@rxap/preset-workspace@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.8-dev.1...@rxap/preset-workspace@19.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.8-dev.0...@rxap/preset-workspace@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.7...@rxap/preset-workspace@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.7-dev.3...@rxap/preset-workspace@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.7-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.7-dev.2...@rxap/preset-workspace@19.0.7-dev.3) (2024-09-11)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.7-dev.1...@rxap/preset-workspace@19.0.7-dev.2) (2024-09-09)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.7-dev.0...@rxap/preset-workspace@19.0.7-dev.1) (2024-08-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6...@rxap/preset-workspace@19.0.7-dev.0) (2024-08-27)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6-dev.4...@rxap/preset-workspace@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6-dev.3...@rxap/preset-workspace@19.0.6-dev.4) (2024-08-22)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6-dev.2...@rxap/preset-workspace@19.0.6-dev.3) (2024-08-21)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6-dev.1...@rxap/preset-workspace@19.0.6-dev.2) (2024-08-21)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.6-dev.0...@rxap/preset-workspace@19.0.6-dev.1) (2024-08-19)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.5...@rxap/preset-workspace@19.0.6-dev.0) (2024-08-15)

### Bug Fixes

- update default nx targets ([43f6151](https://gitlab.com/rxap/packages/commit/43f6151292a10218735e5cf203ec3cb169211530))

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.5-dev.0...@rxap/preset-workspace@19.0.5) (2024-08-05)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4...@rxap/preset-workspace@19.0.5-dev.0) (2024-08-05)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.9...@rxap/preset-workspace@19.0.4) (2024-07-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.8...@rxap/preset-workspace@19.0.4-dev.9) (2024-07-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.7...@rxap/preset-workspace@19.0.4-dev.8) (2024-07-09)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.6...@rxap/preset-workspace@19.0.4-dev.7) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.5...@rxap/preset-workspace@19.0.4-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.4...@rxap/preset-workspace@19.0.4-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.3...@rxap/preset-workspace@19.0.4-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.2...@rxap/preset-workspace@19.0.4-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.1...@rxap/preset-workspace@19.0.4-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.4-dev.0...@rxap/preset-workspace@19.0.4-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.3...@rxap/preset-workspace@19.0.4-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.3-dev.0...@rxap/preset-workspace@19.0.3) (2024-06-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2...@rxap/preset-workspace@19.0.3-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.5...@rxap/preset-workspace@19.0.2) (2024-06-28)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.4...@rxap/preset-workspace@19.0.2-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.3...@rxap/preset-workspace@19.0.2-dev.4) (2024-06-24)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.2...@rxap/preset-workspace@19.0.2-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.1...@rxap/preset-workspace@19.0.2-dev.2) (2024-06-21)

### Bug Fixes

- run the install task after preset generators ([6eb50fa](https://gitlab.com/rxap/packages/commit/6eb50fabc21d7b0bc403b5bc6bed7331bcedeb7c))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.2-dev.0...@rxap/preset-workspace@19.0.2-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1...@rxap/preset-workspace@19.0.2-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.12...@rxap/preset-workspace@19.0.1) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.11...@rxap/preset-workspace@19.0.1-dev.12) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.10...@rxap/preset-workspace@19.0.1-dev.11) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.9...@rxap/preset-workspace@19.0.1-dev.10) (2024-06-18)

### Bug Fixes

- ensure only valid options are stored ([c754404](https://gitlab.com/rxap/packages/commit/c75440438fe03e550dad208b46b378aee888a995))

## [19.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.8...@rxap/preset-workspace@19.0.1-dev.9) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.7...@rxap/preset-workspace@19.0.1-dev.8) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.6...@rxap/preset-workspace@19.0.1-dev.7) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.5...@rxap/preset-workspace@19.0.1-dev.6) (2024-06-17)

### Bug Fixes

- only add dependencies to preset and plugin projects ([6e779b4](https://gitlab.com/rxap/packages/commit/6e779b46a34b329e4721af7bac529f1f801bb17a))

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.4...@rxap/preset-workspace@19.0.1-dev.5) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.3...@rxap/preset-workspace@19.0.1-dev.4) (2024-06-10)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.2...@rxap/preset-workspace@19.0.1-dev.3) (2024-06-05)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.1...@rxap/preset-workspace@19.0.1-dev.2) (2024-06-04)

**Note:** Version bump only for package @rxap/preset-workspace

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-workspace@19.0.1-dev.0...@rxap/preset-workspace@19.0.1-dev.1) (2024-06-03)

**Note:** Version bump only for package @rxap/preset-workspace

## 19.0.1-dev.0 (2024-06-02)

**Note:** Version bump only for package @rxap/preset-workspace
