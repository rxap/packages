# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.5-dev.3...@rxap/preset-angular@20.0.5-dev.4) (2025-03-12)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.5-dev.2...@rxap/preset-angular@20.0.5-dev.3) (2025-03-12)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.5-dev.1...@rxap/preset-angular@20.0.5-dev.2) (2025-03-10)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.5-dev.0...@rxap/preset-angular@20.0.5-dev.1) (2025-03-10)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4...@rxap/preset-angular@20.0.5-dev.0) (2025-03-10)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4-dev.4...@rxap/preset-angular@20.0.4) (2025-03-07)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4-dev.3...@rxap/preset-angular@20.0.4-dev.4) (2025-02-28)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4-dev.2...@rxap/preset-angular@20.0.4-dev.3) (2025-02-26)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4-dev.1...@rxap/preset-angular@20.0.4-dev.2) (2025-02-25)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.4-dev.0...@rxap/preset-angular@20.0.4-dev.1) (2025-02-25)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3...@rxap/preset-angular@20.0.4-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.6...@rxap/preset-angular@20.0.3) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.5...@rxap/preset-angular@20.0.3-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.4...@rxap/preset-angular@20.0.3-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.3...@rxap/preset-angular@20.0.3-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.2...@rxap/preset-angular@20.0.3-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.1...@rxap/preset-angular@20.0.3-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.3-dev.0...@rxap/preset-angular@20.0.3-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.2...@rxap/preset-angular@20.0.3-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1...@rxap/preset-angular@20.0.2) (2025-02-13)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.22...@rxap/preset-angular@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.22](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.21...@rxap/preset-angular@20.0.1-dev.22) (2025-02-13)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.20...@rxap/preset-angular@20.0.1-dev.21) (2025-02-11)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.19...@rxap/preset-angular@20.0.1-dev.20) (2025-02-11)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.18...@rxap/preset-angular@20.0.1-dev.19) (2025-02-10)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.17...@rxap/preset-angular@20.0.1-dev.18) (2025-02-07)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.16...@rxap/preset-angular@20.0.1-dev.17) (2025-01-30)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.15...@rxap/preset-angular@20.0.1-dev.16) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.14...@rxap/preset-angular@20.0.1-dev.15) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.13...@rxap/preset-angular@20.0.1-dev.14) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.12...@rxap/preset-angular@20.0.1-dev.13) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.11...@rxap/preset-angular@20.0.1-dev.12) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.10...@rxap/preset-angular@20.0.1-dev.11) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.9...@rxap/preset-angular@20.0.1-dev.10) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.8...@rxap/preset-angular@20.0.1-dev.9) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.7...@rxap/preset-angular@20.0.1-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.6...@rxap/preset-angular@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.5...@rxap/preset-angular@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.4...@rxap/preset-angular@20.0.1-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.3...@rxap/preset-angular@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.2...@rxap/preset-angular@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.1...@rxap/preset-angular@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.1-dev.0...@rxap/preset-angular@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/preset-angular

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0...@rxap/preset-angular@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.7...@rxap/preset-angular@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.6...@rxap/preset-angular@20.0.0-dev.7) (2025-01-04)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.5...@rxap/preset-angular@20.0.0-dev.6) (2025-01-04)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.4...@rxap/preset-angular@20.0.0-dev.5) (2025-01-03)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.3...@rxap/preset-angular@20.0.0-dev.4) (2025-01-03)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.2...@rxap/preset-angular@20.0.0-dev.3) (2025-01-03)

**Note:** Version bump only for package @rxap/preset-angular

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@20.0.0-dev.1...@rxap/preset-angular@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([d8ca485](https://gitlab.com/rxap/packages/commit/d8ca4858aa1fd1f8529a75440d4a283a0f03a01e))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.9-dev.1...@rxap/preset-angular@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.9-dev.0...@rxap/preset-angular@19.0.9-dev.1) (2024-12-10)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.8...@rxap/preset-angular@19.0.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.8-dev.1...@rxap/preset-angular@19.0.8) (2024-12-10)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.8-dev.0...@rxap/preset-angular@19.0.8-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7...@rxap/preset-angular@19.0.8-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.5...@rxap/preset-angular@19.0.7) (2024-10-28)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.4...@rxap/preset-angular@19.0.7-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.3...@rxap/preset-angular@19.0.7-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.2...@rxap/preset-angular@19.0.7-dev.3) (2024-10-22)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.1...@rxap/preset-angular@19.0.7-dev.2) (2024-10-04)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.7-dev.0...@rxap/preset-angular@19.0.7-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.6...@rxap/preset-angular@19.0.7-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.6-dev.3...@rxap/preset-angular@19.0.6) (2024-09-18)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.6-dev.2...@rxap/preset-angular@19.0.6-dev.3) (2024-09-11)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.6-dev.1...@rxap/preset-angular@19.0.6-dev.2) (2024-09-09)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.6-dev.0...@rxap/preset-angular@19.0.6-dev.1) (2024-08-30)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5...@rxap/preset-angular@19.0.6-dev.0) (2024-08-27)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.9...@rxap/preset-angular@19.0.5) (2024-08-22)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.8...@rxap/preset-angular@19.0.5-dev.9) (2024-08-22)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.7...@rxap/preset-angular@19.0.5-dev.8) (2024-08-21)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.6...@rxap/preset-angular@19.0.5-dev.7) (2024-08-21)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.5...@rxap/preset-angular@19.0.5-dev.6) (2024-08-19)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.4...@rxap/preset-angular@19.0.5-dev.5) (2024-08-15)

### Bug Fixes

- add utility and shared projects ([1d952d1](https://gitlab.com/rxap/packages/commit/1d952d18c6697417938c941f771bdc18a42a94e8))

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.3...@rxap/preset-angular@19.0.5-dev.4) (2024-08-15)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.2...@rxap/preset-angular@19.0.5-dev.3) (2024-08-15)

### Bug Fixes

- update default nx targets ([43f6151](https://gitlab.com/rxap/packages/commit/43f6151292a10218735e5cf203ec3cb169211530))

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.1...@rxap/preset-angular@19.0.5-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.5-dev.0...@rxap/preset-angular@19.0.5-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.4...@rxap/preset-angular@19.0.5-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.4-dev.0...@rxap/preset-angular@19.0.4) (2024-08-05)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3...@rxap/preset-angular@19.0.4-dev.0) (2024-08-05)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.15...@rxap/preset-angular@19.0.3) (2024-07-30)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.14...@rxap/preset-angular@19.0.3-dev.15) (2024-07-30)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.13...@rxap/preset-angular@19.0.3-dev.14) (2024-07-26)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.12...@rxap/preset-angular@19.0.3-dev.13) (2024-07-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.11...@rxap/preset-angular@19.0.3-dev.12) (2024-07-24)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.10...@rxap/preset-angular@19.0.3-dev.11) (2024-07-24)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.9...@rxap/preset-angular@19.0.3-dev.10) (2024-07-22)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.8...@rxap/preset-angular@19.0.3-dev.9) (2024-07-10)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.7...@rxap/preset-angular@19.0.3-dev.8) (2024-07-09)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.6...@rxap/preset-angular@19.0.3-dev.7) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.5...@rxap/preset-angular@19.0.3-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.4...@rxap/preset-angular@19.0.3-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.3...@rxap/preset-angular@19.0.3-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.2...@rxap/preset-angular@19.0.3-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.1...@rxap/preset-angular@19.0.3-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.3-dev.0...@rxap/preset-angular@19.0.3-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.2...@rxap/preset-angular@19.0.3-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.2-dev.0...@rxap/preset-angular@19.0.2) (2024-06-30)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1...@rxap/preset-angular@19.0.2-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.13...@rxap/preset-angular@19.0.1) (2024-06-28)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.12...@rxap/preset-angular@19.0.1-dev.13) (2024-06-27)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.11...@rxap/preset-angular@19.0.1-dev.12) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.10...@rxap/preset-angular@19.0.1-dev.11) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.9...@rxap/preset-angular@19.0.1-dev.10) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.8...@rxap/preset-angular@19.0.1-dev.9) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.7...@rxap/preset-angular@19.0.1-dev.8) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.6...@rxap/preset-angular@19.0.1-dev.7) (2024-06-25)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.5...@rxap/preset-angular@19.0.1-dev.6) (2024-06-24)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.4...@rxap/preset-angular@19.0.1-dev.5) (2024-06-21)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.3...@rxap/preset-angular@19.0.1-dev.4) (2024-06-21)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.2...@rxap/preset-angular@19.0.1-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.1...@rxap/preset-angular@19.0.1-dev.2) (2024-06-21)

### Bug Fixes

- run the install task after preset generators ([6eb50fa](https://gitlab.com/rxap/packages/commit/6eb50fabc21d7b0bc403b5bc6bed7331bcedeb7c))

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.1-dev.0...@rxap/preset-angular@19.0.1-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/preset-angular

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0...@rxap/preset-angular@19.0.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.13...@rxap/preset-angular@19.0.0) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.12...@rxap/preset-angular@19.0.0-dev.13) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.11...@rxap/preset-angular@19.0.0-dev.12) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.10...@rxap/preset-angular@19.0.0-dev.11) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.9...@rxap/preset-angular@19.0.0-dev.10) (2024-06-18)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.8...@rxap/preset-angular@19.0.0-dev.9) (2024-06-18)

### Bug Fixes

- ensure only valid options are stored ([c754404](https://gitlab.com/rxap/packages/commit/c75440438fe03e550dad208b46b378aee888a995))

# [19.0.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.7...@rxap/preset-angular@19.0.0-dev.8) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.6...@rxap/preset-angular@19.0.0-dev.7) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.5...@rxap/preset-angular@19.0.0-dev.6) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.4...@rxap/preset-angular@19.0.0-dev.5) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.3...@rxap/preset-angular@19.0.0-dev.4) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# [19.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.2...@rxap/preset-angular@19.0.0-dev.3) (2024-06-17)

### Bug Fixes

- skip manuel package install ([6cba945](https://gitlab.com/rxap/packages/commit/6cba9450cc3355fe4cfa8809221e4ccd36d3d15c))

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/preset-angular@19.0.0-dev.1...@rxap/preset-angular@19.0.0-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular

# 19.0.0-dev.1 (2024-06-17)

**Note:** Version bump only for package @rxap/preset-angular
