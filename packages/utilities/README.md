A collection of utility functions, types and interfaces.
[![npm version](https://img.shields.io/npm/v/@rxap/utilities?style=flat-square)](https://www.npmjs.com/package/@rxap/utilities)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/utilities)
![npm](https://img.shields.io/npm/dm/@rxap/utilities)
![NPM](https://img.shields.io/npm/l/@rxap/utilities)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/utilities
```
**Execute the init generator:**
```bash
yarn nx g @rxap/utilities:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/utilities:init
```
