# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.4-dev.0...@rxap/plugin-gitlab-ci@20.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.3...@rxap/plugin-gitlab-ci@20.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.3-dev.1...@rxap/plugin-gitlab-ci@20.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.3-dev.0...@rxap/plugin-gitlab-ci@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2...@rxap/plugin-gitlab-ci@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.6...@rxap/plugin-gitlab-ci@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.5...@rxap/plugin-gitlab-ci@20.0.2-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.4...@rxap/plugin-gitlab-ci@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.3...@rxap/plugin-gitlab-ci@20.0.2-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.2...@rxap/plugin-gitlab-ci@20.0.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.1...@rxap/plugin-gitlab-ci@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.2-dev.0...@rxap/plugin-gitlab-ci@20.0.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1...@rxap/plugin-gitlab-ci@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.13...@rxap/plugin-gitlab-ci@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.12...@rxap/plugin-gitlab-ci@20.0.1-dev.13) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.11...@rxap/plugin-gitlab-ci@20.0.1-dev.12) (2025-02-11)

### Bug Fixes

- safe access project sourceRoot property ([16ca874](https://gitlab.com/rxap/packages/commit/16ca8747120876ad90e38c0cc012c175741fda0b))

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.10...@rxap/plugin-gitlab-ci@20.0.1-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.9...@rxap/plugin-gitlab-ci@20.0.1-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.8...@rxap/plugin-gitlab-ci@20.0.1-dev.9) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.7...@rxap/plugin-gitlab-ci@20.0.1-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.6...@rxap/plugin-gitlab-ci@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.5...@rxap/plugin-gitlab-ci@20.0.1-dev.6) (2025-01-29)

### Bug Fixes

- autodetect dockerfile ([51a0afb](https://gitlab.com/rxap/packages/commit/51a0afbe024c4373dcd7d5184ced9eec086a1769))

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.4...@rxap/plugin-gitlab-ci@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.3...@rxap/plugin-gitlab-ci@20.0.1-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.2...@rxap/plugin-gitlab-ci@20.0.1-dev.3) (2025-01-28)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.1...@rxap/plugin-gitlab-ci@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.1-dev.0...@rxap/plugin-gitlab-ci@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.0...@rxap/plugin-gitlab-ci@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.0-dev.3...@rxap/plugin-gitlab-ci@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.0-dev.2...@rxap/plugin-gitlab-ci@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@20.0.0-dev.1...@rxap/plugin-gitlab-ci@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.10-dev.0...@rxap/plugin-gitlab-ci@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.9...@rxap/plugin-gitlab-ci@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.9-dev.1...@rxap/plugin-gitlab-ci@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.9-dev.0...@rxap/plugin-gitlab-ci@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.8...@rxap/plugin-gitlab-ci@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.8-dev.3...@rxap/plugin-gitlab-ci@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.8-dev.2...@rxap/plugin-gitlab-ci@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.8-dev.1...@rxap/plugin-gitlab-ci@19.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.8-dev.0...@rxap/plugin-gitlab-ci@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.7...@rxap/plugin-gitlab-ci@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.7-dev.1...@rxap/plugin-gitlab-ci@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.7-dev.0...@rxap/plugin-gitlab-ci@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.6...@rxap/plugin-gitlab-ci@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.6-dev.1...@rxap/plugin-gitlab-ci@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.6-dev.0...@rxap/plugin-gitlab-ci@19.0.6-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5...@rxap/plugin-gitlab-ci@19.0.6-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.9...@rxap/plugin-gitlab-ci@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.8...@rxap/plugin-gitlab-ci@19.0.5-dev.9) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.7...@rxap/plugin-gitlab-ci@19.0.5-dev.8) (2024-07-18)

### Bug Fixes

- set default service path for nginx container ([b930a72](https://gitlab.com/rxap/packages/commit/b930a72b4dc0488bacad8cc85bff966a9156386a))

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.6...@rxap/plugin-gitlab-ci@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.5...@rxap/plugin-gitlab-ci@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.4...@rxap/plugin-gitlab-ci@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.3...@rxap/plugin-gitlab-ci@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.2...@rxap/plugin-gitlab-ci@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.1...@rxap/plugin-gitlab-ci@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.5-dev.0...@rxap/plugin-gitlab-ci@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.4...@rxap/plugin-gitlab-ci@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.4-dev.1...@rxap/plugin-gitlab-ci@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.4-dev.0...@rxap/plugin-gitlab-ci@19.0.4-dev.1) (2024-06-30)

### Bug Fixes

- add skip startup and e2e service flags ([225a720](https://gitlab.com/rxap/packages/commit/225a7204aeef9be68f61781443c03d32a1b986d2))

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3...@rxap/plugin-gitlab-ci@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3-dev.4...@rxap/plugin-gitlab-ci@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3-dev.3...@rxap/plugin-gitlab-ci@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3-dev.2...@rxap/plugin-gitlab-ci@19.0.3-dev.3) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3-dev.1...@rxap/plugin-gitlab-ci@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.3-dev.0...@rxap/plugin-gitlab-ci@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2...@rxap/plugin-gitlab-ci@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.5...@rxap/plugin-gitlab-ci@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.4...@rxap/plugin-gitlab-ci@19.0.2-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.3...@rxap/plugin-gitlab-ci@19.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.2...@rxap/plugin-gitlab-ci@19.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.1...@rxap/plugin-gitlab-ci@19.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.2-dev.0...@rxap/plugin-gitlab-ci@19.0.2-dev.1) (2024-06-17)

### Bug Fixes

- support local components ([36be7e8](https://gitlab.com/rxap/packages/commit/36be7e83d4026db7ccbfdf24ef0a4983c8a4dc5c))
- support local components ([322af03](https://gitlab.com/rxap/packages/commit/322af036e58ce81ad549190e0b147c0622cc3295))
- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.1...@rxap/plugin-gitlab-ci@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@19.0.1-dev.0...@rxap/plugin-gitlab-ci@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@18.0.1...@rxap/plugin-gitlab-ci@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@18.0.1-dev.0...@rxap/plugin-gitlab-ci@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@17.0.1...@rxap/plugin-gitlab-ci@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@17.0.1-dev.0...@rxap/plugin-gitlab-ci@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.1...@rxap/plugin-gitlab-ci@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [16.3.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.1-dev.1...@rxap/plugin-gitlab-ci@16.3.1) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [16.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.1-dev.0...@rxap/plugin-gitlab-ci@16.3.1-dev.1) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [16.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0...@rxap/plugin-gitlab-ci@16.3.1-dev.0) (2024-05-28)

### Bug Fixes

- respect the existing component version ([c0e21f3](https://gitlab.com/rxap/packages/commit/c0e21f3c3dbf8bfb1d39d411c71f42e26070e518))

# [16.3.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.12...@rxap/plugin-gitlab-ci@16.3.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [16.3.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.11...@rxap/plugin-gitlab-ci@16.3.0-dev.12) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [16.3.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.10...@rxap/plugin-gitlab-ci@16.3.0-dev.11) (2024-05-22)

### Features

- **gitlab-ci:** enhance docker generation with additional matrices ([54babc7](https://gitlab.com/rxap/packages/commit/54babc704039b0d1d602036afcd5152eaa64dd83))

# [16.3.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.9...@rxap/plugin-gitlab-ci@16.3.0-dev.10) (2024-05-22)

### Bug Fixes

- ensure the gitlab ci object is not null ([100aab6](https://gitlab.com/rxap/packages/commit/100aab6cd936dbed735e36e5a815f89cc8d3fa87))

# [16.3.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.8...@rxap/plugin-gitlab-ci@16.3.0-dev.9) (2024-05-22)

### Features

- introduce gitlab ci/cd components support ([6916f0b](https://gitlab.com/rxap/packages/commit/6916f0b975b0e6ae2eef8cf2ac5baa82635dc67f))
- support component includes ([41f3250](https://gitlab.com/rxap/packages/commit/41f3250fc9a560de1b4a5bfbad99457b5982078f))
- support component includes ([b7d7e6a](https://gitlab.com/rxap/packages/commit/b7d7e6a4760a72f51ddb8924186c54f9b4a270ca))

# [16.3.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.7...@rxap/plugin-gitlab-ci@16.3.0-dev.8) (2024-05-16)

### Bug Fixes

- generate a valid setup script ([bdee594](https://gitlab.com/rxap/packages/commit/bdee594f1aa8ff4b78c2b8e1fd605e93a674b0c8))

### Features

- add update helm chart version ci pipeline ([eccab03](https://gitlab.com/rxap/packages/commit/eccab0373ebd2e083734cd40feb65e968a72bc97))

# [16.3.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.6...@rxap/plugin-gitlab-ci@16.3.0-dev.7) (2024-05-16)

### Bug Fixes

- add e2e stage for service e2e tests ([40967c8](https://gitlab.com/rxap/packages/commit/40967c836b492ebe7e7564b547dff025dc4cad55))
- ensure startup jobs are executed on every default branch ([44256d7](https://gitlab.com/rxap/packages/commit/44256d7e1fa157f1ba878491314f5b10b71e6ab8))

# [16.3.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.5...@rxap/plugin-gitlab-ci@16.3.0-dev.6) (2024-05-16)

### Bug Fixes

- ensure the nest api prefix has a leading slash ([8a82afd](https://gitlab.com/rxap/packages/commit/8a82afd3f86e32767af4b56206415165c206cc41))

# [16.3.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.4...@rxap/plugin-gitlab-ci@16.3.0-dev.5) (2024-05-16)

### Bug Fixes

- add explicit channel definition ([cf2ce2d](https://gitlab.com/rxap/packages/commit/cf2ce2df579007bc201f89b0d520ab3f9e7ffba7))
- add nightly and edge channels ([a30c17b](https://gitlab.com/rxap/packages/commit/a30c17b9965d484d2f93fbbf4fa84d32561e187e))
- remove duplicated code ([e5f8443](https://gitlab.com/rxap/packages/commit/e5f8443f73517bf3ecde1d72dc3bbc3478b651e7))

# [16.3.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.3...@rxap/plugin-gitlab-ci@16.3.0-dev.4) (2024-05-07)

### Features

- generate gitlab setup script ([07ce907](https://gitlab.com/rxap/packages/commit/07ce907d2ea1812477cb3546f9bb6acb6dad83c4))

# [16.3.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.2...@rxap/plugin-gitlab-ci@16.3.0-dev.3) (2024-05-07)

### Features

- support service e2e targets ([422e764](https://gitlab.com/rxap/packages/commit/422e7646276b65b2dd70e2b1fd444ab8d743c072))

# [16.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.1...@rxap/plugin-gitlab-ci@16.3.0-dev.2) (2024-05-07)

### Bug Fixes

- fallback to default gitlab email and username ([bbc5e56](https://gitlab.com/rxap/packages/commit/bbc5e5607c28abfa19f09fa82abc3401fd508699))
- only update the build yaml if required ([0b586cb](https://gitlab.com/rxap/packages/commit/0b586cb75ad35711a1ee28797a21612ed7f971e1))

# [16.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.3.0-dev.0...@rxap/plugin-gitlab-ci@16.3.0-dev.1) (2024-05-04)

### Bug Fixes

- ensure the include rules are correctly defined ([6290bdb](https://gitlab.com/rxap/packages/commit/6290bdb5bb517fe6c33752a13b7671535a9535d9))
- update release it default configuration ([13c408d](https://gitlab.com/rxap/packages/commit/13c408de2ac403eace2e9c26a5dc19c2434f7f45))
- use correct file path for utilities base file ([8a662bd](https://gitlab.com/rxap/packages/commit/8a662bded4786b236e36b4309fe99460a557deee))

### Features

- support replacement of the projectName variable name ([06ed250](https://gitlab.com/rxap/packages/commit/06ed2508794db12fed5558a316ee5b8c862ed92b))

# [16.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.2-dev.0...@rxap/plugin-gitlab-ci@16.3.0-dev.0) (2024-05-03)

### Features

- support release it setup ([f63b1a8](https://gitlab.com/rxap/packages/commit/f63b1a8cb942d234edc58686d209eb5ce6ace925))

## [16.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1...@rxap/plugin-gitlab-ci@16.2.2-dev.0) (2024-05-02)

### Bug Fixes

- add setup job file ([be0ba61](https://gitlab.com/rxap/packages/commit/be0ba6191fb0dc78e8e74e3ffc940137bc0e8acb))
- improve script logging ([c18caa6](https://gitlab.com/rxap/packages/commit/c18caa6384c50012709512d5a592e45d7a56338b))
- install required package ([7730a91](https://gitlab.com/rxap/packages/commit/7730a91c75a45a6a43a1150da1508cad714860eb))

## [16.2.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1-dev.4...@rxap/plugin-gitlab-ci@16.2.1) (2024-04-18)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

## [16.2.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1-dev.3...@rxap/plugin-gitlab-ci@16.2.1-dev.4) (2024-04-17)

### Bug Fixes

- remove unused utility import ([db1a391](https://gitlab.com/rxap/packages/commit/db1a3916fa8f7b6fb74036b175aeeabb6f6caeb6))

## [16.2.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1-dev.2...@rxap/plugin-gitlab-ci@16.2.1-dev.3) (2024-04-17)

### Bug Fixes

- only add component test to angular applications ([24efc1e](https://gitlab.com/rxap/packages/commit/24efc1e2bcdf7b0ff2da6b6a2964fbd6bf6f674e))

## [16.2.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1-dev.1...@rxap/plugin-gitlab-ci@16.2.1-dev.2) (2024-04-17)

### Bug Fixes

- support specific angular workspaces ([b71d2b2](https://gitlab.com/rxap/packages/commit/b71d2b2ff8b978aa78ede3f8cbc49307bc49eaa9))

## [16.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.1-dev.0...@rxap/plugin-gitlab-ci@16.2.1-dev.1) (2024-04-17)

### Bug Fixes

- add default job config to retry on runner system failure ([b8c23b0](https://gitlab.com/rxap/packages/commit/b8c23b088662d314dd9de9d525f1ef413f33bb93))
- add npm cache base job ([f25f78f](https://gitlab.com/rxap/packages/commit/f25f78f7faaa05ae5bc82b51bddb7e03361d8f72))
- remove unused channel parameter ([93dc787](https://gitlab.com/rxap/packages/commit/93dc7871d5b9cf49a39a132892dcc454062ca346))

## [16.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0...@rxap/plugin-gitlab-ci@16.2.1-dev.0) (2024-04-17)

### Bug Fixes

- include all projects with a docker target ([b2aa2bf](https://gitlab.com/rxap/packages/commit/b2aa2bf4cabbaf021f5dcd6653e154606ae5178a))

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.9...@rxap/plugin-gitlab-ci@16.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [16.2.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.8...@rxap/plugin-gitlab-ci@16.2.0-dev.9) (2024-04-16)

### Bug Fixes

- use generic tag names ([8a0e96b](https://gitlab.com/rxap/packages/commit/8a0e96b0a70fb1594291355cff14f917e43446ca))
- use separate build pipelines ([c651e2c](https://gitlab.com/rxap/packages/commit/c651e2c0072766497552e833d549444f71bc2e12))

# [16.2.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.7...@rxap/plugin-gitlab-ci@16.2.0-dev.8) (2024-04-16)

### Bug Fixes

- include storybook and compodoc ([4e5dc21](https://gitlab.com/rxap/packages/commit/4e5dc21d2c28819cfa2af9b2053f091df6aed095))

# [16.2.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.6...@rxap/plugin-gitlab-ci@16.2.0-dev.7) (2024-04-12)

### Bug Fixes

- run docker and startup for renovate and snyk branches ([3ab5201](https://gitlab.com/rxap/packages/commit/3ab5201ce8a8664e0a23f06515e526f9722bf513))

# [16.2.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.5...@rxap/plugin-gitlab-ci@16.2.0-dev.6) (2024-04-12)

### Bug Fixes

- only build build arg mapping ([b36e859](https://gitlab.com/rxap/packages/commit/b36e859c715e9989e21a28ff3db6e286327943d8))

# [16.2.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.4...@rxap/plugin-gitlab-ci@16.2.0-dev.5) (2024-04-12)

### Bug Fixes

- use correct write side script ([43a40e5](https://gitlab.com/rxap/packages/commit/43a40e5263c5a23f382024ac014efda539d17daf))

# [16.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.3...@rxap/plugin-gitlab-ci@16.2.0-dev.4) (2024-04-12)

### Bug Fixes

- add job config ([cc97385](https://gitlab.com/rxap/packages/commit/cc973851473750a56fcb8a2f36401e443c524ab3))

# [16.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.2...@rxap/plugin-gitlab-ci@16.2.0-dev.3) (2024-04-12)

### Features

- support non application development workspaces ([d4bf3f5](https://gitlab.com/rxap/packages/commit/d4bf3f59169d47e44bc86b32afbc7395caf3599d))

# [16.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.1...@rxap/plugin-gitlab-ci@16.2.0-dev.2) (2024-04-10)

### Bug Fixes

- set the release build args ([64139ae](https://gitlab.com/rxap/packages/commit/64139ae4600b46a7d857c002a48bffa755f745ff))
- update default ci configuration ([5f06efb](https://gitlab.com/rxap/packages/commit/5f06efbbdd298ee0a7aa0d31aa830d8af8306437))

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

# [16.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.2.0-dev.0...@rxap/plugin-gitlab-ci@16.2.0-dev.1) (2024-04-10)

### Bug Fixes

- ensure git can always be installed ([af60415](https://gitlab.com/rxap/packages/commit/af6041513ef8e6fb493d802fb51b06cf91cea210))
- update tool scripts ([ae62df2](https://gitlab.com/rxap/packages/commit/ae62df2cc4fe79dc52732fb40df85c3bf4645726))

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.1.1-dev.0...@rxap/plugin-gitlab-ci@16.2.0-dev.0) (2024-04-10)

### Features

- add gitlab ci init generator ([d2a412d](https://gitlab.com/rxap/packages/commit/d2a412d1907921159467ba2c764327547db6534f))

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.1.0...@rxap/plugin-gitlab-ci@16.1.1-dev.0) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gitlab-ci@16.1.0-dev.0...@rxap/plugin-gitlab-ci@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-gitlab-ci

# 16.1.0-dev.0 (2024-02-05)

### Features

- add component-test generator ([9046e55](https://gitlab.com/rxap/packages/commit/9046e552b6d86a47f77933027fbc0d8058e13512))
