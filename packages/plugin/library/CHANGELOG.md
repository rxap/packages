# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.1-dev.0...@rxap/plugin-library@20.2.1-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-library

## [20.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0...@rxap/plugin-library@20.2.1-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-library

# [20.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0-dev.4...@rxap/plugin-library@20.2.0) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-library

# [20.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0-dev.3...@rxap/plugin-library@20.2.0-dev.4) (2025-02-28)

### Bug Fixes

- check if file exists ([1e84487](https://gitlab.com/rxap/packages/commit/1e84487cebf07b415b2b0a1b0ac499c347d5d0f9))
- **readme:** remove version specification from peer dependencies ([f1d1350](https://gitlab.com/rxap/packages/commit/f1d1350ec097b518814cb3bc9584194dd148d7c1))

# [20.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0-dev.2...@rxap/plugin-library@20.2.0-dev.3) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-library

# [20.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0-dev.1...@rxap/plugin-library@20.2.0-dev.2) (2025-02-25)

### Bug Fixes

- disable init generator creation by default ([9c4b325](https://gitlab.com/rxap/packages/commit/9c4b325c69bf66982eeb6e355ef7e750e25dfb58))

# [20.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.2.0-dev.0...@rxap/plugin-library@20.2.0-dev.1) (2025-02-25)

### Bug Fixes

- update package groups ([7060546](https://gitlab.com/rxap/packages/commit/7060546f6e8c0c7644bd18f5ad6b1ed62c672fc2))

# [20.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2...@rxap/plugin-library@20.2.0-dev.0) (2025-02-25)

### Bug Fixes

- check buildable project by tsconfig ([260be75](https://gitlab.com/rxap/packages/commit/260be754f55a446fd4df6a6f97d272d0e704ae19))

### Features

- disable init generator creation by default ([3117888](https://gitlab.com/rxap/packages/commit/3117888347e9e315042e7db00e43eef1065b2a0a))

## [20.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.6...@rxap/plugin-library@20.1.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.5...@rxap/plugin-library@20.1.2-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.4...@rxap/plugin-library@20.1.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.3...@rxap/plugin-library@20.1.2-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.1.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.2...@rxap/plugin-library@20.1.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.1...@rxap/plugin-library@20.1.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.2-dev.0...@rxap/plugin-library@20.1.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.1...@rxap/plugin-library@20.1.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-library

## [20.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0...@rxap/plugin-library@20.1.1) (2025-02-13)

### Bug Fixes

- disable readme generation cache ([ed22424](https://gitlab.com/rxap/packages/commit/ed22424053388f9af6208cd7a3c652337f5fcfcd))

# [20.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.13...@rxap/plugin-library@20.1.0) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.12...@rxap/plugin-library@20.1.0-dev.13) (2025-02-13)

### Bug Fixes

- resolve readme generator issue ([09a5223](https://gitlab.com/rxap/packages/commit/09a5223c144d729cbd2c9fc6f6e6d06fe4e36fc3))
- use correct generator base path ([4ab73ce](https://gitlab.com/rxap/packages/commit/4ab73ceed62ab05e899f04762f396731589bcecd))

# [20.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.11...@rxap/plugin-library@20.1.0-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.10...@rxap/plugin-library@20.1.0-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.9...@rxap/plugin-library@20.1.0-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.8...@rxap/plugin-library@20.1.0-dev.9) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.7...@rxap/plugin-library@20.1.0-dev.8) (2025-01-30)

### Bug Fixes

- missing xml2js dep ([1f11b99](https://gitlab.com/rxap/packages/commit/1f11b99cfc50654020eeaafec031314584299edb))

# [20.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.6...@rxap/plugin-library@20.1.0-dev.7) (2025-01-29)

### Bug Fixes

- add additionalEntryPoints option ([b333289](https://gitlab.com/rxap/packages/commit/b333289aec81b6419900dd8e2a364d925fc6fd35))

# [20.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.5...@rxap/plugin-library@20.1.0-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.4...@rxap/plugin-library@20.1.0-dev.5) (2025-01-29)

### Bug Fixes

- remove old defaults ([6815377](https://gitlab.com/rxap/packages/commit/6815377eea559e215dbf68db28c1963bd92bbca7))

# [20.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.3...@rxap/plugin-library@20.1.0-dev.4) (2025-01-29)

### Bug Fixes

- add jetbrains ignores ([0b59259](https://gitlab.com/rxap/packages/commit/0b5925911be0429e1634388816b91c0d419156ad))

# [20.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.2...@rxap/plugin-library@20.1.0-dev.3) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-library

# [20.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.1...@rxap/plugin-library@20.1.0-dev.2) (2025-01-28)

### Bug Fixes

- use correct workspace root check ([4d0c0ed](https://gitlab.com/rxap/packages/commit/4d0c0ed5e1f3bd9db2866162476a290028f70b33))

# [20.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.1.0-dev.0...@rxap/plugin-library@20.1.0-dev.1) (2025-01-28)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))

# [20.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.1-dev.2...@rxap/plugin-library@20.1.0-dev.0) (2025-01-28)

### Features

- add generate schema executor ([429c4f8](https://gitlab.com/rxap/packages/commit/429c4f8b9c57936a1307a2abf4a43795f32aa781))

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.1-dev.1...@rxap/plugin-library@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-library

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.1-dev.0...@rxap/plugin-library@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-library

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0...@rxap/plugin-library@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-library

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0-dev.5...@rxap/plugin-library@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-library

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0-dev.4...@rxap/plugin-library@20.0.0-dev.5) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-library

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0-dev.3...@rxap/plugin-library@20.0.0-dev.4) (2025-01-03)

### Bug Fixes

- add library plugin on workspace init ([cbd8fde](https://gitlab.com/rxap/packages/commit/cbd8fde53887c7a069fec1e7ef06770b2de653dd))

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0-dev.2...@rxap/plugin-library@20.0.0-dev.3) (2025-01-03)

### Bug Fixes

- improve cache detection ([ac3b8fe](https://gitlab.com/rxap/packages/commit/ac3b8fe591c09df1ca68dfe8475a9268b9380fad))

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@20.0.0-dev.1...@rxap/plugin-library@20.0.0-dev.2) (2025-01-03)

### Bug Fixes

- add default lint ignores ([5782709](https://gitlab.com/rxap/packages/commit/5782709a83542a77e18471a2d3d17ff5a9f65f66))

### Features

- add exclude folders to jetbrains config ([562cada](https://gitlab.com/rxap/packages/commit/562cadaa63964287987b34f804f1ce0aa211ffaf))
- add fix-dependencies executor ([0fd50fc](https://gitlab.com/rxap/packages/commit/0fd50fcbd6a8f8ffce5e179531e402de52f6a8b7))
- support nx plugins ([3ce5d42](https://gitlab.com/rxap/packages/commit/3ce5d428533386361226f95202e7b970021c1104))
- support nx plugins ([a28c12d](https://gitlab.com/rxap/packages/commit/a28c12d411a1ce331a9b25f201d8b4674ac84e88))
- support nx plugins ([959c478](https://gitlab.com/rxap/packages/commit/959c478fb89f5688a3d741823ea0a67878b1d4cb))
- support nx plugins ([a8a6b9d](https://gitlab.com/rxap/packages/commit/a8a6b9d084c4bc55b7fff4dbd533c3e9d727e106))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))
- support nx plugins ([d5e192a](https://gitlab.com/rxap/packages/commit/d5e192a0c8e2972ff36b6af8054eee81bf57f29c))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.5.0-dev.0...@rxap/plugin-library@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-library

# [19.5.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.5-dev.0...@rxap/plugin-library@19.5.0-dev.0) (2024-12-10)

### Features

- add index-export executor ([7be9c36](https://gitlab.com/rxap/packages/commit/7be9c36a4f8ac366268a52910e250c10993b88db))

## [19.4.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.4...@rxap/plugin-library@19.4.5-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.4-dev.1...@rxap/plugin-library@19.4.4) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.4-dev.0...@rxap/plugin-library@19.4.4-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3...@rxap/plugin-library@19.4.4-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.5...@rxap/plugin-library@19.4.3) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.4...@rxap/plugin-library@19.4.3-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.3...@rxap/plugin-library@19.4.3-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.2...@rxap/plugin-library@19.4.3-dev.3) (2024-10-22)

### Bug Fixes

- remove static package scope ([03c171f](https://gitlab.com/rxap/packages/commit/03c171f8fbdfba2525d6482520f8b0c1fc1def59))

## [19.4.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.1...@rxap/plugin-library@19.4.3-dev.2) (2024-10-04)

### Bug Fixes

- check if schema path is in project source root ([db05770](https://gitlab.com/rxap/packages/commit/db05770280f3d951e06236b798c7a72356f64187))

## [19.4.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.3-dev.0...@rxap/plugin-library@19.4.3-dev.1) (2024-10-04)

### Bug Fixes

- cleanup generated lib ([57137dd](https://gitlab.com/rxap/packages/commit/57137dd146a56844cdd62fa88dd6a5d9663ef638))

## [19.4.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.2...@rxap/plugin-library@19.4.3-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.2-dev.1...@rxap/plugin-library@19.4.2) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.2-dev.0...@rxap/plugin-library@19.4.2-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1...@rxap/plugin-library@19.4.2-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.5...@rxap/plugin-library@19.4.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.4...@rxap/plugin-library@19.4.1-dev.5) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.3...@rxap/plugin-library@19.4.1-dev.4) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.2...@rxap/plugin-library@19.4.1-dev.3) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.1...@rxap/plugin-library@19.4.1-dev.2) (2024-08-15)

### Bug Fixes

- update default nx targets ([43f6151](https://gitlab.com/rxap/packages/commit/43f6151292a10218735e5cf203ec3cb169211530))

## [19.4.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.1-dev.0...@rxap/plugin-library@19.4.1-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-library

## [19.4.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.0...@rxap/plugin-library@19.4.1-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-library

# [19.4.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.0-dev.3...@rxap/plugin-library@19.4.0) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-library

# [19.4.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.0-dev.2...@rxap/plugin-library@19.4.0-dev.3) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-library

# [19.4.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.0-dev.1...@rxap/plugin-library@19.4.0-dev.2) (2024-07-26)

### Features

- support force peer dependencies ([4bba38e](https://gitlab.com/rxap/packages/commit/4bba38efebab361fd84cb2ff31dac6b24b4353ec))

# [19.4.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.4.0-dev.0...@rxap/plugin-library@19.4.0-dev.1) (2024-07-25)

### Bug Fixes

- use correct assets pattern for ng-package assets ([262964d](https://gitlab.com/rxap/packages/commit/262964d0b207971792bcdb1a302239c8c9eaa421))
- use correct assets pattern for ng-package assets ([e0ffd08](https://gitlab.com/rxap/packages/commit/e0ffd0887b5a6aa4a82237499d1d19ebe20c48e0))

# [19.4.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.8...@rxap/plugin-library@19.4.0-dev.0) (2024-07-24)

### Features

- support auto version set ([602e045](https://gitlab.com/rxap/packages/commit/602e0454538a5179142b7abebe83d61d7461f61f))

## [19.3.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.7...@rxap/plugin-library@19.3.2-dev.8) (2024-07-22)

### Bug Fixes

- include sentry in package auto update ([0f65b81](https://gitlab.com/rxap/packages/commit/0f65b8172e28c5734b1a853f4ec3c9a33b2f4c63))

## [19.3.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.6...@rxap/plugin-library@19.3.2-dev.7) (2024-07-09)

### Bug Fixes

- use package version ([2ab086a](https://gitlab.com/rxap/packages/commit/2ab086a33cee0798489172ad625d2c9b99670b5c))

## [19.3.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.5...@rxap/plugin-library@19.3.2-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.4...@rxap/plugin-library@19.3.2-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.3...@rxap/plugin-library@19.3.2-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.2...@rxap/plugin-library@19.3.2-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.1...@rxap/plugin-library@19.3.2-dev.2) (2024-07-03)

### Bug Fixes

- add force dependencies options ([cc188c2](https://gitlab.com/rxap/packages/commit/cc188c2b549642007d31582e021a6cfee0d05a47))
- allow non peer dependencies ([c67292c](https://gitlab.com/rxap/packages/commit/c67292c7f5d6839e3d587e9f98eddad597a307bd))
- ensure the secondary entry point is checked ([722cbc3](https://gitlab.com/rxap/packages/commit/722cbc39e2ecc1c429ecbc7c7a426c07e5a33d73))
- include executors ([a962d21](https://gitlab.com/rxap/packages/commit/a962d215411bbad404e5435ae11f64cba184b878))
- support migrations and generators ([a52ba38](https://gitlab.com/rxap/packages/commit/a52ba3808eb926b02309ff5301548a457ac3802b))
- update ng package json for angular projects ([a059bef](https://gitlab.com/rxap/packages/commit/a059bef26b1af1d5601ff41c63b16959bd940cdb))

## [19.3.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.2-dev.0...@rxap/plugin-library@19.3.2-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.1...@rxap/plugin-library@19.3.2-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.1-dev.0...@rxap/plugin-library@19.3.1) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-library

## [19.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0...@rxap/plugin-library@19.3.1-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.6...@rxap/plugin-library@19.3.0) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.5...@rxap/plugin-library@19.3.0-dev.6) (2024-06-27)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.4...@rxap/plugin-library@19.3.0-dev.5) (2024-06-25)

### Bug Fixes

- overwrite the index-export default configurations ([0da34ea](https://gitlab.com/rxap/packages/commit/0da34eadb9d916d624608a9c6bcc0fa528e456ec))

# [19.3.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.3...@rxap/plugin-library@19.3.0-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.2...@rxap/plugin-library@19.3.0-dev.3) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.1...@rxap/plugin-library@19.3.0-dev.2) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-library

# [19.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.3.0-dev.0...@rxap/plugin-library@19.3.0-dev.1) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

# [19.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.1-dev.1...@rxap/plugin-library@19.3.0-dev.0) (2024-06-21)

### Bug Fixes

- add @nx/devkit as allowedNonPeerDependencies ([7d0aede](https://gitlab.com/rxap/packages/commit/7d0aede55c99ad770efd13ed49b2400762e7e635))
- catch generator error ([adefd88](https://gitlab.com/rxap/packages/commit/adefd8873a2649c605625eb1415f65d5081267a6))
- expose generator ([70eec21](https://gitlab.com/rxap/packages/commit/70eec215c1a8f6e670026b0532d5ef2ee6063eaa))
- support secondary entry points ([c1463e6](https://gitlab.com/rxap/packages/commit/c1463e6c1daa61fa8d370e7be456ac4e490df376))
- update root package json ([052235d](https://gitlab.com/rxap/packages/commit/052235d3ce0f35d96bc1d08d3e152475b4eb6435))

### Features

- add CoerceInitGenerator function ([573e842](https://gitlab.com/rxap/packages/commit/573e8423536042f210c401a687bffefc0678da44))
- add CoerceInitGenerator function ([c3fd633](https://gitlab.com/rxap/packages/commit/c3fd6332fe0baf13038ac9e6a436f0c233699a1d))
- add run once guard ([92136b9](https://gitlab.com/rxap/packages/commit/92136b9a0bfac5f78ed5217a724576542d01b867))

## [19.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.1-dev.0...@rxap/plugin-library@19.2.1-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-library

## [19.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0...@rxap/plugin-library@19.2.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-library

# [19.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.10...@rxap/plugin-library@19.2.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-library

# [19.2.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.9...@rxap/plugin-library@19.2.0-dev.10) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-library

# [19.2.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.8...@rxap/plugin-library@19.2.0-dev.9) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-library

# [19.2.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.7...@rxap/plugin-library@19.2.0-dev.8) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-library

# [19.2.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.6...@rxap/plugin-library@19.2.0-dev.7) (2024-06-17)

### Bug Fixes

- ensure all new publishable libraries are added to the rxap project ([ee25608](https://gitlab.com/rxap/packages/commit/ee25608ffbd869a187739ac31b31a216308e5e91))

### Features

- add include dependencies option ([c5fcf7c](https://gitlab.com/rxap/packages/commit/c5fcf7cf8ed4d3f72c6e45496b9cd849208fa5ba))

# [19.2.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.5...@rxap/plugin-library@19.2.0-dev.6) (2024-06-17)

### Bug Fixes

- ensure the package version is only updated if required ([b1a9cd3](https://gitlab.com/rxap/packages/commit/b1a9cd33ba603401361dacc100ef6ff14317a6f1))

# [19.2.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.4...@rxap/plugin-library@19.2.0-dev.5) (2024-06-17)

### Bug Fixes

- always add the expose-as-schematic target to plugin projects ([fb8e9f0](https://gitlab.com/rxap/packages/commit/fb8e9f0771dcb4cfdc25334775ddcedba0a3e4bc))
- ensure standalone packages only have dependencies ([877a4ca](https://gitlab.com/rxap/packages/commit/877a4ca54a2cb211089a1bdec05246b36873fbc4))
- only add dependencies to preset and plugin projects ([6e779b4](https://gitlab.com/rxap/packages/commit/6e779b46a34b329e4721af7bac529f1f801bb17a))
- only include direct package peer dependencies ([dd0e600](https://gitlab.com/rxap/packages/commit/dd0e60025c26e6ce26637b1e10ec8d09fccc03f3))
- resolve package group based on peer dependencies ([6f76b16](https://gitlab.com/rxap/packages/commit/6f76b163d1ae3f2a9ec4949401cdff2f23fcd7a1))
- simplify index-export target adding ([8241689](https://gitlab.com/rxap/packages/commit/824168987779b6c260d16c950895ef1b3514b44f))

### Features

- add init preset generator ([1ebe1f8](https://gitlab.com/rxap/packages/commit/1ebe1f839a4b0620efcb0d5b46b09182d07d7893))
- add init schematic generator ([121026d](https://gitlab.com/rxap/packages/commit/121026db02845c41b95c3ba405f897c02c7cca9c))
- add onlyDependencies option ([459144b](https://gitlab.com/rxap/packages/commit/459144b1b66a1744994e97e42929874dc1c33415))

# [19.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.3...@rxap/plugin-library@19.2.0-dev.4) (2024-06-17)

### Bug Fixes

- delete target ([2a08aa2](https://gitlab.com/rxap/packages/commit/2a08aa23b8a7fdb4fc6fe7d72a5fd51b1d98672a))
- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

# [19.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.2...@rxap/plugin-library@19.2.0-dev.3) (2024-06-06)

### Bug Fixes

- check in any file ([84b98e3](https://gitlab.com/rxap/packages/commit/84b98e39c4273d6146b1be8f777b2a50e0877d15))

# [19.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.1...@rxap/plugin-library@19.2.0-dev.2) (2024-06-05)

### Bug Fixes

- only set the tslib version initial ([964b896](https://gitlab.com/rxap/packages/commit/964b896375d547c9fd98752faeb3052a5ef0b2de))

# [19.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.2.0-dev.0...@rxap/plugin-library@19.2.0-dev.1) (2024-06-04)

### Features

- add init feature library generator ([f91f4e3](https://gitlab.com/rxap/packages/commit/f91f4e36f033be89f64c2971cb550fa54ffcdf5e))

# [19.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.1.0...@rxap/plugin-library@19.2.0-dev.0) (2024-06-02)

### Features

- add target option ([b40711f](https://gitlab.com/rxap/packages/commit/b40711f1473df09ec8e4afc49e0c8366824d565f))

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.1.0-dev.1...@rxap/plugin-library@19.1.0) (2024-06-02)

**Note:** Version bump only for package @rxap/plugin-library

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.0.1...@rxap/plugin-library@19.1.0-dev.1) (2024-06-02)

### Features

- add generator to add a migration to a library ([2f34670](https://gitlab.com/rxap/packages/commit/2f346707a9ff924189fe494e581e4e4914035518))
- add generator to init a library with migrations ([13be015](https://gitlab.com/rxap/packages/commit/13be015dd671c7b2d68776eef3257e35c5d97a52))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.0.1...@rxap/plugin-library@19.1.0-dev.0) (2024-05-31)

### Features

- add generator to add a migration to a library ([368f672](https://gitlab.com/rxap/packages/commit/368f67284f97649b20be6f27e3476a49c6c9e973))
- add generator to init a library with migrations ([ca4ad16](https://gitlab.com/rxap/packages/commit/ca4ad16d4a674f89f40924979a70004867b9a7df))

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@19.0.1-dev.0...@rxap/plugin-library@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-library

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@18.0.1...@rxap/plugin-library@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-library

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@18.0.1-dev.0...@rxap/plugin-library@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-library

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@17.0.1...@rxap/plugin-library@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-library

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@17.0.1-dev.0...@rxap/plugin-library@17.0.1) (2024-05-29)

### Bug Fixes

- support async package json reading ([6b8a163](https://gitlab.com/rxap/packages/commit/6b8a16323b25108a81291ccf976ee7c2aaedacc3))

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.3.1...@rxap/plugin-library@17.0.1-dev.0) (2024-05-29)

### Bug Fixes

- add [@nestjs](https://gitlab.com/nestjs) to default package groups ([17da65d](https://gitlab.com/rxap/packages/commit/17da65dcb8e51004b30320e448daf146fba47101))
- wait for json file to be ready ([496a316](https://gitlab.com/rxap/packages/commit/496a31651dcc923a51684b33d5c4599de37ccd7b))

## [16.3.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.3.1-dev.1...@rxap/plugin-library@16.3.1) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-library

## [16.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.3.1-dev.0...@rxap/plugin-library@16.3.1-dev.1) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-library

## [16.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.3.0...@rxap/plugin-library@16.3.1-dev.0) (2024-05-28)

### Bug Fixes

- remove migrate branch exemption ([a36ccb1](https://gitlab.com/rxap/packages/commit/a36ccb13f08f4d84b12cd1cd7775159036ab8629))

# [16.3.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.3.0-dev.0...@rxap/plugin-library@16.3.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-library

# [16.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.2.0...@rxap/plugin-library@16.3.0-dev.0) (2024-05-27)

### Features

- add include option ([ad5ad54](https://gitlab.com/rxap/packages/commit/ad5ad5406bb2797f9bcfb9d742b1a7c53f7c349a))

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.2.0-dev.1...@rxap/plugin-library@16.2.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-library

# [16.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.2.0-dev.0...@rxap/plugin-library@16.2.0-dev.1) (2024-05-27)

### Bug Fixes

- skip version check in migration branches ([7a0047f](https://gitlab.com/rxap/packages/commit/7a0047f627461179102e19b1ef2c53fa2eb6c8a6))

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0...@rxap/plugin-library@16.2.0-dev.0) (2024-05-27)

### Features

- **plugin-library:** enhance update-package-group executor functionality ([38f76c6](https://gitlab.com/rxap/packages/commit/38f76c6e3c628711a7617484925d024a445b840d))

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.12...@rxap/plugin-library@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-library

# [16.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.11...@rxap/plugin-library@16.1.0-dev.12) (2024-04-10)

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

# [16.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.10...@rxap/plugin-library@16.1.0-dev.11) (2024-04-09)

### Features

- serialize the generator options ([e781286](https://gitlab.com/rxap/packages/commit/e78128606eaac8a0dd7168f568e9aa7a3fa710e8))

# [16.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.9...@rxap/plugin-library@16.1.0-dev.10) (2024-04-07)

### Bug Fixes

- only set license if not defined ([bdfd74b](https://gitlab.com/rxap/packages/commit/bdfd74b6f00a794687782d6bc6d1d0e46cdf75d3))

### Features

- add index-export option ([801b4a4](https://gitlab.com/rxap/packages/commit/801b4a42034defa9bfe00683c71a04e859063ad0))

# [16.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.8...@rxap/plugin-library@16.1.0-dev.9) (2024-03-31)

**Note:** Version bump only for package @rxap/plugin-library

# [16.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.7...@rxap/plugin-library@16.1.0-dev.8) (2024-03-15)

**Note:** Version bump only for package @rxap/plugin-library

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.6...@rxap/plugin-library@16.1.0-dev.7) (2024-03-15)

### Features

- exclude marked files from index export ([51f2afc](https://gitlab.com/rxap/packages/commit/51f2afc3f30c87e391d7e6fcfc430db411da9d21))

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.5...@rxap/plugin-library@16.1.0-dev.6) (2024-03-14)

### Bug Fixes

- add missing nx json write ([4de8ffc](https://gitlab.com/rxap/packages/commit/4de8ffc544f169c0a08a087d4b4db207053f7e07))

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.4...@rxap/plugin-library@16.1.0-dev.5) (2024-03-11)

### Bug Fixes

- link dependent project on the fly ([ffb247e](https://gitlab.com/rxap/packages/commit/ffb247eada915b9e547fd63a323fcb9ded9f239c))
- resolve inverse dependency ([3e06851](https://gitlab.com/rxap/packages/commit/3e06851cd1432f596a4ecf6f34d077eb76d225dc))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.3...@rxap/plugin-library@16.1.0-dev.4) (2024-03-09)

### Bug Fixes

- support complex data properties ([3405885](https://gitlab.com/rxap/packages/commit/34058851b85062956effea40aec201e4581d9780))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.2...@rxap/plugin-library@16.1.0-dev.3) (2024-03-05)

### Bug Fixes

- add dependent linking targets to task queue ([91f7e18](https://gitlab.com/rxap/packages/commit/91f7e18ad41690a9a63f041726a0a01aea038c4c))
- link packages into the dist folder ([a28b2d2](https://gitlab.com/rxap/packages/commit/a28b2d29951ca6ce204baf9d9316925c70437bea))

### Features

- add node-modules-linking executor ([88f3d9c](https://gitlab.com/rxap/packages/commit/88f3d9c1e6259e3620eaf5c9e3e449219afe1cc9))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.1...@rxap/plugin-library@16.1.0-dev.2) (2024-03-04)

### Bug Fixes

- generate valid schema ([aff3e0e](https://gitlab.com/rxap/packages/commit/aff3e0e0e340f1f2fbb6d08483a6199a1cabeb32))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.1.0-dev.0...@rxap/plugin-library@16.1.0-dev.1) (2024-02-28)

### Features

- add bundle json schema ([07ff6b5](https://gitlab.com/rxap/packages/commit/07ff6b5ee90aa20912ece6ae4237faa037fe5088))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.1-dev.0...@rxap/plugin-library@16.1.0-dev.0) (2024-02-25)

### Bug Fixes

- add nested definition cleanup ([f9c5637](https://gitlab.com/rxap/packages/commit/f9c5637fdba8ba458443c923c584564761e077ca))
- cleanup bundled schema ([4af344f](https://gitlab.com/rxap/packages/commit/4af344fa89837f228f04f96166f5b21932b50018))

### Features

- add bundle-json-schema generator ([8c4b9d9](https://gitlab.com/rxap/packages/commit/8c4b9d93730290c9b40ecf20490e87de34e6b1d2))

## [16.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0...@rxap/plugin-library@16.0.1-dev.0) (2024-02-09)

### Bug Fixes

- remove FlexLayout dependency ([1fea895](https://gitlab.com/rxap/packages/commit/1fea895ea326d64e3ca230386175d1cd71d25ace))

# [16.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.43...@rxap/plugin-library@16.0.0) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.43](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.42...@rxap/plugin-library@16.0.0-dev.43) (2024-02-05)

### Bug Fixes

- ensure the packageJson name is correct ([37dee1a](https://gitlab.com/rxap/packages/commit/37dee1a28c650673d980a1a37d6b82b117ac7933))
- ensure the packageJson version is correct ([635ff0e](https://gitlab.com/rxap/packages/commit/635ff0e68d571943e0e6ee40ec72627853734628))

# [16.0.0-dev.42](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.41...@rxap/plugin-library@16.0.0-dev.42) (2023-11-16)

### Bug Fixes

- support custom inline options ([468985a](https://gitlab.com/rxap/packages/commit/468985a004993bb4dcbf6dd6a55aae973c509935))

# [16.0.0-dev.41](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.40...@rxap/plugin-library@16.0.0-dev.41) (2023-10-16)

### Bug Fixes

- use utility CreateProject function to create a ts-morph Project instance ([78b308f](https://gitlab.com/rxap/packages/commit/78b308fd10747616c7c7f27e81501a4ad5052a77))

# [16.0.0-dev.40](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.39...@rxap/plugin-library@16.0.0-dev.40) (2023-10-12)

### Bug Fixes

- remove package json update targets from cacheable operations ([6dd372d](https://gitlab.com/rxap/packages/commit/6dd372d0ed9a182d5b99d413a35c1dc29513fc77))

# [16.0.0-dev.39](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.38...@rxap/plugin-library@16.0.0-dev.39) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.38](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.37...@rxap/plugin-library@16.0.0-dev.38) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.37](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.36...@rxap/plugin-library@16.0.0-dev.37) (2023-10-11)

### Bug Fixes

- ensure overwrite option is respected ([672c315](https://gitlab.com/rxap/packages/commit/672c3152403ae577fb8d4d34e060971d2e53d0af))

# [16.0.0-dev.36](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.7...@rxap/plugin-library@16.0.0-dev.36) (2023-10-11)

### Bug Fixes

- add expose-as-schematic to plugin projects with generators ([32b28ae](https://gitlab.com/rxap/packages/commit/32b28ae005e3e73d96f0ae77b457b35f1a5721f9))
- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add missing regex flags ([54fad73](https://gitlab.com/rxap/packages/commit/54fad735b55107c04ba45ae9511bf52c4a309cec))
- add skip-projects flag ([e1f31ed](https://gitlab.com/rxap/packages/commit/e1f31ed837646f605ced82a52749e62af07ba939))
- add the index-export target to all non plugin/schematic/generator project targets ([6d1c533](https://gitlab.com/rxap/packages/commit/6d1c533aee590907e23160d4d54ad04c4c13e4b2))
- array parameters escape ([72925cd](https://gitlab.com/rxap/packages/commit/72925cd4b08344385cab3210221b7b3af43e187d))
- call the schematic utility script if rxap repository ([da3dc46](https://gitlab.com/rxap/packages/commit/da3dc46f3193babb04a90e8c4a5a538f2e9ea6e3))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- check if homepage is set prevent ref by null ([1b2898e](https://gitlab.com/rxap/packages/commit/1b2898e54b403c9a250129f18e9084bc3f5e6b62))
- check if the LICENSE file exists before access the file ([f14b7e3](https://gitlab.com/rxap/packages/commit/f14b7e3d55e153b5cc2b960a0ee3f5585a96ae9f))
- **check-version:** exclude pre release suffix in comparison ([083f96f](https://gitlab.com/rxap/packages/commit/083f96fe71601da7fcededf378fbaf0fd667e1cc))
- cleanup target version ([6627983](https://gitlab.com/rxap/packages/commit/6627983dbe314fd49484b925515bbb09aa1fd606))
- coerce the index-export default target options ([fc9d188](https://gitlab.com/rxap/packages/commit/fc9d188dd16a910ce2d503b6501bbe3ec186cbf5))
- create publish directory with relative output path ([08884cd](https://gitlab.com/rxap/packages/commit/08884cd41483fc466db7e6f934a233f0ea0c654d))
- enforce that the production configuration is the default configuration ([00ac30e](https://gitlab.com/rxap/packages/commit/00ac30e65dbe1008bff6d4f149631405fc81c200))
- ensure all required cacheable operations are defined ([d9ded9c](https://gitlab.com/rxap/packages/commit/d9ded9c5e150d9781ce490ad7ac292194d09bf2a))
- ensure fix-dependencies for dependencies is run first ([6560021](https://gitlab.com/rxap/packages/commit/6560021bc93d8657e97b363fbee20a0ca64df7b3))
- ensure index-export is run before build target ([5ea6ba8](https://gitlab.com/rxap/packages/commit/5ea6ba85add4ce96af04b3f5da1e8e6e25cdbbb6))
- ensure new line ([5faca82](https://gitlab.com/rxap/packages/commit/5faca824bd3eb43e61ed06ff004bb6b2f15669e0))
- ensure overwrite option is passed to sub schematics ([0c8a19b](https://gitlab.com/rxap/packages/commit/0c8a19b5166f804aa335f739a00a5415bd97f61a))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- exclude .cy.ts files ([b6a0989](https://gitlab.com/rxap/packages/commit/b6a09891e5178c306824bb9671125604625b29f9))
- expose generators as schematics ([8a58d07](https://gitlab.com/rxap/packages/commit/8a58d07c2f1dcfff75e724a418d7c3bddb2d0bbc))
- generate index file for non angular projects ([00ee701](https://gitlab.com/rxap/packages/commit/00ee701811805a38ea6a71284a94b39b02e8a3a4))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- handle readme generator execution errors ([7c2762d](https://gitlab.com/rxap/packages/commit/7c2762da753edd28210e3e35bef5048964f5beea))
- ignore express and axios packages ([f159354](https://gitlab.com/rxap/packages/commit/f159354d89a2f28d6adefdeab59e2e592c612204))
- ignore projects that have no package.json ([427f3f3](https://gitlab.com/rxap/packages/commit/427f3f3f6933dab1ca211702149f2746f0ac6fe3))
- **init:** use init plugin generator for plugin projects ([e9417c2](https://gitlab.com/rxap/packages/commit/e9417c2b500a08e65f34860d42379c64a16545b9))
- introduce Is\*Project functions ([3c9f251](https://gitlab.com/rxap/packages/commit/3c9f251f1d7be46ca366171e79e86ef2764fa3b0))
- only include file if has export statement ([bf33058](https://gitlab.com/rxap/packages/commit/bf33058e3bcf9ff9479cd3db333d8703157b3bf9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- remove nx dependency ([b2b98b0](https://gitlab.com/rxap/packages/commit/b2b98b01438e9439f9743fb27629c7e96072df45))
- support schema with $ref and allOf properties ([486ed06](https://gitlab.com/rxap/packages/commit/486ed06fb3f9aa33b68a74e024e449e628afd585))
- update default root project name ([71908f4](https://gitlab.com/rxap/packages/commit/71908f43258a3cb1aa0c7cbf1fbf17c5a544a57b))
- use absolute path to access files ([063676e](https://gitlab.com/rxap/packages/commit/063676e3a1f6061c9f3284f79e6ca8091242c0c7))
- use UpdatePackageJson utility function ([1b00ccf](https://gitlab.com/rxap/packages/commit/1b00ccf4608cf5b56b8075497e7a89bdb98fcb59))
- use utility function to set targets and target defaults ([00b5e6a](https://gitlab.com/rxap/packages/commit/00b5e6acd086c72ea99272ed6bb3fd094d7bb654))

### Features

- add init-buildable and init-publishable generator ([90c6f9a](https://gitlab.com/rxap/packages/commit/90c6f9a4d238539ded4e3ed4007793ef835127ab))
- add project to package name mapping utilities ([a0760db](https://gitlab.com/rxap/packages/commit/a0760db47705928ca94803bb6868b9310a6a5b5f))
- **executor:** add check version ([22de28a](https://gitlab.com/rxap/packages/commit/22de28a9802b2d21469723cfdf18ae4e04df718e))
- **generator:** add init-plugin ([c381500](https://gitlab.com/rxap/packages/commit/c38150066b5a63ba70371913d7f729637c19ce80))
- **init:** execute nest init generator if project is for nestjs ([809d428](https://gitlab.com/rxap/packages/commit/809d4280f83bb3127eca044b0e064e5c66ea221b))
- support angular library entrypoints ([f74241d](https://gitlab.com/rxap/packages/commit/f74241de483eaed44241a8e4e2a6268036317851))
- support string interpolation ([3ebbdd9](https://gitlab.com/rxap/packages/commit/3ebbdd98ec87eee7e7fe6af68fd287ad083619cb))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [16.0.0-dev.35](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.34...@rxap/plugin-library@16.0.0-dev.35) (2023-10-09)

### Bug Fixes

- add missing regex flags ([da557ea](https://gitlab.com/rxap/packages/commit/da557eaf8edea23963fb5aa5e9f5b54baddef41c))

# [16.0.0-dev.34](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.33...@rxap/plugin-library@16.0.0-dev.34) (2023-10-09)

### Bug Fixes

- only include file if has export statement ([acab338](https://gitlab.com/rxap/packages/commit/acab3385744d9bfbf4c6ce1e1423c8ef8f87c46a))

# [16.0.0-dev.33](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.32...@rxap/plugin-library@16.0.0-dev.33) (2023-10-08)

### Bug Fixes

- ensure index-export is run before build target ([5008aeb](https://gitlab.com/rxap/packages/commit/5008aeb3847cb3ff2b248c83685153abc4a4ffcb))

# [16.0.0-dev.32](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.31...@rxap/plugin-library@16.0.0-dev.32) (2023-10-03)

### Bug Fixes

- ensure all required cacheable operations are defined ([49a9199](https://gitlab.com/rxap/packages/commit/49a9199cd2592cf8550650dc17f9995e4f6727f8))

# [16.0.0-dev.31](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.30...@rxap/plugin-library@16.0.0-dev.31) (2023-10-02)

### Bug Fixes

- cleanup target version ([06558eb](https://gitlab.com/rxap/packages/commit/06558eb200e620dfb6ea217885520c6561cd1c25))
- coerce the index-export default target options ([1de131c](https://gitlab.com/rxap/packages/commit/1de131c42dfa133dc448d0d1a300d49e1ae9b7cc))

# [16.0.0-dev.30](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.29...@rxap/plugin-library@16.0.0-dev.30) (2023-10-02)

### Bug Fixes

- add the index-export target to all non plugin/schematic/generator project targets ([efaac2e](https://gitlab.com/rxap/packages/commit/efaac2e7aca03589c1b80559b51eb08297f6e933))
- check if the LICENSE file exists before access the file ([de5bb8e](https://gitlab.com/rxap/packages/commit/de5bb8ec6ca66c4bfc663152f26b98b77273d30a))
- introduce Is\*Project functions ([0f4a53a](https://gitlab.com/rxap/packages/commit/0f4a53a2a68c7f854d819c005a30957d8b1cb3c6))

# [16.0.0-dev.29](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.28...@rxap/plugin-library@16.0.0-dev.29) (2023-10-02)

### Bug Fixes

- handle readme generator execution errors ([a5c5204](https://gitlab.com/rxap/packages/commit/a5c5204b6e2bcd39db3d868e1bfa5955b0570dbf))
- support schema with $ref and allOf properties ([c8a1640](https://gitlab.com/rxap/packages/commit/c8a164039443b37e2c0a6b2d1dceb1bfa6312b97))

# [16.0.0-dev.28](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.27...@rxap/plugin-library@16.0.0-dev.28) (2023-09-21)

### Bug Fixes

- add skip-projects flag ([0f45987](https://gitlab.com/rxap/packages/commit/0f45987bc9dd927b1ede9eb53256125fa0e33674))

# [16.0.0-dev.27](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.26...@rxap/plugin-library@16.0.0-dev.27) (2023-09-19)

### Bug Fixes

- update default root project name ([85e724a](https://gitlab.com/rxap/packages/commit/85e724a7f08bcffdd4637311fe9560a674672a8f))

# [16.0.0-dev.26](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.25...@rxap/plugin-library@16.0.0-dev.26) (2023-09-13)

### Bug Fixes

- array parameters escape ([4fd79fe](https://gitlab.com/rxap/packages/commit/4fd79feeafdfece921f6ad9e32269cca882d04f0))

# [16.0.0-dev.25](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.24...@rxap/plugin-library@16.0.0-dev.25) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [16.0.0-dev.24](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.23...@rxap/plugin-library@16.0.0-dev.24) (2023-09-12)

### Bug Fixes

- use UpdatePackageJson utility function ([6e0e735](https://gitlab.com/rxap/packages/commit/6e0e735797462edd447bc1c78c804829c6f3c16f))

# [16.0.0-dev.23](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.22...@rxap/plugin-library@16.0.0-dev.23) (2023-09-07)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.22](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.21...@rxap/plugin-library@16.0.0-dev.22) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.20...@rxap/plugin-library@16.0.0-dev.21) (2023-09-03)

### Features

- support string interpolation ([dfcdc79](https://gitlab.com/rxap/packages/commit/dfcdc798f8856dae07b2b7704c60dd145d44a65a))

# [16.0.0-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.19...@rxap/plugin-library@16.0.0-dev.20) (2023-09-02)

### Bug Fixes

- generate index file for non angular projects ([5d88306](https://gitlab.com/rxap/packages/commit/5d8830611d751c3f17eb18a2e5db9aa80e3d2312))

### Features

- support angular library entrypoints ([7718326](https://gitlab.com/rxap/packages/commit/7718326464e1971d6b9463a6ae611a279ba4c1a7))

# [16.0.0-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.18...@rxap/plugin-library@16.0.0-dev.19) (2023-08-31)

### Bug Fixes

- ensure overwrite option is passed to sub schematics ([8472aab](https://gitlab.com/rxap/packages/commit/8472aab8814227c851fab9ae4c1b9ec3019d6f4e))
- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [16.0.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.17...@rxap/plugin-library@16.0.0-dev.18) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [16.0.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.16...@rxap/plugin-library@16.0.0-dev.17) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [16.0.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.15...@rxap/plugin-library@16.0.0-dev.16) (2023-08-14)

### Bug Fixes

- exclude .cy.ts files ([bbaf974](https://gitlab.com/rxap/packages/commit/bbaf97402207b09744fae8b9fb9c5c88f3cf4759))

# [16.0.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.14...@rxap/plugin-library@16.0.0-dev.15) (2023-08-06)

### Bug Fixes

- add expose-as-schematic to plugin projects with generators ([6c9fac8](https://gitlab.com/rxap/packages/commit/6c9fac892950e2942e7994673c35987b9b1b6fe7))
- ensure new line ([2db00f1](https://gitlab.com/rxap/packages/commit/2db00f115cf59e0e89fa53c70426b3e7cb829461))
- expose generators as schematics ([679ca36](https://gitlab.com/rxap/packages/commit/679ca36d3712a11e4dc838762bca2f7c471e1e04))

# [16.0.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.13...@rxap/plugin-library@16.0.0-dev.14) (2023-08-05)

### Bug Fixes

- check if homepage is set prevent ref by null ([6e86439](https://gitlab.com/rxap/packages/commit/6e8643966ff79d3f329a9a2e73b39a7e27c55849))

# [16.0.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.12...@rxap/plugin-library@16.0.0-dev.13) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- call the schematic utility script if rxap repository ([6497a76](https://gitlab.com/rxap/packages/commit/6497a7680503cf0aab38e5de7db813c0733d191c))
- enforce that the production configuration is the default configuration ([6e9c3b7](https://gitlab.com/rxap/packages/commit/6e9c3b7a58e92bcb5a1b9b772a34153b44acc8f9))

# [16.0.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.11...@rxap/plugin-library@16.0.0-dev.12) (2023-08-04)

### Bug Fixes

- remove nx dependency ([5cc2200](https://gitlab.com/rxap/packages/commit/5cc2200ca3f12ef39bb959f98730975978b5194e))

### Features

- add init-buildable and init-publishable generator ([b4ca2ba](https://gitlab.com/rxap/packages/commit/b4ca2ba9ff370be4e9c6d0948f8c62635c8ffac3))

# [16.0.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.10...@rxap/plugin-library@16.0.0-dev.11) (2023-08-03)

### Bug Fixes

- ensure fix-dependencies for dependencies is run first ([c01d930](https://gitlab.com/rxap/packages/commit/c01d93095a05877094e701ac385ec7521335a6a4))
- use absolute path to access files ([32323ab](https://gitlab.com/rxap/packages/commit/32323ab900da408dcd6ae05dc12e562feff798f9))
- use utility function to set targets and target defaults ([5914d7e](https://gitlab.com/rxap/packages/commit/5914d7efae28b891044da79f02f077d7b2398d2b))

# [16.0.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.9...@rxap/plugin-library@16.0.0-dev.10) (2023-08-03)

### Features

- add project to package name mapping utilities ([86ce9ab](https://gitlab.com/rxap/packages/commit/86ce9abfff3cba758c1f12667002953e5f5f4464))

# [16.0.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.8...@rxap/plugin-library@16.0.0-dev.9) (2023-08-01)

### Bug Fixes

- create publish directory with relative output path ([55f5755](https://gitlab.com/rxap/packages/commit/55f575535c58436f8625c148da9d9ec3c25f451d))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

# 16.0.0-dev.8 (2023-08-01)

### Bug Fixes

- add glob and fx-extra to package blacklist ([ac94d83](https://gitlab.com/rxap/packages/commit/ac94d83a52efd694fff36afafaadd824df54c525))
- **check-version:** exclude pre release suffix in comparison ([4dce004](https://gitlab.com/rxap/packages/commit/4dce0048bc2985d8d04f4653a41669591dc54b68))
- **fix-dependencies:** reuse existing project dependency version ([288768c](https://gitlab.com/rxap/packages/commit/288768cb2680560313da862b0c2aabc3e8f3207c))
- ignore express and axios packages ([f9234c8](https://gitlab.com/rxap/packages/commit/f9234c88bdec7ffaeb93d5a3a5e1de1d35bbb587))
- ignore projects that have no package.json ([0299bfc](https://gitlab.com/rxap/packages/commit/0299bfc45553853b1c6c9e3e88e4c8ce2b4ac6b4))
- **init:** add the fix-dependencies target and update the preversion script ([05cfb76](https://gitlab.com/rxap/packages/commit/05cfb7626a3329b3c419016525d4ad1d1f1212ef))
- **init:** use init plugin generator for plugin projects ([602f623](https://gitlab.com/rxap/packages/commit/602f62361ac6e10502cfe775d7ec50527e6afec3))
- move GetLatestPackageVersion to @rxap/node-utilities ([72cfdb8](https://gitlab.com/rxap/packages/commit/72cfdb8863b9e6ca567f87d90b8851eca930a2e1))
- remove scss assets copies if \_index.scss does not exist ([22aa410](https://gitlab.com/rxap/packages/commit/22aa41063fa0d24cdcfaee4a85e042390b3d44de))
- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))
- **run-generator:** support dry-run and without-project-argument flags ([19e3fa5](https://gitlab.com/rxap/packages/commit/19e3fa514af26044191a147a77c877e14c701b97))
- update package dependency versions ([45bd022](https://gitlab.com/rxap/packages/commit/45bd022d755c0c11f7d0bcc76d26b39928007941))
- **update-package-group:** only include @rxap/\* packages ([20e6c72](https://gitlab.com/rxap/packages/commit/20e6c72f1f69c0569bfe2655443b4f7533b31c79))
- **update-package-group:** use pined version instead of range ([a18c421](https://gitlab.com/rxap/packages/commit/a18c421093a98b84a3041052a4739df418f970f4))

### Features

- **executor:** add check version ([16b9f21](https://gitlab.com/rxap/packages/commit/16b9f21e4ad29fabe08fea6e095d3f9a469b03b7))
- **generator:** add init-plugin ([fa4d996](https://gitlab.com/rxap/packages/commit/fa4d9967229918b96c8976881705ab7726c1e96c))
- **init:** execute nest init generator if project is for nestjs ([94b2a18](https://gitlab.com/rxap/packages/commit/94b2a1841a0c6c65bed4ececefa2fed033be8c14))

# [16.0.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.6...@rxap/plugin-library@16.0.0-dev.7) (2023-07-20)

### Bug Fixes

- add glob and fx-extra to package blacklist ([5f3a7d7](https://gitlab.com/rxap/packages/commit/5f3a7d7e0bcf4cc6ae2fee0210a458f59a103e36))

# [16.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.5...@rxap/plugin-library@16.0.0-dev.6) (2023-07-13)

### Bug Fixes

- move GetLatestPackageVersion to @rxap/node-utilities ([56bc441](https://gitlab.com/rxap/packages/commit/56bc441d3af89c5c2a99cd3d300e2f4b55dbb624))

# [16.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-library@16.0.0-dev.4...@rxap/plugin-library@16.0.0-dev.5) (2023-07-10)

### Bug Fixes

- update package dependency versions ([8479f5c](https://gitlab.com/rxap/packages/commit/8479f5c405a885cc0f300cec6156584e4c65d59c))
- **update-package-group:** only include @rxap/\* packages ([f4631e3](https://gitlab.com/rxap/packages/commit/f4631e3713ed994c5f2cf5b64fd1563e1d6a22a9))

# 16.0.0-dev.4 (2023-07-10)

### Bug Fixes

- **fix-dependencies:** reuse existing project dependency version ([664c64e](https://gitlab.com/rxap/packages/commit/664c64e94d0f5717fa6e78180fbaadb7337f6ffe))
- **init:** add the fix-dependencies target and update the preversion script ([548740b](https://gitlab.com/rxap/packages/commit/548740b8560320afa69566c54ec709171a6ff7b5))
- remove scss assets copies if \_index.scss does not exist ([8d6ef93](https://gitlab.com/rxap/packages/commit/8d6ef93f3ec62cf915743a475ed48258e836f49a))
- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
- **run-generator:** support dry-run and without-project-argument flags ([7f4d918](https://gitlab.com/rxap/packages/commit/7f4d918e4852131550571779ca25b02c904ad83c))
- **update-package-group:** use pined version instead of range ([30b1307](https://gitlab.com/rxap/packages/commit/30b130780dde4fba670429e6dd56377cb94f8513))

# [16.0.0-dev.3](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@16.0.0-dev.2...@rxap/plugin-library@16.0.0-dev.3) (2023-05-18)

### Bug Fixes

- remove deprecated pack targetconcept ([39f1869](https://gitlab.com/rxap/schematics/commit/39f18698795cc6f5d8db81b43581c7d75244021f))

# [16.0.0-dev.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@16.0.0-dev.1...@rxap/plugin-library@16.0.0-dev.2) (2023-05-18)

### Bug Fixes

- **deps:** update rxap packages to16.x.x ([bd63f0b](https://gitlab.com/rxap/schematics/commit/bd63f0bfe3356eb1d00bb136253789f2e481e04b))

# [16.0.0-dev.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@16.0.0-dev.0...@rxap/plugin-library@16.0.0-dev.1) (2023-05-17)

**Note:** Version bump only for package @rxap/plugin-library

# [16.0.0-dev.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@15.0.2...@rxap/plugin-library@16.0.0-dev.0) (2023-05-11)

### Build System

- upgrade to nrwl 16.x.x ([de73759](https://gitlab.com/rxap/schematics/commit/de737599e984ce6e0dd19776ffbd04ab2c4085f3))

### BREAKING CHANGES

- upgrade nrwl 16.x.x

## [15.0.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@15.0.1...@rxap/plugin-library@15.0.2) (2023-05-11)

### Bug Fixes

- update to nrwl 15.9.4 ([c9ab045](https://gitlab.com/rxap/schematics/commit/c9ab0454484162e633b789a6274d77793179df23))

## [15.0.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@15.0.0...@rxap/plugin-library@15.0.1) (2023-01-30)

**Note:** Version bump only for package @rxap/plugin-library

# [15.0.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@14.0.3...@rxap/plugin-library@15.0.0) (2022-12-14)

### chore

- upgrade to nrwl 15.x.x ([b0694b6](https://gitlab.com/rxap/schematics/commit/b0694b6550730b80fb7356f6c225787fda1ff6be))

### Features

- **library:** only include direct dependencies ondefault ([8892585](https://gitlab.com/rxap/schematics/commit/889258508b6a157b049ba9ebd74d6c32f8865331))

### BREAKING CHANGES

- upgrade nrwl 15.x.x

## [14.0.3](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@14.0.2...@rxap/plugin-library@14.0.3) (2022-12-14)

**Note:** Version bump only for package @rxap/plugin-library

## [14.0.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@14.0.1...@rxap/plugin-library@14.0.2) (2022-10-06)

### Bug Fixes

- update rxap packages to14.x.x ([eda3337](https://gitlab.com/rxap/schematics/commit/eda3337af2c477126a3d83715cdc7a955c239cb6))

## [14.0.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@14.0.0...@rxap/plugin-library@14.0.1) (2022-10-06)

### Bug Fixes

- remove @rxap/utilitiesdependency ([fa8b3e6](https://gitlab.com/rxap/schematics/commit/fa8b3e667f7c4e5e1a5f15a5d0cc9543da72729d))

# [14.0.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@13.0.1...@rxap/plugin-library@14.0.0) (2022-09-08)

### chore

- upgrade to 14.x.x ([52cccdb](https://gitlab.com/rxap/schematics/commit/52cccdb066599a3c333117107a06169e5d42c604))

### BREAKING CHANGES

- upgrade to 14.x.x

## [13.0.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@13.0.0...@rxap/plugin-library@13.0.1) (2022-09-08)

**Note:** Version bump only for package @rxap/plugin-library

# [13.0.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@13.0.0-next.2...@rxap/plugin-library@13.0.0) (2022-09-08)

**Note:** Version bump only for package @rxap/plugin-library

# [13.0.0-next.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.2...@rxap/plugin-library@13.0.0-next.2) (2022-03-24)

### Bug Fixes

- update catch type ([a3a376b](https://gitlab.com/rxap/schematics/commit/a3a376be772f10889a1f7e1afdf18895ce070d9e))
- **update-peer-dependencies:** migrate to use the createProjectGraphAsync if cache isunavailable ([3454d1d](https://gitlab.com/rxap/schematics/commit/3454d1d0f21a201da01169e4bd470811bcbac919))
- **update-peer-dependencies:** update to the new project graphstructure ([1b86986](https://gitlab.com/rxap/schematics/commit/1b86986ab259eaa6859f827164c389fd17fa63fc))

### Build System

- upgrade to nrwl 13.x.x ([8f07b6b](https://gitlab.com/rxap/schematics/commit/8f07b6b82fb82e8b70fbc82bd91a08d69cc52692))

### BREAKING CHANGES

- update the core nrwl packages to 13.x.x

Signed-off-by: Merzough Münker <mmuenker@digitaix.com>

# [13.0.0-next.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@13.0.0-next.0...@rxap/plugin-library@13.0.0-next.1) (2022-02-25)

### Bug Fixes

- update catch type ([4587831](https://gitlab.com/rxap/schematics/commit/45878319c926061dc8995c568278c4ae7a903feb))

# [13.0.0-next.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.2...@rxap/plugin-library@13.0.0-next.0) (2022-02-19)

### Bug Fixes

- **update-peer-dependencies:** migrate to use the createProjectGraphAsync if cache isunavailable ([5307344](https://gitlab.com/rxap/schematics/commit/530734419cd4c05a1c13443da0e0cad0ce510d79))
- **update-peer-dependencies:** update to the new project graphstructure ([3cc7378](https://gitlab.com/rxap/schematics/commit/3cc7378eef2372b1caf4c4587a70d6e23e0aadcf))

### Build System

- upgrade to nrwl 13.x.x ([f6fb1fd](https://gitlab.com/rxap/schematics/commit/f6fb1fde34006136be4dadd72795d2d43207072a))

### BREAKING CHANGES

- update the core nrwl packages to 13.x.x

Signed-off-by: Merzough Münker <mmuenker@digitaix.com>

## [12.4.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.2-next.1...@rxap/plugin-library@12.4.2) (2021-10-07)

**Note:** Version bump only for package @rxap/plugin-library

## [12.4.2-next.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.2-next.0...@rxap/plugin-library@12.4.2-next.1) (2021-08-11)

### Bug Fixes

- add nrwl package to peerdependencies ([e15f848](https://gitlab.com/rxap/schematics/commit/e15f848369366bad60b63b32c7e71710b1ded826))

## [12.4.2-next.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.1...@rxap/plugin-library@12.4.2-next.0) (2021-08-10)

### Bug Fixes

- add strict tsc compileroption ([a262758](https://gitlab.com/rxap/schematics/commit/a2627582222671e58f6feaed0309d33ab13e6984))

## [12.4.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.0...@rxap/plugin-library@12.4.1) (2021-07-27)

### Bug Fixes

- replace deprecated schema idproperty ([cb8dedb](https://gitlab.com/rxap/schematics/commit/cb8dedb0c15c774f6c101df150f0d98242bc511a))

# [12.4.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.0-next.1...@rxap/plugin-library@12.4.0) (2021-07-09)

**Note:** Version bump only for package @rxap/plugin-library

# [12.4.0-next.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.4.0-next.0...@rxap/plugin-library@12.4.0-next.1) (2021-07-08)

### Bug Fixes

- include hidden files in schematics filesfolder ([028450e](https://gitlab.com/rxap/schematics/commit/028450e11e0f0caeec7ace91e84d45a606ff2dad))

# [12.4.0-next.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.3.2...@rxap/plugin-library@12.4.0-next.0) (2021-06-28)

### Bug Fixes

- **update-package-dependencies:** only update ng-package ifexists ([0374698](https://gitlab.com/rxap/schematics/commit/0374698b552356409336b4389e09b16b44746f57))

### Features

- **update-package-dependencies:** add optiondependencies ([c7c7fd8](https://gitlab.com/rxap/schematics/commit/c7c7fd83d3f143508009d3af34ed1daac37d90a2))

## [12.3.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.3.1...@rxap/plugin-library@12.3.2) (2021-06-23)

**Note:** Version bump only for package @rxap/plugin-library

## [12.3.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.3.0...@rxap/plugin-library@12.3.1) (2021-06-23)

### Bug Fixes

- mv typescript package todependencies ([06a7d2b](https://gitlab.com/rxap/schematics/commit/06a7d2bbbf5b0018cacdeb2445942719682c6937))

# [12.3.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.2.0...@rxap/plugin-library@12.3.0) (2021-06-23)

### Features

- remove lernadependency ([652fed9](https://gitlab.com/rxap/schematics/commit/652fed9b577a2554e0212320ed78281ef204eb10))

# [12.2.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.1.2...@rxap/plugin-library@12.2.0) (2021-06-23)

### Bug Fixes

- mv peerDependencies todependencies ([97be8bf](https://gitlab.com/rxap/schematics/commit/97be8bf8395ede8e5a50804b9ad7f72fde12bc81))

### Features

- **update-package-dependencies:** update the package.jsondependencies ([6afdf0e](https://gitlab.com/rxap/schematics/commit/6afdf0ed0881b6ff85e720ff1ca0d6cd3f4dbfdd))

## [12.1.2](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.1.1...@rxap/plugin-library@12.1.2) (2021-06-23)

### Bug Fixes

- only write json file ifchanged ([b39207b](https://gitlab.com/rxap/schematics/commit/b39207b446fde7ea294a3eb1313ed714f6861ece))

## [12.1.1](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.1.0...@rxap/plugin-library@12.1.1) (2021-06-23)

### Bug Fixes

- set ng-add save target todevDependencies ([c213f21](https://gitlab.com/rxap/schematics/commit/c213f21067e8bb280a48ae726840bfe0f5c4ff11))

# [12.1.0](https://gitlab.com/rxap/schematics/compare/@rxap/plugin-library@12.0.1...@rxap/plugin-library@12.1.0) (2021-06-23)

### Bug Fixes

- ensure the peerDependencies property isdefined ([90122eb](https://gitlab.com/rxap/schematics/commit/90122eb42382c2761f8124a88d79f3a16e8c5635))
- use correct optionsproperty ([2fea2f8](https://gitlab.com/rxap/schematics/commit/2fea2f801c9d3fe4ecceac0a568cfe93cfa42971))

### Features

- add update-package-groupbuilder ([e04006b](https://gitlab.com/rxap/schematics/commit/e04006bf53b3988fd110f8e61c899503e0e98c3d))
- add update-peer-dependencybuilder ([f473dbe](https://gitlab.com/rxap/schematics/commit/f473dbeef0b1fcd5d5bba52ebff8311a6578e7a7))

## 12.0.1 (2021-06-21)

**Note:** Version bump only for package @rxap/plugin-library
