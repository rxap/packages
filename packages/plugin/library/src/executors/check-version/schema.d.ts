export interface CheckVersionExecutorSchema {
  /** The name of the package used as minimum version reference */
  packageName: string;
}
