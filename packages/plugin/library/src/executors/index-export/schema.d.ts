export interface IndexExportExecutorSchema {
  /** Generate index.ts in the source root of the project */
  generateRootExport?: boolean;
}
