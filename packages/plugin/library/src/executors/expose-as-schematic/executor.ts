import { PromiseExecutor } from '@nx/devkit';
import { DeleteEmptyProperties } from '@rxap/utilities';
import { runExecutor as runGenerator } from '../run-generator/executor';
import { ExposeAsSchematicExecutorSchema } from './schema';

const runExecutor: PromiseExecutor<ExposeAsSchematicExecutorSchema> = async (
  options, context
) => {
  console.log('Executor ran for IndexExport', options);
  return runGenerator({
    generator: '@rxap/plugin-library:expose-as-schematic',
    options: DeleteEmptyProperties({})
  }, context);
};

export default runExecutor;
