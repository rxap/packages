export interface GenerateSchemaExecutorSchema {
  /** A list of glob patterns relative to the project that should be excluded */
  excludes?: Array<string>;
}
