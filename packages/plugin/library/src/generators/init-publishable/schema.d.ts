export interface InitPublishableGeneratorSchema {
  project?: string;
  projects?: Array<string>;
  skipFormat?: boolean;
  /** Whether to overwrite existing files */
  overwrite?: boolean;
  /** Whether to skip executing project specific initialization */
  skipProjects?: boolean;
  withInitGenerator?: boolean;
  targets?: {
      /** If set to false the target fix-dependencies will not be added to the project */
      fixDependencies?: boolean;
    };
}
