export interface BundleJsonSchemaGeneratorSchema {
  /** Name of the project */
  project: string;
}
