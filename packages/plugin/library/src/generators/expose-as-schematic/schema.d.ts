export interface ExposeAsSchematicGeneratorSchema {
  projects?: Array<string>;
  project?: string;
}
