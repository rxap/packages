# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.4-dev.0...@rxap/plugin-storybook@20.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.3...@rxap/plugin-storybook@20.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.3-dev.1...@rxap/plugin-storybook@20.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.3-dev.0...@rxap/plugin-storybook@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2...@rxap/plugin-storybook@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.5...@rxap/plugin-storybook@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.4...@rxap/plugin-storybook@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.3...@rxap/plugin-storybook@20.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.2...@rxap/plugin-storybook@20.0.2-dev.3) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.1...@rxap/plugin-storybook@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.2-dev.0...@rxap/plugin-storybook@20.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1...@rxap/plugin-storybook@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.13...@rxap/plugin-storybook@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.12...@rxap/plugin-storybook@20.0.1-dev.13) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.11...@rxap/plugin-storybook@20.0.1-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.10...@rxap/plugin-storybook@20.0.1-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.9...@rxap/plugin-storybook@20.0.1-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.8...@rxap/plugin-storybook@20.0.1-dev.9) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.7...@rxap/plugin-storybook@20.0.1-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.6...@rxap/plugin-storybook@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.5...@rxap/plugin-storybook@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.4...@rxap/plugin-storybook@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.3...@rxap/plugin-storybook@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.2...@rxap/plugin-storybook@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.1...@rxap/plugin-storybook@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.1-dev.0...@rxap/plugin-storybook@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-storybook

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.0...@rxap/plugin-storybook@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-storybook

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.0-dev.3...@rxap/plugin-storybook@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-storybook

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.0-dev.2...@rxap/plugin-storybook@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-storybook

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@20.0.0-dev.1...@rxap/plugin-storybook@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.10-dev.0...@rxap/plugin-storybook@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.9...@rxap/plugin-storybook@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.9-dev.1...@rxap/plugin-storybook@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.9-dev.0...@rxap/plugin-storybook@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.8...@rxap/plugin-storybook@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.8-dev.3...@rxap/plugin-storybook@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.8-dev.2...@rxap/plugin-storybook@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.8-dev.1...@rxap/plugin-storybook@19.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.8-dev.0...@rxap/plugin-storybook@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.7...@rxap/plugin-storybook@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.7-dev.1...@rxap/plugin-storybook@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.7-dev.0...@rxap/plugin-storybook@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6...@rxap/plugin-storybook@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.7...@rxap/plugin-storybook@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.6...@rxap/plugin-storybook@19.0.6-dev.7) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.5...@rxap/plugin-storybook@19.0.6-dev.6) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.4...@rxap/plugin-storybook@19.0.6-dev.5) (2024-08-19)

### Bug Fixes

- use development build target ([a2393b3](https://gitlab.com/rxap/packages/commit/a2393b39e8919d2597711a92458e80c24ff269a2))

## [19.0.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.3...@rxap/plugin-storybook@19.0.6-dev.4) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.2...@rxap/plugin-storybook@19.0.6-dev.3) (2024-08-15)

### Bug Fixes

- update default nx targets ([43f6151](https://gitlab.com/rxap/packages/commit/43f6151292a10218735e5cf203ec3cb169211530))

## [19.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.1...@rxap/plugin-storybook@19.0.6-dev.2) (2024-08-15)

### Bug Fixes

- add missing packages ([40d602e](https://gitlab.com/rxap/packages/commit/40d602ed9e4ec4d972c3e743063abff26110e64c))

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.6-dev.0...@rxap/plugin-storybook@19.0.6-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5...@rxap/plugin-storybook@19.0.6-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.9...@rxap/plugin-storybook@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.8...@rxap/plugin-storybook@19.0.5-dev.9) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.7...@rxap/plugin-storybook@19.0.5-dev.8) (2024-07-18)

### Bug Fixes

- proper dark mode support ([baeaf3c](https://gitlab.com/rxap/packages/commit/baeaf3c986e635fdb057c156e15192eff5617c58))
- use provideHttpClient instead of HttpClientModule ([acb957d](https://gitlab.com/rxap/packages/commit/acb957dff2a446c135d1f5105bdb4fb395dcb1a1))

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.6...@rxap/plugin-storybook@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.5...@rxap/plugin-storybook@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.4...@rxap/plugin-storybook@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.3...@rxap/plugin-storybook@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.2...@rxap/plugin-storybook@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.1...@rxap/plugin-storybook@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.5-dev.0...@rxap/plugin-storybook@19.0.5-dev.1) (2024-07-02)

### Bug Fixes

- ensure stories from secondary entry points are included ([4f4e382](https://gitlab.com/rxap/packages/commit/4f4e3829c75f5d3198a7df8e7db6279e57a7077e))

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.4...@rxap/plugin-storybook@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.4-dev.0...@rxap/plugin-storybook@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3...@rxap/plugin-storybook@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.7...@rxap/plugin-storybook@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.6...@rxap/plugin-storybook@19.0.3-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.5...@rxap/plugin-storybook@19.0.3-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.4...@rxap/plugin-storybook@19.0.3-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.3...@rxap/plugin-storybook@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.2...@rxap/plugin-storybook@19.0.3-dev.3) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.1...@rxap/plugin-storybook@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.3-dev.0...@rxap/plugin-storybook@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2...@rxap/plugin-storybook@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.9...@rxap/plugin-storybook@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.8...@rxap/plugin-storybook@19.0.2-dev.9) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.7...@rxap/plugin-storybook@19.0.2-dev.8) (2024-06-18)

### Bug Fixes

- add provide config ([a48caea](https://gitlab.com/rxap/packages/commit/a48caeac63bf3108e1f4c092a44bbfe1fc64ee63))
- ensure the environment provider is available ([08fe207](https://gitlab.com/rxap/packages/commit/08fe20702d79395e871d73c76ace653cde9c3c4d))

## [19.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.6...@rxap/plugin-storybook@19.0.2-dev.7) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.5...@rxap/plugin-storybook@19.0.2-dev.6) (2024-06-17)

### Bug Fixes

- add nx packages to workspace ([18c54bb](https://gitlab.com/rxap/packages/commit/18c54bbf7fe4cbe994fafa35e9eb5356b74c61e2))

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.4...@rxap/plugin-storybook@19.0.2-dev.5) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.3...@rxap/plugin-storybook@19.0.2-dev.4) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.2...@rxap/plugin-storybook@19.0.2-dev.3) (2024-06-11)

### Bug Fixes

- ensure the correct browser target is set ([27d05f7](https://gitlab.com/rxap/packages/commit/27d05f723c08d82e543d66d6b8d6a753360b471e))

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.1...@rxap/plugin-storybook@19.0.2-dev.2) (2024-06-10)

### Bug Fixes

- add missing deps ([8fd7c02](https://gitlab.com/rxap/packages/commit/8fd7c02d1d1d7cfd9b72c45928cb54e035261ea9))
- ensure feature stories are included ([7132ec6](https://gitlab.com/rxap/packages/commit/7132ec6cb614b1531e5ebe180a46660870d5478f))
- ensure noop animation is added ([e0ca175](https://gitlab.com/rxap/packages/commit/e0ca175b0cb0ed776aa7840f372df1797c100c2b))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.2-dev.0...@rxap/plugin-storybook@19.0.2-dev.1) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.1...@rxap/plugin-storybook@19.0.2-dev.0) (2024-06-04)

### Bug Fixes

- add missing required package ([b71ceee](https://gitlab.com/rxap/packages/commit/b71ceeeb0dbaf84fe4ad4798682a7eadab6d2b43))

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@19.0.1-dev.0...@rxap/plugin-storybook@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@18.0.1...@rxap/plugin-storybook@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-storybook

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@18.0.1-dev.0...@rxap/plugin-storybook@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@17.0.1...@rxap/plugin-storybook@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@17.0.1-dev.0...@rxap/plugin-storybook@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.1.1...@rxap/plugin-storybook@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-storybook

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.1.1-dev.0...@rxap/plugin-storybook@16.1.1) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-storybook

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.1.0...@rxap/plugin-storybook@16.1.1-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-storybook

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.1.0-dev.1...@rxap/plugin-storybook@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-storybook

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.1.0-dev.0...@rxap/plugin-storybook@16.1.0-dev.1) (2024-04-12)

### Bug Fixes

- add angular localize import ([80f3256](https://gitlab.com/rxap/packages/commit/80f325662d2ad6aa7805578e7d02bc2a1a5792da))
- add required package to workspace ([ddf8db8](https://gitlab.com/rxap/packages/commit/ddf8db8519e0815c69e5c53755ce35610a627d17))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.0.1-dev.2...@rxap/plugin-storybook@16.1.0-dev.0) (2024-04-10)

### Bug Fixes

- add schematic serializer ([57b1d08](https://gitlab.com/rxap/packages/commit/57b1d0871f5bee9ab7bd9cb9bc489f87da1e1429))

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.0.1-dev.1...@rxap/plugin-storybook@16.0.1-dev.2) (2024-04-09)

### Bug Fixes

- add required compodoc package ([1e5ee10](https://gitlab.com/rxap/packages/commit/1e5ee101b592b706c1d0f104f33125ddf4339cdf))

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-storybook@16.0.1-dev.0...@rxap/plugin-storybook@16.0.1-dev.1) (2024-04-07)

### Bug Fixes

- coerce base configuration ([ee8d8f3](https://gitlab.com/rxap/packages/commit/ee8d8f36960cb2a5e525822222832aba7970e5ac))

## 16.0.1-dev.0 (2024-04-07)

**Note:** Version bump only for package @rxap/plugin-storybook
