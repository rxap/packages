# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.2-dev.0...@rxap/plugin-n8n@20.1.2-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1...@rxap/plugin-n8n@20.1.2-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1-dev.4...@rxap/plugin-n8n@20.1.1) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1-dev.3...@rxap/plugin-n8n@20.1.1-dev.4) (2025-02-28)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1-dev.2...@rxap/plugin-n8n@20.1.1-dev.3) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1-dev.1...@rxap/plugin-n8n@20.1.1-dev.2) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.1-dev.0...@rxap/plugin-n8n@20.1.1-dev.1) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0...@rxap/plugin-n8n@20.1.1-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.6...@rxap/plugin-n8n@20.1.0) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.5...@rxap/plugin-n8n@20.1.0-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.4...@rxap/plugin-n8n@20.1.0-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.3...@rxap/plugin-n8n@20.1.0-dev.4) (2025-02-19)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.2...@rxap/plugin-n8n@20.1.0-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.1...@rxap/plugin-n8n@20.1.0-dev.2) (2025-02-18)

### Bug Fixes

- set the n8n-workflow package version ([b8f14f0](https://gitlab.com/rxap/packages/commit/b8f14f063c7d918b46aa8a8b5149a7ec9490216b))

### Features

- add the node trigger generator ([85b07cb](https://gitlab.com/rxap/packages/commit/85b07cb2001d7e2d8617ee79d44df0626649ad95))

# [20.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.1.0-dev.0...@rxap/plugin-n8n@20.1.0-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.2...@rxap/plugin-n8n@20.1.0-dev.0) (2025-02-17)

### Bug Fixes

- add the office npm keyword ([49e0165](https://gitlab.com/rxap/packages/commit/49e01658bddd4cbf4fe42f9bb6c9f7a5e3b55051))
- only include nodes projects ([a298f8a](https://gitlab.com/rxap/packages/commit/a298f8acf0145c9634b793c785883e16f1d86f12))
- update the package json ([4316b0a](https://gitlab.com/rxap/packages/commit/4316b0a7a609f3cd5a2bf278d747e18eb8faa723))

### Features

- add plugin ([ed0689e](https://gitlab.com/rxap/packages/commit/ed0689e90cb283e6501f05ee15ca7c5d0a3661f2))

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1...@rxap/plugin-n8n@20.0.2) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.16...@rxap/plugin-n8n@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.15...@rxap/plugin-n8n@20.0.1-dev.16) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.14...@rxap/plugin-n8n@20.0.1-dev.15) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.13...@rxap/plugin-n8n@20.0.1-dev.14) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.12...@rxap/plugin-n8n@20.0.1-dev.13) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.11...@rxap/plugin-n8n@20.0.1-dev.12) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.10...@rxap/plugin-n8n@20.0.1-dev.11) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.9...@rxap/plugin-n8n@20.0.1-dev.10) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.8...@rxap/plugin-n8n@20.0.1-dev.9) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.7...@rxap/plugin-n8n@20.0.1-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.6...@rxap/plugin-n8n@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.5...@rxap/plugin-n8n@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.4...@rxap/plugin-n8n@20.0.1-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.3...@rxap/plugin-n8n@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.2...@rxap/plugin-n8n@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.1...@rxap/plugin-n8n@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.1-dev.0...@rxap/plugin-n8n@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-n8n

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0...@rxap/plugin-n8n@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0-dev.5...@rxap/plugin-n8n@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0-dev.4...@rxap/plugin-n8n@20.0.0-dev.5) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0-dev.3...@rxap/plugin-n8n@20.0.0-dev.4) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0-dev.2...@rxap/plugin-n8n@20.0.0-dev.3) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-n8n

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@20.0.0-dev.1...@rxap/plugin-n8n@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.2-dev.1...@rxap/plugin-n8n@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-n8n

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.2-dev.0...@rxap/plugin-n8n@19.0.2-dev.1) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-n8n

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.1...@rxap/plugin-n8n@19.0.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-n8n

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.1-dev.1...@rxap/plugin-n8n@19.0.1) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-n8n

## [19.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.1-dev.0...@rxap/plugin-n8n@19.0.1-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-n8n

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0...@rxap/plugin-n8n@19.0.1-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-n8n

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0-dev.5...@rxap/plugin-n8n@19.0.0) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-n8n

# [19.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0-dev.4...@rxap/plugin-n8n@19.0.0-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-n8n

# [19.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0-dev.3...@rxap/plugin-n8n@19.0.0-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-n8n

# [19.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0-dev.2...@rxap/plugin-n8n@19.0.0-dev.3) (2024-10-22)

**Note:** Version bump only for package @rxap/plugin-n8n

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-n8n@19.0.0-dev.1...@rxap/plugin-n8n@19.0.0-dev.2) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-n8n

# 19.0.0-dev.1 (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-n8n
