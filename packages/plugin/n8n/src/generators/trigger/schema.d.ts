export interface TriggerGeneratorSchema {
  name: string;
  project: string;
  description?: string;
  nodeNamePrefix?: string;
}
