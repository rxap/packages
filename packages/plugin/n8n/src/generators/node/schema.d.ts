export interface NodeGeneratorSchema {
  name: string;
  project: string;
  description?: string;
  nodeNamePrefix?: string;
}
