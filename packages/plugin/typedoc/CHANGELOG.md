# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [20.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.3.0-dev.0...@rxap/plugin-typedoc@20.3.0-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.1...@rxap/plugin-typedoc@20.3.0-dev.0) (2025-03-12)

### Bug Fixes

- add support for JetBrains project folder exclusion ([dd924a3](https://gitlab.com/rxap/packages/commit/dd924a33c33b772d64457659cab4307c258b1fd0))

### Features

- **workspace-utilities:** add function to check tsconfig.json existence ([e25238b](https://gitlab.com/rxap/packages/commit/e25238b78bdb874b3b3df73f1be54553b494f5e2))

## [20.2.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.1-dev.1...@rxap/plugin-typedoc@20.2.1) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.1-dev.0...@rxap/plugin-typedoc@20.2.1-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0...@rxap/plugin-typedoc@20.2.1-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.5...@rxap/plugin-typedoc@20.2.0) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.2.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.4...@rxap/plugin-typedoc@20.2.0-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.3...@rxap/plugin-typedoc@20.2.0-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.2...@rxap/plugin-typedoc@20.2.0-dev.3) (2025-02-22)

### Bug Fixes

- set required typedoc config static ([8058138](https://gitlab.com/rxap/packages/commit/8058138ff98d9e37b711846892c79ebc45ed17d4))

# [20.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.1...@rxap/plugin-typedoc@20.2.0-dev.2) (2025-02-19)

### Bug Fixes

- exit with non zero on typedoc error ([90aecff](https://gitlab.com/rxap/packages/commit/90aecffc990b5c7d58b6979c66e6650eecf69e6d))
- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

# [20.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.2.0-dev.0...@rxap/plugin-typedoc@20.2.0-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

# [20.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.1-dev.2...@rxap/plugin-typedoc@20.2.0-dev.0) (2025-02-18)

### Features

- support entryPointStrategy ([a105521](https://gitlab.com/rxap/packages/commit/a105521495b0218f696abca3abb0ba759cfc58e6))
- support workspace typedoc.json ([5eefbae](https://gitlab.com/rxap/packages/commit/5eefbae962821dee06b2e34e16194bfd860c5dce))

## [20.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.1-dev.1...@rxap/plugin-typedoc@20.1.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.1-dev.0...@rxap/plugin-typedoc@20.1.1-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.0...@rxap/plugin-typedoc@20.1.1-dev.0) (2025-02-17)

### Bug Fixes

- ignore non typedoc related files ([b1efa25](https://gitlab.com/rxap/packages/commit/b1efa25c7fe760a8fec14bba050fba2492c4319b))

# [20.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.0-dev.2...@rxap/plugin-typedoc@20.1.0) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.0-dev.1...@rxap/plugin-typedoc@20.1.0-dev.2) (2025-02-13)

### Bug Fixes

- add docs folder to idea ignore ([24aa7d7](https://gitlab.com/rxap/packages/commit/24aa7d701a1f90e3dc0e2b99a58776f419d43054))

# [20.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.1.0-dev.0...@rxap/plugin-typedoc@20.1.0-dev.1) (2025-02-11)

### Bug Fixes

- safe access project sourceRoot property ([16ca874](https://gitlab.com/rxap/packages/commit/16ca8747120876ad90e38c0cc012c175741fda0b))

# [20.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.9...@rxap/plugin-typedoc@20.1.0-dev.0) (2025-02-11)

### Bug Fixes

- remove static tsconfig options ([84a2cf1](https://gitlab.com/rxap/packages/commit/84a2cf10394f7641a24993364589ee71d004b795))
- support workspace generation ([b3a0e6f](https://gitlab.com/rxap/packages/commit/b3a0e6ff58611e2f2144c33e265efb47d846579b))

### Features

- support typedoc markdown plugins ([94b91ff](https://gitlab.com/rxap/packages/commit/94b91ff2bba30e79de77b1250b10525b8269c8fd))

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.8...@rxap/plugin-typedoc@20.0.1-dev.9) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.7...@rxap/plugin-typedoc@20.0.1-dev.8) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.6...@rxap/plugin-typedoc@20.0.1-dev.7) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.5...@rxap/plugin-typedoc@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.4...@rxap/plugin-typedoc@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.3...@rxap/plugin-typedoc@20.0.1-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.2...@rxap/plugin-typedoc@20.0.1-dev.3) (2025-01-28)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.1...@rxap/plugin-typedoc@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.1-dev.0...@rxap/plugin-typedoc@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.0...@rxap/plugin-typedoc@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.0-dev.3...@rxap/plugin-typedoc@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.0-dev.2...@rxap/plugin-typedoc@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@20.0.0-dev.1...@rxap/plugin-typedoc@20.0.0-dev.2) (2025-01-03)

### Bug Fixes

- only use the build target if exists ([ce3d483](https://gitlab.com/rxap/packages/commit/ce3d483fae9998a1ef99a7d57fb830abea410f4d))
- only use the build target if exists ([bc323dd](https://gitlab.com/rxap/packages/commit/bc323dd192399ffd29b5fcfa9213e3192bc4115f))
- use correct prefixes ([f080f2e](https://gitlab.com/rxap/packages/commit/f080f2e43e24ca7a3f188f1b9d7ab69447f87af6))

### Features

- init plugin use ([cb80eb0](https://gitlab.com/rxap/packages/commit/cb80eb01ebd1c53b5e411cda02ce4e5b976cb74d))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))
- support nx plugins ([f40f273](https://gitlab.com/rxap/packages/commit/f40f2735e35f92f94d26f558946e79354e2a77a7))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.7-dev.0...@rxap/plugin-typedoc@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.6...@rxap/plugin-typedoc@19.0.7-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.6-dev.1...@rxap/plugin-typedoc@19.0.6) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.6-dev.0...@rxap/plugin-typedoc@19.0.6-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.5...@rxap/plugin-typedoc@19.0.6-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.5-dev.3...@rxap/plugin-typedoc@19.0.5) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.5-dev.2...@rxap/plugin-typedoc@19.0.5-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.5-dev.1...@rxap/plugin-typedoc@19.0.5-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.5-dev.0...@rxap/plugin-typedoc@19.0.5-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.4...@rxap/plugin-typedoc@19.0.5-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.4-dev.1...@rxap/plugin-typedoc@19.0.4) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.4-dev.0...@rxap/plugin-typedoc@19.0.4-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.3...@rxap/plugin-typedoc@19.0.4-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.3-dev.1...@rxap/plugin-typedoc@19.0.3) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.3-dev.0...@rxap/plugin-typedoc@19.0.3-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2...@rxap/plugin-typedoc@19.0.3-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.8...@rxap/plugin-typedoc@19.0.2) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.7...@rxap/plugin-typedoc@19.0.2-dev.8) (2024-07-30)

### Bug Fixes

- set the tsconfig to the lib file ([4c88033](https://gitlab.com/rxap/packages/commit/4c880330634b938589d8f7450d07c5b5fc9cb8db))

## [19.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.6...@rxap/plugin-typedoc@19.0.2-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.5...@rxap/plugin-typedoc@19.0.2-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.4...@rxap/plugin-typedoc@19.0.2-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.3...@rxap/plugin-typedoc@19.0.2-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.2...@rxap/plugin-typedoc@19.0.2-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.1...@rxap/plugin-typedoc@19.0.2-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.2-dev.0...@rxap/plugin-typedoc@19.0.2-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.1...@rxap/plugin-typedoc@19.0.2-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.1-dev.0...@rxap/plugin-typedoc@19.0.1) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-typedoc

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.0...@rxap/plugin-typedoc@19.0.1-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [19.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.0-dev.4...@rxap/plugin-typedoc@19.0.0) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [19.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.0-dev.3...@rxap/plugin-typedoc@19.0.0-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [19.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.0-dev.2...@rxap/plugin-typedoc@19.0.0-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-typedoc

# [19.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-typedoc@19.0.0-dev.1...@rxap/plugin-typedoc@19.0.0-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-typedoc

# 19.0.0-dev.1 (2024-06-20)

### Bug Fixes

- handle root project correctly ([9cbedb8](https://gitlab.com/rxap/packages/commit/9cbedb8181ff34379e80e68900e851865032ad04))
