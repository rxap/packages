# @throws

The @throws tag can be used to document an exception that can be thrown by a function or method.

```typescript
/**
 * @throws {BadRequestException} if `max < min`
 */
export function rand(min: number, max: number): number {
  if (max < min) {
    throw new BadRequestException('Max is less then min')
  }
}
```

```typescript
/**
 * @throws if `max < min`
 */
export function rand(min: number, max: number): number {
  if (max < min) {
    throw new Error('Max is less then min')
  }
}
```
