# @property

The @property tag can be used to add a comment to a child of the current reflection. It is intended for use with the @namespace and @interface tags which might not have convenient places to include comments for each member.

```typescript
/**
 * This will be displayed as an interface
 * @property a comment for a
 * @property b comment for b
 * @interface
 */
export type Resolved = Record<"a" | "b" | "c", string>;

// will be documented as if you wrote

/**
 * This will be displayed as an interface
 */
export interface Resolved {
    /** comment for a */
    a: string;
    /** comment for b */
    b: string;
    c: string;
}
```
