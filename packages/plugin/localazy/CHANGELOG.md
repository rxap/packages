# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.4-dev.0...@rxap/plugin-localazy@20.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.3...@rxap/plugin-localazy@20.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.3-dev.1...@rxap/plugin-localazy@20.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.3-dev.0...@rxap/plugin-localazy@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2...@rxap/plugin-localazy@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.6...@rxap/plugin-localazy@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.5...@rxap/plugin-localazy@20.0.2-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.4...@rxap/plugin-localazy@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.3...@rxap/plugin-localazy@20.0.2-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.2...@rxap/plugin-localazy@20.0.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.1...@rxap/plugin-localazy@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.2-dev.0...@rxap/plugin-localazy@20.0.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1...@rxap/plugin-localazy@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.12...@rxap/plugin-localazy@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.11...@rxap/plugin-localazy@20.0.1-dev.12) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.10...@rxap/plugin-localazy@20.0.1-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.9...@rxap/plugin-localazy@20.0.1-dev.10) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.8...@rxap/plugin-localazy@20.0.1-dev.9) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.7...@rxap/plugin-localazy@20.0.1-dev.8) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.6...@rxap/plugin-localazy@20.0.1-dev.7) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.5...@rxap/plugin-localazy@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.4...@rxap/plugin-localazy@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.3...@rxap/plugin-localazy@20.0.1-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.2...@rxap/plugin-localazy@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.1...@rxap/plugin-localazy@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.1-dev.0...@rxap/plugin-localazy@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-localazy

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.0...@rxap/plugin-localazy@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-localazy

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.0-dev.3...@rxap/plugin-localazy@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-localazy

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.0-dev.2...@rxap/plugin-localazy@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-localazy

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@20.0.0-dev.1...@rxap/plugin-localazy@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))
- support nx plugins ([42982f2](https://gitlab.com/rxap/packages/commit/42982f2fde5af200ab03c90ab96085d6631ee995))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.10-dev.0...@rxap/plugin-localazy@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.9...@rxap/plugin-localazy@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.9-dev.1...@rxap/plugin-localazy@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.9-dev.0...@rxap/plugin-localazy@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.8...@rxap/plugin-localazy@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.8-dev.3...@rxap/plugin-localazy@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.8-dev.2...@rxap/plugin-localazy@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.8-dev.1...@rxap/plugin-localazy@19.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.8-dev.0...@rxap/plugin-localazy@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.7...@rxap/plugin-localazy@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.7-dev.1...@rxap/plugin-localazy@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.7-dev.0...@rxap/plugin-localazy@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.6...@rxap/plugin-localazy@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.6-dev.1...@rxap/plugin-localazy@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.6-dev.0...@rxap/plugin-localazy@19.0.6-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5...@rxap/plugin-localazy@19.0.6-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.9...@rxap/plugin-localazy@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.8...@rxap/plugin-localazy@19.0.5-dev.9) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.7...@rxap/plugin-localazy@19.0.5-dev.8) (2024-07-10)

### Bug Fixes

- only create a auto tag for protected refs ([3641c69](https://gitlab.com/rxap/packages/commit/3641c695e3a70f0b6418700987c23346f64329d4))

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.6...@rxap/plugin-localazy@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.5...@rxap/plugin-localazy@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.4...@rxap/plugin-localazy@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.3...@rxap/plugin-localazy@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.2...@rxap/plugin-localazy@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.1...@rxap/plugin-localazy@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.5-dev.0...@rxap/plugin-localazy@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.4...@rxap/plugin-localazy@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.4-dev.0...@rxap/plugin-localazy@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3...@rxap/plugin-localazy@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3-dev.4...@rxap/plugin-localazy@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3-dev.3...@rxap/plugin-localazy@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3-dev.2...@rxap/plugin-localazy@19.0.3-dev.3) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3-dev.1...@rxap/plugin-localazy@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.3-dev.0...@rxap/plugin-localazy@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2...@rxap/plugin-localazy@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2-dev.4...@rxap/plugin-localazy@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2-dev.3...@rxap/plugin-localazy@19.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2-dev.2...@rxap/plugin-localazy@19.0.2-dev.3) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2-dev.1...@rxap/plugin-localazy@19.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.2-dev.0...@rxap/plugin-localazy@19.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.1...@rxap/plugin-localazy@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@19.0.1-dev.0...@rxap/plugin-localazy@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@18.0.1...@rxap/plugin-localazy@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-localazy

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@18.0.1-dev.0...@rxap/plugin-localazy@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@17.0.1...@rxap/plugin-localazy@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@17.0.1-dev.0...@rxap/plugin-localazy@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.3...@rxap/plugin-localazy@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.3-dev.0...@rxap/plugin-localazy@16.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.2...@rxap/plugin-localazy@16.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.2-dev.0...@rxap/plugin-localazy@16.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.1...@rxap/plugin-localazy@16.1.2-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.1-dev.3...@rxap/plugin-localazy@16.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.1-dev.2...@rxap/plugin-localazy@16.1.1-dev.3) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.1-dev.1...@rxap/plugin-localazy@16.1.1-dev.2) (2024-03-31)

**Note:** Version bump only for package @rxap/plugin-localazy

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.1-dev.0...@rxap/plugin-localazy@16.1.1-dev.1) (2024-03-14)

### Bug Fixes

- update to the new localazy ci concept ([21dbf31](https://gitlab.com/rxap/packages/commit/21dbf318e629b86f155bdb8ca44ae4048a7dbae1))

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0...@rxap/plugin-localazy@16.1.1-dev.0) (2024-03-11)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.23...@rxap/plugin-localazy@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.23](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.22...@rxap/plugin-localazy@16.1.0-dev.23) (2024-01-30)

### Features

- support loading write and read keys from file ([2d492cf](https://gitlab.com/rxap/packages/commit/2d492cf3294cc710bf34d4d58e3b3ec608e6272e))

# [16.1.0-dev.22](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.21...@rxap/plugin-localazy@16.1.0-dev.22) (2024-01-29)

### Bug Fixes

- use the CI_COMMIT_REF_SLUG env to determine the default tag ([d096d7c](https://gitlab.com/rxap/packages/commit/d096d7c42f6a02d1ba888e2d52a8c62f35a9cb40))

# [16.1.0-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.20...@rxap/plugin-localazy@16.1.0-dev.21) (2024-01-25)

### Bug Fixes

- ensure the tag is normalized ([d71414e](https://gitlab.com/rxap/packages/commit/d71414e70d302365f9c7d513f84b8e531c01d2d4))

# [16.1.0-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.19...@rxap/plugin-localazy@16.1.0-dev.20) (2024-01-25)

### Bug Fixes

- improve logging output ([d91436e](https://gitlab.com/rxap/packages/commit/d91436efb980ba47e77e7c26296e2c2a1e2f3fe0))

# [16.1.0-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.18...@rxap/plugin-localazy@16.1.0-dev.19) (2023-11-24)

### Bug Fixes

- add required common options for tag publish ([24f984b](https://gitlab.com/rxap/packages/commit/24f984b213cbf075efe462144eaac7ab2621e1f0))

# [16.1.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.17...@rxap/plugin-localazy@16.1.0-dev.18) (2023-10-20)

### Bug Fixes

- support all possible command arguments ([49fd214](https://gitlab.com/rxap/packages/commit/49fd2143a6a0512d18aad546c09cc0a387585d94))

### Features

- support auto tagging ([e133066](https://gitlab.com/rxap/packages/commit/e133066606e3139c7dc2312b843181cd797543cc))

# [16.1.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.16...@rxap/plugin-localazy@16.1.0-dev.17) (2023-10-18)

### Bug Fixes

- change the localazy job dependency to run ([e51c23a](https://gitlab.com/rxap/packages/commit/e51c23a566e9f2fe1c23a367a1289c65e02c91f7))

# [16.1.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.15...@rxap/plugin-localazy@16.1.0-dev.16) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.14...@rxap/plugin-localazy@16.1.0-dev.15) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.13...@rxap/plugin-localazy@16.1.0-dev.14) (2023-10-11)

### Bug Fixes

- ensure overwrite option is respected ([672c315](https://gitlab.com/rxap/packages/commit/672c3152403ae577fb8d4d34e060971d2e53d0af))

# [16.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@1.0.1-dev.1...@rxap/plugin-localazy@16.1.0-dev.13) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add missing job tags ([0ad32ca](https://gitlab.com/rxap/packages/commit/0ad32cab8439370bae80cfbd8a1abc173c8447fb))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- expose generators as schematics ([8a58d07](https://gitlab.com/rxap/packages/commit/8a58d07c2f1dcfff75e724a418d7c3bddb2d0bbc))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- introduce more Is\*Project functions ([41a3713](https://gitlab.com/rxap/packages/commit/41a3713e2965f46900e80902a455b62e08686989))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- remove nx dependency ([b2b98b0](https://gitlab.com/rxap/packages/commit/b2b98b01438e9439f9743fb27629c7e96072df45))
- remove this access ([91a8252](https://gitlab.com/rxap/packages/commit/91a8252b888486c27b2980e8f3406a760181b767))
- remove unnecessary deployment tier ([be4c65a](https://gitlab.com/rxap/packages/commit/be4c65a7d1d5dc9c02e6bda8ac6fba6a0c21cd96))
- run gitlab ci generators on application init ([84f804f](https://gitlab.com/rxap/packages/commit/84f804fb533ac84f80a708cff8c1b8c78f23707c))

### Features

- **gitlab-ci:** add generator ([89be767](https://gitlab.com/rxap/packages/commit/89be7678f6d267be188962d1506c400680aadfe3))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [16.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.11...@rxap/plugin-localazy@16.1.0-dev.12) (2023-10-02)

### Bug Fixes

- introduce more Is\*Project functions ([8d37211](https://gitlab.com/rxap/packages/commit/8d37211fb1906f90d7176cfcfe43f755f04a0fa6))

# [16.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.10...@rxap/plugin-localazy@16.1.0-dev.11) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [16.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.9...@rxap/plugin-localazy@16.1.0-dev.10) (2023-09-12)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.8...@rxap/plugin-localazy@16.1.0-dev.9) (2023-09-07)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.7...@rxap/plugin-localazy@16.1.0-dev.8) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.6...@rxap/plugin-localazy@16.1.0-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-localazy

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.5...@rxap/plugin-localazy@16.1.0-dev.6) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.4...@rxap/plugin-localazy@16.1.0-dev.5) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.3...@rxap/plugin-localazy@16.1.0-dev.4) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.2...@rxap/plugin-localazy@16.1.0-dev.3) (2023-08-06)

### Bug Fixes

- expose generators as schematics ([679ca36](https://gitlab.com/rxap/packages/commit/679ca36d3712a11e4dc838762bca2f7c471e1e04))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.1...@rxap/plugin-localazy@16.1.0-dev.2) (2023-08-05)

### Bug Fixes

- remove this access ([9aeb654](https://gitlab.com/rxap/packages/commit/9aeb654939de369efb0945c43cb9bc1255f665a3))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.1.0-dev.0...@rxap/plugin-localazy@16.1.0-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- remove unnecessary deployment tier ([53b4790](https://gitlab.com/rxap/packages/commit/53b4790aad58644e40b65d6ef2967fcc73c94462))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@16.0.1-dev.0...@rxap/plugin-localazy@16.1.0-dev.0) (2023-08-04)

### Bug Fixes

- add missing job tags ([3f6790b](https://gitlab.com/rxap/packages/commit/3f6790be7da03e1f73247419e3336bdfb93613d5))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))
- remove nx dependency ([5cc2200](https://gitlab.com/rxap/packages/commit/5cc2200ca3f12ef39bb959f98730975978b5194e))
- run gitlab ci generators on application init ([9a15981](https://gitlab.com/rxap/packages/commit/9a15981fd5b573db47259014b2582373867179f2))

### Features

- **gitlab-ci:** add generator ([43d2208](https://gitlab.com/rxap/packages/commit/43d220863c73eed488f044a1aab72587aa2fc905))

## 16.0.1-dev.0 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))
- update package dependency versions ([45bd022](https://gitlab.com/rxap/packages/commit/45bd022d755c0c11f7d0bcc76d26b39928007941))

## [1.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-localazy@1.0.1-dev.0...@rxap/plugin-localazy@1.0.1-dev.1) (2023-07-10)

### Bug Fixes

- update package dependency versions ([8479f5c](https://gitlab.com/rxap/packages/commit/8479f5c405a885cc0f300cec6156584e4c65d59c))

## 1.0.1-dev.0 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
