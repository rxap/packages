# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.5-dev.2...@rxap/plugin-cypress@20.0.5-dev.3) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.5-dev.1...@rxap/plugin-cypress@20.0.5-dev.2) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.5-dev.0...@rxap/plugin-cypress@20.0.5-dev.1) (2025-03-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4...@rxap/plugin-cypress@20.0.5-dev.0) (2025-03-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4-dev.4...@rxap/plugin-cypress@20.0.4) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4-dev.3...@rxap/plugin-cypress@20.0.4-dev.4) (2025-02-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4-dev.2...@rxap/plugin-cypress@20.0.4-dev.3) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4-dev.1...@rxap/plugin-cypress@20.0.4-dev.2) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.4-dev.0...@rxap/plugin-cypress@20.0.4-dev.1) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3...@rxap/plugin-cypress@20.0.4-dev.0) (2025-02-25)

### Bug Fixes

- check buildable project by tsconfig ([260be75](https://gitlab.com/rxap/packages/commit/260be754f55a446fd4df6a6f97d272d0e704ae19))

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.6...@rxap/plugin-cypress@20.0.3) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.5...@rxap/plugin-cypress@20.0.3-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.4...@rxap/plugin-cypress@20.0.3-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.3...@rxap/plugin-cypress@20.0.3-dev.4) (2025-02-19)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.2...@rxap/plugin-cypress@20.0.3-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.1...@rxap/plugin-cypress@20.0.3-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.3-dev.0...@rxap/plugin-cypress@20.0.3-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.2...@rxap/plugin-cypress@20.0.3-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1...@rxap/plugin-cypress@20.0.2) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.22...@rxap/plugin-cypress@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.22](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.21...@rxap/plugin-cypress@20.0.1-dev.22) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.20...@rxap/plugin-cypress@20.0.1-dev.21) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.19...@rxap/plugin-cypress@20.0.1-dev.20) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.18...@rxap/plugin-cypress@20.0.1-dev.19) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.17...@rxap/plugin-cypress@20.0.1-dev.18) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.16...@rxap/plugin-cypress@20.0.1-dev.17) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.15...@rxap/plugin-cypress@20.0.1-dev.16) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.14...@rxap/plugin-cypress@20.0.1-dev.15) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.13...@rxap/plugin-cypress@20.0.1-dev.14) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.12...@rxap/plugin-cypress@20.0.1-dev.13) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.11...@rxap/plugin-cypress@20.0.1-dev.12) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.10...@rxap/plugin-cypress@20.0.1-dev.11) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.9...@rxap/plugin-cypress@20.0.1-dev.10) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.8...@rxap/plugin-cypress@20.0.1-dev.9) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.7...@rxap/plugin-cypress@20.0.1-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.6...@rxap/plugin-cypress@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.5...@rxap/plugin-cypress@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.4...@rxap/plugin-cypress@20.0.1-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.3...@rxap/plugin-cypress@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.2...@rxap/plugin-cypress@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.1...@rxap/plugin-cypress@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.1-dev.0...@rxap/plugin-cypress@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-cypress

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0...@rxap/plugin-cypress@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-cypress

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0-dev.5...@rxap/plugin-cypress@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-cypress

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0-dev.4...@rxap/plugin-cypress@20.0.0-dev.5) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-cypress

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0-dev.3...@rxap/plugin-cypress@20.0.0-dev.4) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-cypress

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0-dev.2...@rxap/plugin-cypress@20.0.0-dev.3) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-cypress

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@20.0.0-dev.1...@rxap/plugin-cypress@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.11-dev.1...@rxap/plugin-cypress@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.11-dev.0...@rxap/plugin-cypress@19.0.11-dev.1) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.10...@rxap/plugin-cypress@19.0.11-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.10-dev.1...@rxap/plugin-cypress@19.0.10) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.10-dev.0...@rxap/plugin-cypress@19.0.10-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9...@rxap/plugin-cypress@19.0.10-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.5...@rxap/plugin-cypress@19.0.9) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.4...@rxap/plugin-cypress@19.0.9-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.3...@rxap/plugin-cypress@19.0.9-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.2...@rxap/plugin-cypress@19.0.9-dev.3) (2024-10-22)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.1...@rxap/plugin-cypress@19.0.9-dev.2) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.9-dev.0...@rxap/plugin-cypress@19.0.9-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.8...@rxap/plugin-cypress@19.0.9-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.8-dev.2...@rxap/plugin-cypress@19.0.8) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.8-dev.1...@rxap/plugin-cypress@19.0.8-dev.2) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.8-dev.0...@rxap/plugin-cypress@19.0.8-dev.1) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7...@rxap/plugin-cypress@19.0.8-dev.0) (2024-08-27)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.8...@rxap/plugin-cypress@19.0.7) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.7...@rxap/plugin-cypress@19.0.7-dev.8) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.6...@rxap/plugin-cypress@19.0.7-dev.7) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.5...@rxap/plugin-cypress@19.0.7-dev.6) (2024-08-19)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.4...@rxap/plugin-cypress@19.0.7-dev.5) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.3...@rxap/plugin-cypress@19.0.7-dev.4) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.2...@rxap/plugin-cypress@19.0.7-dev.3) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.1...@rxap/plugin-cypress@19.0.7-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.7-dev.0...@rxap/plugin-cypress@19.0.7-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.6...@rxap/plugin-cypress@19.0.7-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.6-dev.0...@rxap/plugin-cypress@19.0.6) (2024-08-05)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5...@rxap/plugin-cypress@19.0.6-dev.0) (2024-08-05)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.14...@rxap/plugin-cypress@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.13...@rxap/plugin-cypress@19.0.5-dev.14) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.12...@rxap/plugin-cypress@19.0.5-dev.13) (2024-07-26)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.11...@rxap/plugin-cypress@19.0.5-dev.12) (2024-07-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.10...@rxap/plugin-cypress@19.0.5-dev.11) (2024-07-24)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.9...@rxap/plugin-cypress@19.0.5-dev.10) (2024-07-24)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.8...@rxap/plugin-cypress@19.0.5-dev.9) (2024-07-22)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.7...@rxap/plugin-cypress@19.0.5-dev.8) (2024-07-10)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.6...@rxap/plugin-cypress@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.5...@rxap/plugin-cypress@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.4...@rxap/plugin-cypress@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.3...@rxap/plugin-cypress@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.2...@rxap/plugin-cypress@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.1...@rxap/plugin-cypress@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.5-dev.0...@rxap/plugin-cypress@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.4...@rxap/plugin-cypress@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.4-dev.0...@rxap/plugin-cypress@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3...@rxap/plugin-cypress@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.12...@rxap/plugin-cypress@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.11...@rxap/plugin-cypress@19.0.3-dev.12) (2024-06-27)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.10...@rxap/plugin-cypress@19.0.3-dev.11) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.9...@rxap/plugin-cypress@19.0.3-dev.10) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.8...@rxap/plugin-cypress@19.0.3-dev.9) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.7...@rxap/plugin-cypress@19.0.3-dev.8) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.6...@rxap/plugin-cypress@19.0.3-dev.7) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.5...@rxap/plugin-cypress@19.0.3-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.4...@rxap/plugin-cypress@19.0.3-dev.5) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.3...@rxap/plugin-cypress@19.0.3-dev.4) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.2...@rxap/plugin-cypress@19.0.3-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.1...@rxap/plugin-cypress@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.3-dev.0...@rxap/plugin-cypress@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2...@rxap/plugin-cypress@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.13...@rxap/plugin-cypress@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.12...@rxap/plugin-cypress@19.0.2-dev.13) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.11...@rxap/plugin-cypress@19.0.2-dev.12) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.10...@rxap/plugin-cypress@19.0.2-dev.11) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.9...@rxap/plugin-cypress@19.0.2-dev.10) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.8...@rxap/plugin-cypress@19.0.2-dev.9) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.7...@rxap/plugin-cypress@19.0.2-dev.8) (2024-06-17)

### Bug Fixes

- add nx packages to workspace ([18c54bb](https://gitlab.com/rxap/packages/commit/18c54bbf7fe4cbe994fafa35e9eb5356b74c61e2))

## [19.0.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.6...@rxap/plugin-cypress@19.0.2-dev.7) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.5...@rxap/plugin-cypress@19.0.2-dev.6) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.4...@rxap/plugin-cypress@19.0.2-dev.5) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.3...@rxap/plugin-cypress@19.0.2-dev.4) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.2...@rxap/plugin-cypress@19.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.1...@rxap/plugin-cypress@19.0.2-dev.2) (2024-06-17)

### Bug Fixes

- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.2-dev.0...@rxap/plugin-cypress@19.0.2-dev.1) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.1...@rxap/plugin-cypress@19.0.2-dev.0) (2024-06-04)

### Bug Fixes

- workflow issues ([ca5317e](https://gitlab.com/rxap/packages/commit/ca5317ea32743f55f8e22815da782f20d63f5e34))

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@19.0.1-dev.0...@rxap/plugin-cypress@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@18.0.1...@rxap/plugin-cypress@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-cypress

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@18.0.1-dev.0...@rxap/plugin-cypress@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@17.0.1...@rxap/plugin-cypress@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@17.0.1-dev.0...@rxap/plugin-cypress@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.2...@rxap/plugin-cypress@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-cypress

## [16.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.2-dev.0...@rxap/plugin-cypress@16.1.2) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [16.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.1...@rxap/plugin-cypress@16.1.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-cypress

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.1-dev.1...@rxap/plugin-cypress@16.1.1) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-cypress

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.1-dev.0...@rxap/plugin-cypress@16.1.1-dev.1) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-cypress

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.0...@rxap/plugin-cypress@16.1.1-dev.0) (2024-05-16)

### Bug Fixes

- add missing schematics exports ([15cf180](https://gitlab.com/rxap/packages/commit/15cf18040ac81e9020750faae7eaaf8ccb3d0eb2))

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-cypress@16.1.0-dev.0...@rxap/plugin-cypress@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-cypress

# 16.1.0-dev.0 (2024-04-12)

### Features

- init the cypress application project ([343bd42](https://gitlab.com/rxap/packages/commit/343bd424b3a6566fcd756290008e603d3a3625ae))
