export interface FixImplicitInternalDependenciesGeneratorSchema {
  projects?: Array<string>;
  project?: string;
}
