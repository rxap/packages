export interface CiInfoExecutorSchema {
  branch?: string;
  tag?: string;
  release?: string;
  commit?: string;
  timestamp?: string;
}
