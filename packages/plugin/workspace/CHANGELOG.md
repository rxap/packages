# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.1.1-dev.0...@rxap/plugin-workspace@20.1.1-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.1.0...@rxap/plugin-workspace@20.1.1-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-workspace

# [20.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.1.0-dev.0...@rxap/plugin-workspace@20.1.0) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-workspace

# [20.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.3-dev.1...@rxap/plugin-workspace@20.1.0-dev.0) (2025-02-28)

### Bug Fixes

- **workspace:** pass options to addPackageDependencies ([f3490c7](https://gitlab.com/rxap/packages/commit/f3490c72404e0f8abf6b4f1a00a5079f91ca142f))

### Features

- **init:** add optional husky integration ([dc71571](https://gitlab.com/rxap/packages/commit/dc715713293fefc3f85fba34cf684fe40dd92f48))

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.3-dev.0...@rxap/plugin-workspace@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2...@rxap/plugin-workspace@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.6...@rxap/plugin-workspace@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.5...@rxap/plugin-workspace@20.0.2-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.4...@rxap/plugin-workspace@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.3...@rxap/plugin-workspace@20.0.2-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.2...@rxap/plugin-workspace@20.0.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.1...@rxap/plugin-workspace@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.2-dev.0...@rxap/plugin-workspace@20.0.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1...@rxap/plugin-workspace@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.15...@rxap/plugin-workspace@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.14...@rxap/plugin-workspace@20.0.1-dev.15) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.13...@rxap/plugin-workspace@20.0.1-dev.14) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.12...@rxap/plugin-workspace@20.0.1-dev.13) (2025-02-11)

### Bug Fixes

- ignore vcs.xml file ([31f85a7](https://gitlab.com/rxap/packages/commit/31f85a7b103e5ab8dba2369316b40ce7bcb7f6dc))

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.11...@rxap/plugin-workspace@20.0.1-dev.12) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.10...@rxap/plugin-workspace@20.0.1-dev.11) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.9...@rxap/plugin-workspace@20.0.1-dev.10) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.8...@rxap/plugin-workspace@20.0.1-dev.9) (2025-01-29)

### Bug Fixes

- remove deprecated versio number ([bb71998](https://gitlab.com/rxap/packages/commit/bb7199872389b9ffc13cf55ad912b56699304b27))

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.7...@rxap/plugin-workspace@20.0.1-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.6...@rxap/plugin-workspace@20.0.1-dev.7) (2025-01-29)

### Bug Fixes

- add docker compose target to workspace root ([f895515](https://gitlab.com/rxap/packages/commit/f895515d97197771908e47e508c0997c7fb93895))

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.5...@rxap/plugin-workspace@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.4...@rxap/plugin-workspace@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.3...@rxap/plugin-workspace@20.0.1-dev.4) (2025-01-28)

### Bug Fixes

- ensure the default project is set ([5d1610d](https://gitlab.com/rxap/packages/commit/5d1610d33ad409400bc93a99b38b88d0ec46f368))
- use correct workspace root check ([4d0c0ed](https://gitlab.com/rxap/packages/commit/4d0c0ed5e1f3bd9db2866162476a290028f70b33))

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.2...@rxap/plugin-workspace@20.0.1-dev.3) (2025-01-28)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))
- support workspace plugin ([13c4683](https://gitlab.com/rxap/packages/commit/13c46832bb240efa7e22be6a895932caf3da025d))

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.1...@rxap/plugin-workspace@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.1-dev.0...@rxap/plugin-workspace@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-workspace

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.0...@rxap/plugin-workspace@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-workspace

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.0-dev.4...@rxap/plugin-workspace@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-workspace

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.0-dev.3...@rxap/plugin-workspace@20.0.0-dev.4) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-workspace

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.0-dev.2...@rxap/plugin-workspace@20.0.0-dev.3) (2025-01-03)

### Bug Fixes

- use correct entry point name ([69e7e56](https://gitlab.com/rxap/packages/commit/69e7e56f773afe06c3716784898f45d3dcb44c16))
- use nx release instead of lerna ([411635a](https://gitlab.com/rxap/packages/commit/411635ae1a5370f162433701e960f48339726261))

### Features

- support fullstack workspaces ([b0a2eb5](https://gitlab.com/rxap/packages/commit/b0a2eb5555e402700751c585ce3d36ff7e1dc5ca))

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@20.0.0-dev.1...@rxap/plugin-workspace@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.9-dev.0...@rxap/plugin-workspace@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.8...@rxap/plugin-workspace@19.1.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.8-dev.1...@rxap/plugin-workspace@19.1.8) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.8-dev.0...@rxap/plugin-workspace@19.1.8-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.7...@rxap/plugin-workspace@19.1.8-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.7-dev.3...@rxap/plugin-workspace@19.1.7) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.7-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.7-dev.2...@rxap/plugin-workspace@19.1.7-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.7-dev.1...@rxap/plugin-workspace@19.1.7-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.7-dev.0...@rxap/plugin-workspace@19.1.7-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.6...@rxap/plugin-workspace@19.1.7-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.6-dev.3...@rxap/plugin-workspace@19.1.6) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.6-dev.2...@rxap/plugin-workspace@19.1.6-dev.3) (2024-09-11)

### Bug Fixes

- remove force http upgrade ([7046943](https://gitlab.com/rxap/packages/commit/7046943d50885181643604170a70c9758e386ac7))

## [19.1.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.6-dev.1...@rxap/plugin-workspace@19.1.6-dev.2) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.6-dev.0...@rxap/plugin-workspace@19.1.6-dev.1) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5...@rxap/plugin-workspace@19.1.6-dev.0) (2024-08-27)

### Bug Fixes

- ignore firebase cache ([a33aed9](https://gitlab.com/rxap/packages/commit/a33aed9ecec6eb1c80e745ef01698ebf6cb0c484))

## [19.1.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5-dev.4...@rxap/plugin-workspace@19.1.5) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5-dev.3...@rxap/plugin-workspace@19.1.5-dev.4) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5-dev.2...@rxap/plugin-workspace@19.1.5-dev.3) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5-dev.1...@rxap/plugin-workspace@19.1.5-dev.2) (2024-08-21)

### Bug Fixes

- ignore data source storage ([d00fd3a](https://gitlab.com/rxap/packages/commit/d00fd3ab19111bec6aae6921458148bdb484120f))

## [19.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.5-dev.0...@rxap/plugin-workspace@19.1.5-dev.1) (2024-08-19)

### Bug Fixes

- set lint to quiet by default ([0c734ac](https://gitlab.com/rxap/packages/commit/0c734ac10f807f1a33bb1d9a403807262837e38e))

## [19.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.4...@rxap/plugin-workspace@19.1.5-dev.0) (2024-08-15)

### Bug Fixes

- add missing named import defaults ([d46ce8e](https://gitlab.com/rxap/packages/commit/d46ce8eca1ba45f93cc2f64571dd054b109612b5))
- reduce output ([08e3a02](https://gitlab.com/rxap/packages/commit/08e3a0282cc3f264b39a046b31c87da92c2fc7f7))
- update default nx targets ([43f6151](https://gitlab.com/rxap/packages/commit/43f6151292a10218735e5cf203ec3cb169211530))

## [19.1.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.4-dev.0...@rxap/plugin-workspace@19.1.4) (2024-08-05)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3...@rxap/plugin-workspace@19.1.4-dev.0) (2024-08-05)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.9...@rxap/plugin-workspace@19.1.3) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.8...@rxap/plugin-workspace@19.1.3-dev.9) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.7...@rxap/plugin-workspace@19.1.3-dev.8) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.6...@rxap/plugin-workspace@19.1.3-dev.7) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.5...@rxap/plugin-workspace@19.1.3-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.4...@rxap/plugin-workspace@19.1.3-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.3...@rxap/plugin-workspace@19.1.3-dev.4) (2024-07-03)

### Bug Fixes

- support getting service port from main.ts ([bad904a](https://gitlab.com/rxap/packages/commit/bad904a16d1f6d1ff1f1bbfa88e81ea360ce6179))

## [19.1.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.2...@rxap/plugin-workspace@19.1.3-dev.3) (2024-07-03)

### Bug Fixes

- extract api prefix from build args ([c89cd94](https://gitlab.com/rxap/packages/commit/c89cd945a20f7e2d784bba83c3865ec707e97515))

## [19.1.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.1...@rxap/plugin-workspace@19.1.3-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.3-dev.0...@rxap/plugin-workspace@19.1.3-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.2...@rxap/plugin-workspace@19.1.3-dev.0) (2024-07-01)

### Bug Fixes

- install the rxap cli tool ([415c70e](https://gitlab.com/rxap/packages/commit/415c70e7ef985f1014b81864362a70e34a16ac64))

## [19.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.2-dev.0...@rxap/plugin-workspace@19.1.2) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1...@rxap/plugin-workspace@19.1.2-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.5...@rxap/plugin-workspace@19.1.1) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.4...@rxap/plugin-workspace@19.1.1-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.3...@rxap/plugin-workspace@19.1.1-dev.4) (2024-06-24)

### Bug Fixes

- use dynamic name ([4aa6ac0](https://gitlab.com/rxap/packages/commit/4aa6ac079856c6ecdb52397909ae8a369cf88b24))

## [19.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.2...@rxap/plugin-workspace@19.1.1-dev.3) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.1...@rxap/plugin-workspace@19.1.1-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.1-dev.0...@rxap/plugin-workspace@19.1.1-dev.1) (2024-06-20)

### Bug Fixes

- add html code coverage reporters ([73da1d8](https://gitlab.com/rxap/packages/commit/73da1d85274686590952a97e202c713718988caa))

## [19.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0...@rxap/plugin-workspace@19.1.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-workspace

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.10...@rxap/plugin-workspace@19.1.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-workspace

# [19.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.9...@rxap/plugin-workspace@19.1.0-dev.10) (2024-06-18)

### Bug Fixes

- mfe dev routing ([6d14920](https://gitlab.com/rxap/packages/commit/6d149204b5917badd15563c44cbce6125f8a8bf5))

# [19.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.8...@rxap/plugin-workspace@19.1.0-dev.9) (2024-06-18)

### Bug Fixes

- add the composer package ([49e1b58](https://gitlab.com/rxap/packages/commit/49e1b58fb7b843d8922fbb17e6b740c1b63bdd3a))
- move idea ignores to root ignore ([27181d9](https://gitlab.com/rxap/packages/commit/27181d989998940c5c7800ca638be77c568adf9a))

# [19.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.7...@rxap/plugin-workspace@19.1.0-dev.8) (2024-06-18)

### Bug Fixes

- ensure only valid options are stored ([c9bc9cd](https://gitlab.com/rxap/packages/commit/c9bc9cd0443fc4defc4c5c73cd4cabee63f22d2b))
- ensure only valid options are stored ([c754404](https://gitlab.com/rxap/packages/commit/c75440438fe03e550dad208b46b378aee888a995))

# [19.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.6...@rxap/plugin-workspace@19.1.0-dev.7) (2024-06-17)

### Bug Fixes

- ensure the cert directory exists ([3aebf3c](https://gitlab.com/rxap/packages/commit/3aebf3c510a8fa0acc82e45811f54c9de16d5527))
- update to latest sentry version ([9caf6ce](https://gitlab.com/rxap/packages/commit/9caf6ce42f72b053d1ea779a457fe73dd46dea8b))

# [19.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.5...@rxap/plugin-workspace@19.1.0-dev.6) (2024-06-17)

### Bug Fixes

- add the rxap package ([15399ee](https://gitlab.com/rxap/packages/commit/15399eed87c4444c85af31ac1b61cb286d6dc4b8))
- only merge if file already exists ([9b49189](https://gitlab.com/rxap/packages/commit/9b4918971acd8ff79d90dc0d7fc24a0e2be46273))

# [19.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.4...@rxap/plugin-workspace@19.1.0-dev.5) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-workspace

# [19.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.3...@rxap/plugin-workspace@19.1.0-dev.4) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-workspace

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.2...@rxap/plugin-workspace@19.1.0-dev.3) (2024-06-17)

### Bug Fixes

- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.1.0-dev.1...@rxap/plugin-workspace@19.1.0-dev.2) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-workspace

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.2...@rxap/plugin-workspace@19.1.0-dev.1) (2024-06-02)

### Features

- add preset generator ([dde3853](https://gitlab.com/rxap/packages/commit/dde3853980431d877fd54709f6abd38ff5ee2a3a))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.2...@rxap/plugin-workspace@19.1.0-dev.0) (2024-06-02)

### Features

- add preset generator ([0299580](https://gitlab.com/rxap/packages/commit/02995803e3bcb8f5fecf718312198a35047d067e))

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.2-dev.1...@rxap/plugin-workspace@19.0.2) (2024-06-02)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.1...@rxap/plugin-workspace@19.0.2-dev.1) (2024-06-02)

### Bug Fixes

- ensure the nx-cloud package is installed ([cbfed8c](https://gitlab.com/rxap/packages/commit/cbfed8c63bee9671ef04fe7ac01603c14a08f208))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.1...@rxap/plugin-workspace@19.0.2-dev.0) (2024-05-31)

### Bug Fixes

- ensure the nx-cloud package is installed ([cbfed8c](https://gitlab.com/rxap/packages/commit/cbfed8c63bee9671ef04fe7ac01603c14a08f208))

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@19.0.1-dev.0...@rxap/plugin-workspace@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@18.0.1...@rxap/plugin-workspace@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-workspace

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@18.0.1-dev.0...@rxap/plugin-workspace@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@17.0.1...@rxap/plugin-workspace@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@17.0.1-dev.0...@rxap/plugin-workspace@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-workspace

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.3.0...@rxap/plugin-workspace@17.0.1-dev.0) (2024-05-29)

### Bug Fixes

- merge instead of overwrite ([e249167](https://gitlab.com/rxap/packages/commit/e249167172681e447b889048959286e6a0272040))

# [16.3.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.3.0-dev.2...@rxap/plugin-workspace@16.3.0) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.3.0-dev.1...@rxap/plugin-workspace@16.3.0-dev.2) (2024-05-28)

### Bug Fixes

- use yarn schematic in rxap workspace ([8c6f0b4](https://gitlab.com/rxap/packages/commit/8c6f0b4576696f0b1bcf33adb55410b8d0553f3a))

# [16.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.3.0-dev.0...@rxap/plugin-workspace@16.3.0-dev.1) (2024-05-28)

### Bug Fixes

- use the correct package ([25f44b4](https://gitlab.com/rxap/packages/commit/25f44b4ec4070cf39a961203c6809992f22fa0a8))

# [16.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2...@rxap/plugin-workspace@16.3.0-dev.0) (2024-05-28)

### Features

- automatically determine the repository url ([2be6e3b](https://gitlab.com/rxap/packages/commit/2be6e3b9bb0b0940193b07715e8e189fdd055e2d))

## [16.2.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.6...@rxap/plugin-workspace@16.2.2) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-workspace

## [16.2.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.5...@rxap/plugin-workspace@16.2.2-dev.6) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-workspace

## [16.2.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.4...@rxap/plugin-workspace@16.2.2-dev.5) (2024-05-22)

### Bug Fixes

- add missing ignore entry ([dc1db8d](https://gitlab.com/rxap/packages/commit/dc1db8d129a46da76e04a66822b85f7bdb786a6d))

## [16.2.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.3...@rxap/plugin-workspace@16.2.2-dev.4) (2024-05-16)

### Bug Fixes

- add missing ignore entry ([5b1ceb8](https://gitlab.com/rxap/packages/commit/5b1ceb833bfea3070baccb93b7e011a46e350c60))

## [16.2.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.2...@rxap/plugin-workspace@16.2.2-dev.3) (2024-05-16)

### Bug Fixes

- ignore idea utilities ([cbc7690](https://gitlab.com/rxap/packages/commit/cbc7690538ee2d873bfa8e963dec009d920c4be0))

## [16.2.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.1...@rxap/plugin-workspace@16.2.2-dev.2) (2024-05-07)

### Bug Fixes

- add missing default gitignore entries ([6286fa4](https://gitlab.com/rxap/packages/commit/6286fa463c3d468964b128de2b8775002cee6266))

## [16.2.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.2-dev.0...@rxap/plugin-workspace@16.2.2-dev.1) (2024-05-04)

### Bug Fixes

- overwrite default nx readme ([064832c](https://gitlab.com/rxap/packages/commit/064832c8a142e65d8c399270ff07600932547c77))

## [16.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.1...@rxap/plugin-workspace@16.2.2-dev.0) (2024-04-18)

### Bug Fixes

- add prompt to repository url option ([254eb21](https://gitlab.com/rxap/packages/commit/254eb21da369af3904454511371a03d5b50fc3ac))
- add utility script ([da3f548](https://gitlab.com/rxap/packages/commit/da3f5488bdfcddca103bbd2b1113d4dc5aff5aea))

## [16.2.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.1-dev.0...@rxap/plugin-workspace@16.2.1) (2024-04-18)

**Note:** Version bump only for package @rxap/plugin-workspace

## [16.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0...@rxap/plugin-workspace@16.2.1-dev.0) (2024-04-17)

### Bug Fixes

- add rxap utility scripts to workspace package json ([3b866cf](https://gitlab.com/rxap/packages/commit/3b866cf72b8d96364544bd4180123a755cdab55e))

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.18...@rxap/plugin-workspace@16.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.2.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.17...@rxap/plugin-workspace@16.2.0-dev.18) (2024-04-17)

### Bug Fixes

- skip projects that do not have an output ([338cd19](https://gitlab.com/rxap/packages/commit/338cd19cddc62e4062441f07ea8feb74c06f0dd9))
- skip projects that do not have an output ([f8362c3](https://gitlab.com/rxap/packages/commit/f8362c3b8c30ef0b5b80770a44a213cf9e13cfe6))

# [16.2.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.16...@rxap/plugin-workspace@16.2.0-dev.17) (2024-04-16)

### Bug Fixes

- ensure the angular.json is created ([bd10e36](https://gitlab.com/rxap/packages/commit/bd10e366481f2353da8f48d3428f0e4f2838a4eb))

# [16.2.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.15...@rxap/plugin-workspace@16.2.0-dev.16) (2024-04-15)

### Bug Fixes

- add a empty angular.json config file ([98524fd](https://gitlab.com/rxap/packages/commit/98524fdc8e720029ac89cb5037e0c79d98e64e84))
- coerce file .prettierrc ([7375540](https://gitlab.com/rxap/packages/commit/73755400ff9335b50fefde1f2adc52cbf67ba259))

# [16.2.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.14...@rxap/plugin-workspace@16.2.0-dev.15) (2024-04-12)

### Bug Fixes

- add documentation.json to default gitignore ([be61f74](https://gitlab.com/rxap/packages/commit/be61f7419af7c7be6ce6e33c2fd49cbd59612bfb))
- cleanup the workspace tools project ([ebe5841](https://gitlab.com/rxap/packages/commit/ebe58411f1d296e29f004a3c46acc949e86d47b9))
- serialize schematic options ([258a9b4](https://gitlab.com/rxap/packages/commit/258a9b49311264fe1caab5c8f4f759735b0ab904))
- support license option none ([fd23b06](https://gitlab.com/rxap/packages/commit/fd23b06a644a0eaf419320cb521d73853a9e2cab))

### Features

- coerce the workspace tools project ([a810a2f](https://gitlab.com/rxap/packages/commit/a810a2fea5225e2bbefada1d81ce542c475df2e1))

# [16.2.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.13...@rxap/plugin-workspace@16.2.0-dev.14) (2024-04-10)

### Bug Fixes

- coerce prettier config ([d19907f](https://gitlab.com/rxap/packages/commit/d19907f378a8ca6d49b337baf28460808fc43b5f))

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

# [16.2.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.12...@rxap/plugin-workspace@16.2.0-dev.13) (2024-04-10)

### Bug Fixes

- add missing path prefix ([d85f512](https://gitlab.com/rxap/packages/commit/d85f5123086b60b47c94921672519d9a8f46e2dd))

# [16.2.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.11...@rxap/plugin-workspace@16.2.0-dev.12) (2024-04-10)

### Bug Fixes

- support custom router rule for mfe application ([e317ea1](https://gitlab.com/rxap/packages/commit/e317ea1ba77c5df86b4037d1dee9d3594d381bba))

# [16.2.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.10...@rxap/plugin-workspace@16.2.0-dev.11) (2024-04-10)

### Bug Fixes

- set default prefix ([6d24e50](https://gitlab.com/rxap/packages/commit/6d24e50ce7e0126ed946b4fb53c308868d1bd9a8))

# [16.2.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.9...@rxap/plugin-workspace@16.2.0-dev.10) (2024-04-09)

### Features

- add the traefik middleware option ([c3dce9d](https://gitlab.com/rxap/packages/commit/c3dce9d5417d8d71f8c72fe60b83ecef402c09cb))

# [16.2.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.8...@rxap/plugin-workspace@16.2.0-dev.9) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.2.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.7...@rxap/plugin-workspace@16.2.0-dev.8) (2024-04-07)

### Bug Fixes

- set default package manager ([628ad09](https://gitlab.com/rxap/packages/commit/628ad09b0b9fb48665b57502859af5986cde9d9b))

# [16.2.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.6...@rxap/plugin-workspace@16.2.0-dev.7) (2024-04-07)

### Features

- merge with existing traefik config ([34c646a](https://gitlab.com/rxap/packages/commit/34c646a61a49ddfdb1c3d1226963d67e8f054013))
- support mit license preset ([da1aaad](https://gitlab.com/rxap/packages/commit/da1aaad4213c67ad13ad29d989973c04109e7042))

# [16.2.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.5...@rxap/plugin-workspace@16.2.0-dev.6) (2024-04-03)

### Features

- add dev container initialization ([b0f0518](https://gitlab.com/rxap/packages/commit/b0f051894b581593ea1735c7864691e50a75de92))

# [16.2.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.4...@rxap/plugin-workspace@16.2.0-dev.5) (2024-03-31)

### Bug Fixes

- add initial package json properties ([3788575](https://gitlab.com/rxap/packages/commit/378857514fd8b72176b9e8705010bb2765a348bf))
- add junit gitignore ([3d17f3c](https://gitlab.com/rxap/packages/commit/3d17f3c7363dfb9bff1749e2fbdad8c4180696b5))
- add missing default git ignore entries ([342cdc3](https://gitlab.com/rxap/packages/commit/342cdc3f6c8fcf53b9a68d95d940293dad1f528f))
- remove direct dependency to library and application plugin ([22c7834](https://gitlab.com/rxap/packages/commit/22c7834d292da1b3c1c8b0222b6d77c0bcd2ba67))
- restructure tool scripts ([e156546](https://gitlab.com/rxap/packages/commit/e156546c06dfb8f611c6a23dbbcd8f27f87e1ec2))

### Features

- add rename generator ([b580b45](https://gitlab.com/rxap/packages/commit/b580b4579f68f36c36d1668dab3f6642e7fcbdae))

# [16.2.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.3...@rxap/plugin-workspace@16.2.0-dev.4) (2024-03-26)

### Bug Fixes

- exclude standalone project from dev docker compose ([d3740d2](https://gitlab.com/rxap/packages/commit/d3740d2ce546927fed34597c07f1d96ca9a020af))

# [16.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.2...@rxap/plugin-workspace@16.2.0-dev.3) (2024-03-23)

### Bug Fixes

- add default workspace test configuration ([e42e0da](https://gitlab.com/rxap/packages/commit/e42e0da205f285d61e4f025bb6ae392ff62db769))
- add missing outputs prefix ([f046fc7](https://gitlab.com/rxap/packages/commit/f046fc72f2982bd6f8a6ff8e872de1f5cc324f09))
- ensure the proper license is set ([a476a16](https://gitlab.com/rxap/packages/commit/a476a16f59721d326cb6f1e3fdb9aca99b62abd3))

### Features

- support standalone workspaces ([bb75b98](https://gitlab.com/rxap/packages/commit/bb75b98bec2bd07107909755196b1ad234933527))

# [16.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.1...@rxap/plugin-workspace@16.2.0-dev.2) (2024-03-15)

### Bug Fixes

- disable buildable libraries by default ([a1f77d2](https://gitlab.com/rxap/packages/commit/a1f77d2fbbc83842b83fd69e2dbf8ee40ed7c218))

# [16.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.2.0-dev.0...@rxap/plugin-workspace@16.2.0-dev.1) (2024-03-14)

### Bug Fixes

- set nest library publishable default to false ([d478d10](https://gitlab.com/rxap/packages/commit/d478d10faf2e3bbc96f00c97eb3709eb956231e1))

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.1-dev.0...@rxap/plugin-workspace@16.2.0-dev.0) (2024-03-11)

### Features

- add generator fix-implicit-internal-dependencies ([4a3eb8c](https://gitlab.com/rxap/packages/commit/4a3eb8c143c8a78598bba7e974eb12662e7734ed))

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0...@rxap/plugin-workspace@16.1.1-dev.0) (2024-03-11)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.45...@rxap/plugin-workspace@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.1.0-dev.45](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.44...@rxap/plugin-workspace@16.1.0-dev.45) (2024-01-11)

### Bug Fixes

- ensure the service path prefix has '/' as suffix ([48012ac](https://gitlab.com/rxap/packages/commit/48012ac8dc191bdbc1000f16990ff866aa43826a))

# [16.1.0-dev.44](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.43...@rxap/plugin-workspace@16.1.0-dev.44) (2023-10-30)

### Bug Fixes

- use subdomain build for service router rule construction ([bdc8eca](https://gitlab.com/rxap/packages/commit/bdc8ecaf2ffb648e36de6654873092637b2c4bff))
- use subdomain from docker args config ([6053cc9](https://gitlab.com/rxap/packages/commit/6053cc9e837b040a4566abb2226a7eb64fff9a0e))

# [16.1.0-dev.43](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.42...@rxap/plugin-workspace@16.1.0-dev.43) (2023-10-30)

### Bug Fixes

- use subdomain from docker args config ([b1b8f11](https://gitlab.com/rxap/packages/commit/b1b8f11bfc4dfa648fdcdebf2653a0735bb2cb81))

# [16.1.0-dev.42](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.41...@rxap/plugin-workspace@16.1.0-dev.42) (2023-10-30)

### Bug Fixes

- use subdomain from docker args config ([d0035e6](https://gitlab.com/rxap/packages/commit/d0035e659e128c5c310e0c077551c370e9e31fce))

# [16.1.0-dev.41](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.40...@rxap/plugin-workspace@16.1.0-dev.41) (2023-10-30)

### Bug Fixes

- load .env file for docker services ([94d9256](https://gitlab.com/rxap/packages/commit/94d92569fc259e4aff2b1ae4ec064a2eb6ff51d3))

# [16.1.0-dev.40](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.39...@rxap/plugin-workspace@16.1.0-dev.40) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.1.0-dev.39](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.38...@rxap/plugin-workspace@16.1.0-dev.39) (2023-10-11)

### Bug Fixes

- ensure yarn cache is not included in git versions ([eab3e41](https://gitlab.com/rxap/packages/commit/eab3e411a405869fd0e139ee16e061d11b2b5933))

# [16.1.0-dev.38](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.37...@rxap/plugin-workspace@16.1.0-dev.38) (2023-10-11)

### Bug Fixes

- ask what kind of workspace to init ([d16f70f](https://gitlab.com/rxap/packages/commit/d16f70f6440fabff712d9dc2832bfd9b2907fe12))
- remove git lfs support ([08c8a0f](https://gitlab.com/rxap/packages/commit/08c8a0fe4c3fb1a230354eb63be62b285b1af6ca))

# 16.1.0-dev.37 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add missing initial workspace files ([dbb5494](https://gitlab.com/rxap/packages/commit/dbb54941ad5dbfde267fb27767ded9ca5c16a4cc))
- add missing project config update ([5afe381](https://gitlab.com/rxap/packages/commit/5afe381ee15013b4646f0eb99d236c0a8d5f95de))
- add skip-projects flag ([e1f31ed](https://gitlab.com/rxap/packages/commit/e1f31ed837646f605ced82a52749e62af07ba939))
- add yarn rc file ([c40f569](https://gitlab.com/rxap/packages/commit/c40f569fe548fa4c58658f2f76be2bca24add650))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- coerce workspace project ([f8ceba0](https://gitlab.com/rxap/packages/commit/f8ceba021126b34c8e5b11ce22c0d7e04f9df734))
- ensure overwrite option is passed to sub schematics ([0c8a19b](https://gitlab.com/rxap/packages/commit/0c8a19b5166f804aa335f739a00a5415bd97f61a))
- ensure the commitlint packages is installed ([09e76c6](https://gitlab.com/rxap/packages/commit/09e76c68dd6648b429ad60c13540e49d3b38f98c))
- ensure the environment name is set to development for each service ([1293921](https://gitlab.com/rxap/packages/commit/1293921584a1a6779b3c7990ad12aa878841e374))
- ensure the husky package is installed ([ecc7c45](https://gitlab.com/rxap/packages/commit/ecc7c455c4c14269e70401ac34e50e8378981b68))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- expose generators as schematics ([8a58d07](https://gitlab.com/rxap/packages/commit/8a58d07c2f1dcfff75e724a418d7c3bddb2d0bbc))
- generate traefik default certs ([eda4fd9](https://gitlab.com/rxap/packages/commit/eda4fd9a595b4a4de352c85330cf86e4d841d428))
- in application workspace the tailwind is disabled for libraries per default ([fe56263](https://gitlab.com/rxap/packages/commit/fe562639560216a57df8af8b8e8c1968eb487a41))
- include missing domains ([ea906e1](https://gitlab.com/rxap/packages/commit/ea906e196450f79fc095b374b6e5d1f1593d3b71))
- init nx json configurations ([4c14f56](https://gitlab.com/rxap/packages/commit/4c14f56da8bae5971165cbff2269d86fa9951629))
- install yarn 3 in the before script ([d547fda](https://gitlab.com/rxap/packages/commit/d547fdac24a8ffae5f88cd67cee340330c1b48c3))
- introduce more Is\*Project functions ([41a3713](https://gitlab.com/rxap/packages/commit/41a3713e2965f46900e80902a455b62e08686989))
- mount custom service settings from detected folder ([cfed00e](https://gitlab.com/rxap/packages/commit/cfed00ef930a6dfe9037a522df89a3bc4dc744fe))
- move nx parallel parameter to run all utility ([52c178a](https://gitlab.com/rxap/packages/commit/52c178acf16259f4ad5a1c31ace6c1feb45106b8))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- refactor the build.json concept ([3526821](https://gitlab.com/rxap/packages/commit/3526821aecd59e92ba9d5f2c6d9001dc936d007f))
- relative path issue and spelling ([beb55a7](https://gitlab.com/rxap/packages/commit/beb55a74704638dbd018ba6b6db7cb83a7e56333))
- remove application specific generator steps ([858fc3a](https://gitlab.com/rxap/packages/commit/858fc3acdaa0c5afbb88212800e7e2f4debba58d))
- remove error message ([b2f4974](https://gitlab.com/rxap/packages/commit/b2f49746e723a3fdce3cc956d595600c6f6d9e86))
- remove explicit domain ([ba3037a](https://gitlab.com/rxap/packages/commit/ba3037afcfe1d07a23216e12e262b573056b1241))
- remove m2v dependency ([6a57b28](https://gitlab.com/rxap/packages/commit/6a57b286c31aeb8d154f5e9b54c049a98dcecb23))
- remove minio container name ([996fc36](https://gitlab.com/rxap/packages/commit/996fc3689ade5d112eac091a84c1aa49c879915e))
- remove redundant command parameters ([b3e474b](https://gitlab.com/rxap/packages/commit/b3e474b668f477d0194c8ae9ee847a13bdb35776))
- remove redundant traefik labels ([62c2625](https://gitlab.com/rxap/packages/commit/62c2625a5f6d77cb0f327e5fc46a8ea6e5c99a03))
- remove unnecessary deployment tier ([be4c65a](https://gitlab.com/rxap/packages/commit/be4c65a7d1d5dc9c02e6bda8ac6fba6a0c21cd96))
- set default nest and angular directory ([329299a](https://gitlab.com/rxap/packages/commit/329299a4f621a9ec08b4d882078d6013f302628a))
- set explicit root domain ([5f68bd6](https://gitlab.com/rxap/packages/commit/5f68bd6e4e7c5f18cf48124fcf8a9afcf441c076))
- split GuessOutputPath function ([470b93a](https://gitlab.com/rxap/packages/commit/470b93a97a44b11435ff045c79896d712c9721a9))
- split local and remote pull ([8056e92](https://gitlab.com/rxap/packages/commit/8056e927d5e21ff3e7479cd4f310f4b8f31716cd))
- stop all container from all docker compose files ([64f6fcb](https://gitlab.com/rxap/packages/commit/64f6fcb155ce1d87fd40bd0f99dc6351dcf18cdd))
- support any docker compose file name ([b093d10](https://gitlab.com/rxap/packages/commit/b093d10fdaddb1d643231129f6ec0623ace01ea7))
- support docker options from nx.json file ([931ffdc](https://gitlab.com/rxap/packages/commit/931ffdc960f4fb0c92fcbff1a5b1966df7fd074e))
- support local service execution ([5c0dbee](https://gitlab.com/rxap/packages/commit/5c0dbee76504762fa889b892f7c6231dd6db9c73))
- support new nest application structure ([090d742](https://gitlab.com/rxap/packages/commit/090d7422c29dc75d55c3fed6ef5faac8ff6d319b))
- update authentik service init ([e1d61b6](https://gitlab.com/rxap/packages/commit/e1d61b6d0bb3f6d348c79498527b6b08710e90ed))
- update initial workspace files ([fbc17d4](https://gitlab.com/rxap/packages/commit/fbc17d4b4d4ea1a74fe78fe90b93f0f81aa51729))
- update initial workspace files ([5be3709](https://gitlab.com/rxap/packages/commit/5be3709d5b9644958cb44d203225eb720c00a158))
- update service configuration name ([dda304f](https://gitlab.com/rxap/packages/commit/dda304f2de845539a9291c47c6ba3dcac617fdf6))
- update service status name ([595cd07](https://gitlab.com/rxap/packages/commit/595cd07ab522dc1818c6f275e7a526e6b31d63ac))
- use CoerceFilesStructure function ([51f13e0](https://gitlab.com/rxap/packages/commit/51f13e0e261e1a93a95f5a96fe8ec4a0597d4ad3))
- use correct project name ([5b9e432](https://gitlab.com/rxap/packages/commit/5b9e432ed0bd7d73941bd1a82eac70fcdcc7b0a5))
- use function CoerceNxJsonCacheableOperation ([485e598](https://gitlab.com/rxap/packages/commit/485e598e7a1192b5635f6c54dee5349b9d2889c3))
- use registry.gitlab.com as default registry ([41ef6f0](https://gitlab.com/rxap/packages/commit/41ef6f0b1db418abcd8aef09263779ce1ba76724))
- use updated path for health checks ([56dc4cb](https://gitlab.com/rxap/packages/commit/56dc4cbad4bd75e0a6f6cb614ea513af680a9c8f))

### Features

- add ci info executor ([8b63c13](https://gitlab.com/rxap/packages/commit/8b63c13392940b8b2475689a6a767efc9970b083))
- add docker compose generator files ([cb16192](https://gitlab.com/rxap/packages/commit/cb161928f5e60d564814e67fb299b123a1e8339d))
- add docker-compose executor ([54eaaa9](https://gitlab.com/rxap/packages/commit/54eaaa92f7360665bd5fa71e74a263e0d6307d2b))
- add project-target generator ([4cb0462](https://gitlab.com/rxap/packages/commit/4cb0462fc3044cfdcf4fb993b74f7c6ff97aa3c2))
- coerce the lerna.json file ([88e5f9f](https://gitlab.com/rxap/packages/commit/88e5f9fbd8a9309071baa8c9ab8984f1bebfdcc9))
- generate project traefik file ([075dfc5](https://gitlab.com/rxap/packages/commit/075dfc58539ea6c32949de5d7048cf87812a616f))
- **init:** add generator ([b9b7d4d](https://gitlab.com/rxap/packages/commit/b9b7d4ddda7ab62a8ea6b3278dc2736bbf6dd894))
- **init:** create workspace specific files ([17f1056](https://gitlab.com/rxap/packages/commit/17f1056ef761d01a4d9f8733584e4e57a5b3586e))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [16.1.0-dev.36](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.35...@rxap/plugin-workspace@16.1.0-dev.36) (2023-10-09)

### Bug Fixes

- ensure the commitlint packages is installed ([7bcdf87](https://gitlab.com/rxap/packages/commit/7bcdf871633e77fed3b7f092cc3538b958fa0f05))
- ensure the husky package is installed ([9a057c4](https://gitlab.com/rxap/packages/commit/9a057c47ab58f03c1e69eec2cdd2aba50aef4cbc))

# [16.1.0-dev.35](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.34...@rxap/plugin-workspace@16.1.0-dev.35) (2023-10-02)

### Bug Fixes

- use function CoerceNxJsonCacheableOperation ([14f26b0](https://gitlab.com/rxap/packages/commit/14f26b0d679f5d1208a23ae20d6d9f6f4516a60d))

# [16.1.0-dev.34](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.33...@rxap/plugin-workspace@16.1.0-dev.34) (2023-10-02)

### Bug Fixes

- in application workspace the tailwind is disabled for libraries per default ([2bd87fc](https://gitlab.com/rxap/packages/commit/2bd87fc2d697cbf75cf6e7d7f35b50043f2a9737))
- introduce more Is\*Project functions ([8d37211](https://gitlab.com/rxap/packages/commit/8d37211fb1906f90d7176cfcfe43f755f04a0fa6))

# [16.1.0-dev.33](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.32...@rxap/plugin-workspace@16.1.0-dev.33) (2023-10-02)

### Bug Fixes

- ensure the environment name is set to development for each service ([716c311](https://gitlab.com/rxap/packages/commit/716c31102c2b7468c78e04178a56a97ba239335e))

# [16.1.0-dev.32](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.31...@rxap/plugin-workspace@16.1.0-dev.32) (2023-09-27)

### Bug Fixes

- mount custom service settings from detected folder ([ec8e511](https://gitlab.com/rxap/packages/commit/ec8e511da580abf02686a89be660c7624fbae13e))
- remove application specific generator steps ([2a026e0](https://gitlab.com/rxap/packages/commit/2a026e0755d3736caa3daffb83b49368de5b3b6b))
- use CoerceFilesStructure function ([4bc088e](https://gitlab.com/rxap/packages/commit/4bc088eefdc1d2cf8444c395c7d8e648330b7f6f))

### Features

- add docker compose generator files ([b2994bf](https://gitlab.com/rxap/packages/commit/b2994bf52dae8395f114a7a1fd45c8dc6e912fc3))
- coerce the lerna.json file ([6554b3c](https://gitlab.com/rxap/packages/commit/6554b3c223241c7155ee45063a60e787c0037c32))

# [16.1.0-dev.31](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.30...@rxap/plugin-workspace@16.1.0-dev.31) (2023-09-21)

### Bug Fixes

- add skip-projects flag ([0f45987](https://gitlab.com/rxap/packages/commit/0f45987bc9dd927b1ede9eb53256125fa0e33674))

# [16.1.0-dev.30](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.29...@rxap/plugin-workspace@16.1.0-dev.30) (2023-09-21)

### Bug Fixes

- support any docker compose file name ([cd44128](https://gitlab.com/rxap/packages/commit/cd44128797bbc42105d673934b398b37d5bd809b))

# [16.1.0-dev.29](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.28...@rxap/plugin-workspace@16.1.0-dev.29) (2023-09-20)

### Bug Fixes

- stop all container from all docker compose files ([fb377fd](https://gitlab.com/rxap/packages/commit/fb377fd249515070f73912780ac119ac37ea83a6))

# [16.1.0-dev.28](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.27...@rxap/plugin-workspace@16.1.0-dev.28) (2023-09-20)

### Bug Fixes

- remove minio container name ([9eaf12a](https://gitlab.com/rxap/packages/commit/9eaf12a607bc3b72f3ab34c06660687d141b4809))

# [16.1.0-dev.27](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.26...@rxap/plugin-workspace@16.1.0-dev.27) (2023-09-19)

### Bug Fixes

- use updated path for health checks ([dbc737f](https://gitlab.com/rxap/packages/commit/dbc737f121eba9857a99c8066264750ee18cf143))

# [16.1.0-dev.26](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.25...@rxap/plugin-workspace@16.1.0-dev.26) (2023-09-19)

### Bug Fixes

- add yarn rc file ([59f5588](https://gitlab.com/rxap/packages/commit/59f5588c8b5d59b64d1fd2db695ef8260eaff45e))
- remove explicit domain ([b287242](https://gitlab.com/rxap/packages/commit/b287242cf4b8ab73895c40e989eb9d8e3a292d9c))
- support local service execution ([67fb93f](https://gitlab.com/rxap/packages/commit/67fb93f79ec98ea03ad2d2624384649517c0bce3))
- update authentik service init ([4049928](https://gitlab.com/rxap/packages/commit/4049928a2b076f94503f97d0c21370710925c3b7))
- use correct project name ([0dd02ec](https://gitlab.com/rxap/packages/commit/0dd02ec8f6c4874b700a48746904f206694c5dcf))

# [16.1.0-dev.25](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.24...@rxap/plugin-workspace@16.1.0-dev.25) (2023-09-18)

### Bug Fixes

- set explicit root domain ([f1566a7](https://gitlab.com/rxap/packages/commit/f1566a79d2bc21dc2bbb3a58f2a4df9367b7612b))
- split local and remote pull ([76bec5a](https://gitlab.com/rxap/packages/commit/76bec5abb3ece8b9fbd500eee20bb371c6d0b690))

# [16.1.0-dev.24](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.23...@rxap/plugin-workspace@16.1.0-dev.24) (2023-09-18)

### Bug Fixes

- set default nest and angular directory ([c24219c](https://gitlab.com/rxap/packages/commit/c24219c7e143decb81c1cec8805970bdfee815c0))

# [16.1.0-dev.23](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.22...@rxap/plugin-workspace@16.1.0-dev.23) (2023-09-18)

### Bug Fixes

- use registry.gitlab.com as default registry ([49015c8](https://gitlab.com/rxap/packages/commit/49015c8d45779992ae9ee861073e6bb500c0ed4a))

# [16.1.0-dev.22](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.21...@rxap/plugin-workspace@16.1.0-dev.22) (2023-09-18)

### Bug Fixes

- init nx json configurations ([9b4b023](https://gitlab.com/rxap/packages/commit/9b4b023e849d1c0bf21b14a9e219a0e9cd6ab2f6))

# [16.1.0-dev.21](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.20...@rxap/plugin-workspace@16.1.0-dev.21) (2023-09-18)

### Bug Fixes

- include missing domains ([ae2c306](https://gitlab.com/rxap/packages/commit/ae2c306212777735f3a5570218abef651088ec20))
- update initial workspace files ([5b732f3](https://gitlab.com/rxap/packages/commit/5b732f355267ce5ef77f8f474595a09e58b4ae82))
- update service configuration name ([2686a78](https://gitlab.com/rxap/packages/commit/2686a7821685c0206eb7d390bfa1943c44795662))
- update service status name ([238353c](https://gitlab.com/rxap/packages/commit/238353c821f25268af93599756406e73431fca21))

# [16.1.0-dev.20](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.19...@rxap/plugin-workspace@16.1.0-dev.20) (2023-09-18)

### Bug Fixes

- generate traefik default certs ([c3b23ff](https://gitlab.com/rxap/packages/commit/c3b23ff1c050b34462111490aec9d4b5f6636640))

# [16.1.0-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.18...@rxap/plugin-workspace@16.1.0-dev.19) (2023-09-18)

### Bug Fixes

- add missing project config update ([ea08942](https://gitlab.com/rxap/packages/commit/ea08942b273c0957bb7f75bd8f2bed769ae4b07a))

# [16.1.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.17...@rxap/plugin-workspace@16.1.0-dev.18) (2023-09-18)

### Bug Fixes

- update initial workspace files ([7978009](https://gitlab.com/rxap/packages/commit/7978009bdf2185d5776efe780c162a13c8ecd72c))

### Features

- generate project traefik file ([3ccb370](https://gitlab.com/rxap/packages/commit/3ccb37099cb3416bc39bac4511e35366a368eea6))

# [16.1.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.16...@rxap/plugin-workspace@16.1.0-dev.17) (2023-09-18)

### Bug Fixes

- add missing initial workspace files ([a2938ca](https://gitlab.com/rxap/packages/commit/a2938ca405718d0dee72d4c9dbc6437fb9621289))
- coerce workspace project ([5510394](https://gitlab.com/rxap/packages/commit/5510394d5b4e09df28083bfabc8852063264c640))

# [16.1.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.15...@rxap/plugin-workspace@16.1.0-dev.16) (2023-09-17)

### Bug Fixes

- split GuessOutputPath function ([5fc44e1](https://gitlab.com/rxap/packages/commit/5fc44e1470ca16b542e0b45049bfd9a83b8baab8))

# [16.1.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.14...@rxap/plugin-workspace@16.1.0-dev.15) (2023-09-13)

### Bug Fixes

- remove error message ([16600e9](https://gitlab.com/rxap/packages/commit/16600e957d76e0ee9b799dd835adc08e7ee7193b))

# [16.1.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.13...@rxap/plugin-workspace@16.1.0-dev.14) (2023-09-13)

### Bug Fixes

- remove m2v dependency ([73f0203](https://gitlab.com/rxap/packages/commit/73f0203a21aeaa3a0405f8ce3baa6b1b2a1a755f))

# [16.1.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.12...@rxap/plugin-workspace@16.1.0-dev.13) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [16.1.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.11...@rxap/plugin-workspace@16.1.0-dev.12) (2023-09-12)

### Bug Fixes

- refactor the build.json concept ([7193cef](https://gitlab.com/rxap/packages/commit/7193cef9ffe76efdfedcd6e6d82e947c1be9c15b))

### Features

- add ci info executor ([b6f6496](https://gitlab.com/rxap/packages/commit/b6f6496e1399b63656ecc794f31728e20f853f20))

# [16.1.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.10...@rxap/plugin-workspace@16.1.0-dev.11) (2023-09-07)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.1.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.9...@rxap/plugin-workspace@16.1.0-dev.10) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-workspace

# [16.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.8...@rxap/plugin-workspace@16.1.0-dev.9) (2023-09-03)

### Features

- add project-target generator ([737dd40](https://gitlab.com/rxap/packages/commit/737dd405eac736449dbce395c32425812e3b9d39))

# [16.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.7...@rxap/plugin-workspace@16.1.0-dev.8) (2023-09-03)

### Bug Fixes

- support docker options from nx.json file ([adb9c0e](https://gitlab.com/rxap/packages/commit/adb9c0e9a3cc082e35dffd10b39458ac29c32c44))

# [16.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.6...@rxap/plugin-workspace@16.1.0-dev.7) (2023-09-01)

### Bug Fixes

- remove redundant traefik labels ([02d572d](https://gitlab.com/rxap/packages/commit/02d572d73eb5c9a9db8ba50b5a7fe7e04429e369))

# [16.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.5...@rxap/plugin-workspace@16.1.0-dev.6) (2023-08-31)

### Bug Fixes

- ensure overwrite option is passed to sub schematics ([8472aab](https://gitlab.com/rxap/packages/commit/8472aab8814227c851fab9ae4c1b9ec3019d6f4e))
- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))
- support new nest application structure ([1131c06](https://gitlab.com/rxap/packages/commit/1131c068e5967708283d0d9c79b81ea63af7d51c))

# [16.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.4...@rxap/plugin-workspace@16.1.0-dev.5) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

# [16.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.3...@rxap/plugin-workspace@16.1.0-dev.4) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.2...@rxap/plugin-workspace@16.1.0-dev.3) (2023-08-06)

### Bug Fixes

- expose generators as schematics ([679ca36](https://gitlab.com/rxap/packages/commit/679ca36d3712a11e4dc838762bca2f7c471e1e04))

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.1...@rxap/plugin-workspace@16.1.0-dev.2) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- install yarn 3 in the before script ([b1378f8](https://gitlab.com/rxap/packages/commit/b1378f80b10760024da2167600c09fb9acc56cd5))
- remove redundant command parameters ([80d360f](https://gitlab.com/rxap/packages/commit/80d360ff3060d50f3636b9796d59a1f726630adf))
- remove unnecessary deployment tier ([53b4790](https://gitlab.com/rxap/packages/commit/53b4790aad58644e40b65d6ef2967fcc73c94462))

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-workspace@16.1.0-dev.0...@rxap/plugin-workspace@16.1.0-dev.1) (2023-08-04)

### Bug Fixes

- move nx parallel parameter to run all utility ([38334ca](https://gitlab.com/rxap/packages/commit/38334ca40ef7c54674905adc035fe8bde2128248))
- relative path issue and spelling ([29fa19c](https://gitlab.com/rxap/packages/commit/29fa19cb47f21264aa94f989a08182ab9f8f2f9a))

### Features

- **init:** add generator ([cc49b7d](https://gitlab.com/rxap/packages/commit/cc49b7d4732092e83335c9809e3458cf29c731be))
- **init:** create workspace specific files ([8fd0dce](https://gitlab.com/rxap/packages/commit/8fd0dce224dc6c834bee48301ed86ce064158bf2))

# 16.1.0-dev.0 (2023-08-03)

### Features

- add docker-compose executor ([d3f6bba](https://gitlab.com/rxap/packages/commit/d3f6bbaa05d8ff9565fd131cdf8d45b64cee108c))
