export const NESTJS_VERSION = '~10.3.8';
export const NESTJS_THROTTLE_VERSION = '~5.1.2';
export const NESTJS_TERMINUS_VERSION = '~10.2.3';
export const NESTJS_CACHE_MANAGER_VERSION = '~2.2.2';
export const NESTJS_CONFIG_VERSION = '~3.2.2';
