export interface ValidatorGeneratorSchema {
  /** The name of the project. */
  project: string;
  overwrite?: boolean;
}
