export interface HealthIndicatorInitGeneratorSchema {
  /** The name of the project. */
  project: string;
  overwrite?: boolean;
}
