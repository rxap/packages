export interface OpenApiGeneratorSchema {
  project: string;
  overwrite?: boolean;
}
