export interface JwtGeneratorSchema {
  project: string;
  overwrite?: boolean;
}
