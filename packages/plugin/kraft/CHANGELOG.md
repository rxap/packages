# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.3-dev.0...@rxap/plugin-kraft@20.0.3-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.2...@rxap/plugin-kraft@20.0.3-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.2-dev.1...@rxap/plugin-kraft@20.0.2) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.2-dev.0...@rxap/plugin-kraft@20.0.2-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1...@rxap/plugin-kraft@20.0.2-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.6...@rxap/plugin-kraft@20.0.1) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.5...@rxap/plugin-kraft@20.0.1-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.4...@rxap/plugin-kraft@20.0.1-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.3...@rxap/plugin-kraft@20.0.1-dev.4) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.2...@rxap/plugin-kraft@20.0.1-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.1...@rxap/plugin-kraft@20.0.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.1-dev.0...@rxap/plugin-kraft@20.0.1-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-kraft

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0...@rxap/plugin-kraft@20.0.1-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.14...@rxap/plugin-kraft@20.0.0) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.13...@rxap/plugin-kraft@20.0.0-dev.14) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

# [20.0.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.12...@rxap/plugin-kraft@20.0.0-dev.13) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.11...@rxap/plugin-kraft@20.0.0-dev.12) (2025-02-11)

### Bug Fixes

- remove unused options ([d2186d5](https://gitlab.com/rxap/packages/commit/d2186d5edb9ab22c7393cff2735e340ce0f4357e))

# [20.0.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.10...@rxap/plugin-kraft@20.0.0-dev.11) (2025-02-11)

### Bug Fixes

- search for project recursive ([5e3edc4](https://gitlab.com/rxap/packages/commit/5e3edc4ef182971bb6e069f52841794c64b37504))

# [20.0.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.9...@rxap/plugin-kraft@20.0.0-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.8...@rxap/plugin-kraft@20.0.0-dev.9) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.7...@rxap/plugin-kraft@20.0.0-dev.8) (2025-01-30)

### Bug Fixes

- check if url path is defined ([a64bcf8](https://gitlab.com/rxap/packages/commit/a64bcf8663f33b53be4856a1b7eb361af3af4167))

# [20.0.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.6...@rxap/plugin-kraft@20.0.0-dev.7) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.5...@rxap/plugin-kraft@20.0.0-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.4...@rxap/plugin-kraft@20.0.0-dev.5) (2025-01-29)

### Bug Fixes

- add jetbrains ignores ([0b59259](https://gitlab.com/rxap/packages/commit/0b5925911be0429e1634388816b91c0d419156ad))

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.3...@rxap/plugin-kraft@20.0.0-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-kraft

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.2...@rxap/plugin-kraft@20.0.0-dev.3) (2025-01-28)

### Bug Fixes

- use correct workspace root check ([4d0c0ed](https://gitlab.com/rxap/packages/commit/4d0c0ed5e1f3bd9db2866162476a290028f70b33))

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-kraft@20.0.0-dev.1...@rxap/plugin-kraft@20.0.0-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-kraft

# 20.0.0-dev.1 (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-kraft
