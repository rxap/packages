import {
  formatFiles,
  getProjects,
  ProjectConfiguration,
  Tree,
  updateProjectConfiguration,
} from '@nx/devkit';
import { CoerceArrayItems } from '@rxap/utilities';
import {
  ForeachInitProject,
  GenerateSerializedSchematicFile,
  GetProjectRoot,
  SkipNonAngularProject,
  SkipNonApplicationProject,
} from '@rxap/workspace-utilities';
import { initProject } from './init-project';
import { initWorkspace } from './init-workspace';
import { InitApplicationGeneratorSchema } from './schema';

function skipProject(
  tree: Tree,
  options: InitApplicationGeneratorSchema,
  project: ProjectConfiguration,
  projectName: string,
): boolean {

  if (options.project === projectName) {
    return false;
  }

  if (SkipNonApplicationProject(tree, options, project, projectName)) {
    return true;
  }

  if (SkipNonAngularProject(tree, options, project, projectName)) {
    return true;
  }

  return false;

}

export async function initApplicationGenerator(
  tree: Tree,
  options: InitApplicationGeneratorSchema,
) {
  options.project ??= undefined;
  options.projects ??= [];
  if (options.project) {
    CoerceArrayItems(options.projects, [ options.project ]);
  }
  console.log('compodoc application init generator:', options);

  await initWorkspace(tree, options);

  for (const [projectName, project] of ForeachInitProject(tree, options, skipProject)) {

    await initProject(tree, projectName, project, options);

    GenerateSerializedSchematicFile(
      tree,
      GetProjectRoot(tree, projectName),
      '@rxap/plugin-compodoc',
      'init-application',
      options,
    );

  }

  if (!options.skipFormat) {
    await formatFiles(tree);
  }

}

export default initApplicationGenerator;
