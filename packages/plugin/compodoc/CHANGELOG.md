# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.3-dev.0...@rxap/plugin-compodoc@20.0.3-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.2...@rxap/plugin-compodoc@20.0.3-dev.0) (2025-03-12)

### Bug Fixes

- add support for JetBrains project folder exclusion ([dd924a3](https://gitlab.com/rxap/packages/commit/dd924a33c33b772d64457659cab4307c258b1fd0))
- **compodoc:** guard tsconfig update with existence check ([0ac866e](https://gitlab.com/rxap/packages/commit/0ac866e9b712d16e956fb1d564271f719554a6ff))

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.2-dev.1...@rxap/plugin-compodoc@20.0.2) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.2-dev.0...@rxap/plugin-compodoc@20.0.2-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1...@rxap/plugin-compodoc@20.0.2-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.5...@rxap/plugin-compodoc@20.0.1) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.4...@rxap/plugin-compodoc@20.0.1-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.3...@rxap/plugin-compodoc@20.0.1-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.2...@rxap/plugin-compodoc@20.0.1-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.1...@rxap/plugin-compodoc@20.0.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.1-dev.0...@rxap/plugin-compodoc@20.0.1-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.0...@rxap/plugin-compodoc@20.0.1-dev.0) (2025-02-17)

### Bug Fixes

- ignore non compodoc related files ([fbea062](https://gitlab.com/rxap/packages/commit/fbea06200e09e356db1b47c1c23df1284ef1bbac))

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.0-dev.3...@rxap/plugin-compodoc@20.0.0) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.0-dev.2...@rxap/plugin-compodoc@20.0.0-dev.3) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@20.0.0-dev.1...@rxap/plugin-compodoc@20.0.0-dev.2) (2025-02-11)

### Bug Fixes

- remove static name requirement for workspace project ([9100223](https://gitlab.com/rxap/packages/commit/9100223a036d60b0d9d41e7648606599720ddf9c))
- use ForeachInitProject Generator ([7f9aec0](https://gitlab.com/rxap/packages/commit/7f9aec065e55a127db4407add7a7e9a772a9289b))

### Features

- init plugin use ([1dc8e3a](https://gitlab.com/rxap/packages/commit/1dc8e3ac6c40c8ea3d8832942aa3d47790bfbd22))
- init plugin use ([1666499](https://gitlab.com/rxap/packages/commit/1666499e3d86e9e0e1f5cd2f39e87f289cb1770c))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))
- support nx plugins ([0709ac0](https://gitlab.com/rxap/packages/commit/0709ac0069174e9ffef265251afb82845094f079))
- support nx plugins ([f6e5aee](https://gitlab.com/rxap/packages/commit/f6e5aeeeaac85a0f75a24ca4139dd13b2bc9bde1))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.7-dev.0...@rxap/plugin-compodoc@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.6...@rxap/plugin-compodoc@19.1.7-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.6-dev.1...@rxap/plugin-compodoc@19.1.6) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.6-dev.0...@rxap/plugin-compodoc@19.1.6-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.5...@rxap/plugin-compodoc@19.1.6-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.5-dev.3...@rxap/plugin-compodoc@19.1.5) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.5-dev.2...@rxap/plugin-compodoc@19.1.5-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.5-dev.1...@rxap/plugin-compodoc@19.1.5-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.5-dev.0...@rxap/plugin-compodoc@19.1.5-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.4...@rxap/plugin-compodoc@19.1.5-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.4-dev.1...@rxap/plugin-compodoc@19.1.4) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.4-dev.0...@rxap/plugin-compodoc@19.1.4-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.3...@rxap/plugin-compodoc@19.1.4-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.3-dev.1...@rxap/plugin-compodoc@19.1.3) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.3-dev.0...@rxap/plugin-compodoc@19.1.3-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2...@rxap/plugin-compodoc@19.1.3-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.8...@rxap/plugin-compodoc@19.1.2) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.7...@rxap/plugin-compodoc@19.1.2-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.6...@rxap/plugin-compodoc@19.1.2-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.5...@rxap/plugin-compodoc@19.1.2-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.4...@rxap/plugin-compodoc@19.1.2-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.3...@rxap/plugin-compodoc@19.1.2-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.2...@rxap/plugin-compodoc@19.1.2-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.1...@rxap/plugin-compodoc@19.1.2-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.2-dev.0...@rxap/plugin-compodoc@19.1.2-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.1...@rxap/plugin-compodoc@19.1.2-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.1-dev.0...@rxap/plugin-compodoc@19.1.1) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.0...@rxap/plugin-compodoc@19.1.1-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.0-dev.3...@rxap/plugin-compodoc@19.1.0) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.0-dev.2...@rxap/plugin-compodoc@19.1.0-dev.3) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.0-dev.1...@rxap/plugin-compodoc@19.1.0-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.1.0-dev.0...@rxap/plugin-compodoc@19.1.0-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.3-dev.0...@rxap/plugin-compodoc@19.1.0-dev.0) (2024-06-20)

### Bug Fixes

- handle root project correctly ([9cbedb8](https://gitlab.com/rxap/packages/commit/9cbedb8181ff34379e80e68900e851865032ad04))

### Features

- support multiple output paths ([b89c4d8](https://gitlab.com/rxap/packages/commit/b89c4d812a502c04f9b5d44e195efe2200326223))
- support packaged compodoc ([82d0909](https://gitlab.com/rxap/packages/commit/82d0909a28fb3452ddef12da1b9ba4cf32a352ff))

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2...@rxap/plugin-compodoc@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.6...@rxap/plugin-compodoc@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.5...@rxap/plugin-compodoc@19.0.2-dev.6) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.4...@rxap/plugin-compodoc@19.0.2-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.3...@rxap/plugin-compodoc@19.0.2-dev.4) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.2...@rxap/plugin-compodoc@19.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.1...@rxap/plugin-compodoc@19.0.2-dev.2) (2024-06-17)

### Bug Fixes

- check for root path ([416f613](https://gitlab.com/rxap/packages/commit/416f613510c4bfead5d4dc725ef86698006f7c4c))

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.2-dev.0...@rxap/plugin-compodoc@19.0.2-dev.1) (2024-06-11)

### Bug Fixes

- ensure the dist path is valid ([6a06b62](https://gitlab.com/rxap/packages/commit/6a06b6254a0d91254a585634c96d3c4d165b99eb))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.1...@rxap/plugin-compodoc@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@19.0.1-dev.0...@rxap/plugin-compodoc@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@18.0.1...@rxap/plugin-compodoc@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@18.0.1-dev.0...@rxap/plugin-compodoc@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@17.0.1...@rxap/plugin-compodoc@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@17.0.1-dev.0...@rxap/plugin-compodoc@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.1.1...@rxap/plugin-compodoc@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [16.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.1.1-dev.0...@rxap/plugin-compodoc@16.1.1) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.1.0...@rxap/plugin-compodoc@16.1.1-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.1.0-dev.1...@rxap/plugin-compodoc@16.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-compodoc

# [16.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.1.0-dev.0...@rxap/plugin-compodoc@16.1.0-dev.1) (2024-04-12)

### Bug Fixes

- use correct target option property ([2e6b55c](https://gitlab.com/rxap/packages/commit/2e6b55c8bf74b5b215b4618eb3b5387c535dd549))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.0.1-dev.2...@rxap/plugin-compodoc@16.1.0-dev.0) (2024-04-10)

### Bug Fixes

- add schematic serializer ([57b1d08](https://gitlab.com/rxap/packages/commit/57b1d0871f5bee9ab7bd9cb9bc489f87da1e1429))

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.0.1-dev.1...@rxap/plugin-compodoc@16.0.1-dev.2) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-compodoc

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-compodoc@16.0.1-dev.0...@rxap/plugin-compodoc@16.0.1-dev.1) (2024-04-07)

### Bug Fixes

- add the angular localize type to the tsconfig ([4ddf2e0](https://gitlab.com/rxap/packages/commit/4ddf2e04f84e1325931f0cdb008f3e744dacf7e3))
- always add the workspace target ([a8eb31c](https://gitlab.com/rxap/packages/commit/a8eb31c9e4979f9ff9f9f8fc6f4b2474b3e5fd61))
- init the workspace tsconfig if not exists ([bf8eb58](https://gitlab.com/rxap/packages/commit/bf8eb5891428b20c18b0cfb4c19516c9d0d0d32c))

## 16.0.1-dev.0 (2024-04-07)

**Note:** Version bump only for package @rxap/plugin-compodoc
