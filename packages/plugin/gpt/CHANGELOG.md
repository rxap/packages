# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.1.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.3-dev.0...@rxap/plugin-gpt@20.1.3-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.2...@rxap/plugin-gpt@20.1.3-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.2-dev.1...@rxap/plugin-gpt@20.1.2) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.2-dev.0...@rxap/plugin-gpt@20.1.2-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1...@rxap/plugin-gpt@20.1.2-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.5...@rxap/plugin-gpt@20.1.1) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.4...@rxap/plugin-gpt@20.1.1-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.3...@rxap/plugin-gpt@20.1.1-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.2...@rxap/plugin-gpt@20.1.1-dev.3) (2025-02-19)

### Bug Fixes

- use tuples instead of enums ([8e39ee0](https://gitlab.com/rxap/packages/commit/8e39ee0145ed7650f783913857780d276b26069c))

## [20.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.1...@rxap/plugin-gpt@20.1.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.1-dev.0...@rxap/plugin-gpt@20.1.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0...@rxap/plugin-gpt@20.1.1-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0-dev.4...@rxap/plugin-gpt@20.1.0) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0-dev.3...@rxap/plugin-gpt@20.1.0-dev.4) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0-dev.2...@rxap/plugin-gpt@20.1.0-dev.3) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0-dev.1...@rxap/plugin-gpt@20.1.0-dev.2) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.1.0-dev.0...@rxap/plugin-gpt@20.1.0-dev.1) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.8...@rxap/plugin-gpt@20.1.0-dev.0) (2025-02-07)

### Features

- support new ai models ([a43ffc6](https://gitlab.com/rxap/packages/commit/a43ffc6b77b949c6521f0a6a4d7557734d062e42))

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.7...@rxap/plugin-gpt@20.0.1-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.6...@rxap/plugin-gpt@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.5...@rxap/plugin-gpt@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.4...@rxap/plugin-gpt@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.3...@rxap/plugin-gpt@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.2...@rxap/plugin-gpt@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.1...@rxap/plugin-gpt@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.1-dev.0...@rxap/plugin-gpt@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-gpt

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.0...@rxap/plugin-gpt@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.0-dev.3...@rxap/plugin-gpt@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.0-dev.2...@rxap/plugin-gpt@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-gpt

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@20.0.0-dev.1...@rxap/plugin-gpt@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.10-dev.0...@rxap/plugin-gpt@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.9...@rxap/plugin-gpt@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.9-dev.1...@rxap/plugin-gpt@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.9-dev.0...@rxap/plugin-gpt@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.8...@rxap/plugin-gpt@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.8-dev.3...@rxap/plugin-gpt@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.8-dev.2...@rxap/plugin-gpt@19.0.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.8-dev.1...@rxap/plugin-gpt@19.0.8-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.8-dev.0...@rxap/plugin-gpt@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.7...@rxap/plugin-gpt@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.7-dev.1...@rxap/plugin-gpt@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.7-dev.0...@rxap/plugin-gpt@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6...@rxap/plugin-gpt@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6-dev.4...@rxap/plugin-gpt@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6-dev.3...@rxap/plugin-gpt@19.0.6-dev.4) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6-dev.2...@rxap/plugin-gpt@19.0.6-dev.3) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6-dev.1...@rxap/plugin-gpt@19.0.6-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.6-dev.0...@rxap/plugin-gpt@19.0.6-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5...@rxap/plugin-gpt@19.0.6-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.8...@rxap/plugin-gpt@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.7...@rxap/plugin-gpt@19.0.5-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.6...@rxap/plugin-gpt@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.5...@rxap/plugin-gpt@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.4...@rxap/plugin-gpt@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.3...@rxap/plugin-gpt@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.2...@rxap/plugin-gpt@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.1...@rxap/plugin-gpt@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.5-dev.0...@rxap/plugin-gpt@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.4...@rxap/plugin-gpt@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.4-dev.0...@rxap/plugin-gpt@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3...@rxap/plugin-gpt@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.7...@rxap/plugin-gpt@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.6...@rxap/plugin-gpt@19.0.3-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.5...@rxap/plugin-gpt@19.0.3-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.4...@rxap/plugin-gpt@19.0.3-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.3...@rxap/plugin-gpt@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.2...@rxap/plugin-gpt@19.0.3-dev.3) (2024-06-21)

### Bug Fixes

- prevent skip if project is specified ([4b73a6e](https://gitlab.com/rxap/packages/commit/4b73a6e725c2ec0c8d34c35eeb39930f886eabdb))

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.1...@rxap/plugin-gpt@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.3-dev.0...@rxap/plugin-gpt@19.0.3-dev.1) (2024-06-20)

### Bug Fixes

- add html code coverage reporters ([73da1d8](https://gitlab.com/rxap/packages/commit/73da1d85274686590952a97e202c713718988caa))
- update openapi package ([d5afabe](https://gitlab.com/rxap/packages/commit/d5afabec58c65d3178129b826f016e1c593b8846))
- update to new openai models ([882aab3](https://gitlab.com/rxap/packages/commit/882aab3575ba99c7e271969fa94e2f2bb9361b2c))

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2...@rxap/plugin-gpt@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.5...@rxap/plugin-gpt@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.4...@rxap/plugin-gpt@19.0.2-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.3...@rxap/plugin-gpt@19.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.2...@rxap/plugin-gpt@19.0.2-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.1...@rxap/plugin-gpt@19.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.2-dev.0...@rxap/plugin-gpt@19.0.2-dev.1) (2024-06-17)

### Bug Fixes

- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.1...@rxap/plugin-gpt@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@19.0.1-dev.0...@rxap/plugin-gpt@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@18.0.1...@rxap/plugin-gpt@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-gpt

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@18.0.1-dev.0...@rxap/plugin-gpt@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@17.0.1...@rxap/plugin-gpt@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@17.0.1-dev.0...@rxap/plugin-gpt@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.4...@rxap/plugin-gpt@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.4-dev.0...@rxap/plugin-gpt@16.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.3...@rxap/plugin-gpt@16.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.3-dev.0...@rxap/plugin-gpt@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.2...@rxap/plugin-gpt@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.2-dev.2...@rxap/plugin-gpt@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.2-dev.1...@rxap/plugin-gpt@16.0.2-dev.2) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.2-dev.0...@rxap/plugin-gpt@16.0.2-dev.1) (2024-03-31)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1...@rxap/plugin-gpt@16.0.2-dev.0) (2024-03-11)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.12...@rxap/plugin-gpt@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.11...@rxap/plugin-gpt@16.0.1-dev.12) (2023-10-16)

### Bug Fixes

- use utility CreateProject function to create a ts-morph Project instance ([78b308f](https://gitlab.com/rxap/packages/commit/78b308fd10747616c7c7f27e81501a4ad5052a77))

## [16.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.11...@rxap/plugin-gpt@16.0.1-dev.11) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-gpt

## 16.0.1-dev.11 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the API key env is defined ([178dcb0](https://gitlab.com/rxap/packages/commit/178dcb08356c82720b6fa10f8e751323ae2a12fa))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- expose generators as schematics ([8a58d07](https://gitlab.com/rxap/packages/commit/8a58d07c2f1dcfff75e724a418d7c3bddb2d0bbc))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

## [16.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.9...@rxap/plugin-gpt@16.0.1-dev.10) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.8...@rxap/plugin-gpt@16.0.1-dev.9) (2023-09-12)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.7...@rxap/plugin-gpt@16.0.1-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.6...@rxap/plugin-gpt@16.0.1-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.5...@rxap/plugin-gpt@16.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-gpt

## [16.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.4...@rxap/plugin-gpt@16.0.1-dev.5) (2023-08-31)

### Bug Fixes

- ensure the API key env is defined ([3c292ad](https://gitlab.com/rxap/packages/commit/3c292adf8cb5958ca02f7eadca9db92380dcc86b))
- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.3...@rxap/plugin-gpt@16.0.1-dev.4) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.2...@rxap/plugin-gpt@16.0.1-dev.3) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.1...@rxap/plugin-gpt@16.0.1-dev.2) (2023-08-06)

### Bug Fixes

- expose generators as schematics ([679ca36](https://gitlab.com/rxap/packages/commit/679ca36d3712a11e4dc838762bca2f7c471e1e04))

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-gpt@16.0.1-dev.0...@rxap/plugin-gpt@16.0.1-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 16.0.1-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/plugin-gpt
