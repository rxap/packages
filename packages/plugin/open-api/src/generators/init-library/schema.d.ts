export interface InitLibraryGeneratorSchema {
  project: string;
  external?: boolean;
  skipFormat?: boolean;
  persistent?: boolean;
}
