# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.5-dev.0...@rxap/plugin-open-api@20.0.5-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4...@rxap/plugin-open-api@20.0.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4-dev.4...@rxap/plugin-open-api@20.0.4) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4-dev.3...@rxap/plugin-open-api@20.0.4-dev.4) (2025-02-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4-dev.2...@rxap/plugin-open-api@20.0.4-dev.3) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4-dev.1...@rxap/plugin-open-api@20.0.4-dev.2) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.4-dev.0...@rxap/plugin-open-api@20.0.4-dev.1) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3...@rxap/plugin-open-api@20.0.4-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.6...@rxap/plugin-open-api@20.0.3) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.5...@rxap/plugin-open-api@20.0.3-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.4...@rxap/plugin-open-api@20.0.3-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.3...@rxap/plugin-open-api@20.0.3-dev.4) (2025-02-19)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.2...@rxap/plugin-open-api@20.0.3-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.1...@rxap/plugin-open-api@20.0.3-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.3-dev.0...@rxap/plugin-open-api@20.0.3-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.2...@rxap/plugin-open-api@20.0.3-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1...@rxap/plugin-open-api@20.0.2) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.19...@rxap/plugin-open-api@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.19](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.18...@rxap/plugin-open-api@20.0.1-dev.19) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.17...@rxap/plugin-open-api@20.0.1-dev.18) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.16...@rxap/plugin-open-api@20.0.1-dev.17) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.15...@rxap/plugin-open-api@20.0.1-dev.16) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.14...@rxap/plugin-open-api@20.0.1-dev.15) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.13...@rxap/plugin-open-api@20.0.1-dev.14) (2025-01-30)

### Bug Fixes

- cleanup openapi library after setup ([c411a10](https://gitlab.com/rxap/packages/commit/c411a108a791078eb8c498a83cac697a03a0f59d))

## [20.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.12...@rxap/plugin-open-api@20.0.1-dev.13) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.11...@rxap/plugin-open-api@20.0.1-dev.12) (2025-01-29)

### Bug Fixes

- define entry points for index export ([ddf57c4](https://gitlab.com/rxap/packages/commit/ddf57c4b1d7d1c2094d5c97921c3310aa18ad7ab))

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.10...@rxap/plugin-open-api@20.0.1-dev.11) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.9...@rxap/plugin-open-api@20.0.1-dev.10) (2025-01-29)

### Bug Fixes

- use correct package name ([edcd11f](https://gitlab.com/rxap/packages/commit/edcd11f729df64fac68ca6ec0b1a1338edc44f95))

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.8...@rxap/plugin-open-api@20.0.1-dev.9) (2025-01-29)

### Bug Fixes

- use correct package name ([26530d2](https://gitlab.com/rxap/packages/commit/26530d2c21d1e74795f036c873c5f9e7bfd01321))

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.7...@rxap/plugin-open-api@20.0.1-dev.8) (2025-01-29)

### Bug Fixes

- remove old defaults ([6815377](https://gitlab.com/rxap/packages/commit/6815377eea559e215dbf68db28c1963bd92bbca7))

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.6...@rxap/plugin-open-api@20.0.1-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.5...@rxap/plugin-open-api@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.4...@rxap/plugin-open-api@20.0.1-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.3...@rxap/plugin-open-api@20.0.1-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.2...@rxap/plugin-open-api@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.1...@rxap/plugin-open-api@20.0.1-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.1-dev.0...@rxap/plugin-open-api@20.0.1-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/plugin-open-api

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0...@rxap/plugin-open-api@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-open-api

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0-dev.5...@rxap/plugin-open-api@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-open-api

# [20.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0-dev.4...@rxap/plugin-open-api@20.0.0-dev.5) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-open-api

# [20.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0-dev.3...@rxap/plugin-open-api@20.0.0-dev.4) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-open-api

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0-dev.2...@rxap/plugin-open-api@20.0.0-dev.3) (2025-01-03)

**Note:** Version bump only for package @rxap/plugin-open-api

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@20.0.0-dev.1...@rxap/plugin-open-api@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([e328b50](https://gitlab.com/rxap/packages/commit/e328b507488df02562107a6288f60f7953a76191))
- support nx plugins ([3ce5d42](https://gitlab.com/rxap/packages/commit/3ce5d428533386361226f95202e7b970021c1104))
- support nx plugins ([a28c12d](https://gitlab.com/rxap/packages/commit/a28c12d411a1ce331a9b25f201d8b4674ac84e88))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.9-dev.1...@rxap/plugin-open-api@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.9-dev.0...@rxap/plugin-open-api@19.1.9-dev.1) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8...@rxap/plugin-open-api@19.1.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8-dev.4...@rxap/plugin-open-api@19.1.8) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8-dev.3...@rxap/plugin-open-api@19.1.8-dev.4) (2024-11-11)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8-dev.2...@rxap/plugin-open-api@19.1.8-dev.3) (2024-11-07)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8-dev.1...@rxap/plugin-open-api@19.1.8-dev.2) (2024-11-07)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.8-dev.0...@rxap/plugin-open-api@19.1.8-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7...@rxap/plugin-open-api@19.1.8-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.6...@rxap/plugin-open-api@19.1.7) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.5...@rxap/plugin-open-api@19.1.7-dev.6) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.4...@rxap/plugin-open-api@19.1.7-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.3...@rxap/plugin-open-api@19.1.7-dev.4) (2024-10-23)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.2...@rxap/plugin-open-api@19.1.7-dev.3) (2024-10-22)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.1...@rxap/plugin-open-api@19.1.7-dev.2) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.7-dev.0...@rxap/plugin-open-api@19.1.7-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6...@rxap/plugin-open-api@19.1.7-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6-dev.4...@rxap/plugin-open-api@19.1.6) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6-dev.3...@rxap/plugin-open-api@19.1.6-dev.4) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6-dev.2...@rxap/plugin-open-api@19.1.6-dev.3) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6-dev.1...@rxap/plugin-open-api@19.1.6-dev.2) (2024-08-29)

### Bug Fixes

- support guessing the swagger build output path ([2997d89](https://gitlab.com/rxap/packages/commit/2997d899658dae34240630bbb63ea4c25aebf90c))

## [19.1.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.6-dev.0...@rxap/plugin-open-api@19.1.6-dev.1) (2024-08-27)

### Bug Fixes

- ensure the project cache is up-to-date ([039955f](https://gitlab.com/rxap/packages/commit/039955f3ee4fa45907d9a103f483389e47ed5531))

## [19.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5...@rxap/plugin-open-api@19.1.6-dev.0) (2024-08-27)

### Bug Fixes

- use file project configuration ([78c2d6d](https://gitlab.com/rxap/packages/commit/78c2d6dd65b643c57c3c49c279fe04fa2c81f169))

## [19.1.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.6...@rxap/plugin-open-api@19.1.5) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.5...@rxap/plugin-open-api@19.1.5-dev.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.4...@rxap/plugin-open-api@19.1.5-dev.5) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.3...@rxap/plugin-open-api@19.1.5-dev.4) (2024-08-19)

### Bug Fixes

- correctly setup open api generation ([8845eb4](https://gitlab.com/rxap/packages/commit/8845eb45895322fc76ce228928451fd8eaa0eb96))

## [19.1.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.2...@rxap/plugin-open-api@19.1.5-dev.3) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.1...@rxap/plugin-open-api@19.1.5-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.5-dev.0...@rxap/plugin-open-api@19.1.5-dev.1) (2024-08-12)

### Bug Fixes

- add generate-open-api target ([a94a36e](https://gitlab.com/rxap/packages/commit/a94a36e4fbac586bdc0cd065625070838b3bc45f))

## [19.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4...@rxap/plugin-open-api@19.1.5-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.15...@rxap/plugin-open-api@19.1.4) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.14...@rxap/plugin-open-api@19.1.4-dev.15) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.13...@rxap/plugin-open-api@19.1.4-dev.14) (2024-07-26)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.12...@rxap/plugin-open-api@19.1.4-dev.13) (2024-07-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.11...@rxap/plugin-open-api@19.1.4-dev.12) (2024-07-24)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.10...@rxap/plugin-open-api@19.1.4-dev.11) (2024-07-22)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.9...@rxap/plugin-open-api@19.1.4-dev.10) (2024-07-16)

### Bug Fixes

- skip format by default ([92f567b](https://gitlab.com/rxap/packages/commit/92f567bffc560faedbfcb12392b7a83259be39c1))

## [19.1.4-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.8...@rxap/plugin-open-api@19.1.4-dev.9) (2024-07-16)

### Bug Fixes

- skip format by default ([fb355cd](https://gitlab.com/rxap/packages/commit/fb355cd2eefb833d1364e38bd454ea43a893a1a6))

## [19.1.4-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.7...@rxap/plugin-open-api@19.1.4-dev.8) (2024-07-09)

### Bug Fixes

- use /swagger as swagger build output ([4609b23](https://gitlab.com/rxap/packages/commit/4609b23fdfcec1cf2e9fe458d8648ac7ee40dc36))
- use correct import alias ([8ec3262](https://gitlab.com/rxap/packages/commit/8ec3262025cf8e4cba3730060413e84a4dda76bd))

## [19.1.4-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.6...@rxap/plugin-open-api@19.1.4-dev.7) (2024-07-03)

### Bug Fixes

- generate index files by default ([688c875](https://gitlab.com/rxap/packages/commit/688c87551a6f93d09e52e37adb779757697f47c0))

## [19.1.4-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.5...@rxap/plugin-open-api@19.1.4-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.4...@rxap/plugin-open-api@19.1.4-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.3...@rxap/plugin-open-api@19.1.4-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.2...@rxap/plugin-open-api@19.1.4-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.1...@rxap/plugin-open-api@19.1.4-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.4-dev.0...@rxap/plugin-open-api@19.1.4-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.3...@rxap/plugin-open-api@19.1.4-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.2...@rxap/plugin-open-api@19.1.3) (2024-06-30)

### Bug Fixes

- skip index.ts generation ([fe40628](https://gitlab.com/rxap/packages/commit/fe406281caef65ffb15b33c08a0fcdd734f2ce2f))

## [19.1.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.2-dev.0...@rxap/plugin-open-api@19.1.2) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1...@rxap/plugin-open-api@19.1.2-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.10...@rxap/plugin-open-api@19.1.1) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.9...@rxap/plugin-open-api@19.1.1-dev.10) (2024-06-27)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.8...@rxap/plugin-open-api@19.1.1-dev.9) (2024-06-25)

### Bug Fixes

- spelling ([14360cc](https://gitlab.com/rxap/packages/commit/14360cc293fc9227c959f93149496cfd8d7492a1))
- use the library init generator for open api projects ([d275054](https://gitlab.com/rxap/packages/commit/d275054aca1e4ebf6c75451b378d342b232d7bf2))

## [19.1.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.7...@rxap/plugin-open-api@19.1.1-dev.8) (2024-06-25)

### Bug Fixes

- remove package name concept ([9e02801](https://gitlab.com/rxap/packages/commit/9e0280128c3ae9ccef8dafa2aa6af90d7f99e267))

## [19.1.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.6...@rxap/plugin-open-api@19.1.1-dev.7) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.5...@rxap/plugin-open-api@19.1.1-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.4...@rxap/plugin-open-api@19.1.1-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.3...@rxap/plugin-open-api@19.1.1-dev.4) (2024-06-24)

### Bug Fixes

- add build target to open api client sdk projects ([8e49bb1](https://gitlab.com/rxap/packages/commit/8e49bb1a28e33a58cc52bdc949df080c0f0cb625))

## [19.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.2...@rxap/plugin-open-api@19.1.1-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.1...@rxap/plugin-open-api@19.1.1-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.1-dev.0...@rxap/plugin-open-api@19.1.1-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0...@rxap/plugin-open-api@19.1.1-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.6...@rxap/plugin-open-api@19.1.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.5...@rxap/plugin-open-api@19.1.0-dev.6) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.4...@rxap/plugin-open-api@19.1.0-dev.5) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.3...@rxap/plugin-open-api@19.1.0-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.2...@rxap/plugin-open-api@19.1.0-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.1...@rxap/plugin-open-api@19.1.0-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.1.0-dev.0...@rxap/plugin-open-api@19.1.0-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-open-api

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.0.2-dev.0...@rxap/plugin-open-api@19.1.0-dev.0) (2024-06-17)

### Bug Fixes

- dont delete generate files ([f8711b6](https://gitlab.com/rxap/packages/commit/f8711b6291a3bb61524a3577514d012dc3be43f4))
- use coerce file function ([822c33c](https://gitlab.com/rxap/packages/commit/822c33c0021276844114e859d53c79cad6feb51a))

### Features

- add persistent property ([500495f](https://gitlab.com/rxap/packages/commit/500495f49c69070a51a93c9e964dad3c1ca4babc))

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.0.1...@rxap/plugin-open-api@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@19.0.1-dev.0...@rxap/plugin-open-api@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@18.0.1...@rxap/plugin-open-api@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-open-api

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@18.0.1-dev.0...@rxap/plugin-open-api@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@17.0.1...@rxap/plugin-open-api@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@17.0.1-dev.0...@rxap/plugin-open-api@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.1...@rxap/plugin-open-api@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.3.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.1-dev.0...@rxap/plugin-open-api@16.3.1) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.0...@rxap/plugin-open-api@16.3.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.3.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.0-dev.2...@rxap/plugin-open-api@16.3.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.0-dev.1...@rxap/plugin-open-api@16.3.0-dev.2) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.3.0-dev.0...@rxap/plugin-open-api@16.3.0-dev.1) (2024-04-28)

### Bug Fixes

- only delete generated files ([aa7f7f1](https://gitlab.com/rxap/packages/commit/aa7f7f13f4794cf636b4ac14e173a7d8a7439d63))

# [16.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.2.0...@rxap/plugin-open-api@16.3.0-dev.0) (2024-04-18)

### Features

- support standalone open api project init ([a2dbd15](https://gitlab.com/rxap/packages/commit/a2dbd15a76b6df05581cfd5506fc5ede40a540ec))

# [16.2.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.2.0-dev.2...@rxap/plugin-open-api@16.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.2.0-dev.1...@rxap/plugin-open-api@16.2.0-dev.2) (2024-04-10)

### Features

- add format files ([672c753](https://gitlab.com/rxap/packages/commit/672c7533b8b0248d19c9dc2ad4a203c482cfd9ae))

# [16.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.2.0-dev.0...@rxap/plugin-open-api@16.2.0-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.1-dev.3...@rxap/plugin-open-api@16.2.0-dev.0) (2024-04-03)

### Bug Fixes

- use the old import path scopes ([fa6fe9d](https://gitlab.com/rxap/packages/commit/fa6fe9d9530335979f734c1f49c19f97f6118832))

### Features

- add open api client sdk library init generator ([1c5e8e0](https://gitlab.com/rxap/packages/commit/1c5e8e05614124a1d7d618d013a433830ec80832))

## [16.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.1-dev.2...@rxap/plugin-open-api@16.1.1-dev.3) (2024-03-31)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.1-dev.1...@rxap/plugin-open-api@16.1.1-dev.2) (2024-03-26)

### Bug Fixes

- only generate index.ts if export is set to true ([89bb181](https://gitlab.com/rxap/packages/commit/89bb1816764e1f23bc317b2dd9f6f7c0601b947d))

## [16.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.1-dev.0...@rxap/plugin-open-api@16.1.1-dev.1) (2024-03-26)

### Bug Fixes

- reduce the code duplication when generating the open api client sdk files ([1ea4ac1](https://gitlab.com/rxap/packages/commit/1ea4ac1beb38ddf9d404c72698d1a582bb8d9837)), closes [#36](https://gitlab.com/rxap/packages/issues/36)

## [16.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.0...@rxap/plugin-open-api@16.1.1-dev.0) (2024-03-11)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.1.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.0-dev.3...@rxap/plugin-open-api@16.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.0-dev.2...@rxap/plugin-open-api@16.1.0-dev.3) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-open-api

# [16.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.1.0-dev.1...@rxap/plugin-open-api@16.1.0-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/plugin-open-api

# 16.1.0-dev.1 (2023-10-11)

### Bug Fixes

- only clear source files ([7dddd96](https://gitlab.com/rxap/packages/commit/7dddd96dcaa8be256e5bc6c41a93881ca9ac19ee))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- remove support for open-api client sdk packages ([0015878](https://gitlab.com/rxap/packages/commit/0015878e53cba42943d37354ef5c7d5f17828fd7))
- use prefix for directives if defined in project.json ([acaa916](https://gitlab.com/rxap/packages/commit/acaa91657e477221ed6f36c589fe8546bd17f277))

### Features

- add the copy-client-sdk executor ([6742469](https://gitlab.com/rxap/packages/commit/674246926cb882ba6a94c5081e19bca8b74f3a59))

# [16.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.6...@rxap/plugin-open-api@16.1.0-dev.0) (2023-10-01)

### Features

- add the copy-client-sdk executor ([6c5c46a](https://gitlab.com/rxap/packages/commit/6c5c46a6d1f8f12d3f0f77117aa6070d7c2507e2))

## [16.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.5...@rxap/plugin-open-api@16.0.1-dev.6) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [16.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.4...@rxap/plugin-open-api@16.0.1-dev.5) (2023-09-12)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.3...@rxap/plugin-open-api@16.0.1-dev.4) (2023-09-07)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.2...@rxap/plugin-open-api@16.0.1-dev.3) (2023-09-03)

**Note:** Version bump only for package @rxap/plugin-open-api

## [16.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.1...@rxap/plugin-open-api@16.0.1-dev.2) (2023-09-03)

### Bug Fixes

- remove support for open-api client sdk packages ([ace153f](https://gitlab.com/rxap/packages/commit/ace153f977690e7714c3c4110600e2a8916a0d52))

## [16.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-open-api@16.0.1-dev.0...@rxap/plugin-open-api@16.0.1-dev.1) (2023-09-02)

### Bug Fixes

- use prefix for directives if defined in project.json ([74da847](https://gitlab.com/rxap/packages/commit/74da8470f24f5e7b21404cb7f97973ec6630ae7b))

## 16.0.1-dev.0 (2023-09-02)

### Bug Fixes

- only clear source files ([9f83c4a](https://gitlab.com/rxap/packages/commit/9f83c4a4328de5e97841947856577789ebe744a0))
