export interface BuildInfoExecutorSchema {
  branch?: string;
  tag?: string;
  release?: string;
  commit?: string;
  timestamp?: string;
}
