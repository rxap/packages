# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.4-dev.0...@rxap/plugin-web3-storage@20.0.4-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.3...@rxap/plugin-web3-storage@20.0.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.3-dev.1...@rxap/plugin-web3-storage@20.0.3) (2025-03-07)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.3-dev.0...@rxap/plugin-web3-storage@20.0.3-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2...@rxap/plugin-web3-storage@20.0.3-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.5...@rxap/plugin-web3-storage@20.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.4...@rxap/plugin-web3-storage@20.0.2-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.3...@rxap/plugin-web3-storage@20.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.2...@rxap/plugin-web3-storage@20.0.2-dev.3) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.1...@rxap/plugin-web3-storage@20.0.2-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.2-dev.0...@rxap/plugin-web3-storage@20.0.2-dev.1) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1...@rxap/plugin-web3-storage@20.0.2-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.12...@rxap/plugin-web3-storage@20.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.11...@rxap/plugin-web3-storage@20.0.1-dev.12) (2025-02-13)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.10...@rxap/plugin-web3-storage@20.0.1-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.9...@rxap/plugin-web3-storage@20.0.1-dev.10) (2025-02-11)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.8...@rxap/plugin-web3-storage@20.0.1-dev.9) (2025-02-10)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.7...@rxap/plugin-web3-storage@20.0.1-dev.8) (2025-02-07)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.6...@rxap/plugin-web3-storage@20.0.1-dev.7) (2025-01-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.5...@rxap/plugin-web3-storage@20.0.1-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.4...@rxap/plugin-web3-storage@20.0.1-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.3...@rxap/plugin-web3-storage@20.0.1-dev.4) (2025-01-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.2...@rxap/plugin-web3-storage@20.0.1-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.1...@rxap/plugin-web3-storage@20.0.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.1-dev.0...@rxap/plugin-web3-storage@20.0.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.0...@rxap/plugin-web3-storage@20.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-web3-storage

# [20.0.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.0-dev.3...@rxap/plugin-web3-storage@20.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/plugin-web3-storage

# [20.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.0-dev.2...@rxap/plugin-web3-storage@20.0.0-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/plugin-web3-storage

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@20.0.0-dev.1...@rxap/plugin-web3-storage@20.0.0-dev.2) (2025-01-03)

### Features

- support nx plugins ([959c478](https://gitlab.com/rxap/packages/commit/959c478fb89f5688a3d741823ea0a67878b1d4cb))
- support nx plugins ([011871a](https://gitlab.com/rxap/packages/commit/011871a844241ead7a5dc2a07470af65251c8647))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.10-dev.0...@rxap/plugin-web3-storage@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.9...@rxap/plugin-web3-storage@19.0.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.9](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.9-dev.1...@rxap/plugin-web3-storage@19.0.9) (2024-12-10)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.9-dev.0...@rxap/plugin-web3-storage@19.0.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8...@rxap/plugin-web3-storage@19.0.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.5...@rxap/plugin-web3-storage@19.0.8) (2024-10-28)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.4...@rxap/plugin-web3-storage@19.0.8-dev.5) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.3...@rxap/plugin-web3-storage@19.0.8-dev.4) (2024-10-25)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.2...@rxap/plugin-web3-storage@19.0.8-dev.3) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.1...@rxap/plugin-web3-storage@19.0.8-dev.2) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.8-dev.0...@rxap/plugin-web3-storage@19.0.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.7...@rxap/plugin-web3-storage@19.0.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.7-dev.1...@rxap/plugin-web3-storage@19.0.7) (2024-09-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.7-dev.0...@rxap/plugin-web3-storage@19.0.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.6...@rxap/plugin-web3-storage@19.0.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.6-dev.1...@rxap/plugin-web3-storage@19.0.6) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.6-dev.0...@rxap/plugin-web3-storage@19.0.6-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5...@rxap/plugin-web3-storage@19.0.6-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.8...@rxap/plugin-web3-storage@19.0.5) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.7...@rxap/plugin-web3-storage@19.0.5-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.6...@rxap/plugin-web3-storage@19.0.5-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.5...@rxap/plugin-web3-storage@19.0.5-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.4...@rxap/plugin-web3-storage@19.0.5-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.3...@rxap/plugin-web3-storage@19.0.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.2...@rxap/plugin-web3-storage@19.0.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.1...@rxap/plugin-web3-storage@19.0.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.5-dev.0...@rxap/plugin-web3-storage@19.0.5-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.4...@rxap/plugin-web3-storage@19.0.5-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.4-dev.0...@rxap/plugin-web3-storage@19.0.4) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3...@rxap/plugin-web3-storage@19.0.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3-dev.4...@rxap/plugin-web3-storage@19.0.3) (2024-06-28)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3-dev.3...@rxap/plugin-web3-storage@19.0.3-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3-dev.2...@rxap/plugin-web3-storage@19.0.3-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3-dev.1...@rxap/plugin-web3-storage@19.0.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.3-dev.0...@rxap/plugin-web3-storage@19.0.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2...@rxap/plugin-web3-storage@19.0.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2-dev.4...@rxap/plugin-web3-storage@19.0.2) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2-dev.3...@rxap/plugin-web3-storage@19.0.2-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2-dev.2...@rxap/plugin-web3-storage@19.0.2-dev.3) (2024-06-18)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2-dev.1...@rxap/plugin-web3-storage@19.0.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.2-dev.0...@rxap/plugin-web3-storage@19.0.2-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.1...@rxap/plugin-web3-storage@19.0.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@19.0.1-dev.0...@rxap/plugin-web3-storage@19.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@18.0.1...@rxap/plugin-web3-storage@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [18.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@18.0.1-dev.0...@rxap/plugin-web3-storage@18.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@17.0.1...@rxap/plugin-web3-storage@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [17.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@17.0.1-dev.0...@rxap/plugin-web3-storage@17.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.3...@rxap/plugin-web3-storage@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.3](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.3-dev.0...@rxap/plugin-web3-storage@16.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.2...@rxap/plugin-web3-storage@16.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.2](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.2-dev.1...@rxap/plugin-web3-storage@16.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.2-dev.0...@rxap/plugin-web3-storage@16.0.2-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.1...@rxap/plugin-web3-storage@16.0.2-dev.0) (2024-03-11)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## [16.0.1](https://gitlab.com/rxap/packages/compare/@rxap/plugin-web3-storage@16.0.1-dev.0...@rxap/plugin-web3-storage@16.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/plugin-web3-storage

## 16.0.1-dev.0 (2023-10-18)

### Bug Fixes

- print explore link ([3b733e1](https://gitlab.com/rxap/packages/commit/3b733e1e400bcf05dc75ac54acaad04aca0a5ecc))
- set human readable name for uploads ([c71c484](https://gitlab.com/rxap/packages/commit/c71c48493a16ec96f5abd31c3d6fd68c45a78a85))
- support token from env ([ad3e1cd](https://gitlab.com/rxap/packages/commit/ad3e1cd1787d743ee9a4630a5bcd20d906efe219))
- use absolute output path ([c980a96](https://gitlab.com/rxap/packages/commit/c980a963c27c5a1b4150bf408cae67cc01a128fa))
