export interface TailwindExecutorSchema {
  config?: string;
  input?: string;
  output?: string;
  minify?: boolean;
}
