export interface FixSchematicGeneratorSchema {
  /** The name of the project. */
  project: string;
}
