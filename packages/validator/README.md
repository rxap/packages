
[![npm version](https://img.shields.io/npm/v/@rxap/validator?style=flat-square)](https://www.npmjs.com/package/@rxap/validator)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/validator)
![npm](https://img.shields.io/npm/dm/@rxap/validator)
![NPM](https://img.shields.io/npm/l/@rxap/validator)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/validator
```
**Execute the init generator:**
```bash
yarn nx g @rxap/validator:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/validator:init
```
