# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.20-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.20-dev.0...@rxap/generator-ts-morph@1.0.20-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.20-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.19...@rxap/generator-ts-morph@1.0.20-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.19](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.19-dev.1...@rxap/generator-ts-morph@1.0.19) (2025-03-07)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.19-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.19-dev.0...@rxap/generator-ts-morph@1.0.19-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.19-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18...@rxap/generator-ts-morph@1.0.19-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18-dev.4...@rxap/generator-ts-morph@1.0.18) (2025-02-23)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18-dev.3...@rxap/generator-ts-morph@1.0.18-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18-dev.2...@rxap/generator-ts-morph@1.0.18-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18-dev.1...@rxap/generator-ts-morph@1.0.18-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.18-dev.0...@rxap/generator-ts-morph@1.0.18-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.18-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17...@rxap/generator-ts-morph@1.0.18-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.13...@rxap/generator-ts-morph@1.0.17) (2025-02-13)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.12...@rxap/generator-ts-morph@1.0.17-dev.13) (2025-02-13)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.11...@rxap/generator-ts-morph@1.0.17-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.10...@rxap/generator-ts-morph@1.0.17-dev.11) (2025-02-11)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.9...@rxap/generator-ts-morph@1.0.17-dev.10) (2025-02-10)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.8...@rxap/generator-ts-morph@1.0.17-dev.9) (2025-02-07)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.7...@rxap/generator-ts-morph@1.0.17-dev.8) (2025-01-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.6...@rxap/generator-ts-morph@1.0.17-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.5...@rxap/generator-ts-morph@1.0.17-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.4...@rxap/generator-ts-morph@1.0.17-dev.5) (2025-01-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.3...@rxap/generator-ts-morph@1.0.17-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.2...@rxap/generator-ts-morph@1.0.17-dev.3) (2025-01-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.1...@rxap/generator-ts-morph@1.0.17-dev.2) (2025-01-22)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.17-dev.0...@rxap/generator-ts-morph@1.0.17-dev.1) (2025-01-21)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.17-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.16...@rxap/generator-ts-morph@1.0.17-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.16](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.16-dev.3...@rxap/generator-ts-morph@1.0.16) (2025-01-08)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.16-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.16-dev.2...@rxap/generator-ts-morph@1.0.16-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.16-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.16-dev.1...@rxap/generator-ts-morph@1.0.16-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.16-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.16-dev.0...@rxap/generator-ts-morph@1.0.16-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.16-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.15...@rxap/generator-ts-morph@1.0.16-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.15](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.15-dev.1...@rxap/generator-ts-morph@1.0.15) (2024-12-10)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.15-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.15-dev.0...@rxap/generator-ts-morph@1.0.15-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.15-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.14...@rxap/generator-ts-morph@1.0.15-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.14](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.14-dev.3...@rxap/generator-ts-morph@1.0.14) (2024-10-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.14-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.14-dev.2...@rxap/generator-ts-morph@1.0.14-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.14-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.14-dev.1...@rxap/generator-ts-morph@1.0.14-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.14-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.14-dev.0...@rxap/generator-ts-morph@1.0.14-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.14-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.13...@rxap/generator-ts-morph@1.0.14-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.13](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.13-dev.1...@rxap/generator-ts-morph@1.0.13) (2024-09-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.13-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.13-dev.0...@rxap/generator-ts-morph@1.0.13-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.13-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12...@rxap/generator-ts-morph@1.0.13-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12-dev.4...@rxap/generator-ts-morph@1.0.12) (2024-08-22)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12-dev.3...@rxap/generator-ts-morph@1.0.12-dev.4) (2024-08-22)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12-dev.2...@rxap/generator-ts-morph@1.0.12-dev.3) (2024-08-21)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12-dev.1...@rxap/generator-ts-morph@1.0.12-dev.2) (2024-08-15)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.12-dev.0...@rxap/generator-ts-morph@1.0.12-dev.1) (2024-08-12)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11...@rxap/generator-ts-morph@1.0.12-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.8...@rxap/generator-ts-morph@1.0.11) (2024-07-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.7...@rxap/generator-ts-morph@1.0.11-dev.8) (2024-07-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.6...@rxap/generator-ts-morph@1.0.11-dev.7) (2024-07-09)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.5...@rxap/generator-ts-morph@1.0.11-dev.6) (2024-07-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.4...@rxap/generator-ts-morph@1.0.11-dev.5) (2024-07-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.3...@rxap/generator-ts-morph@1.0.11-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.2...@rxap/generator-ts-morph@1.0.11-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.1...@rxap/generator-ts-morph@1.0.11-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.11-dev.0...@rxap/generator-ts-morph@1.0.11-dev.1) (2024-07-02)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.10...@rxap/generator-ts-morph@1.0.11-dev.0) (2024-07-01)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.10](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.10-dev.0...@rxap/generator-ts-morph@1.0.10) (2024-06-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9...@rxap/generator-ts-morph@1.0.10-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.7...@rxap/generator-ts-morph@1.0.9) (2024-06-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.6...@rxap/generator-ts-morph@1.0.9-dev.7) (2024-06-27)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.5...@rxap/generator-ts-morph@1.0.9-dev.6) (2024-06-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.4...@rxap/generator-ts-morph@1.0.9-dev.5) (2024-06-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.3...@rxap/generator-ts-morph@1.0.9-dev.4) (2024-06-25)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.2...@rxap/generator-ts-morph@1.0.9-dev.3) (2024-06-21)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.1...@rxap/generator-ts-morph@1.0.9-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.9-dev.0...@rxap/generator-ts-morph@1.0.9-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8...@rxap/generator-ts-morph@1.0.9-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8-dev.4...@rxap/generator-ts-morph@1.0.8) (2024-06-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8-dev.3...@rxap/generator-ts-morph@1.0.8-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8-dev.2...@rxap/generator-ts-morph@1.0.8-dev.3) (2024-06-18)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8-dev.1...@rxap/generator-ts-morph@1.0.8-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.8-dev.0...@rxap/generator-ts-morph@1.0.8-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.7...@rxap/generator-ts-morph@1.0.8-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.7](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.7-dev.0...@rxap/generator-ts-morph@1.0.7) (2024-05-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.6...@rxap/generator-ts-morph@1.0.7-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.6](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.6-dev.0...@rxap/generator-ts-morph@1.0.6) (2024-05-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.5...@rxap/generator-ts-morph@1.0.6-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.5](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.5-dev.0...@rxap/generator-ts-morph@1.0.5) (2024-05-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.4...@rxap/generator-ts-morph@1.0.5-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.4-dev.0...@rxap/generator-ts-morph@1.0.4) (2024-05-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.3...@rxap/generator-ts-morph@1.0.4-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.3-dev.0...@rxap/generator-ts-morph@1.0.3) (2024-05-27)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.2...@rxap/generator-ts-morph@1.0.3-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.2](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.2-dev.1...@rxap/generator-ts-morph@1.0.2) (2024-04-17)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.2-dev.0...@rxap/generator-ts-morph@1.0.2-dev.1) (2024-04-09)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1...@rxap/generator-ts-morph@1.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.13...@rxap/generator-ts-morph@1.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.1...@rxap/generator-ts-morph@1.0.1-dev.13) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

## [1.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.11...@rxap/generator-ts-morph@1.0.1-dev.12) (2023-09-27)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.10...@rxap/generator-ts-morph@1.0.1-dev.11) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [1.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.9...@rxap/generator-ts-morph@1.0.1-dev.10) (2023-09-12)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.8...@rxap/generator-ts-morph@1.0.1-dev.9) (2023-09-07)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.7...@rxap/generator-ts-morph@1.0.1-dev.8) (2023-09-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.6...@rxap/generator-ts-morph@1.0.1-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/generator-ts-morph

## [1.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.5...@rxap/generator-ts-morph@1.0.1-dev.6) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [1.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.4...@rxap/generator-ts-morph@1.0.1-dev.5) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [1.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.3...@rxap/generator-ts-morph@1.0.1-dev.4) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [1.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.2...@rxap/generator-ts-morph@1.0.1-dev.3) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 1.0.1-dev.2 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))
- update package dependency versions ([45bd022](https://gitlab.com/rxap/packages/commit/45bd022d755c0c11f7d0bcc76d26b39928007941))

## [1.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/generator-ts-morph@1.0.1-dev.0...@rxap/generator-ts-morph@1.0.1-dev.1) (2023-07-10)

### Bug Fixes

- update package dependency versions ([8479f5c](https://gitlab.com/rxap/packages/commit/8479f5c405a885cc0f300cec6156584e4c65d59c))

## 1.0.1-dev.0 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
