This package provides a set of RxJS operators and utilities. It includes operators for property checking, deep equality comparison, value definition, and logging, as well as utilities for managing subscriptions, toggling boolean values, and handling request progress. It also contains a button definition interface.

[![npm version](https://img.shields.io/npm/v/@rxap/rxjs?style=flat-square)](https://www.npmjs.com/package/@rxap/rxjs)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/rxjs)
![npm](https://img.shields.io/npm/dm/@rxap/rxjs)
![NPM](https://img.shields.io/npm/l/@rxap/rxjs)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/rxjs
```
**Install peer dependencies:**
```bash
yarn add @rxap/utilities rxjs 
```
**Execute the init generator:**
```bash
yarn nx g @rxap/rxjs:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/rxjs:init
```
