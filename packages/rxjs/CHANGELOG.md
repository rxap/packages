# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.16-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.15...@rxap/rxjs@1.1.16-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.15](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.15-dev.0...@rxap/rxjs@1.1.15) (2025-03-07)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.15-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.14...@rxap/rxjs@1.1.15-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.14](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.13...@rxap/rxjs@1.1.14) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [1.1.13](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.13-dev.3...@rxap/rxjs@1.1.13) (2025-02-23)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.13-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.13-dev.2...@rxap/rxjs@1.1.13-dev.3) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [1.1.13-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.13-dev.1...@rxap/rxjs@1.1.13-dev.2) (2025-02-23)

### Bug Fixes

- update package groups ([66f235a](https://gitlab.com/rxap/packages/commit/66f235a6ce54be16b210c3d298bd0c4a88d1066d))

## [1.1.13-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.13-dev.0...@rxap/rxjs@1.1.13-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.13-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12...@rxap/rxjs@1.1.13-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.5...@rxap/rxjs@1.1.12) (2025-02-13)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.4...@rxap/rxjs@1.1.12-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.3...@rxap/rxjs@1.1.12-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.2...@rxap/rxjs@1.1.12-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.1...@rxap/rxjs@1.1.12-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.12-dev.0...@rxap/rxjs@1.1.12-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.11...@rxap/rxjs@1.1.12-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.11](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.11-dev.2...@rxap/rxjs@1.1.11) (2025-01-08)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.11-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.11-dev.1...@rxap/rxjs@1.1.11-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.11-dev.0...@rxap/rxjs@1.1.11-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.10...@rxap/rxjs@1.1.11-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.10](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.10-dev.3...@rxap/rxjs@1.1.10) (2024-10-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.10-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.10-dev.2...@rxap/rxjs@1.1.10-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.10-dev.1...@rxap/rxjs@1.1.10-dev.2) (2024-10-25)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.10-dev.0...@rxap/rxjs@1.1.10-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.9...@rxap/rxjs@1.1.10-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.9](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.9-dev.1...@rxap/rxjs@1.1.9) (2024-08-22)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.9-dev.0...@rxap/rxjs@1.1.9-dev.1) (2024-08-22)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.8...@rxap/rxjs@1.1.9-dev.0) (2024-08-21)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.8](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.8-dev.2...@rxap/rxjs@1.1.8) (2024-07-30)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.8-dev.1...@rxap/rxjs@1.1.8-dev.2) (2024-07-09)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.8-dev.0...@rxap/rxjs@1.1.8-dev.1) (2024-07-03)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.7...@rxap/rxjs@1.1.8-dev.0) (2024-07-02)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.7](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.7-dev.0...@rxap/rxjs@1.1.7) (2024-06-30)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.6...@rxap/rxjs@1.1.7-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.6](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.6-dev.1...@rxap/rxjs@1.1.6) (2024-06-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.6-dev.0...@rxap/rxjs@1.1.6-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.5...@rxap/rxjs@1.1.6-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.5](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.5-dev.1...@rxap/rxjs@1.1.5) (2024-06-18)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.5-dev.0...@rxap/rxjs@1.1.5-dev.1) (2024-06-17)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.4...@rxap/rxjs@1.1.5-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.4](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.4-dev.0...@rxap/rxjs@1.1.4) (2024-05-29)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.3...@rxap/rxjs@1.1.4-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.3](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.3-dev.0...@rxap/rxjs@1.1.3) (2024-05-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.2...@rxap/rxjs@1.1.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.2-dev.0...@rxap/rxjs@1.1.2) (2024-05-27)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.1...@rxap/rxjs@1.1.2-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.1-dev.2...@rxap/rxjs@1.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.1-dev.1...@rxap/rxjs@1.1.1-dev.2) (2024-04-09)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.1-dev.0...@rxap/rxjs@1.1.1-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/rxjs

## [1.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.0...@rxap/rxjs@1.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/rxjs

# [1.1.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.1.0-dev.0...@rxap/rxjs@1.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/rxjs

# [1.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.14...@rxap/rxjs@1.1.0-dev.0) (2023-10-29)

### Features

- add CloneObservable function ([865416f](https://gitlab.com/rxap/packages/commit/865416ff3a1a585010acbd9096f1474103d843ac))

## [1.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.13...@rxap/rxjs@1.0.1-dev.14) (2023-10-18)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.12...@rxap/rxjs@1.0.1-dev.13) (2023-10-11)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.0...@rxap/rxjs@1.0.1-dev.12) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([6223c97](https://gitlab.com/rxap/packages/commit/6223c978078cfa899ca69424b62d2a99cbb290a7))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

## [1.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.10...@rxap/rxjs@1.0.1-dev.11) (2023-09-27)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.9...@rxap/rxjs@1.0.1-dev.10) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [1.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.8...@rxap/rxjs@1.0.1-dev.9) (2023-09-12)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.7...@rxap/rxjs@1.0.1-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.6...@rxap/rxjs@1.0.1-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.5...@rxap/rxjs@1.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/rxjs

## [1.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.4...@rxap/rxjs@1.0.1-dev.5) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [1.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.3...@rxap/rxjs@1.0.1-dev.4) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [1.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.2...@rxap/rxjs@1.0.1-dev.3) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([a69aa25](https://gitlab.com/rxap/packages/commit/a69aa25b9824b94613392b3ea42fba18e5eb1168))

## [1.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/rxjs@1.0.1-dev.1...@rxap/rxjs@1.0.1-dev.2) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 1.0.1-dev.1 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))

## 1.0.1-dev.0 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
