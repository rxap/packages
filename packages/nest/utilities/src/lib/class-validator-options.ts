import type { ValidatorOptions } from 'class-validator';

/**
 * @deprecated import from @rxap/nest-dto instead
 */
export const validatorOptions: ValidatorOptions = {
  enableDebugMessages: true,
  skipUndefinedProperties: false,
  skipNullProperties: false,
  skipMissingProperties: false,
  forbidUnknownValues: true,
};

/**
 * @deprecated import from @rxap/nest-dto instead
 */
export function ValidatorOptionsFactory(additionalOptions: ValidatorOptions = {}) {
  return {
    ...validatorOptions,
    ...additionalOptions,
  };
}
