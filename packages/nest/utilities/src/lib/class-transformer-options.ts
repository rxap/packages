import type { ClassTransformOptions } from 'class-transformer';

/**
 * @deprecated import from @rxap/nest-dto instead
 */
export const classTransformOptions: ClassTransformOptions = {
  enableImplicitConversion: true,
  exposeDefaultValues: true,
  excludeExtraneousValues: true,
  exposeUnsetFields: false,
};

/**
 * @deprecated use classTransformOptions instead
 */
export const transformOptions = classTransformOptions;

/**
 * @deprecated import from @rxap/nest-dto instead
 */
export function ClassTransformOptionsFactory(additionalOptions: ClassTransformOptions = {}) {
  return {
    ...classTransformOptions,
    ...additionalOptions,
  };
}

/**
 * @deprecated use ClassTransformOptions instead
 */
export const TransformOptions = ClassTransformOptionsFactory;
