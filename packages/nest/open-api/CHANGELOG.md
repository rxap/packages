# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.5.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.6...@rxap/nest-open-api@10.5.7-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.6-dev.0...@rxap/nest-open-api@10.5.6) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.5...@rxap/nest-open-api@10.5.6-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4...@rxap/nest-open-api@10.5.5) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.5.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4-dev.4...@rxap/nest-open-api@10.5.4) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4-dev.3...@rxap/nest-open-api@10.5.4-dev.4) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.5.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4-dev.2...@rxap/nest-open-api@10.5.4-dev.3) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4-dev.1...@rxap/nest-open-api@10.5.4-dev.2) (2025-02-18)

### Bug Fixes

- update package groups ([d7297d7](https://gitlab.com/rxap/packages/commit/d7297d70ae488cd81c73761cea1d4fbe1d22203d))

## [10.5.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.4-dev.0...@rxap/nest-open-api@10.5.4-dev.1) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [10.5.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3...@rxap/nest-open-api@10.5.4-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.5...@rxap/nest-open-api@10.5.3) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.4...@rxap/nest-open-api@10.5.3-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.3...@rxap/nest-open-api@10.5.3-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.2...@rxap/nest-open-api@10.5.3-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.1...@rxap/nest-open-api@10.5.3-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.3-dev.0...@rxap/nest-open-api@10.5.3-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.2...@rxap/nest-open-api@10.5.3-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.2-dev.2...@rxap/nest-open-api@10.5.2) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.2-dev.1...@rxap/nest-open-api@10.5.2-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.2-dev.0...@rxap/nest-open-api@10.5.2-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.1...@rxap/nest-open-api@10.5.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.1-dev.3...@rxap/nest-open-api@10.5.1) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.5.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.1-dev.2...@rxap/nest-open-api@10.5.1-dev.3) (2024-11-11)

### Bug Fixes

- use binary type that is supported by nodejs and web ([2a3699c](https://gitlab.com/rxap/packages/commit/2a3699c755fafde8680e4141ed6b7c88f488ca37))

## [10.5.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.1-dev.1...@rxap/nest-open-api@10.5.1-dev.2) (2024-11-07)

### Bug Fixes

- improve console logging ([95383b5](https://gitlab.com/rxap/packages/commit/95383b590576acbe01bc51139d3b48d45390d8c7))

## [10.5.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.1-dev.0...@rxap/nest-open-api@10.5.1-dev.1) (2024-11-07)

### Bug Fixes

- correctly support array with form data request body ([1c2577b](https://gitlab.com/rxap/packages/commit/1c2577b839ab367fb13e8de9e6b39ce48c390daf))
- correctly support array with form data request body ([f8291b6](https://gitlab.com/rxap/packages/commit/f8291b659978601bedccdac521acb9f21ee35ef6))

## [10.5.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.0...@rxap/nest-open-api@10.5.1-dev.0) (2024-11-04)

### Bug Fixes

- support production fallback in scoped file names ([08db421](https://gitlab.com/rxap/packages/commit/08db4217a5ff374f39baa6c2a29d218899fa3ee2))

# [10.5.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.5.0-dev.0...@rxap/nest-open-api@10.5.0) (2024-10-28)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.5.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.4-dev.2...@rxap/nest-open-api@10.5.0-dev.0) (2024-10-25)

### Features

- support non json request bodies ([de661fc](https://gitlab.com/rxap/packages/commit/de661fc6cc762d5584599643db327f2dcd7ac5c5))

## [10.4.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.4-dev.1...@rxap/nest-open-api@10.4.4-dev.2) (2024-10-22)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))
- resolve injection scope issue ([a22578a](https://gitlab.com/rxap/packages/commit/a22578a88f15b36218b73f0d815c6c78677c3c1c))

## [10.4.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.4-dev.0...@rxap/nest-open-api@10.4.4-dev.1) (2024-10-22)

### Bug Fixes

- improve injection and error logging ([ccabde5](https://gitlab.com/rxap/packages/commit/ccabde5292c2602517e0ab14a1103ff5dc62aebb))

## [10.4.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.3...@rxap/nest-open-api@10.4.4-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.3-dev.0...@rxap/nest-open-api@10.4.3) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.2...@rxap/nest-open-api@10.4.3-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.2-dev.2...@rxap/nest-open-api@10.4.2) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.2-dev.1...@rxap/nest-open-api@10.4.2-dev.2) (2024-07-22)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.2-dev.0...@rxap/nest-open-api@10.4.2-dev.1) (2024-07-16)

### Bug Fixes

- coerce the loaded config file to be an array ([0eca709](https://gitlab.com/rxap/packages/commit/0eca709fca4f5f071cff3e880b244fd3b7cfe3d2))

## [10.4.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.1...@rxap/nest-open-api@10.4.2-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.1-dev.0...@rxap/nest-open-api@10.4.1) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.4.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.4.0...@rxap/nest-open-api@10.4.1-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.4.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2...@rxap/nest-open-api@10.4.0) (2024-06-28)

### Features

- support custom open api command interceptors ([e013acd](https://gitlab.com/rxap/packages/commit/e013acde36e428d0730aabb7ca47dbbf05ce17f4))

## [10.3.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.5...@rxap/nest-open-api@10.3.2) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.3.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.4...@rxap/nest-open-api@10.3.2-dev.5) (2024-06-25)

### Bug Fixes

- make member protected ([6163f10](https://gitlab.com/rxap/packages/commit/6163f10aa9bd2694d5ed84d09055c69bf51cefdb))

## [10.3.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.3...@rxap/nest-open-api@10.3.2-dev.4) (2024-06-25)

### Bug Fixes

- make member protected ([893bba2](https://gitlab.com/rxap/packages/commit/893bba2f56a551434978fe81c2a46dd84cf6692e))

## [10.3.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.2...@rxap/nest-open-api@10.3.2-dev.3) (2024-06-25)

### Bug Fixes

- support custom interceptors ([00f2940](https://gitlab.com/rxap/packages/commit/00f2940731c68594cc5b046a7bd9ada6cf9904de))

## [10.3.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.1...@rxap/nest-open-api@10.3.2-dev.2) (2024-06-25)

### Bug Fixes

- make member protected ([6691bf0](https://gitlab.com/rxap/packages/commit/6691bf01719051ab14f51d8fc74ff0513631be3f))

## [10.3.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.2-dev.0...@rxap/nest-open-api@10.3.2-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.1...@rxap/nest-open-api@10.3.2-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.3.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.1-dev.0...@rxap/nest-open-api@10.3.1) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.3.0...@rxap/nest-open-api@10.3.1-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.3.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.2-dev.1...@rxap/nest-open-api@10.3.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.2-dev.0...@rxap/nest-open-api@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1...@rxap/nest-open-api@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1...@rxap/nest-open-api@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1...@rxap/nest-open-api@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1...@rxap/nest-open-api@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1...@rxap/nest-open-api@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.0...@rxap/nest-open-api@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.2...@rxap/nest-open-api@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.2.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.2-dev.0...@rxap/nest-open-api@10.2.2) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.1...@rxap/nest-open-api@10.2.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.2.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.1-dev.0...@rxap/nest-open-api@10.2.1) (2024-05-27)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.0...@rxap/nest-open-api@10.2.1-dev.0) (2024-05-27)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.0-dev.2...@rxap/nest-open-api@10.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.0-dev.1...@rxap/nest-open-api@10.2.0-dev.2) (2024-03-31)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.2.0-dev.0...@rxap/nest-open-api@10.2.0-dev.1) (2024-03-27)

### Bug Fixes

- improve logging ([9561229](https://gitlab.com/rxap/packages/commit/9561229c3d845671f1642912c2f412299583e092))

# [10.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.1-dev.3...@rxap/nest-open-api@10.2.0-dev.0) (2024-03-27)

### Features

- support custom jwt header name ([d8477c4](https://gitlab.com/rxap/packages/commit/d8477c4709c32222fe3807137346abff9e506d77))

## [10.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.1-dev.2...@rxap/nest-open-api@10.1.1-dev.3) (2024-03-26)

### Bug Fixes

- **open-api-operation:** update logging to stringify request data ([0d6f661](https://gitlab.com/rxap/packages/commit/0d6f661772f968fc583c00fab6e676939aabf702))

## [10.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.1-dev.1...@rxap/nest-open-api@10.1.1-dev.2) (2024-03-11)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.1-dev.0...@rxap/nest-open-api@10.1.1-dev.1) (2024-03-05)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0...@rxap/nest-open-api@10.1.1-dev.0) (2024-03-04)

### Bug Fixes

- log composed file path with scope ([57dfadd](https://gitlab.com/rxap/packages/commit/57dfadd106d036f6727be9bd92debbb5a144fde7))

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.9...@rxap/nest-open-api@10.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.1.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.8...@rxap/nest-open-api@10.1.0-dev.9) (2023-10-18)

### Features

- interpolate with environment variables ([a6eb5e0](https://gitlab.com/rxap/packages/commit/a6eb5e0f28ff21d514e32f4893336e36dc575019))

# [10.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.7...@rxap/nest-open-api@10.1.0-dev.8) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.7...@rxap/nest-open-api@10.1.0-dev.7) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-open-api

# 10.1.0-dev.7 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- ignore AppController for request/response interceptor ([0380442](https://gitlab.com/rxap/packages/commit/0380442737abb17a29a980db330e7010bc7515ab))
- load files depending on the env name ([add02da](https://gitlab.com/rxap/packages/commit/add02da826e5aa1e22eea80b7efc473d20d6dc49))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))
- support dynamic OpenApiOperationCommand creation ([8e4e2b0](https://gitlab.com/rxap/packages/commit/8e4e2b0ba7deb1d3153944fb5cb6d1bfcf31be5e))
- support dynamic server config loading ([17f73ab](https://gitlab.com/rxap/packages/commit/17f73aba524837a8e21f85b84d5cef8b2fe1e99b))

### Features

- add GetOpenapiJson function ([0acdc89](https://gitlab.com/rxap/packages/commit/0acdc8990820faf4658e22edca623dbbb41f6d76))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [10.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.5...@rxap/nest-open-api@10.1.0-dev.6) (2023-10-02)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.4...@rxap/nest-open-api@10.1.0-dev.5) (2023-09-27)

### Bug Fixes

- load files depending on the env name ([8291806](https://gitlab.com/rxap/packages/commit/829180655a1f748a6409d5ff276f1d97275ed0dc))

# [10.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.3...@rxap/nest-open-api@10.1.0-dev.4) (2023-09-27)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.2...@rxap/nest-open-api@10.1.0-dev.3) (2023-09-19)

### Bug Fixes

- ignore AppController for request/response interceptor ([b05b972](https://gitlab.com/rxap/packages/commit/b05b972584773e8452128503e9fa71bbfedca85a))
- support dynamic OpenApiOperationCommand creation ([ab4acfb](https://gitlab.com/rxap/packages/commit/ab4acfbee424bda4d97526fe9738bc776c8685e1))
- support dynamic server config loading ([088583a](https://gitlab.com/rxap/packages/commit/088583acece9a693a461c958af3fa27cb20d661f))

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.1...@rxap/nest-open-api@10.1.0-dev.2) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [10.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.1.0-dev.0...@rxap/nest-open-api@10.1.0-dev.1) (2023-09-12)

**Note:** Version bump only for package @rxap/nest-open-api

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.7...@rxap/nest-open-api@10.1.0-dev.0) (2023-09-09)

### Features

- add GetOpenapiJson function ([55a134e](https://gitlab.com/rxap/packages/commit/55a134e22c5d59e19e699183b8658046c8b539d6))

## [10.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.6...@rxap/nest-open-api@10.0.1-dev.7) (2023-09-07)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.5...@rxap/nest-open-api@10.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.4...@rxap/nest-open-api@10.0.1-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-open-api

## [10.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.3...@rxap/nest-open-api@10.0.1-dev.4) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [10.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.2...@rxap/nest-open-api@10.0.1-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.1...@rxap/nest-open-api@10.0.1-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-open-api@10.0.1-dev.0...@rxap/nest-open-api@10.0.1-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 10.0.1-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/nest-open-api
