# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.3.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.3...@rxap/nest-jwt@10.3.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.3-dev.0...@rxap/nest-jwt@10.3.3) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.2...@rxap/nest-jwt@10.3.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1...@rxap/nest-jwt@10.3.2) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.3.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.5...@rxap/nest-jwt@10.3.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.4...@rxap/nest-jwt@10.3.1-dev.5) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.3.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.3...@rxap/nest-jwt@10.3.1-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.2...@rxap/nest-jwt@10.3.1-dev.3) (2025-02-18)

### Bug Fixes

- update package groups ([d7297d7](https://gitlab.com/rxap/packages/commit/d7297d70ae488cd81c73761cea1d4fbe1d22203d))

## [10.3.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.1...@rxap/nest-jwt@10.3.1-dev.2) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [10.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.1-dev.0...@rxap/nest-jwt@10.3.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.0...@rxap/nest-jwt@10.3.1-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.3.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.0-dev.3...@rxap/nest-jwt@10.3.0) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.3.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.0-dev.2...@rxap/nest-jwt@10.3.0-dev.3) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.0-dev.1...@rxap/nest-jwt@10.3.0-dev.2) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.3.0-dev.0...@rxap/nest-jwt@10.3.0-dev.1) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.9-dev.2...@rxap/nest-jwt@10.3.0-dev.0) (2025-02-04)

### Features

- add permission utilities ([a5fa58b](https://gitlab.com/rxap/packages/commit/a5fa58b9dcc8284262d90543c01493c8a107e8d4))

## [10.2.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.9-dev.1...@rxap/nest-jwt@10.2.9-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.9-dev.0...@rxap/nest-jwt@10.2.9-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.8...@rxap/nest-jwt@10.2.9-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.8-dev.2...@rxap/nest-jwt@10.2.8) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.8-dev.1...@rxap/nest-jwt@10.2.8-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.8-dev.0...@rxap/nest-jwt@10.2.8-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.7...@rxap/nest-jwt@10.2.8-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.7-dev.0...@rxap/nest-jwt@10.2.7) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.2.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.6...@rxap/nest-jwt@10.2.7-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.6-dev.0...@rxap/nest-jwt@10.2.6) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.5...@rxap/nest-jwt@10.2.6-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.5-dev.0...@rxap/nest-jwt@10.2.5) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.4...@rxap/nest-jwt@10.2.5-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.4-dev.0...@rxap/nest-jwt@10.2.4) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.3...@rxap/nest-jwt@10.2.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.2...@rxap/nest-jwt@10.2.3) (2024-06-28)

### Bug Fixes

- add utility functions ([35dd532](https://gitlab.com/rxap/packages/commit/35dd532337c24397d61e4489df3a659485b8ed37))

## [10.2.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.2-dev.2...@rxap/nest-jwt@10.2.2) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.2-dev.1...@rxap/nest-jwt@10.2.2-dev.2) (2024-06-25)

### Bug Fixes

- make member protected ([a4ac752](https://gitlab.com/rxap/packages/commit/a4ac7521707f0f1d925db744fdfe582e4f4712fa))

## [10.2.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.2-dev.0...@rxap/nest-jwt@10.2.2-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.1...@rxap/nest-jwt@10.2.2-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.1-dev.0...@rxap/nest-jwt@10.2.1) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.2.0...@rxap/nest-jwt@10.2.1-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.2.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.1...@rxap/nest-jwt@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.1.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.1-dev.0...@rxap/nest-jwt@10.1.1) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.0...@rxap/nest-jwt@10.1.1-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.0-dev.3...@rxap/nest-jwt@10.1.0) (2024-04-17)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.0-dev.2...@rxap/nest-jwt@10.1.0-dev.3) (2024-04-09)

**Note:** Version bump only for package @rxap/nest-jwt

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.0-dev.1...@rxap/nest-jwt@10.1.0-dev.2) (2024-04-04)

### Features

- optional jwt validation ([33e146a](https://gitlab.com/rxap/packages/commit/33e146a9b24a5d1400b5778940a8b82701a845ad))

# [10.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.1.0-dev.0...@rxap/nest-jwt@10.1.0-dev.1) (2024-03-27)

### Bug Fixes

- improve logging ([9561229](https://gitlab.com/rxap/packages/commit/9561229c3d845671f1642912c2f412299583e092))

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.2-dev.1...@rxap/nest-jwt@10.1.0-dev.0) (2024-03-27)

### Features

- support custom jwt headers ([49a64ef](https://gitlab.com/rxap/packages/commit/49a64efc3a8f4aa3742f25c448624c795a6e94fa))

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.2-dev.0...@rxap/nest-jwt@10.0.2-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1...@rxap/nest-jwt@10.0.2-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.14...@rxap/nest-jwt@10.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.14...@rxap/nest-jwt@10.0.1-dev.14) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-jwt

## 10.0.1-dev.14 (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- add more checks ([5220c76](https://gitlab.com/rxap/packages/commit/5220c7652364756891bee6d3195e302d06a21317))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- check if the jwt property is set or throw internal server execution ([1690ce7](https://gitlab.com/rxap/packages/commit/1690ce7f3f4817fcfc2e93df850f0f97101075e4))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- move forRoot logic into detected loader classes ([45812d6](https://gitlab.com/rxap/packages/commit/45812d66901f37130ec4018b0bc9369829800155))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

## [10.0.1-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.12...@rxap/nest-jwt@10.0.1-dev.13) (2023-10-02)

### Bug Fixes

- move forRoot logic into detected loader classes ([01796e0](https://gitlab.com/rxap/packages/commit/01796e0898a3dee4e365278a73029dd023093136))

## [10.0.1-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.11...@rxap/nest-jwt@10.0.1-dev.12) (2023-09-29)

### Bug Fixes

- add more checks ([6855eed](https://gitlab.com/rxap/packages/commit/6855eedf9fc753cd0edd00554058c79fb49cee6d))

## [10.0.1-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.10...@rxap/nest-jwt@10.0.1-dev.11) (2023-09-27)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.9...@rxap/nest-jwt@10.0.1-dev.10) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

## [10.0.1-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.8...@rxap/nest-jwt@10.0.1-dev.9) (2023-09-12)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.7...@rxap/nest-jwt@10.0.1-dev.8) (2023-09-07)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.6...@rxap/nest-jwt@10.0.1-dev.7) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.5...@rxap/nest-jwt@10.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-jwt

## [10.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.4...@rxap/nest-jwt@10.0.1-dev.5) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [10.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.3...@rxap/nest-jwt@10.0.1-dev.4) (2023-08-24)

### Bug Fixes

- check if the jwt property is set or throw internal server execution ([5ae4a66](https://gitlab.com/rxap/packages/commit/5ae4a663404b5319da3b7340839fbacce9985fe3))

## [10.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.2...@rxap/nest-jwt@10.0.1-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.1...@rxap/nest-jwt@10.0.1-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-jwt@10.0.1-dev.0...@rxap/nest-jwt@10.0.1-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 10.0.1-dev.0 (2023-08-01)

**Note:** Version bump only for package @rxap/nest-jwt
