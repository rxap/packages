# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.2.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.3...@rxap/nest-vault@10.2.4-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.3-dev.0...@rxap/nest-vault@10.2.3) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.2...@rxap/nest-vault@10.2.3-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.2-dev.2...@rxap/nest-vault@10.2.2) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.2-dev.1...@rxap/nest-vault@10.2.2-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.2-dev.0...@rxap/nest-vault@10.2.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1...@rxap/nest-vault@10.2.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.5...@rxap/nest-vault@10.2.1) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.4...@rxap/nest-vault@10.2.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.3...@rxap/nest-vault@10.2.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.2...@rxap/nest-vault@10.2.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.1...@rxap/nest-vault@10.2.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.1-dev.0...@rxap/nest-vault@10.2.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-vault

## [10.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.0...@rxap/nest-vault@10.2.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-vault

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.2.0-dev.0...@rxap/nest-vault@10.2.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-vault

# [10.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.2-dev.2...@rxap/nest-vault@10.2.0-dev.0) (2025-01-04)

### Features

- move vault and rabbitmq feature to separate package ([85aa64d](https://gitlab.com/rxap/packages/commit/85aa64df35087dfc662fc89b0812c0e907d37344))

## [10.1.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.2-dev.1...@rxap/nest-vault@10.1.2-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-vault

## [10.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.2-dev.0...@rxap/nest-vault@10.1.2-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-vault

## [10.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.1...@rxap/nest-vault@10.1.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-vault

## [10.1.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.1-dev.0...@rxap/nest-vault@10.1.1) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.0...@rxap/nest-vault@10.1.1-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-vault

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.0-dev.2...@rxap/nest-vault@10.1.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-vault

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.0-dev.1...@rxap/nest-vault@10.1.0-dev.2) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-vault

# [10.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.1.0-dev.0...@rxap/nest-vault@10.1.0-dev.1) (2024-08-12)

### Bug Fixes

- improve logging and error handling ([4a3b69d](https://gitlab.com/rxap/packages/commit/4a3b69db66088ea7e7f16181c22a86b6b6041a17))

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.6...@rxap/nest-vault@10.1.0-dev.0) (2024-08-07)

### Features

- add health indicator ([981e9c0](https://gitlab.com/rxap/packages/commit/981e9c0c2ef12d6d83ca0fd9171fc7f3d8a90c4c))
- add RabbitmqVaultService service ([7fafa96](https://gitlab.com/rxap/packages/commit/7fafa968d91e603387917b5e68649deb046b0b7f))

## [10.0.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.6-dev.0...@rxap/nest-vault@10.0.6) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.5...@rxap/nest-vault@10.0.6-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.5-dev.0...@rxap/nest-vault@10.0.5) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.4...@rxap/nest-vault@10.0.5-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.4-dev.1...@rxap/nest-vault@10.0.4) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.4-dev.0...@rxap/nest-vault@10.0.4-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.3...@rxap/nest-vault@10.0.4-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.3-dev.0...@rxap/nest-vault@10.0.3) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.2...@rxap/nest-vault@10.0.3-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.2-dev.1...@rxap/nest-vault@10.0.2) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.2-dev.0...@rxap/nest-vault@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1...@rxap/nest-vault@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1...@rxap/nest-vault@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1...@rxap/nest-vault@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1...@rxap/nest-vault@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1...@rxap/nest-vault@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@10.0.1-dev.0...@rxap/nest-vault@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-vault

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@0.0.3...@rxap/nest-vault@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-vault

## [0.0.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@0.0.3-dev.0...@rxap/nest-vault@0.0.3) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-vault

## [0.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@0.0.2...@rxap/nest-vault@0.0.3-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-vault

## [0.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-vault@0.0.2-dev.0...@rxap/nest-vault@0.0.2) (2024-05-27)

**Note:** Version bump only for package @rxap/nest-vault

## 0.0.2-dev.0 (2024-05-22)

**Note:** Version bump only for package @rxap/nest-vault
