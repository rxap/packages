# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.3.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.9...@rxap/nest-dto@10.3.10-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.9-dev.1...@rxap/nest-dto@10.3.9) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.9-dev.0...@rxap/nest-dto@10.3.9-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8...@rxap/nest-dto@10.3.9-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.5...@rxap/nest-dto@10.3.8) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.4...@rxap/nest-dto@10.3.8-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.3...@rxap/nest-dto@10.3.8-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.2...@rxap/nest-dto@10.3.8-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.1...@rxap/nest-dto@10.3.8-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.8-dev.0...@rxap/nest-dto@10.3.8-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.7...@rxap/nest-dto@10.3.8-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.7-dev.2...@rxap/nest-dto@10.3.7) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.7-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.7-dev.1...@rxap/nest-dto@10.3.7-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.7-dev.0...@rxap/nest-dto@10.3.7-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.6...@rxap/nest-dto@10.3.7-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.6-dev.1...@rxap/nest-dto@10.3.6) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.3.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.6-dev.0...@rxap/nest-dto@10.3.6-dev.1) (2024-10-17)

### Bug Fixes

- move the class validator and transformer options object to nest-dto ([ccc3976](https://gitlab.com/rxap/packages/commit/ccc3976efa1b9dd3eedc930aea7b9a89197c1635))

## [10.3.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.5...@rxap/nest-dto@10.3.6-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.5-dev.0...@rxap/nest-dto@10.3.5) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.4...@rxap/nest-dto@10.3.5-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.4-dev.0...@rxap/nest-dto@10.3.4) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.3...@rxap/nest-dto@10.3.4-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.3-dev.0...@rxap/nest-dto@10.3.3) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.2...@rxap/nest-dto@10.3.3-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.2-dev.1...@rxap/nest-dto@10.3.2) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.2-dev.0...@rxap/nest-dto@10.3.2-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.1...@rxap/nest-dto@10.3.2-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.1-dev.0...@rxap/nest-dto@10.3.1) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-dto

## [10.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.3.0...@rxap/nest-dto@10.3.1-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-dto

# [10.3.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.2-dev.1...@rxap/nest-dto@10.3.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.2-dev.0...@rxap/nest-dto@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1...@rxap/nest-dto@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1...@rxap/nest-dto@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1...@rxap/nest-dto@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1...@rxap/nest-dto@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1...@rxap/nest-dto@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1-dev.0...@rxap/nest-dto@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-dto

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.1...@rxap/nest-dto@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-dto

## [10.2.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.1-dev.0...@rxap/nest-dto@10.2.1) (2024-05-27)

**Note:** Version bump only for package @rxap/nest-dto

## [10.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.0...@rxap/nest-dto@10.2.1-dev.0) (2024-05-18)

### Bug Fixes

- support dto classes with method members ([2552ce0](https://gitlab.com/rxap/packages/commit/2552ce031b86a029e0e082974ae0d7e19e5e93c7))

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.0-dev.3...@rxap/nest-dto@10.2.0) (2024-04-17)

**Note:** Version bump only for package @rxap/nest-dto

# [10.2.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.0-dev.2...@rxap/nest-dto@10.2.0-dev.3) (2024-03-26)

### Bug Fixes

- **nest:** add icon and tooltip fields to icon dto ([f259cec](https://gitlab.com/rxap/packages/commit/f259cec54a234aab1126c79cd0a8c555eae49dd4))

# [10.2.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.0-dev.1...@rxap/nest-dto@10.2.0-dev.2) (2024-03-11)

**Note:** Version bump only for package @rxap/nest-dto

# [10.2.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.2.0-dev.0...@rxap/nest-dto@10.2.0-dev.1) (2024-03-09)

### Features

- add ToDtoInstanceList ([97d2963](https://gitlab.com/rxap/packages/commit/97d29635fd49dff369067d32a7150c6c169af340))

# [10.2.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.1.0...@rxap/nest-dto@10.2.0-dev.0) (2024-02-28)

### Features

- add ToDtoInstance function ([6ac05ed](https://gitlab.com/rxap/packages/commit/6ac05ed252c669f69b448b7f50409e03c0273814))

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.1.0-dev.2...@rxap/nest-dto@10.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-dto

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.1.0-dev.1...@rxap/nest-dto@10.1.0-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-dto

# 10.1.0-dev.1 (2023-10-11)

### Features

- add ValueDto utility classes ([ebe51dd](https://gitlab.com/rxap/packages/commit/ebe51dd9acfff8b29bb00c58c25272c1ece7594f))

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-dto@10.0.1-dev.0...@rxap/nest-dto@10.1.0-dev.0) (2023-09-29)

### Features

- add ValueDto utility classes ([0c441f0](https://gitlab.com/rxap/packages/commit/0c441f01f3021fe1d5ada07045134299ed90e05e))

## 10.0.1-dev.0 (2023-09-28)

**Note:** Version bump only for package @rxap/nest-dto
