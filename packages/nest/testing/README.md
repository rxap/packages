Provides factories for creating mock config services and loggers, useful for testing NestJS applications. Includes a MockConfigServiceFactory to generate config service mocks and a MockLoggerFactory for logger mocks. Also includes an init generator for adding peer dependencies to a project.

[![npm version](https://img.shields.io/npm/v/@rxap/nest-testing?style=flat-square)](https://www.npmjs.com/package/@rxap/nest-testing)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/nest-testing)
![npm](https://img.shields.io/npm/dm/@rxap/nest-testing)
![NPM](https://img.shields.io/npm/l/@rxap/nest-testing)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/nest-testing
```
**Execute the init generator:**
```bash
yarn nx g @rxap/nest-testing:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/nest-testing:init
```
