# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.12-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.11...@rxap/nest-testing@10.0.12-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.11](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.11-dev.1...@rxap/nest-testing@10.0.11) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.11-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.11-dev.0...@rxap/nest-testing@10.0.11-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10...@rxap/nest-testing@10.0.11-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.5...@rxap/nest-testing@10.0.10) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.4...@rxap/nest-testing@10.0.10-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.3...@rxap/nest-testing@10.0.10-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.2...@rxap/nest-testing@10.0.10-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.1...@rxap/nest-testing@10.0.10-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.10-dev.0...@rxap/nest-testing@10.0.10-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.9...@rxap/nest-testing@10.0.10-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.9-dev.2...@rxap/nest-testing@10.0.9) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.9-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.9-dev.1...@rxap/nest-testing@10.0.9-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.9-dev.0...@rxap/nest-testing@10.0.9-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.8...@rxap/nest-testing@10.0.9-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.8-dev.0...@rxap/nest-testing@10.0.8) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.7...@rxap/nest-testing@10.0.8-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.7-dev.0...@rxap/nest-testing@10.0.7) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.6...@rxap/nest-testing@10.0.7-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.6-dev.0...@rxap/nest-testing@10.0.6) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.5...@rxap/nest-testing@10.0.6-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.5-dev.0...@rxap/nest-testing@10.0.5) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.4...@rxap/nest-testing@10.0.5-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.4-dev.1...@rxap/nest-testing@10.0.4) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.4-dev.0...@rxap/nest-testing@10.0.4-dev.1) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.3...@rxap/nest-testing@10.0.4-dev.0) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.3-dev.0...@rxap/nest-testing@10.0.3) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.2...@rxap/nest-testing@10.0.3-dev.0) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.2-dev.0...@rxap/nest-testing@10.0.2) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.0...@rxap/nest-testing@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1...@rxap/nest-testing@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.3...@rxap/nest-testing@10.0.1) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-testing

## [10.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.2...@rxap/nest-testing@10.0.1-dev.3) (2023-10-18)

### Bug Fixes

- support modification of the config object ([7ce0886](https://gitlab.com/rxap/packages/commit/7ce0886e3ffadbb9506215866826763d21ac0519))

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-testing@10.0.1-dev.1...@rxap/nest-testing@10.0.1-dev.2) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-testing

## 10.0.1-dev.1 (2023-10-11)

**Note:** Version bump only for package @rxap/nest-testing

## 10.0.1-dev.0 (2023-09-29)

**Note:** Version bump only for package @rxap/nest-testing
