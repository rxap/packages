This package provides an Auth0 module for NestJS applications, simplifying integration with Auth0 for authentication and authorization. It includes a configurable module, JWT strategy, and service for interacting with the Auth0 Authentication API. The module also offers validation schemas for environment variables related to Auth0 configuration.

[![npm version](https://img.shields.io/npm/v/@rxap/nest-auth0?style=flat-square)](https://www.npmjs.com/package/@rxap/nest-auth0)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/nest-auth0)
![npm](https://img.shields.io/npm/dm/@rxap/nest-auth0)
![NPM](https://img.shields.io/npm/l/@rxap/nest-auth0)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/nest-auth0
```
**Execute the init generator:**
```bash
yarn nx g @rxap/nest-auth0:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/nest-auth0:init
```
