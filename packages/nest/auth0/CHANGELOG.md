# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.1...@rxap/nest-auth0@10.1.0-dev.0) (2025-03-12)

### Features

- **auth0:** enhance validation schema with default values support ([10cd7ce](https://gitlab.com/rxap/packages/commit/10cd7ce818e022bc46247f761ece006257b4c29a))

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.1-dev.2...@rxap/nest-auth0@10.0.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-auth0

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.1-dev.1...@rxap/nest-auth0@10.0.1-dev.2) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-auth0

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.1-dev.0...@rxap/nest-auth0@10.0.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-auth0

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.0...@rxap/nest-auth0@10.0.1-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-auth0

# [10.0.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.0-dev.4...@rxap/nest-auth0@10.0.0) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-auth0

# [10.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.0-dev.3...@rxap/nest-auth0@10.0.0-dev.4) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-auth0

# [10.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.0-dev.2...@rxap/nest-auth0@10.0.0-dev.3) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-auth0

# [10.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-auth0@10.0.0-dev.1...@rxap/nest-auth0@10.0.0-dev.2) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-auth0

# 10.0.0-dev.1 (2025-02-04)

**Note:** Version bump only for package @rxap/nest-auth0
