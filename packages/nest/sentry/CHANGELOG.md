# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.5.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.5-dev.0...@rxap/nest-sentry@10.5.5-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.4...@rxap/nest-sentry@10.5.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.4-dev.3...@rxap/nest-sentry@10.5.4) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.4-dev.2...@rxap/nest-sentry@10.5.4-dev.3) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.4-dev.1...@rxap/nest-sentry@10.5.4-dev.2) (2025-02-26)

### Bug Fixes

- update package groups ([5810ff6](https://gitlab.com/rxap/packages/commit/5810ff609fe867842a6c9fc8531511dc1e97587a))

## [10.5.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.4-dev.0...@rxap/nest-sentry@10.5.4-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.3...@rxap/nest-sentry@10.5.4-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.2...@rxap/nest-sentry@10.5.3) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.5.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1...@rxap/nest-sentry@10.5.2) (2025-02-23)

### Bug Fixes

- update package groups ([cf8d711](https://gitlab.com/rxap/packages/commit/cf8d711893a84dd040abccfc5a55d0648e40460e))

## [10.5.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.7...@rxap/nest-sentry@10.5.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.6...@rxap/nest-sentry@10.5.1-dev.7) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.5.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.5...@rxap/nest-sentry@10.5.1-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.4...@rxap/nest-sentry@10.5.1-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.3...@rxap/nest-sentry@10.5.1-dev.4) (2025-02-18)

### Bug Fixes

- update package groups ([d7297d7](https://gitlab.com/rxap/packages/commit/d7297d70ae488cd81c73761cea1d4fbe1d22203d))

## [10.5.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.2...@rxap/nest-sentry@10.5.1-dev.3) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [10.5.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.1...@rxap/nest-sentry@10.5.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.1-dev.0...@rxap/nest-sentry@10.5.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.5.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0...@rxap/nest-sentry@10.5.1-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.15...@rxap/nest-sentry@10.5.0) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.14...@rxap/nest-sentry@10.5.0-dev.15) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.13...@rxap/nest-sentry@10.5.0-dev.14) (2025-02-11)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.12...@rxap/nest-sentry@10.5.0-dev.13) (2025-02-11)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.11...@rxap/nest-sentry@10.5.0-dev.12) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.10...@rxap/nest-sentry@10.5.0-dev.11) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.9...@rxap/nest-sentry@10.5.0-dev.10) (2025-01-30)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.8...@rxap/nest-sentry@10.5.0-dev.9) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.7...@rxap/nest-sentry@10.5.0-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.6...@rxap/nest-sentry@10.5.0-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.5...@rxap/nest-sentry@10.5.0-dev.6) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.4...@rxap/nest-sentry@10.5.0-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.3...@rxap/nest-sentry@10.5.0-dev.4) (2025-01-22)

### Features

- add validation schema ([792719a](https://gitlab.com/rxap/packages/commit/792719a1bbea5aef18b7352933f0650725485e99))

# [10.5.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.2...@rxap/nest-sentry@10.5.0-dev.3) (2025-01-21)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.5.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.1...@rxap/nest-sentry@10.5.0-dev.2) (2025-01-16)

### Bug Fixes

- improve logging output ([f9b8a0d](https://gitlab.com/rxap/packages/commit/f9b8a0d59694a6548a73744685c507ff0ceb262a))

# [10.5.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.5.0-dev.0...@rxap/nest-sentry@10.5.0-dev.1) (2025-01-16)

### Features

- support custom print messages function ([db34f07](https://gitlab.com/rxap/packages/commit/db34f070178b9ea7a453304e7f66d20d3a508e01))

# [10.5.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.6-dev.0...@rxap/nest-sentry@10.5.0-dev.0) (2025-01-15)

### Features

- support custom interceptors with module registration ([bb31033](https://gitlab.com/rxap/packages/commit/bb31033cce2f85a315909e477a3ac7605b6c0a9a))

## [10.4.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.5...@rxap/nest-sentry@10.4.6-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.5-dev.3...@rxap/nest-sentry@10.4.5) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.5-dev.2...@rxap/nest-sentry@10.4.5-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.5-dev.1...@rxap/nest-sentry@10.4.5-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.5-dev.0...@rxap/nest-sentry@10.4.5-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.4...@rxap/nest-sentry@10.4.5-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.4-dev.1...@rxap/nest-sentry@10.4.4) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.4-dev.0...@rxap/nest-sentry@10.4.4-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.3...@rxap/nest-sentry@10.4.4-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.3-dev.3...@rxap/nest-sentry@10.4.3) (2024-10-28)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.3-dev.2...@rxap/nest-sentry@10.4.3-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.3-dev.1...@rxap/nest-sentry@10.4.3-dev.2) (2024-10-25)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.4.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.3-dev.0...@rxap/nest-sentry@10.4.3-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.2...@rxap/nest-sentry@10.4.3-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.2-dev.1...@rxap/nest-sentry@10.4.2) (2024-09-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.2-dev.0...@rxap/nest-sentry@10.4.2-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1...@rxap/nest-sentry@10.4.2-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.6...@rxap/nest-sentry@10.4.1) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.5...@rxap/nest-sentry@10.4.1-dev.6) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.4...@rxap/nest-sentry@10.4.1-dev.5) (2024-08-21)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.3...@rxap/nest-sentry@10.4.1-dev.4) (2024-08-15)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.4.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.2...@rxap/nest-sentry@10.4.1-dev.3) (2024-08-15)

### Bug Fixes

- improve logging ([7469409](https://gitlab.com/rxap/packages/commit/746940977a9dcfa101d5499069635b36de831740))

## [10.4.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.1...@rxap/nest-sentry@10.4.1-dev.2) (2024-08-14)

### Bug Fixes

- add logger factories ([309bc85](https://gitlab.com/rxap/packages/commit/309bc85e6f0373767a6836d1cd805ea777ea0327))

## [10.4.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.1-dev.0...@rxap/nest-sentry@10.4.1-dev.1) (2024-08-14)

### Bug Fixes

- use global options ([53b414c](https://gitlab.com/rxap/packages/commit/53b414cf018f58ccb38713a0787dc0d5fb2932d4))
- use global options ([9157bc1](https://gitlab.com/rxap/packages/commit/9157bc167f875a4ef0a8b03a14bb0258990f3bff))

## [10.4.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.0...@rxap/nest-sentry@10.4.1-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.4.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.0-dev.1...@rxap/nest-sentry@10.4.0) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.4.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.4.0-dev.0...@rxap/nest-sentry@10.4.0-dev.1) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.4.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.5...@rxap/nest-sentry@10.4.0-dev.0) (2024-07-22)

### Features

- use new nestjs specific sentry instance ([f5472e8](https://gitlab.com/rxap/packages/commit/f5472e83133ad8fdd08b078956482b701ebd18f1))

## [10.3.4-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.4...@rxap/nest-sentry@10.3.4-dev.5) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.4-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.3...@rxap/nest-sentry@10.3.4-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.4-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.2...@rxap/nest-sentry@10.3.4-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.1...@rxap/nest-sentry@10.3.4-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.4-dev.0...@rxap/nest-sentry@10.3.4-dev.1) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.3...@rxap/nest-sentry@10.3.4-dev.0) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.3-dev.0...@rxap/nest-sentry@10.3.3) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.2...@rxap/nest-sentry@10.3.3-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.2-dev.3...@rxap/nest-sentry@10.3.2) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.2-dev.2...@rxap/nest-sentry@10.3.2-dev.3) (2024-06-24)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.2-dev.1...@rxap/nest-sentry@10.3.2-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.2-dev.0...@rxap/nest-sentry@10.3.2-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1...@rxap/nest-sentry@10.3.2-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1-dev.4...@rxap/nest-sentry@10.3.1) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1-dev.3...@rxap/nest-sentry@10.3.1-dev.4) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1-dev.2...@rxap/nest-sentry@10.3.1-dev.3) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1-dev.1...@rxap/nest-sentry@10.3.1-dev.2) (2024-06-11)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.1-dev.0...@rxap/nest-sentry@10.3.1-dev.1) (2024-06-11)

### Bug Fixes

- remove circular dependency ([e6da87d](https://gitlab.com/rxap/packages/commit/e6da87d1ddfe9c52e59ab0375f4be1b20655a1cb))

## [10.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.0...@rxap/nest-sentry@10.3.1-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.3.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.3.0-dev.1...@rxap/nest-sentry@10.3.0) (2024-06-02)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.2.0...@rxap/nest-sentry@10.3.0-dev.1) (2024-06-02)

### Features

- add the SentryLoggerModule ([fedced2](https://gitlab.com/rxap/packages/commit/fedced263a6b00e767b62bfd47f7a30a24cd09dd))
- add the SentryLoggerModule ([761d8b1](https://gitlab.com/rxap/packages/commit/761d8b18547b91e361ee5ef844d3b2eb703f988b))

# [10.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.2.0...@rxap/nest-sentry@10.3.0-dev.0) (2024-05-31)

### Features

- add the SentryLoggerModule ([8961f9b](https://gitlab.com/rxap/packages/commit/8961f9b5089480b202de8bcabc9c94f211f1906d))
- add the SentryLoggerModule ([76039d0](https://gitlab.com/rxap/packages/commit/76039d071726096f65c4c920b667f667b12ac673))

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.2-dev.1...@rxap/nest-sentry@10.2.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.2-dev.0...@rxap/nest-sentry@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1...@rxap/nest-sentry@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1...@rxap/nest-sentry@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1...@rxap/nest-sentry@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1...@rxap/nest-sentry@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1...@rxap/nest-sentry@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.0...@rxap/nest-sentry@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.2...@rxap/nest-sentry@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.1.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.2-dev.0...@rxap/nest-sentry@10.1.2) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.1...@rxap/nest-sentry@10.1.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.1.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.1-dev.1...@rxap/nest-sentry@10.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.1-dev.0...@rxap/nest-sentry@10.1.1-dev.1) (2024-03-11)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0...@rxap/nest-sentry@10.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0-dev.4...@rxap/nest-sentry@10.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0-dev.3...@rxap/nest-sentry@10.1.0-dev.4) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0-dev.3...@rxap/nest-sentry@10.1.0-dev.3) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@9.1.0-dev.2...@rxap/nest-sentry@10.1.0-dev.3) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Features

- add SetupSentryLogger function ([1424be7](https://gitlab.com/rxap/packages/commit/1424be7c4bac5a39782877c4145db5b95646ffb3))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0-dev.1...@rxap/nest-sentry@10.1.0-dev.2) (2023-09-27)

**Note:** Version bump only for package @rxap/nest-sentry

# [10.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.1.0-dev.0...@rxap/nest-sentry@10.1.0-dev.1) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.7...@rxap/nest-sentry@10.1.0-dev.0) (2023-09-12)

### Features

- add SetupSentryLogger function ([154601f](https://gitlab.com/rxap/packages/commit/154601f7fe806afc72e1e602d1d4744a7b5cc065))

## [10.0.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.6...@rxap/nest-sentry@10.0.1-dev.7) (2023-09-07)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.5...@rxap/nest-sentry@10.0.1-dev.6) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.4...@rxap/nest-sentry@10.0.1-dev.5) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-sentry

## [10.0.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.3...@rxap/nest-sentry@10.0.1-dev.4) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

## [10.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.2...@rxap/nest-sentry@10.0.1-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.1...@rxap/nest-sentry@10.0.1-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@10.0.1-dev.0...@rxap/nest-sentry@10.0.1-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 10.0.1-dev.0 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))
- update package dependency versions ([45bd022](https://gitlab.com/rxap/packages/commit/45bd022d755c0c11f7d0bcc76d26b39928007941))
- update to nx 16.5.0 ([1304640](https://gitlab.com/rxap/packages/commit/1304640641e351aef07bc4a2eaff339fcce6ec99))

# [9.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-sentry@9.1.0-dev.1...@rxap/nest-sentry@9.1.0-dev.2) (2023-07-10)

### Bug Fixes

- update package dependency versions ([8479f5c](https://gitlab.com/rxap/packages/commit/8479f5c405a885cc0f300cec6156584e4c65d59c))
- update to nx 16.5.0 ([73f7575](https://gitlab.com/rxap/packages/commit/73f7575ba378b8b03d2a2646f1761c01b16a6e09))

# 9.1.0-dev.1 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))

# [9.1.0-dev.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.5...@rxap/nest-sentry@9.1.0-dev.0) (2023-05-04)

### Features

- add RxapLogger to improve performance with large jsonobjects ([c521ba1](https://gitlab.com/rxap/nest/commit/c521ba1215caa693bda1ef2e41a0d69d633fd363))

## [9.0.3-dev.5](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.4...@rxap/nest-sentry@9.0.3-dev.5) (2023-04-27)

### Bug Fixes

- use correct base optionstype ([b5395c7](https://gitlab.com/rxap/nest/commit/b5395c7ff46e2f1b9f7dacd2d6526ccb37f12e7d))

## [9.0.3-dev.4](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.3...@rxap/nest-sentry@9.0.3-dev.4) (2023-04-04)

### Bug Fixes

- **interceptor:** allow exceptions to inject additionallyscopes ([0fd500b](https://gitlab.com/rxap/nest/commit/0fd500ba1eaea4b5d0bcefdb6679be596476331f))

## [9.0.3-dev.3](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.2...@rxap/nest-sentry@9.0.3-dev.3) (2023-04-04)

### Bug Fixes

- **logger:** mirror the ConsoleLoggerimplementation ([4ce7f2a](https://gitlab.com/rxap/nest/commit/4ce7f2a144d1e1ab3f0b3e6f858c928cc0482587))

## [9.0.3-dev.2](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.1...@rxap/nest-sentry@9.0.3-dev.2) (2023-04-04)

### Bug Fixes

- **logger:** split options into sentry andconsole ([21e8b59](https://gitlab.com/rxap/nest/commit/21e8b5991b0b30bb30bfc4d11d7b703c2a26904b))

## [9.0.3-dev.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.3-dev.0...@rxap/nest-sentry@9.0.3-dev.1) (2023-03-13)

### Bug Fixes

- add Global decorator toSentryModule ([9777d29](https://gitlab.com/rxap/nest/commit/9777d2911c140291786d81ca892e2eeabb817eff))

## [9.0.3-dev.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-sentry@9.0.2...@rxap/nest-sentry@9.0.3-dev.0) (2023-01-23)

### Bug Fixes

- **sentry:** support optional sentrydsn ([85688ab](https://gitlab.com/rxap/nest/commit/85688ab8812d782d2074ad6601e85324e25541f5))

## 9.0.2 (2022-12-28)

**Note:** Version bump only for package @rxap/nest-sentry

## 9.0.1 (2022-12-28)

**Note:** Version bump only for package @rxap/sentry
