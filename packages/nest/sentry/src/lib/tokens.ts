export const SENTRY_MODULE_OPTIONS = Symbol('SentryModuleOptions');
/**
 * InjectionToken for an object of type SentryInterceptorOptions
 */
export const SENTRY_INTERCEPTOR_OPTIONS = Symbol('SENTRY_INTERCEPTOR_OPTIONS');
