# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.3.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.5-dev.0...@rxap/nest-logger@10.3.5-dev.1) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.4...@rxap/nest-logger@10.3.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.4-dev.2...@rxap/nest-logger@10.3.4) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.4-dev.1...@rxap/nest-logger@10.3.4-dev.2) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.4-dev.0...@rxap/nest-logger@10.3.4-dev.1) (2025-02-26)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.3...@rxap/nest-logger@10.3.4-dev.0) (2025-02-25)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.2...@rxap/nest-logger@10.3.3) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.3.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1...@rxap/nest-logger@10.3.2) (2025-02-23)

### Bug Fixes

- update package groups ([cf8d711](https://gitlab.com/rxap/packages/commit/cf8d711893a84dd040abccfc5a55d0648e40460e))

## [10.3.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.7...@rxap/nest-logger@10.3.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.1-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.6...@rxap/nest-logger@10.3.1-dev.7) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.3.1-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.5...@rxap/nest-logger@10.3.1-dev.6) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.4...@rxap/nest-logger@10.3.1-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.3...@rxap/nest-logger@10.3.1-dev.4) (2025-02-18)

### Bug Fixes

- update package groups ([d7297d7](https://gitlab.com/rxap/packages/commit/d7297d70ae488cd81c73761cea1d4fbe1d22203d))

## [10.3.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.2...@rxap/nest-logger@10.3.1-dev.3) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [10.3.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.1...@rxap/nest-logger@10.3.1-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.1-dev.0...@rxap/nest-logger@10.3.1-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.3.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0...@rxap/nest-logger@10.3.1-dev.0) (2025-02-17)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.14...@rxap/nest-logger@10.3.0) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.13...@rxap/nest-logger@10.3.0-dev.14) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.12...@rxap/nest-logger@10.3.0-dev.13) (2025-02-11)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.11...@rxap/nest-logger@10.3.0-dev.12) (2025-02-11)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.10...@rxap/nest-logger@10.3.0-dev.11) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.9...@rxap/nest-logger@10.3.0-dev.10) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.8...@rxap/nest-logger@10.3.0-dev.9) (2025-01-30)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.7...@rxap/nest-logger@10.3.0-dev.8) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.6...@rxap/nest-logger@10.3.0-dev.7) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.5...@rxap/nest-logger@10.3.0-dev.6) (2025-01-29)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.4...@rxap/nest-logger@10.3.0-dev.5) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.3...@rxap/nest-logger@10.3.0-dev.4) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.2...@rxap/nest-logger@10.3.0-dev.3) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.1...@rxap/nest-logger@10.3.0-dev.2) (2025-01-21)

**Note:** Version bump only for package @rxap/nest-logger

# [10.3.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.3.0-dev.0...@rxap/nest-logger@10.3.0-dev.1) (2025-01-16)

### Features

- add google logging print messages function ([8bcf9ad](https://gitlab.com/rxap/packages/commit/8bcf9ad442259cc958878ef276582c3421d1b901))

# [10.3.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.11-dev.0...@rxap/nest-logger@10.3.0-dev.0) (2025-01-16)

### Features

- support custom print messages function ([db34f07](https://gitlab.com/rxap/packages/commit/db34f070178b9ea7a453304e7f66d20d3a508e01))

## [10.2.11-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.10...@rxap/nest-logger@10.2.11-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.10-dev.3...@rxap/nest-logger@10.2.10) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.10-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.10-dev.2...@rxap/nest-logger@10.2.10-dev.3) (2025-01-04)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.10-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.10-dev.1...@rxap/nest-logger@10.2.10-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.10-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.10-dev.0...@rxap/nest-logger@10.2.10-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.10-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.9...@rxap/nest-logger@10.2.10-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.9-dev.1...@rxap/nest-logger@10.2.9) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.9-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.9-dev.0...@rxap/nest-logger@10.2.9-dev.1) (2024-11-05)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.8...@rxap/nest-logger@10.2.9-dev.0) (2024-11-04)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.8-dev.3...@rxap/nest-logger@10.2.8) (2024-10-28)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.8-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.8-dev.2...@rxap/nest-logger@10.2.8-dev.3) (2024-10-25)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.8-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.8-dev.1...@rxap/nest-logger@10.2.8-dev.2) (2024-10-25)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.2.8-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.8-dev.0...@rxap/nest-logger@10.2.8-dev.1) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.7...@rxap/nest-logger@10.2.8-dev.0) (2024-09-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.7-dev.1...@rxap/nest-logger@10.2.7) (2024-09-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.7-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.7-dev.0...@rxap/nest-logger@10.2.7-dev.1) (2024-09-09)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.7-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6...@rxap/nest-logger@10.2.7-dev.0) (2024-08-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.7...@rxap/nest-logger@10.2.6) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.6-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.6...@rxap/nest-logger@10.2.6-dev.7) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.6-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.5...@rxap/nest-logger@10.2.6-dev.6) (2024-08-21)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.6-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.4...@rxap/nest-logger@10.2.6-dev.5) (2024-08-15)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.6-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.3...@rxap/nest-logger@10.2.6-dev.4) (2024-08-15)

### Bug Fixes

- correctly process custom values ([c952cd7](https://gitlab.com/rxap/packages/commit/c952cd7d2a0a764e167acba58e44ac50096c345e))

## [10.2.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.2...@rxap/nest-logger@10.2.6-dev.3) (2024-08-14)

### Bug Fixes

- improve logging ([79def46](https://gitlab.com/rxap/packages/commit/79def460275e9e46e52765a88c4088ed5e34d903))

## [10.2.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.1...@rxap/nest-logger@10.2.6-dev.2) (2024-08-14)

### Bug Fixes

- add logger factories ([309bc85](https://gitlab.com/rxap/packages/commit/309bc85e6f0373767a6836d1cd805ea777ea0327))

## [10.2.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.6-dev.0...@rxap/nest-logger@10.2.6-dev.1) (2024-08-14)

### Bug Fixes

- use global options ([9157bc1](https://gitlab.com/rxap/packages/commit/9157bc167f875a4ef0a8b03a14bb0258990f3bff))

## [10.2.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5...@rxap/nest-logger@10.2.6-dev.0) (2024-08-12)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.6...@rxap/nest-logger@10.2.5) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.5...@rxap/nest-logger@10.2.5-dev.6) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.4...@rxap/nest-logger@10.2.5-dev.5) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.3...@rxap/nest-logger@10.2.5-dev.4) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.2...@rxap/nest-logger@10.2.5-dev.3) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.1...@rxap/nest-logger@10.2.5-dev.2) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.5-dev.0...@rxap/nest-logger@10.2.5-dev.1) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.4...@rxap/nest-logger@10.2.5-dev.0) (2024-07-03)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.4-dev.0...@rxap/nest-logger@10.2.4) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.3...@rxap/nest-logger@10.2.4-dev.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.3-dev.3...@rxap/nest-logger@10.2.3) (2024-06-28)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.3-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.3-dev.2...@rxap/nest-logger@10.2.3-dev.3) (2024-06-24)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.3-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.3-dev.1...@rxap/nest-logger@10.2.3-dev.2) (2024-06-21)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.3-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.3-dev.0...@rxap/nest-logger@10.2.3-dev.1) (2024-06-20)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.2...@rxap/nest-logger@10.2.3-dev.0) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.2-dev.2...@rxap/nest-logger@10.2.2) (2024-06-18)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.2-dev.1...@rxap/nest-logger@10.2.2-dev.2) (2024-06-17)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.2-dev.0...@rxap/nest-logger@10.2.2-dev.1) (2024-06-11)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.1...@rxap/nest-logger@10.2.2-dev.0) (2024-06-05)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.1-dev.1...@rxap/nest-logger@10.2.1) (2024-06-02)

**Note:** Version bump only for package @rxap/nest-logger

## [10.2.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.0...@rxap/nest-logger@10.2.1-dev.1) (2024-06-02)

### Bug Fixes

- use the rxap logger as logger ([3d92d65](https://gitlab.com/rxap/packages/commit/3d92d65e312ca54e2b3b529c12de63e023795ba9))

## [10.2.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.2.0...@rxap/nest-logger@10.2.1-dev.0) (2024-05-31)

### Bug Fixes

- use the rxap logger as logger ([2d655ba](https://gitlab.com/rxap/packages/commit/2d655bad802e4b524575b0dd7a6fbd8e9e2fea11))

# [10.2.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.2-dev.1...@rxap/nest-logger@10.2.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.2-dev.0...@rxap/nest-logger@10.0.2-dev.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1...@rxap/nest-logger@10.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1...@rxap/nest-logger@10.0.1) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1...@rxap/nest-logger@10.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1...@rxap/nest-logger@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1...@rxap/nest-logger@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1-dev.0...@rxap/nest-logger@10.0.1) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-logger

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.2...@rxap/nest-logger@10.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package @rxap/nest-logger

## [10.1.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.2-dev.0...@rxap/nest-logger@10.1.2) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-logger

## [10.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.1...@rxap/nest-logger@10.1.2-dev.0) (2024-05-28)

**Note:** Version bump only for package @rxap/nest-logger

## [10.1.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.1-dev.0...@rxap/nest-logger@10.1.1) (2024-04-17)

**Note:** Version bump only for package @rxap/nest-logger

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0...@rxap/nest-logger@10.1.1-dev.0) (2024-03-05)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.8...@rxap/nest-logger@10.1.0) (2024-02-07)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.7...@rxap/nest-logger@10.1.0-dev.8) (2023-10-11)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@1.0.1-dev.1...@rxap/nest-logger@10.1.0-dev.7) (2023-10-11)

### Bug Fixes

- add licence file to publishable packages ([d7de1cb](https://gitlab.com/rxap/packages/commit/d7de1cb9db1bd1628f37084e3b0ffd1755aa75f6))
- change from commonjs to es2022 ([cf675a7](https://gitlab.com/rxap/packages/commit/cf675a7254de9ce4b269264df59794dd42fcbd8b))
- ensure the project name is not included in the project tag list ([46d4479](https://gitlab.com/rxap/packages/commit/46d44798258ea1b20df9d4408b9c0809f55027b2))
- generate readme with peer dependencies to install ([27c2cd7](https://gitlab.com/rxap/packages/commit/27c2cd7d98f0c8a499b8c30719f49d69e4970ae9))
- peer dependency issue ([ee95415](https://gitlab.com/rxap/packages/commit/ee95415370d9ef2396916d6c25061a0df791034a))

### Features

- add a LoggerModule that expose the Logger service globally ([4792828](https://gitlab.com/rxap/packages/commit/479282849b58cdf49aeef2f45419c3735fb01606))

### Reverts

- change from commonjs to es2022 ([50eca61](https://gitlab.com/rxap/packages/commit/50eca61e9a89388d1cfeefb8b1029b302b6f307e))

# [10.1.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.5...@rxap/nest-logger@10.1.0-dev.6) (2023-09-27)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.4...@rxap/nest-logger@10.1.0-dev.5) (2023-09-12)

### Bug Fixes

- peer dependency issue ([e67e2b8](https://gitlab.com/rxap/packages/commit/e67e2b8eb884b598536d16c2c544a9ad9be5b53e))

# [10.1.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.3...@rxap/nest-logger@10.1.0-dev.4) (2023-09-12)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.2...@rxap/nest-logger@10.1.0-dev.3) (2023-09-07)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.1...@rxap/nest-logger@10.1.0-dev.2) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.1.0-dev.0...@rxap/nest-logger@10.1.0-dev.1) (2023-09-03)

**Note:** Version bump only for package @rxap/nest-logger

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1-dev.3...@rxap/nest-logger@10.1.0-dev.0) (2023-08-31)

### Bug Fixes

- ensure the project name is not included in the project tag list ([b131ac3](https://gitlab.com/rxap/packages/commit/b131ac3bd92b3b8799d62f15bbd30a1997d7c753))

### Features

- add a LoggerModule that expose the Logger service globally ([de5d1b4](https://gitlab.com/rxap/packages/commit/de5d1b4c44e2094161ff8bfd47610ff54083a997))

## [10.0.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1-dev.2...@rxap/nest-logger@10.0.1-dev.3) (2023-08-17)

### Reverts

- change from commonjs to es2022 ([747a381](https://gitlab.com/rxap/packages/commit/747a381a090f0a276cf363da61bb19ed0c9cb5b7))

## [10.0.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1-dev.1...@rxap/nest-logger@10.0.1-dev.2) (2023-08-16)

### Bug Fixes

- change from commonjs to es2022 ([fd0f2ba](https://gitlab.com/rxap/packages/commit/fd0f2bae24eae7c854e96f630076cd5598c30be6))

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@10.0.1-dev.0...@rxap/nest-logger@10.0.1-dev.1) (2023-08-04)

### Bug Fixes

- add licence file to publishable packages ([ca6d4d5](https://gitlab.com/rxap/packages/commit/ca6d4d509a743b89bad5ed7ae935d3007231705a))
- generate readme with peer dependencies to install ([e7039bb](https://gitlab.com/rxap/packages/commit/e7039bb5e86ffeadfe7cc92d5fc71d32f8efb4fb))

## 10.0.1-dev.0 (2023-08-01)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([a057d77](https://gitlab.com/rxap/packages/commit/a057d77ca2acf9426a03a497da8532f8a2fe2c86))
- update package dependency versions ([45bd022](https://gitlab.com/rxap/packages/commit/45bd022d755c0c11f7d0bcc76d26b39928007941))
- update to nx 16.5.0 ([1304640](https://gitlab.com/rxap/packages/commit/1304640641e351aef07bc4a2eaff339fcce6ec99))

## [1.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-logger@1.0.1-dev.0...@rxap/nest-logger@1.0.1-dev.1) (2023-07-10)

### Bug Fixes

- update package dependency versions ([8479f5c](https://gitlab.com/rxap/packages/commit/8479f5c405a885cc0f300cec6156584e4c65d59c))
- update to nx 16.5.0 ([73f7575](https://gitlab.com/rxap/packages/commit/73f7575ba378b8b03d2a2646f1761c01b16a6e09))

## 1.0.1-dev.0 (2023-07-10)

### Bug Fixes

- restructure and merge mono repos packages, schematics, plugins and nest ([653b4cd](https://gitlab.com/rxap/packages/commit/653b4cd39fc92d322df9b3959651fea0aa6079da))
