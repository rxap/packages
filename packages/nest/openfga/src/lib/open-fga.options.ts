import {
  ClientConfiguration,
  UserClientConfigurationParams
} from '@openfga/sdk/dist/client';


export type OpenFgaOptions = ClientConfiguration | UserClientConfigurationParams;
