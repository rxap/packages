# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.1.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.4...@rxap/nest-rabbitmq@10.1.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.4-dev.0...@rxap/nest-rabbitmq@10.1.4) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.3...@rxap/nest-rabbitmq@10.1.4-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.2...@rxap/nest-rabbitmq@10.1.3) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.1.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.2-dev.3...@rxap/nest-rabbitmq@10.1.2) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.2-dev.2...@rxap/nest-rabbitmq@10.1.2-dev.3) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.1.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.2-dev.1...@rxap/nest-rabbitmq@10.1.2-dev.2) (2025-02-23)

### Bug Fixes

- update package groups ([66f235a](https://gitlab.com/rxap/packages/commit/66f235a6ce54be16b210c3d298bd0c4a88d1066d))

## [10.1.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.2-dev.0...@rxap/nest-rabbitmq@10.1.2-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1...@rxap/nest-rabbitmq@10.1.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.5...@rxap/nest-rabbitmq@10.1.1) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.4...@rxap/nest-rabbitmq@10.1.1-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.3...@rxap/nest-rabbitmq@10.1.1-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.2...@rxap/nest-rabbitmq@10.1.1-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.1...@rxap/nest-rabbitmq@10.1.1-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.1-dev.0...@rxap/nest-rabbitmq@10.1.1-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.1.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.0...@rxap/nest-rabbitmq@10.1.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-rabbitmq

# [10.1.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.1.0-dev.0...@rxap/nest-rabbitmq@10.1.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-rabbitmq

# [10.1.0-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.2-dev.3...@rxap/nest-rabbitmq@10.1.0-dev.0) (2025-01-04)

### Features

- move vault and rabbitmq feature to separate package ([85aa64d](https://gitlab.com/rxap/packages/commit/85aa64df35087dfc662fc89b0812c0e907d37344))

## [10.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.2-dev.2...@rxap/nest-rabbitmq@10.0.2-dev.3) (2025-01-03)

### Bug Fixes

- resolve small issues ([75bfb03](https://gitlab.com/rxap/packages/commit/75bfb03c280ca99183c1dd580f148b10d28a428e))

## [10.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.2-dev.1...@rxap/nest-rabbitmq@10.0.2-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.2-dev.0...@rxap/nest-rabbitmq@10.0.2-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.1...@rxap/nest-rabbitmq@10.0.2-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-rabbitmq

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.1-dev.0...@rxap/nest-rabbitmq@10.0.1) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0...@rxap/nest-rabbitmq@10.0.1-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-rabbitmq

# [10.0.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.18...@rxap/nest-rabbitmq@10.0.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-rabbitmq

# [10.0.0-dev.18](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.17...@rxap/nest-rabbitmq@10.0.0-dev.18) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-rabbitmq

# [10.0.0-dev.17](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.16...@rxap/nest-rabbitmq@10.0.0-dev.17) (2024-08-15)

### Bug Fixes

- support proper error de/serialization ([1ab9454](https://gitlab.com/rxap/packages/commit/1ab945453a3f6c418e98a77c56097c97df318922))
- support proper error de/serialization ([a95f946](https://gitlab.com/rxap/packages/commit/a95f94609018cb18d1a947681818905c6c5562a6))

# [10.0.0-dev.16](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.15...@rxap/nest-rabbitmq@10.0.0-dev.16) (2024-08-14)

### Bug Fixes

- improve logging ([abcd7ae](https://gitlab.com/rxap/packages/commit/abcd7ae24b412447b3a2467c067d0c9737893137))
- improve logging ([e04a801](https://gitlab.com/rxap/packages/commit/e04a80182cca43cfeaea8a0bdd6b236fa997756b))

# [10.0.0-dev.15](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.14...@rxap/nest-rabbitmq@10.0.0-dev.15) (2024-08-14)

### Bug Fixes

- improve logging ([49ee3ed](https://gitlab.com/rxap/packages/commit/49ee3edf9503c15816c69c53e6a910230362a1e5))
- improve logging ([7b5acec](https://gitlab.com/rxap/packages/commit/7b5acece500da382b479010f112a7b5e3609ee2d))

# [10.0.0-dev.14](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.13...@rxap/nest-rabbitmq@10.0.0-dev.14) (2024-08-14)

### Bug Fixes

- improve logging ([951b4e5](https://gitlab.com/rxap/packages/commit/951b4e5acc5b24bb5821bc1e356c6f5add4403b8))

# [10.0.0-dev.13](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.12...@rxap/nest-rabbitmq@10.0.0-dev.13) (2024-08-14)

### Bug Fixes

- improve logging ([c6fd022](https://gitlab.com/rxap/packages/commit/c6fd022f771a712deaaa41d4637c352f8a9fc518))

# [10.0.0-dev.12](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.11...@rxap/nest-rabbitmq@10.0.0-dev.12) (2024-08-14)

### Bug Fixes

- use correct logger type ([63ed422](https://gitlab.com/rxap/packages/commit/63ed422480d333b09dd7f6df68e26378c9fd7566))

# [10.0.0-dev.11](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.10...@rxap/nest-rabbitmq@10.0.0-dev.11) (2024-08-14)

### Bug Fixes

- support exchange setup ([323bf2a](https://gitlab.com/rxap/packages/commit/323bf2a303ba9de67205b34bf9fd43f569a70e15))

# [10.0.0-dev.10](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.9...@rxap/nest-rabbitmq@10.0.0-dev.10) (2024-08-14)

### Bug Fixes

- improve logging ([e453c9d](https://gitlab.com/rxap/packages/commit/e453c9d1e550c4336e7b71cc2db7a0d9551da367))
- improve logging ([fadb8e1](https://gitlab.com/rxap/packages/commit/fadb8e1e59bfb3aabc633fe27c72e901ceb72fe3))
- support custom logger ([ae7096d](https://gitlab.com/rxap/packages/commit/ae7096d9a416fe1a3cb398c738fe0bb92df603f7))

# [10.0.0-dev.9](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.8...@rxap/nest-rabbitmq@10.0.0-dev.9) (2024-08-14)

### Bug Fixes

- improve types ([bef0438](https://gitlab.com/rxap/packages/commit/bef04385bf30c4e7fbd0a2b231d2d67d70f07bd3))

# [10.0.0-dev.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.7...@rxap/nest-rabbitmq@10.0.0-dev.8) (2024-08-14)

### Bug Fixes

- improve logging ([1be6bba](https://gitlab.com/rxap/packages/commit/1be6bbab1f82a20981412710d14cc7ddb9bd53f8))

# [10.0.0-dev.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.6...@rxap/nest-rabbitmq@10.0.0-dev.7) (2024-08-13)

### Bug Fixes

- add bindQueue method ([68c2b04](https://gitlab.com/rxap/packages/commit/68c2b04d0b82d5104a942f7799b76c2d57534dbf))
- correctly use defaults ([735463d](https://gitlab.com/rxap/packages/commit/735463d91e7f6296eae99a815b0481270c06f896))

# [10.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.5...@rxap/nest-rabbitmq@10.0.0-dev.6) (2024-08-09)

### Bug Fixes

- cleanup implementation ([ac3f924](https://gitlab.com/rxap/packages/commit/ac3f924a1f819a6d4cb1a60081aaf165cdcc9e8d))

# [10.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.4...@rxap/nest-rabbitmq@10.0.0-dev.5) (2024-08-07)

### Bug Fixes

- use amqp-connection-manager package directly ([4a0da91](https://gitlab.com/rxap/packages/commit/4a0da91ca6ad071c05d22d02d67ac92306fdb8f5))

# [10.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.3...@rxap/nest-rabbitmq@10.0.0-dev.4) (2024-08-07)

### Bug Fixes

- use types correctly ([dc68de5](https://gitlab.com/rxap/packages/commit/dc68de53b16c4ca4db0fd2882acd60511576977a))

# [10.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.2...@rxap/nest-rabbitmq@10.0.0-dev.3) (2024-08-07)

### Bug Fixes

- use unique transport id ([60fdc4a](https://gitlab.com/rxap/packages/commit/60fdc4a893e14063e53d8eca404ad2cdecaa93b6))

# [10.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq@10.0.0-dev.1...@rxap/nest-rabbitmq@10.0.0-dev.2) (2024-08-07)

### Features

- add server ([5f939ba](https://gitlab.com/rxap/packages/commit/5f939ba4213da4304cfea2ead6b915f6e32fb7b1))

# 10.0.0-dev.1 (2024-08-07)

**Note:** Version bump only for package @rxap/nest-rabbitmq
