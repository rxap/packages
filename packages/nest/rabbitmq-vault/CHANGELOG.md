# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.4...@rxap/nest-rabbitmq-vault@10.0.5-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.4-dev.0...@rxap/nest-rabbitmq-vault@10.0.4) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.3...@rxap/nest-rabbitmq-vault@10.0.4-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2...@rxap/nest-rabbitmq-vault@10.0.3) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.5...@rxap/nest-rabbitmq-vault@10.0.2) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.2-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.4...@rxap/nest-rabbitmq-vault@10.0.2-dev.5) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.0.2-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.3...@rxap/nest-rabbitmq-vault@10.0.2-dev.4) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.2-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.2...@rxap/nest-rabbitmq-vault@10.0.2-dev.3) (2025-02-22)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.2-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.1...@rxap/nest-rabbitmq-vault@10.0.2-dev.2) (2025-02-18)

### Bug Fixes

- update package groups ([4afd316](https://gitlab.com/rxap/packages/commit/4afd316e33c6edab0e500d7ddc572ae8e48f8c34))

## [10.0.2-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.2-dev.0...@rxap/nest-rabbitmq-vault@10.0.2-dev.1) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.1...@rxap/nest-rabbitmq-vault@10.0.2-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.1-dev.1...@rxap/nest-rabbitmq-vault@10.0.1) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.1-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.1-dev.0...@rxap/nest-rabbitmq-vault@10.0.1-dev.1) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.0...@rxap/nest-rabbitmq-vault@10.0.1-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

# [10.0.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-rabbitmq-vault@10.0.0-dev.1...@rxap/nest-rabbitmq-vault@10.0.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-rabbitmq-vault

# 10.0.0-dev.1 (2025-01-04)

### Features

- move vault and rabbitmq feature to separate package ([85aa64d](https://gitlab.com/rxap/packages/commit/85aa64df35087dfc662fc89b0812c0e907d37344))
