# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.2...@rxap/nest-keyv-minio@10.0.3-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-keyv-minio

## [10.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.2-dev.0...@rxap/nest-keyv-minio@10.0.2) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-keyv-minio

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.1...@rxap/nest-keyv-minio@10.0.2-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-keyv-minio

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0...@rxap/nest-keyv-minio@10.0.1) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

# [10.0.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.6...@rxap/nest-keyv-minio@10.0.0) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-keyv-minio

# [10.0.0-dev.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.5...@rxap/nest-keyv-minio@10.0.0-dev.6) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

# [10.0.0-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.4...@rxap/nest-keyv-minio@10.0.0-dev.5) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-keyv-minio

# [10.0.0-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.3...@rxap/nest-keyv-minio@10.0.0-dev.4) (2025-02-19)

### Bug Fixes

- update package groups ([5b3aa54](https://gitlab.com/rxap/packages/commit/5b3aa54554582462026df82a4d3e79b89dacdf05))

# [10.0.0-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.2...@rxap/nest-keyv-minio@10.0.0-dev.3) (2025-02-19)

### Bug Fixes

- use correct default bucket name ([62767b5](https://gitlab.com/rxap/packages/commit/62767b5b7a582f21c925ce84e3212a9656f6ec1a))

# [10.0.0-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-keyv-minio@10.0.0-dev.1...@rxap/nest-keyv-minio@10.0.0-dev.2) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-keyv-minio

# 10.0.0-dev.1 (2025-02-18)

**Note:** Version bump only for package @rxap/nest-keyv-minio
