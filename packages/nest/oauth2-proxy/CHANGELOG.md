# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.9-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.8...@rxap/nest-oauth2-proxy@10.0.9-dev.0) (2025-03-12)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.8](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.8-dev.0...@rxap/nest-oauth2-proxy@10.0.8) (2025-03-07)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.8-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.7...@rxap/nest-oauth2-proxy@10.0.8-dev.0) (2025-02-28)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.7](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.6...@rxap/nest-oauth2-proxy@10.0.7) (2025-02-23)

### Bug Fixes

- update package groups ([463a7f8](https://gitlab.com/rxap/packages/commit/463a7f820dc5d10a9c2983346875856c10a75be8))

## [10.0.6](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.6-dev.3...@rxap/nest-oauth2-proxy@10.0.6) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.6-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.6-dev.2...@rxap/nest-oauth2-proxy@10.0.6-dev.3) (2025-02-23)

### Bug Fixes

- update package groups ([210e086](https://gitlab.com/rxap/packages/commit/210e086dd0c55ab0da0bf8ca35829d401dd0c395))

## [10.0.6-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.6-dev.1...@rxap/nest-oauth2-proxy@10.0.6-dev.2) (2025-02-23)

### Bug Fixes

- update package groups ([66f235a](https://gitlab.com/rxap/packages/commit/66f235a6ce54be16b210c3d298bd0c4a88d1066d))

## [10.0.6-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.6-dev.0...@rxap/nest-oauth2-proxy@10.0.6-dev.1) (2025-02-23)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.6-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5...@rxap/nest-oauth2-proxy@10.0.6-dev.0) (2025-02-18)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.5...@rxap/nest-oauth2-proxy@10.0.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.5](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.4...@rxap/nest-oauth2-proxy@10.0.5-dev.5) (2025-02-13)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.3...@rxap/nest-oauth2-proxy@10.0.5-dev.4) (2025-02-10)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.2...@rxap/nest-oauth2-proxy@10.0.5-dev.3) (2025-02-07)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.1...@rxap/nest-oauth2-proxy@10.0.5-dev.2) (2025-01-28)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.5-dev.0...@rxap/nest-oauth2-proxy@10.0.5-dev.1) (2025-01-22)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.5-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.4...@rxap/nest-oauth2-proxy@10.0.5-dev.0) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.4](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.4-dev.2...@rxap/nest-oauth2-proxy@10.0.4) (2025-01-08)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.4-dev.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.4-dev.1...@rxap/nest-oauth2-proxy@10.0.4-dev.2) (2025-01-03)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.4-dev.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.4-dev.0...@rxap/nest-oauth2-proxy@10.0.4-dev.1) (2024-12-11)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.4-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.3...@rxap/nest-oauth2-proxy@10.0.4-dev.0) (2024-12-10)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.3](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.3-dev.0...@rxap/nest-oauth2-proxy@10.0.3) (2024-10-28)

### Bug Fixes

- omit js maps for production builds ([6666e4a](https://gitlab.com/rxap/packages/commit/6666e4aaef7c5b6b5e75b1926358a7e287f2e903))

## [10.0.3-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.2...@rxap/nest-oauth2-proxy@10.0.3-dev.0) (2024-10-04)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.2](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.2-dev.0...@rxap/nest-oauth2-proxy@10.0.2) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.2-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.1...@rxap/nest-oauth2-proxy@10.0.2-dev.0) (2024-08-22)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.1](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.1-dev.0...@rxap/nest-oauth2-proxy@10.0.1) (2024-07-30)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

## [10.0.1-dev.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.0...@rxap/nest-oauth2-proxy@10.0.1-dev.0) (2024-07-09)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

# [10.0.0](https://gitlab.com/rxap/packages/compare/@rxap/nest-oauth2-proxy@10.0.0-dev.1...@rxap/nest-oauth2-proxy@10.0.0) (2024-06-30)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy

# 10.0.0-dev.1 (2024-06-30)

**Note:** Version bump only for package @rxap/nest-oauth2-proxy
