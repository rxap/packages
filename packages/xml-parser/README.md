Provides a set of decorators and services for parsing and serializing XML documents into TypeScript classes. It simplifies the process of mapping XML elements and attributes to class properties, handling data validation, and serializing objects back into XML. The library supports defining XML structures using decorators and offers a flexible way to customize parsing and serialization logic.

[![npm version](https://img.shields.io/npm/v/@rxap/xml-parser?style=flat-square)](https://www.npmjs.com/package/@rxap/xml-parser)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/xml-parser)
![npm](https://img.shields.io/npm/dm/@rxap/xml-parser)
![NPM](https://img.shields.io/npm/l/@rxap/xml-parser)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/xml-parser
```
**Execute the init generator:**
```bash
yarn nx g @rxap/xml-parser:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/xml-parser:init
```
