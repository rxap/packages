export const ElementParserMetaData = {
  REQUIRED_ELEMENT_PROPERTIES: 'rxap-xml-parser-required-element-properties',
  ATTRIBUTE: 'rxap-xml-parser-attribute',
  OPTIONS: 'rxap/xml-parser/decorators/element-options',
  PARSER: 'rxap-xml-parser-element-parsers',
  SERIALIZER: 'rxap-xml-parser-element-serializers',
  PARSER_INSTANCE: 'rxap/xml-parser/decorators/element-parser/instance',
  SERIALIZER_INSTANCE: 'rxap/xml-parser/decorators/element-serializer/instance',
  /**
   * @deprecated
   */
  NAME: 'rxap-xml-parser-element-name',
  EXTENDS: 'rxap-xml-parser-extends',
};
