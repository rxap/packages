import { parseValue } from './parse-value';

export interface RxapElementOptions {
  withNamespace?: boolean;
  caseSensitive?: boolean;
}

/**
 * Normalizes the provided node name based on the specified options.
 *
 * This function adjusts the `nodeName` according to the `options` provided. It can convert the node name to lowercase if `caseSensitive` is set to false. Additionally, it can remove the namespace prefix from the node name if `withNamespace` is set to false.
 *
 * @param {string} nodeName - The original node name to be normalized.
 * @param {RxapElementOptions} options - Configuration options that determine how the node name should be normalized. The options include:
 * - `caseSensitive`: A boolean that indicates whether the node name should be treated as case-sensitive. If false, the node name will be converted to lowercase.
 * - `withNamespace`: A boolean that indicates whether the namespace prefix should be included in the node name. If false, any namespace prefix is removed.
 * @returns {string} The normalized node name based on the provided options.
 */
export function normalizeNodeName(nodeName: string, options: RxapElementOptions): string {
  let name = nodeName;
  if (!options.caseSensitive) {
    name = name.toLowerCase();
  }
  if (!options.withNamespace) {
    name = name.replace(/[^:]+:/, '');
  }
  return name;
}

export class RxapElement {


  constructor(
    public readonly element: Element,
    public readonly DOMParser: typeof window.DOMParser,
    public readonly options: RxapElementOptions = {},
  ) {
  }

  get attributes(): Record<string, string> {
    return Array.from(this.element.attributes).reduce((acc, attr) => ({
      ...acc,
      [attr.name]: attr.value,
    }), {});
  }

  get attributeNames(): string[] {
    return Object.keys(this.attributes);
  }

  public get name(): string {
    return this.normalizeNodeName(this.nodeName);
  }

  public get nodeName(): string {
    return this.element.nodeName;
  }

  public getString<T = undefined>(qualifiedName: string, defaultValue?: T): T | string {
    if (this.element.hasAttribute(qualifiedName)) {
      return this.element.getAttribute(qualifiedName) as string;
    }
    return defaultValue as any;
  }

  public get<D = undefined, T = any>(qualifiedName: string, defaultValue?: D, raw = false): T | D {
    if (this.element.hasAttribute(qualifiedName)) {
      const value = this.element.getAttribute(qualifiedName)!;
      return raw ? value : parseValue(value) as any;
    }
    return defaultValue as any;
  }

  public set(qualifiedName: string, value: any): void {
    this.element.setAttribute(qualifiedName, value);
  }

  public hasName(name: string): boolean {
    return this.name === this.normalizeNodeName(name);
  }

  public getDate(qualifiedName: string): number | undefined {
    return undefined;
  }

  public getNumber<T = undefined>(qualifiedName: string, defaultValue?: T): number | T {
    if (this.element.hasAttribute(qualifiedName)) {
      return Number(this.element.getAttribute(qualifiedName));
    }
    return defaultValue as any;
  }

  public getBoolean(qualifiedName: string, defaultValue?: boolean): boolean | undefined {
    if (this.element.hasAttribute(qualifiedName)) {
      return this.element.getAttribute(qualifiedName) !== 'false';
    }
    return defaultValue;
  }

  public has(qualifiedName: string): boolean {
    return this.element.hasAttribute(qualifiedName);
  }

  public hasChildren(): boolean {
    return this.getAllChildNodes().length !== 0;
  }

  public hasChild(nodeName: string): boolean {
    return !!this.getChild(nodeName);
  }

  public getAllChildNodes(): RxapElement[] {
    return Array.from(this.element.childNodes)
                .filter(n => !!n.nodeName && n.nodeType === 1)
      .map((child: ChildNode) => new RxapElement(child as any, this.DOMParser, this.options));
  }

  public getCountChildren(): number {
    return Array.from(this.element.childNodes).filter(n => !!n.nodeName && n.nodeType === 1).length;
  }

  public getChildren(...nodeNames: string[]): RxapElement[] {
    return this.getAllChildNodes().filter(
      e => nodeNames.map(nodeName => this.normalizeNodeName(nodeName)).includes(e.name));
  }

  public getChild(nodeName: string): RxapElement | undefined {
    return this.getAllChildNodes().find(e => e.hasName(nodeName));
  }

  public getTextContent<T = string>(defaultValue?: any, raw = false): T {
    const textContent = this.element.textContent === null || this.element.textContent === '' ?
      defaultValue :
      this.element.textContent;
    return textContent !== undefined ? raw ? textContent.trim() : parseValue(textContent.trim()) : undefined as any;
  }

  public getRawContent(): string {
    if (this.element.innerHTML !== undefined) {
      return this.element.innerHTML;
    }
    return Array.from(this.element.childNodes).map(child => child.toString()).join('');
  }

  public removeAllChildren(): void {
    for (const child of this.getAllChildNodes()) {
      this.element.removeChild(child.element);
    }
  }

  public setRawContent(value: string): void {
    this.removeAllChildren();
    if (this.element.innerHTML !== undefined) {
      this.element.innerHTML = value;
    } else {
      this.element.textContent = value;
    }
  }

  public getChildRawContent(nodeName: string, defaultValue?: string): string {
    if (this.hasChild(nodeName)) {
      return this.getChild(nodeName)!.getRawContent();
    }
    return defaultValue ?? '';
  }

  public setChildRawContent(nodeName: string, value: string): void {
    let child: RxapElement | undefined = this.getChild(nodeName);
    if (!child) {
      child = this.addChild(nodeName);
    }
    child.setRawContent(value);
  }

  public getChildTextContent<T = string>(nodeName: string, defaultValue?: any, raw = false): T {
    if (this.hasChild(nodeName)) {
      return this.getChild(nodeName)!.getTextContent(defaultValue, raw) ?? defaultValue;
    }
    return defaultValue as any;
  }

  public getChildrenTextContent<T = string>(nodeName: string, defaultValue?: any): T[] {
    if (this.hasChild(nodeName)) {
      return this.getChildren(nodeName)!.map(child => child.getTextContent(defaultValue));
    }
    return [];
  }

  public normalizeNodeName(nodeName: string): string {
    return normalizeNodeName(nodeName, this.options);
  }

  appendChild(node: any | RxapElement) {
    if (node instanceof RxapElement) {
      node = node.element;
    }
    this.element.appendChild(node);
  }

  addChild(nodeName: string) {
    nodeName = this.normalizeNodeName(nodeName);
    const element = (this.element.ownerDocument ?? new this.DOMParser().parseFromString('<html></html>', 'application/xml')).createElement(nodeName);
    this.element.appendChild(element);
    return new RxapElement(element, this.DOMParser, this.options);
  }

  setChildTextContent(nodeName: string, value: string) {
    let child: RxapElement | undefined = this.getChild(nodeName);
    if (!child) {
      child = this.addChild(nodeName);
    }
    child.setTextContent(value);
  }

  addChildTextContent(nodeName: string, value: string) {
    const child = this.addChild(nodeName);
    child.setTextContent(value);
  }

  setTextContent(value: string) {
    this.removeAllChildren();
    if (this.element.textContent !== undefined) {
      this.element.textContent = value;
    }
    else if (this.element.innerHTML !== undefined) {
      this.element.innerHTML = value;
    }
    else {
      throw new Error('Could not set text content. Element does not have the property textContent or innerHTML');
    }
  }

  setChildren(children: RxapElement[]) {
    this.removeAllChildren();
    for (const child of children) {
      this.appendChild(child);
    }
  }

}
