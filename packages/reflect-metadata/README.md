Provides utilities for working with Reflect Metadata. It includes functions for setting, getting, and manipulating metadata on objects and properties. This package also offers functionalities for change detection using proxies and metadata.

[![npm version](https://img.shields.io/npm/v/@rxap/reflect-metadata?style=flat-square)](https://www.npmjs.com/package/@rxap/reflect-metadata)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/reflect-metadata)
![npm](https://img.shields.io/npm/dm/@rxap/reflect-metadata)
![NPM](https://img.shields.io/npm/l/@rxap/reflect-metadata)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/reflect-metadata
```
**Execute the init generator:**
```bash
yarn nx g @rxap/reflect-metadata:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/reflect-metadata:init
```
