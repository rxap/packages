A utility for creating slugified strings from input strings. It provides options for customizing the replacement character, removing unwanted characters, lowercasing, strict mode, locale-specific replacements, and adding a random suffix. The package includes a class and a function for easy use.

[![npm version](https://img.shields.io/npm/v/@rxap/slugify?style=flat-square)](https://www.npmjs.com/package/@rxap/slugify)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/slugify)
![npm](https://img.shields.io/npm/dm/@rxap/slugify)
![NPM](https://img.shields.io/npm/l/@rxap/slugify)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/slugify
```
**Execute the init generator:**
```bash
yarn nx g @rxap/slugify:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/slugify:init
```
