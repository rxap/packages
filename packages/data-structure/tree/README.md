Provides a tree data structure with node manipulation capabilities, including adding, removing, and traversing nodes.  It supports hierarchical data representation with features like expanding, collapsing, selecting, and deselecting nodes.  The package also includes utilities for managing node visibility and custom parameters.

[![npm version](https://img.shields.io/npm/v/@rxap/data-structure-tree?style=flat-square)](https://www.npmjs.com/package/@rxap/data-structure-tree)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/data-structure-tree)
![npm](https://img.shields.io/npm/dm/@rxap/data-structure-tree)
![NPM](https://img.shields.io/npm/l/@rxap/data-structure-tree)

- [Installation](#installation)
- [Generators](#generators)
  - [init](#init)

# Installation

**Add the package to your workspace:**
```bash
yarn add @rxap/data-structure-tree
```
**Execute the init generator:**
```bash
yarn nx g @rxap/data-structure-tree:init
```
# Generators

## init
> Initialize the package in the workspace

```bash
nx g @rxap/data-structure-tree:init
```
