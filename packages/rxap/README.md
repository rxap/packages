# rxap

[![npm version](https://img.shields.io/npm/v/rxap?style=flat-square)](https://www.npmjs.com/package/rxap)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/rxap)
![npm](https://img.shields.io/npm/dm/rxap)
![NPM](https://img.shields.io/npm/l/rxap)

- [Installation](#installation)

# Installation

**Add the package to your workspace:**
```bash
yarn add rxap
```
