# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [20.0.3-dev.0](https://gitlab.com/rxap/packages/compare/rxap@20.0.2...rxap@20.0.3-dev.0) (2025-03-12)

### Bug Fixes

- update package groups ([ec9c0b1](https://gitlab.com/rxap/packages/commit/ec9c0b1410f8682e0ff6af54d493c474f81bc770))
- update package groups ([d426886](https://gitlab.com/rxap/packages/commit/d426886c1c11eb47529cebaca71df10cca9abb5b))
- update package groups ([2b1e357](https://gitlab.com/rxap/packages/commit/2b1e35754f020a89677daf058c00895cfac32e1d))

## [20.0.2](https://gitlab.com/rxap/packages/compare/rxap@20.0.2-dev.0...rxap@20.0.2) (2025-02-23)

**Note:** Version bump only for package rxap

## [20.0.2-dev.0](https://gitlab.com/rxap/packages/compare/rxap@20.0.1...rxap@20.0.2-dev.0) (2025-02-23)

### Bug Fixes

- update package groups ([8ec6303](https://gitlab.com/rxap/packages/commit/8ec6303e186dad3e922780c042621a97c4153e69))
- update package groups ([9a10fde](https://gitlab.com/rxap/packages/commit/9a10fde838a6ee4f180c0a4272febb8350426d57))
- update package groups ([c3d9b82](https://gitlab.com/rxap/packages/commit/c3d9b823c742a645f8d87b5bebe5492e7fcf82f9))
- update package groups ([b92da77](https://gitlab.com/rxap/packages/commit/b92da7761fdf48ccb7bce5d6a92d8a34fdefa220))
- update package groups ([239765c](https://gitlab.com/rxap/packages/commit/239765cbca3157003819922d95f74c37968d3ce8))

## [20.0.1](https://gitlab.com/rxap/packages/compare/rxap@20.0.1-dev.1...rxap@20.0.1) (2025-02-13)

### Bug Fixes

- update package groups ([21378b7](https://gitlab.com/rxap/packages/commit/21378b776550fac07c12e59e98c1466e80ea1232))

## [20.0.1-dev.1](https://gitlab.com/rxap/packages/compare/rxap@20.0.1-dev.0...rxap@20.0.1-dev.1) (2025-02-13)

### Bug Fixes

- update package groups ([f950115](https://gitlab.com/rxap/packages/commit/f950115835e025e73e541534f67836e11080c587))

## [20.0.1-dev.0](https://gitlab.com/rxap/packages/compare/rxap@20.0.0...rxap@20.0.1-dev.0) (2025-02-11)

### Bug Fixes

- update package groups ([cab8b6b](https://gitlab.com/rxap/packages/commit/cab8b6bd6266b3eadc0fef70fbe2ddd2b4dd5857))
- update package groups ([00babd0](https://gitlab.com/rxap/packages/commit/00babd0ac6c83976c096f2f23b4249651ee08bfd))

# [20.0.0](https://gitlab.com/rxap/packages/compare/rxap@20.0.0-dev.2...rxap@20.0.0) (2025-01-08)

### Bug Fixes

- update package groups ([aeb7821](https://gitlab.com/rxap/packages/commit/aeb7821d7c030cd00ad1603407764b32074cd8b7))
- update package groups ([db75b68](https://gitlab.com/rxap/packages/commit/db75b6814f56f5a7daed03ad20bfcfca3872f298))

# [20.0.0-dev.2](https://gitlab.com/rxap/packages/compare/rxap@20.0.0-dev.1...rxap@20.0.0-dev.2) (2024-12-11)

### Bug Fixes

- update package groups ([0499472](https://gitlab.com/rxap/packages/commit/04994720de41b99881e81049bd71200ecff66a4f))

# [20.0.0-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.25-dev.1...rxap@20.0.0-dev.1) (2024-12-11)

**Note:** Version bump only for package rxap

## [19.1.25-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.25-dev.0...rxap@19.1.25-dev.1) (2024-12-10)

### Bug Fixes

- update package groups ([6bd3829](https://gitlab.com/rxap/packages/commit/6bd38295d8fe17e288ce5f8a3c867cbad587e242))

## [19.1.25-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.24...rxap@19.1.25-dev.0) (2024-12-10)

### Bug Fixes

- update package groups ([abca6b5](https://gitlab.com/rxap/packages/commit/abca6b558fcc6cd629109ed34db4cb85d3694eec))

## [19.1.24](https://gitlab.com/rxap/packages/compare/rxap@19.1.23...rxap@19.1.24) (2024-12-10)

### Bug Fixes

- update package groups ([1638e5a](https://gitlab.com/rxap/packages/commit/1638e5aa39dcf931b3fe2092339197d725df7ff5))

## [19.1.23](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.7...rxap@19.1.23) (2024-12-10)

**Note:** Version bump only for package rxap

## [19.1.23-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.6...rxap@19.1.23-dev.7) (2024-11-11)

### Bug Fixes

- update package groups ([f61c251](https://gitlab.com/rxap/packages/commit/f61c251a09e1be0eb340cf689c28395311bf6053))

## [19.1.23-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.5...rxap@19.1.23-dev.6) (2024-11-07)

### Bug Fixes

- update package groups ([45b4132](https://gitlab.com/rxap/packages/commit/45b4132efc81a950dbcafe380ab7855e34725914))

## [19.1.23-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.4...rxap@19.1.23-dev.5) (2024-11-07)

### Bug Fixes

- update package groups ([006edc7](https://gitlab.com/rxap/packages/commit/006edc7cc838749021a07721973fefb906e30238))

## [19.1.23-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.3...rxap@19.1.23-dev.4) (2024-11-07)

### Bug Fixes

- update package groups ([afae049](https://gitlab.com/rxap/packages/commit/afae04983fa7b002c13d9d7991f43ef926bbe97d))

## [19.1.23-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.2...rxap@19.1.23-dev.3) (2024-11-05)

### Bug Fixes

- update package groups ([cf8d48d](https://gitlab.com/rxap/packages/commit/cf8d48dafc7430d6dc53ebff3773e9b0b89ee0ac))

## [19.1.23-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.1...rxap@19.1.23-dev.2) (2024-11-04)

### Bug Fixes

- update package groups ([e5c5d43](https://gitlab.com/rxap/packages/commit/e5c5d43b439aebfbadb5f8e38080f2ffdf11fd44))

## [19.1.23-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.23-dev.0...rxap@19.1.23-dev.1) (2024-10-30)

### Bug Fixes

- update package groups ([0777865](https://gitlab.com/rxap/packages/commit/077786595bf152417afadc0051f206cf8c61261f))

## [19.1.23-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.22...rxap@19.1.23-dev.0) (2024-10-28)

### Bug Fixes

- update package groups ([a105c5b](https://gitlab.com/rxap/packages/commit/a105c5ba3da8495c0163187f8c0687f8b2572c2b))

## [19.1.22](https://gitlab.com/rxap/packages/compare/rxap@19.1.21...rxap@19.1.22) (2024-10-28)

### Bug Fixes

- update package groups ([f900176](https://gitlab.com/rxap/packages/commit/f900176d1e9b4057766ff6659e5400a61357fc99))

## [19.1.21](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.17...rxap@19.1.21) (2024-10-28)

**Note:** Version bump only for package rxap

## [19.1.21-dev.17](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.16...rxap@19.1.21-dev.17) (2024-10-25)

### Bug Fixes

- update package groups ([7671ed8](https://gitlab.com/rxap/packages/commit/7671ed8229c5a7b768785cde716168e37e71034f))

## [19.1.21-dev.16](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.15...rxap@19.1.21-dev.16) (2024-10-25)

### Bug Fixes

- update package groups ([f46ce35](https://gitlab.com/rxap/packages/commit/f46ce35a832b2473384dbd60871a18a7cf4fd28b))

## [19.1.21-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.14...rxap@19.1.21-dev.15) (2024-10-25)

### Bug Fixes

- update package groups ([2d16d33](https://gitlab.com/rxap/packages/commit/2d16d3383420a180a641e8aa643e6bc760b3efbe))

## [19.1.21-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.13...rxap@19.1.21-dev.14) (2024-10-25)

### Bug Fixes

- update package groups ([55da2f3](https://gitlab.com/rxap/packages/commit/55da2f317add6a5a5a66ad339ad7869866b3a6e9))

## [19.1.21-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.12...rxap@19.1.21-dev.13) (2024-10-24)

### Bug Fixes

- update package groups ([58bd20a](https://gitlab.com/rxap/packages/commit/58bd20a2222dccffa427b852a150c136be779716))

## [19.1.21-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.11...rxap@19.1.21-dev.12) (2024-10-23)

### Bug Fixes

- update package groups ([74e5378](https://gitlab.com/rxap/packages/commit/74e5378e19e719865b14204a64ccb0c9ce1cd5aa))

## [19.1.21-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.10...rxap@19.1.21-dev.11) (2024-10-22)

### Bug Fixes

- update package groups ([c31b3f0](https://gitlab.com/rxap/packages/commit/c31b3f07352269e672c615775175387efd4ca100))

## [19.1.21-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.9...rxap@19.1.21-dev.10) (2024-10-22)

### Bug Fixes

- update package groups ([2a7073d](https://gitlab.com/rxap/packages/commit/2a7073d2c94f6ef709b39e147c44ea92a3ad6486))

## [19.1.21-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.8...rxap@19.1.21-dev.9) (2024-10-22)

### Bug Fixes

- update package groups ([d292713](https://gitlab.com/rxap/packages/commit/d2927134f283221dbc693722b52e7a6634ac54be))

## [19.1.21-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.7...rxap@19.1.21-dev.8) (2024-10-17)

### Bug Fixes

- update package groups ([1f18261](https://gitlab.com/rxap/packages/commit/1f1826113256f72e9669348d656f5c83cc3c01d6))

## [19.1.21-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.6...rxap@19.1.21-dev.7) (2024-10-17)

### Bug Fixes

- update package groups ([a87efc9](https://gitlab.com/rxap/packages/commit/a87efc9ea634951bafef58232bc57bfee8a28092))

## [19.1.21-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.5...rxap@19.1.21-dev.6) (2024-10-16)

### Bug Fixes

- update package groups ([3b4c033](https://gitlab.com/rxap/packages/commit/3b4c033a7fe52707b80ceb7facf1d5f004b0acc1))

## [19.1.21-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.4...rxap@19.1.21-dev.5) (2024-10-08)

### Bug Fixes

- update package groups ([86ebac4](https://gitlab.com/rxap/packages/commit/86ebac4a6c5a6af916e899ce3356e2027a2fffbe))

## [19.1.21-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.3...rxap@19.1.21-dev.4) (2024-10-04)

### Bug Fixes

- update package groups ([25f3490](https://gitlab.com/rxap/packages/commit/25f3490b556ff5aad769035ed3da7b568e7c34ba))

## [19.1.21-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.2...rxap@19.1.21-dev.3) (2024-10-04)

### Bug Fixes

- update package groups ([6fdd3e3](https://gitlab.com/rxap/packages/commit/6fdd3e3a0a2b4552fb07cb7be5ca8e5729f94e63))
- update package groups ([14a1477](https://gitlab.com/rxap/packages/commit/14a14770bd80536b3ce5447365420d43a5f2a01c))

## [19.1.21-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.1...rxap@19.1.21-dev.2) (2024-10-04)

### Bug Fixes

- update package groups ([bb91534](https://gitlab.com/rxap/packages/commit/bb91534076203aa4411b87d30b0dd948db7d15f3))

## [19.1.21-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.21-dev.0...rxap@19.1.21-dev.1) (2024-10-04)

**Note:** Version bump only for package rxap

## [19.1.21-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.20...rxap@19.1.21-dev.0) (2024-09-18)

### Bug Fixes

- update package groups ([e086a1c](https://gitlab.com/rxap/packages/commit/e086a1c6db0a556244c2429a6311386a51ecb362))

## [19.1.20](https://gitlab.com/rxap/packages/compare/rxap@19.1.19...rxap@19.1.20) (2024-09-18)

### Bug Fixes

- update package groups ([9ac9368](https://gitlab.com/rxap/packages/commit/9ac9368bd9cf8feeeeaf5ee186f9d707001c5449))

## [19.1.19](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.15...rxap@19.1.19) (2024-09-18)

**Note:** Version bump only for package rxap

## [19.1.19-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.14...rxap@19.1.19-dev.15) (2024-09-11)

### Bug Fixes

- update package groups ([fd4b146](https://gitlab.com/rxap/packages/commit/fd4b14614327516ca1faca995760c8966ab73804))
- update package groups ([1dafb1d](https://gitlab.com/rxap/packages/commit/1dafb1daa34dde01e1f7c7f8286774c8847c591e))

## [19.1.19-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.13...rxap@19.1.19-dev.14) (2024-09-09)

### Bug Fixes

- update package groups ([f65ed10](https://gitlab.com/rxap/packages/commit/f65ed100c4d3f00acc674acb27a144a89ce60f16))

## [19.1.19-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.12...rxap@19.1.19-dev.13) (2024-09-09)

### Bug Fixes

- update package groups ([fe86e92](https://gitlab.com/rxap/packages/commit/fe86e923cdf04e6c5ee4b87bf594e28dff15ed4d))

## [19.1.19-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.11...rxap@19.1.19-dev.12) (2024-09-03)

### Bug Fixes

- update package groups ([30299aa](https://gitlab.com/rxap/packages/commit/30299aa9e5d6896c48dc49911ca273823bf16768))

## [19.1.19-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.10...rxap@19.1.19-dev.11) (2024-09-03)

### Bug Fixes

- update package groups ([68319df](https://gitlab.com/rxap/packages/commit/68319df0aa2f6b03abdec12d260c7d176073ce7a))

## [19.1.19-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.9...rxap@19.1.19-dev.10) (2024-08-30)

### Bug Fixes

- update package groups ([87b5f46](https://gitlab.com/rxap/packages/commit/87b5f46c16cd8486047ea7272fa6f30c75ff334e))

## [19.1.19-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.8...rxap@19.1.19-dev.9) (2024-08-29)

### Bug Fixes

- update package groups ([686495a](https://gitlab.com/rxap/packages/commit/686495a7ca9a4f66493564353da4edacd55103b8))

## [19.1.19-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.7...rxap@19.1.19-dev.8) (2024-08-29)

### Bug Fixes

- update package groups ([7d8ba61](https://gitlab.com/rxap/packages/commit/7d8ba615ac03e2a03eca35010a5bb28b1cfb8f20))

## [19.1.19-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.6...rxap@19.1.19-dev.7) (2024-08-29)

### Bug Fixes

- update package groups ([e442cea](https://gitlab.com/rxap/packages/commit/e442cea922526363102f3d474c012d1c35da2c39))

## [19.1.19-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.5...rxap@19.1.19-dev.6) (2024-08-29)

### Bug Fixes

- update package groups ([8a68bd9](https://gitlab.com/rxap/packages/commit/8a68bd9652fb9600b061a426122e11ad5f9df85b))

## [19.1.19-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.4...rxap@19.1.19-dev.5) (2024-08-29)

### Bug Fixes

- update package groups ([d78714c](https://gitlab.com/rxap/packages/commit/d78714cd192b1f9d070c6e6ee1feb444b7f91b41))

## [19.1.19-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.3...rxap@19.1.19-dev.4) (2024-08-29)

### Bug Fixes

- update package groups ([6b64d40](https://gitlab.com/rxap/packages/commit/6b64d40c764adfbef7e1fa1694488a4d3ac9eff9))

## [19.1.19-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.2...rxap@19.1.19-dev.3) (2024-08-29)

### Bug Fixes

- update package groups ([e441767](https://gitlab.com/rxap/packages/commit/e44176774a15f55a4b9d192a62e6a95b816e15cb))

## [19.1.19-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.1...rxap@19.1.19-dev.2) (2024-08-29)

### Bug Fixes

- update package groups ([f9c3d8e](https://gitlab.com/rxap/packages/commit/f9c3d8eaf67e3220ba099d9341cc03fe5d572093))

## [19.1.19-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.19-dev.0...rxap@19.1.19-dev.1) (2024-08-27)

### Bug Fixes

- update package groups ([eb01306](https://gitlab.com/rxap/packages/commit/eb01306da68d7e5d8213c312ec1539d368f1bf0d))

## [19.1.19-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.18...rxap@19.1.19-dev.0) (2024-08-27)

### Bug Fixes

- update package groups ([5bc07b8](https://gitlab.com/rxap/packages/commit/5bc07b8d15d7bd9b7e87c3663bf646dc52527a3b))

## [19.1.18](https://gitlab.com/rxap/packages/compare/rxap@19.1.17...rxap@19.1.18) (2024-08-22)

**Note:** Version bump only for package rxap

## [19.1.17](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.37...rxap@19.1.17) (2024-08-22)

**Note:** Version bump only for package rxap

## [19.1.17-dev.37](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.36...rxap@19.1.17-dev.37) (2024-08-22)

### Bug Fixes

- update package groups ([8b29a4b](https://gitlab.com/rxap/packages/commit/8b29a4bad49422a2061e90c033006d625022831a))

## [19.1.17-dev.36](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.35...rxap@19.1.17-dev.36) (2024-08-21)

### Bug Fixes

- update package groups ([ace2326](https://gitlab.com/rxap/packages/commit/ace2326d4def9798cd467537ce24ae5cb0bb3a1b))

## [19.1.17-dev.35](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.34...rxap@19.1.17-dev.35) (2024-08-21)

### Bug Fixes

- update package groups ([aedfd32](https://gitlab.com/rxap/packages/commit/aedfd32b75fb4d956cc831ed711eca48fed97217))

## [19.1.17-dev.34](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.33...rxap@19.1.17-dev.34) (2024-08-19)

### Bug Fixes

- update package groups ([3fb825b](https://gitlab.com/rxap/packages/commit/3fb825b27f82f0f3f27c99dd0b46a0629c49f983))

## [19.1.17-dev.33](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.32...rxap@19.1.17-dev.33) (2024-08-16)

### Bug Fixes

- update package groups ([2db045e](https://gitlab.com/rxap/packages/commit/2db045eb22cc06f115570286d6441622c93c9c91))

## [19.1.17-dev.32](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.31...rxap@19.1.17-dev.32) (2024-08-15)

### Bug Fixes

- update package groups ([a8782ed](https://gitlab.com/rxap/packages/commit/a8782ed964f1d0cf18790d6f930b925c77068170))

## [19.1.17-dev.31](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.30...rxap@19.1.17-dev.31) (2024-08-15)

### Bug Fixes

- update package groups ([5da58ac](https://gitlab.com/rxap/packages/commit/5da58acb9e6d57e5cd66bae6a4cffac2c9254a0f))

## [19.1.17-dev.30](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.29...rxap@19.1.17-dev.30) (2024-08-15)

### Bug Fixes

- update package groups ([5891dbc](https://gitlab.com/rxap/packages/commit/5891dbc1d72968b56f6097836733ddcb4bef354d))

## [19.1.17-dev.29](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.28...rxap@19.1.17-dev.29) (2024-08-15)

### Bug Fixes

- update package groups ([0445078](https://gitlab.com/rxap/packages/commit/0445078b77fe9c0335931c20814d0554c950398b))

## [19.1.17-dev.28](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.27...rxap@19.1.17-dev.28) (2024-08-15)

### Bug Fixes

- update package groups ([ceac3cb](https://gitlab.com/rxap/packages/commit/ceac3cbd8c9b57beef540b6d902025f2740f39d5))

## [19.1.17-dev.27](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.26...rxap@19.1.17-dev.27) (2024-08-15)

### Bug Fixes

- update package groups ([661401b](https://gitlab.com/rxap/packages/commit/661401b27b83ba557d5205e52c0b3697e1a2e2e2))

## [19.1.17-dev.26](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.25...rxap@19.1.17-dev.26) (2024-08-15)

### Bug Fixes

- update package groups ([a0a6ffc](https://gitlab.com/rxap/packages/commit/a0a6ffc6d0d260c981dafc1b54861ff8cd01a20e))

## [19.1.17-dev.25](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.24...rxap@19.1.17-dev.25) (2024-08-15)

### Bug Fixes

- update package groups ([7868df0](https://gitlab.com/rxap/packages/commit/7868df0f4c451ff64a67c1de818acbf1eff25c9a))

## [19.1.17-dev.24](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.23...rxap@19.1.17-dev.24) (2024-08-14)

### Bug Fixes

- update package groups ([435c3d6](https://gitlab.com/rxap/packages/commit/435c3d6d48eb5b55e75112a522da090e7121f0d3))

## [19.1.17-dev.23](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.22...rxap@19.1.17-dev.23) (2024-08-14)

### Bug Fixes

- update package groups ([4341174](https://gitlab.com/rxap/packages/commit/4341174f63ebf1021d41d3dfc567b94a10678e1f))

## [19.1.17-dev.22](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.21...rxap@19.1.17-dev.22) (2024-08-14)

### Bug Fixes

- update package groups ([9963cde](https://gitlab.com/rxap/packages/commit/9963cde43699d8a1560509e53a62307a01c397e3))

## [19.1.17-dev.21](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.20...rxap@19.1.17-dev.21) (2024-08-14)

### Bug Fixes

- update package groups ([982e63d](https://gitlab.com/rxap/packages/commit/982e63d9e50ad6d45bd59a156ae9e80eff9dd817))

## [19.1.17-dev.20](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.19...rxap@19.1.17-dev.20) (2024-08-14)

### Bug Fixes

- update package groups ([f5bb4f6](https://gitlab.com/rxap/packages/commit/f5bb4f64db6002c2ec6c9d80ac5216cb38edf126))

## [19.1.17-dev.19](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.18...rxap@19.1.17-dev.19) (2024-08-14)

### Bug Fixes

- update package groups ([ffe1b1a](https://gitlab.com/rxap/packages/commit/ffe1b1a07a44c3e4af520533c840ef8f0a2b6334))

## [19.1.17-dev.18](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.17...rxap@19.1.17-dev.18) (2024-08-14)

### Bug Fixes

- update package groups ([0db9861](https://gitlab.com/rxap/packages/commit/0db986131b4ab38e6226135dcced1d7a0e8eb4f8))

## [19.1.17-dev.17](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.16...rxap@19.1.17-dev.17) (2024-08-14)

### Bug Fixes

- update package groups ([f1e9a14](https://gitlab.com/rxap/packages/commit/f1e9a14ecd81d04c1220d566072b6e4ca3146244))

## [19.1.17-dev.16](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.15...rxap@19.1.17-dev.16) (2024-08-14)

### Bug Fixes

- update package groups ([908854a](https://gitlab.com/rxap/packages/commit/908854a27078c45255b00b7c678af78d2adca9d1))

## [19.1.17-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.14...rxap@19.1.17-dev.15) (2024-08-14)

### Bug Fixes

- update package groups ([89a6b1d](https://gitlab.com/rxap/packages/commit/89a6b1d8ef9cc1baac759200bbb70cfe9f1b1aca))

## [19.1.17-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.13...rxap@19.1.17-dev.14) (2024-08-14)

### Bug Fixes

- update package groups ([735b88e](https://gitlab.com/rxap/packages/commit/735b88ef4df72640cb8a7719a5137ada72b18358))

## [19.1.17-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.12...rxap@19.1.17-dev.13) (2024-08-14)

### Bug Fixes

- update package groups ([cbc8261](https://gitlab.com/rxap/packages/commit/cbc8261a438cd41a2398f3b3ffe12cd1b031f81f))

## [19.1.17-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.11...rxap@19.1.17-dev.12) (2024-08-14)

### Bug Fixes

- update package groups ([5e9ce27](https://gitlab.com/rxap/packages/commit/5e9ce27d64d55262397ddeddc89fbf34cfa9a879))

## [19.1.17-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.10...rxap@19.1.17-dev.11) (2024-08-13)

### Bug Fixes

- update package groups ([7d4849e](https://gitlab.com/rxap/packages/commit/7d4849e29105d993c0e128a8580c7d9129bf4030))

## [19.1.17-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.9...rxap@19.1.17-dev.10) (2024-08-12)

### Bug Fixes

- update package groups ([3c918f0](https://gitlab.com/rxap/packages/commit/3c918f0f17d7f8cb59b56dcf92626af81ab5c424))

## [19.1.17-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.8...rxap@19.1.17-dev.9) (2024-08-12)

### Bug Fixes

- update package groups ([11bf424](https://gitlab.com/rxap/packages/commit/11bf424d9f46ef15062b871ebfb0befb56743eb7))

## [19.1.17-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.7...rxap@19.1.17-dev.8) (2024-08-12)

### Bug Fixes

- update package groups ([f95f5b3](https://gitlab.com/rxap/packages/commit/f95f5b35950e7601ec1e93a92bf519e66393348d))

## [19.1.17-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.6...rxap@19.1.17-dev.7) (2024-08-09)

### Bug Fixes

- update package groups ([0abc629](https://gitlab.com/rxap/packages/commit/0abc62910c569827a194c42b23d69cb533c35372))

## [19.1.17-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.5...rxap@19.1.17-dev.6) (2024-08-07)

### Bug Fixes

- update package groups ([8939c7c](https://gitlab.com/rxap/packages/commit/8939c7c2f376ce6b6f6b7ee9f71bcccb40e6235e))

## [19.1.17-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.4...rxap@19.1.17-dev.5) (2024-08-07)

### Bug Fixes

- update package groups ([56e9395](https://gitlab.com/rxap/packages/commit/56e93950bd7db7d69909ea582bae224dbf945183))

## [19.1.17-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.3...rxap@19.1.17-dev.4) (2024-08-07)

### Bug Fixes

- update package groups ([e4b2de8](https://gitlab.com/rxap/packages/commit/e4b2de8b1d50d23413012750caba319f2f281498))

## [19.1.17-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.2...rxap@19.1.17-dev.3) (2024-08-07)

### Bug Fixes

- update package groups ([6a34227](https://gitlab.com/rxap/packages/commit/6a3422728d28adcdc0ceab5d46685cac03a4c6a9))

## [19.1.17-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.1...rxap@19.1.17-dev.2) (2024-08-07)

### Bug Fixes

- update package groups ([24c9c1d](https://gitlab.com/rxap/packages/commit/24c9c1dce85c1cdcd1060fe7dfb06e703d8ff6f1))

## [19.1.17-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.17-dev.0...rxap@19.1.17-dev.1) (2024-08-05)

### Bug Fixes

- update package groups ([1c24ad7](https://gitlab.com/rxap/packages/commit/1c24ad7ac6c16c96ad67e1eae0568702b8834ffb))

## [19.1.17-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.16...rxap@19.1.17-dev.0) (2024-08-05)

### Bug Fixes

- update package groups ([9ede24d](https://gitlab.com/rxap/packages/commit/9ede24dcbd240959f9bdd0fada31219e409dc24a))

## [19.1.16](https://gitlab.com/rxap/packages/compare/rxap@19.1.15...rxap@19.1.16) (2024-08-05)

### Bug Fixes

- update package groups ([1754621](https://gitlab.com/rxap/packages/commit/1754621a2139c16ef8911e735203449d76ff7d7e))

## [19.1.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.15-dev.0...rxap@19.1.15) (2024-08-05)

**Note:** Version bump only for package rxap

## [19.1.15-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.14...rxap@19.1.15-dev.0) (2024-08-05)

### Bug Fixes

- update package groups ([5ea5fd7](https://gitlab.com/rxap/packages/commit/5ea5fd7601dbb39e42e59d2ea648b69cbadf99e7))

## [19.1.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.13...rxap@19.1.14) (2024-07-30)

### Bug Fixes

- update package groups ([891a0e6](https://gitlab.com/rxap/packages/commit/891a0e62d1b71b661ba19685523e224103c09a5b))

## [19.1.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.28...rxap@19.1.13) (2024-07-30)

**Note:** Version bump only for package rxap

## [19.1.13-dev.28](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.27...rxap@19.1.13-dev.28) (2024-07-30)

### Bug Fixes

- update package groups ([a50ff0f](https://gitlab.com/rxap/packages/commit/a50ff0fdcea0cc72e0d993e432aff214a591f50f))

## [19.1.13-dev.27](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.26...rxap@19.1.13-dev.27) (2024-07-26)

### Bug Fixes

- update package groups ([c015462](https://gitlab.com/rxap/packages/commit/c01546273b3a9a1d715906bd1cb43564f261deef))

## [19.1.13-dev.26](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.25...rxap@19.1.13-dev.26) (2024-07-26)

### Bug Fixes

- update package groups ([98c2136](https://gitlab.com/rxap/packages/commit/98c2136bfed477a6ab026614a65120e79fd0a8b9))

## [19.1.13-dev.25](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.24...rxap@19.1.13-dev.25) (2024-07-26)

### Bug Fixes

- update package groups ([18b396d](https://gitlab.com/rxap/packages/commit/18b396d1d21d46b16f3fd5c80ddb82471cbf8de7))

## [19.1.13-dev.24](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.23...rxap@19.1.13-dev.24) (2024-07-25)

### Bug Fixes

- update package groups ([20f8ce2](https://gitlab.com/rxap/packages/commit/20f8ce24ff5ed8efe160ffe583463f2890a3e4df))

## [19.1.13-dev.23](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.22...rxap@19.1.13-dev.23) (2024-07-24)

### Bug Fixes

- update package groups ([813aa7a](https://gitlab.com/rxap/packages/commit/813aa7adc11460cb96fcc2d8bc30e90e39411c1d))

## [19.1.13-dev.22](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.21...rxap@19.1.13-dev.22) (2024-07-24)

### Bug Fixes

- update package groups ([0d77e07](https://gitlab.com/rxap/packages/commit/0d77e07cbace69e645fcc42313e0c522d8166f64))

## [19.1.13-dev.21](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.20...rxap@19.1.13-dev.21) (2024-07-22)

### Bug Fixes

- update package groups ([41742c5](https://gitlab.com/rxap/packages/commit/41742c511e145827b9916cd01f0d1b7178e1c879))

## [19.1.13-dev.20](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.19...rxap@19.1.13-dev.20) (2024-07-18)

### Bug Fixes

- update package groups ([c420e1a](https://gitlab.com/rxap/packages/commit/c420e1aca6a8de117a1ec1e291e5f00677adbd1c))

## [19.1.13-dev.19](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.18...rxap@19.1.13-dev.19) (2024-07-18)

### Bug Fixes

- update package groups ([48e3563](https://gitlab.com/rxap/packages/commit/48e3563f225fccdb97230aa0ddc6d64068123d26))

## [19.1.13-dev.18](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.17...rxap@19.1.13-dev.18) (2024-07-16)

### Bug Fixes

- update package groups ([bb8c49a](https://gitlab.com/rxap/packages/commit/bb8c49a22909fce7bf01d0369680f1a372911bb5))

## [19.1.13-dev.17](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.16...rxap@19.1.13-dev.17) (2024-07-16)

### Bug Fixes

- update package groups ([813a949](https://gitlab.com/rxap/packages/commit/813a949cefbad3453453e160c18f459bd70b2b25))

## [19.1.13-dev.16](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.15...rxap@19.1.13-dev.16) (2024-07-10)

### Bug Fixes

- update package groups ([c642ea3](https://gitlab.com/rxap/packages/commit/c642ea3057199bc9050c267127d9ae6c9e4c19b9))

## [19.1.13-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.14...rxap@19.1.13-dev.15) (2024-07-10)

### Bug Fixes

- update package groups ([0c24f97](https://gitlab.com/rxap/packages/commit/0c24f9709336061d2155e1d54fbda494c165ade8))

## [19.1.13-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.13...rxap@19.1.13-dev.14) (2024-07-10)

### Bug Fixes

- update package groups ([6c0f1d0](https://gitlab.com/rxap/packages/commit/6c0f1d0078a362035281305b4b76b7790ff9efdc))

## [19.1.13-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.12...rxap@19.1.13-dev.13) (2024-07-10)

### Bug Fixes

- update package groups ([5f2e221](https://gitlab.com/rxap/packages/commit/5f2e22121a60716ecd7c5826dc22478a8d669ed4))

## [19.1.13-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.11...rxap@19.1.13-dev.12) (2024-07-10)

### Bug Fixes

- update package groups ([4d48802](https://gitlab.com/rxap/packages/commit/4d48802a82cf5f35d586b268a1459dddeb6df124))

## [19.1.13-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.10...rxap@19.1.13-dev.11) (2024-07-09)

### Bug Fixes

- update package groups ([3bf1764](https://gitlab.com/rxap/packages/commit/3bf1764e70c452536df6416557c41e31d8b82831))

## [19.1.13-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.9...rxap@19.1.13-dev.10) (2024-07-09)

**Note:** Version bump only for package rxap

## [19.1.13-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.8...rxap@19.1.13-dev.9) (2024-07-03)

### Bug Fixes

- update package groups ([fd0d3ba](https://gitlab.com/rxap/packages/commit/fd0d3ba80c95960d3fbe4c58d26807d5d6e895cd))

## [19.1.13-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.7...rxap@19.1.13-dev.8) (2024-07-03)

### Bug Fixes

- update package groups ([b53eb57](https://gitlab.com/rxap/packages/commit/b53eb57f11eee2efe1452294329cd905ba848f09))

## [19.1.13-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.6...rxap@19.1.13-dev.7) (2024-07-03)

### Bug Fixes

- update package groups ([f8ad6ea](https://gitlab.com/rxap/packages/commit/f8ad6ea1be3ae53ae966574844b0e3c8cbe353d4))

## [19.1.13-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.5...rxap@19.1.13-dev.6) (2024-07-03)

### Bug Fixes

- update package groups ([6b22c6c](https://gitlab.com/rxap/packages/commit/6b22c6cc24793c62773b94190e59fa352163acd1))

## [19.1.13-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.4...rxap@19.1.13-dev.5) (2024-07-03)

### Bug Fixes

- update package groups ([4949d61](https://gitlab.com/rxap/packages/commit/4949d61e98f104e534b5d65ec786a71390275ba6))

## [19.1.13-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.3...rxap@19.1.13-dev.4) (2024-07-03)

### Bug Fixes

- update package groups ([452c5a4](https://gitlab.com/rxap/packages/commit/452c5a47442ebf62531b99a4510e3241539abca6))

## [19.1.13-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.2...rxap@19.1.13-dev.3) (2024-07-03)

### Bug Fixes

- update package groups ([3b35c58](https://gitlab.com/rxap/packages/commit/3b35c588882dd8fcf981ca7defbf6467f902572d))

## [19.1.13-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.1...rxap@19.1.13-dev.2) (2024-07-03)

### Bug Fixes

- update package groups ([efca4b2](https://gitlab.com/rxap/packages/commit/efca4b25880c0dd440a511061897d2bee62b8cab))

## [19.1.13-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.13-dev.0...rxap@19.1.13-dev.1) (2024-07-02)

### Bug Fixes

- update package groups ([89749a6](https://gitlab.com/rxap/packages/commit/89749a691a9e2a00979abd7d6ad304c858dd1b10))

## [19.1.13-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.12...rxap@19.1.13-dev.0) (2024-07-01)

### Bug Fixes

- update package groups ([8d7519a](https://gitlab.com/rxap/packages/commit/8d7519afea69a6d6ea42aac4113f932b450807e9))

## [19.1.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.11...rxap@19.1.12) (2024-06-30)

### Bug Fixes

- update package groups ([962efca](https://gitlab.com/rxap/packages/commit/962efcaa473351a44cc78229bf2d3ed345a1e5be))

## [19.1.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.10...rxap@19.1.11) (2024-06-30)

### Bug Fixes

- update package groups ([ee8a8a8](https://gitlab.com/rxap/packages/commit/ee8a8a8e82100c10b5a3781d050dc117c2cf94ec))

## [19.1.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.9...rxap@19.1.10) (2024-06-30)

### Bug Fixes

- update package groups ([700fc0c](https://gitlab.com/rxap/packages/commit/700fc0c9b012d25c9c48211054769445efd47056))

## [19.1.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.8...rxap@19.1.9) (2024-06-30)

### Bug Fixes

- update package groups ([da28c35](https://gitlab.com/rxap/packages/commit/da28c35118d00570f570374bdcdf7f72074a3be5))

## [19.1.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.8-dev.2...rxap@19.1.8) (2024-06-30)

**Note:** Version bump only for package rxap

## [19.1.8-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.8-dev.1...rxap@19.1.8-dev.2) (2024-06-30)

### Bug Fixes

- update package groups ([fb34167](https://gitlab.com/rxap/packages/commit/fb34167a6eb40e244713ded038a4bea0c5545c85))

## [19.1.8-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.8-dev.0...rxap@19.1.8-dev.1) (2024-06-30)

### Bug Fixes

- update package groups ([37b5e47](https://gitlab.com/rxap/packages/commit/37b5e474652f548454ff6c2dc95ecf4831b80d49))

## [19.1.8-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.7...rxap@19.1.8-dev.0) (2024-06-30)

**Note:** Version bump only for package rxap

## [19.1.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.6...rxap@19.1.7) (2024-06-28)

### Bug Fixes

- update package groups ([a008ae9](https://gitlab.com/rxap/packages/commit/a008ae9947ce41a634c9788f12afc44e4d0a41dd))

## [19.1.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.5...rxap@19.1.6) (2024-06-28)

### Bug Fixes

- update package groups ([f73aec2](https://gitlab.com/rxap/packages/commit/f73aec2e2bc0a487a432dae23cd65a49fb89776c))

## [19.1.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.4...rxap@19.1.5) (2024-06-28)

### Bug Fixes

- update package groups ([add4a65](https://gitlab.com/rxap/packages/commit/add4a658d63a28b055b3c64777979f067f804c42))

## [19.1.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.3...rxap@19.1.4) (2024-06-28)

### Bug Fixes

- update package groups ([10047d5](https://gitlab.com/rxap/packages/commit/10047d5a46bd35ba60e9cb800bdf5c90efd34666))

## [19.1.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.2...rxap@19.1.3) (2024-06-28)

### Bug Fixes

- update package groups ([ce7343f](https://gitlab.com/rxap/packages/commit/ce7343ffa7a4765e9f080736de48f40af2cee238))

## [19.1.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.21...rxap@19.1.2) (2024-06-28)

**Note:** Version bump only for package rxap

## [19.1.2-dev.21](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.20...rxap@19.1.2-dev.21) (2024-06-27)

### Bug Fixes

- update package groups ([c504049](https://gitlab.com/rxap/packages/commit/c504049c41ee36709fc39a3183c332d75da0b8ce))

## [19.1.2-dev.20](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.19...rxap@19.1.2-dev.20) (2024-06-27)

### Bug Fixes

- update package groups ([d319c44](https://gitlab.com/rxap/packages/commit/d319c4401c511813536391ba02b6b529318dea7e))

## [19.1.2-dev.19](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.18...rxap@19.1.2-dev.19) (2024-06-26)

### Bug Fixes

- update package groups ([889207d](https://gitlab.com/rxap/packages/commit/889207d267c63825b6840e74c0d3876f5ecb22f4))

## [19.1.2-dev.18](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.17...rxap@19.1.2-dev.18) (2024-06-26)

### Bug Fixes

- update package groups ([40b1664](https://gitlab.com/rxap/packages/commit/40b16647b488e3114a6c576d2f423007fb4fb083))

## [19.1.2-dev.17](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.16...rxap@19.1.2-dev.17) (2024-06-25)

### Bug Fixes

- update package groups ([a68f01b](https://gitlab.com/rxap/packages/commit/a68f01bdb323d1b3464b44d0f03a4293f64950e5))

## [19.1.2-dev.16](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.15...rxap@19.1.2-dev.16) (2024-06-25)

### Bug Fixes

- update package groups ([13e8ba4](https://gitlab.com/rxap/packages/commit/13e8ba484981be7dd5e2c84bd37b122b3699bb03))

## [19.1.2-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.14...rxap@19.1.2-dev.15) (2024-06-25)

### Bug Fixes

- update package groups ([33a7749](https://gitlab.com/rxap/packages/commit/33a7749425dffd06cb53f8f8a624ada6474acde6))

## [19.1.2-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.13...rxap@19.1.2-dev.14) (2024-06-25)

### Bug Fixes

- update package groups ([4abdc50](https://gitlab.com/rxap/packages/commit/4abdc50486b7dc4407c705f4e37e24d479887ccd))

## [19.1.2-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.12...rxap@19.1.2-dev.13) (2024-06-25)

### Bug Fixes

- update package groups ([8250ee6](https://gitlab.com/rxap/packages/commit/8250ee6702ea0945f22503c38bafe6f7684fb63f))

## [19.1.2-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.11...rxap@19.1.2-dev.12) (2024-06-25)

### Bug Fixes

- update package groups ([11fc686](https://gitlab.com/rxap/packages/commit/11fc6864c342afdc07e7b0c89f8e097851655d51))

## [19.1.2-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.10...rxap@19.1.2-dev.11) (2024-06-25)

### Bug Fixes

- update package groups ([545c67c](https://gitlab.com/rxap/packages/commit/545c67c49461ca43bd99c8937ccab0f8cd0a06d0))

## [19.1.2-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.9...rxap@19.1.2-dev.10) (2024-06-25)

### Bug Fixes

- update package groups ([4256935](https://gitlab.com/rxap/packages/commit/4256935085d1b06acbd4aef2961aed14e9f8f0ae))

## [19.1.2-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.8...rxap@19.1.2-dev.9) (2024-06-25)

### Bug Fixes

- update package groups ([ad1154f](https://gitlab.com/rxap/packages/commit/ad1154fa98c41c0275ec97d7c9cf318c6c080c15))

## [19.1.2-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.7...rxap@19.1.2-dev.8) (2024-06-24)

### Bug Fixes

- update package groups ([a9e1aa4](https://gitlab.com/rxap/packages/commit/a9e1aa4713ef5a9c7d84807cbb52eb6efb3c48f7))

## [19.1.2-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.6...rxap@19.1.2-dev.7) (2024-06-21)

### Bug Fixes

- update package groups ([f1410b6](https://gitlab.com/rxap/packages/commit/f1410b6e2afe9dc8d3f8fe1a5e851d3c029433ae))

## [19.1.2-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.5...rxap@19.1.2-dev.6) (2024-06-21)

### Bug Fixes

- update package groups ([dba8a91](https://gitlab.com/rxap/packages/commit/dba8a917a3f6950f15a7e29d8854d0cf195ee0f2))

## [19.1.2-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.4...rxap@19.1.2-dev.5) (2024-06-21)

### Bug Fixes

- update package groups ([5200f2a](https://gitlab.com/rxap/packages/commit/5200f2a3b7d70a3a61336f7d6d1007f9d82d49ce))

## [19.1.2-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.3...rxap@19.1.2-dev.4) (2024-06-21)

### Bug Fixes

- update package groups ([f848171](https://gitlab.com/rxap/packages/commit/f848171ebd83d8eefef4db4578b2a49f428dc307))

## [19.1.2-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.2...rxap@19.1.2-dev.3) (2024-06-21)

**Note:** Version bump only for package rxap

## [19.1.2-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.1...rxap@19.1.2-dev.2) (2024-06-20)

### Bug Fixes

- update package groups ([a0547f8](https://gitlab.com/rxap/packages/commit/a0547f8067efd5363d0e89171b95c4512cf5a465))

## [19.1.2-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.2-dev.0...rxap@19.1.2-dev.1) (2024-06-20)

**Note:** Version bump only for package rxap

## [19.1.2-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.1...rxap@19.1.2-dev.0) (2024-06-18)

### Bug Fixes

- update package groups ([24c3278](https://gitlab.com/rxap/packages/commit/24c327847c1e0764a008a6fabab1bfee441bb3ac))

## [19.1.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.0...rxap@19.1.1) (2024-06-18)

### Bug Fixes

- update package groups ([a231c94](https://gitlab.com/rxap/packages/commit/a231c94f220823acdd3e17bd31b57ca263b49f21))

# [19.1.0](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.11...rxap@19.1.0) (2024-06-18)

**Note:** Version bump only for package rxap

# [19.1.0-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.10...rxap@19.1.0-dev.11) (2024-06-18)

### Bug Fixes

- update package groups ([d75c213](https://gitlab.com/rxap/packages/commit/d75c213bcfc564c111577ff907b19000e2cb2632))

# [19.1.0-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.9...rxap@19.1.0-dev.10) (2024-06-18)

### Bug Fixes

- update package groups ([cbceeb6](https://gitlab.com/rxap/packages/commit/cbceeb69104704b3087951ef4f385dbe29d7a7c4))

# [19.1.0-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.8...rxap@19.1.0-dev.9) (2024-06-18)

### Bug Fixes

- update package groups ([53a81d9](https://gitlab.com/rxap/packages/commit/53a81d98d27f29c0325ed1d1ff48a3dd0b172c3e))

# [19.1.0-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.7...rxap@19.1.0-dev.8) (2024-06-18)

### Bug Fixes

- update package groups ([91c6ff6](https://gitlab.com/rxap/packages/commit/91c6ff6f7f7e0a029e9e4ccd03ee38a95a157468))

# [19.1.0-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.6...rxap@19.1.0-dev.7) (2024-06-18)

### Bug Fixes

- update package groups ([211b230](https://gitlab.com/rxap/packages/commit/211b230b5c552c47af9dc87c06dc76da653ae3bf))

# [19.1.0-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.5...rxap@19.1.0-dev.6) (2024-06-18)

### Bug Fixes

- update package groups ([b0b4ca4](https://gitlab.com/rxap/packages/commit/b0b4ca4d3e0d755e5df2c018a3cd9d1978ad5352))

# [19.1.0-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.4...rxap@19.1.0-dev.5) (2024-06-17)

### Bug Fixes

- update package groups ([9afca2e](https://gitlab.com/rxap/packages/commit/9afca2e61b270b3ae577f5e164f310cdcad23b14))

# [19.1.0-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.3...rxap@19.1.0-dev.4) (2024-06-17)

### Bug Fixes

- update package groups ([b8a6200](https://gitlab.com/rxap/packages/commit/b8a620043596ac1af317451d5bbd2d9384fe66bf))

# [19.1.0-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.2...rxap@19.1.0-dev.3) (2024-06-17)

### Bug Fixes

- update package groups ([f6d2a3f](https://gitlab.com/rxap/packages/commit/f6d2a3f43819493313b89e4900ab1b4fcfd6732e))

# [19.1.0-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.1...rxap@19.1.0-dev.2) (2024-06-17)

### Bug Fixes

- update package groups ([86f0552](https://gitlab.com/rxap/packages/commit/86f0552c4622d138a647f1cc1872cf5795c62455))

# [19.1.0-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.1.0-dev.0...rxap@19.1.0-dev.1) (2024-06-17)

### Bug Fixes

- update package groups ([18a4b51](https://gitlab.com/rxap/packages/commit/18a4b514c3c815531f064719b58b2d4369cd9867))

# [19.1.0-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.16...rxap@19.1.0-dev.0) (2024-06-17)

### Features

- add include dependencies option ([c5fcf7c](https://gitlab.com/rxap/packages/commit/c5fcf7cf8ed4d3f72c6e45496b9cd849208fa5ba))

## [19.0.7-dev.16](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.15...rxap@19.0.7-dev.16) (2024-06-17)

### Bug Fixes

- update package groups ([f6af62a](https://gitlab.com/rxap/packages/commit/f6af62a351f04f15a4d477144a332bd90a80f846))

## [19.0.7-dev.15](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.14...rxap@19.0.7-dev.15) (2024-06-17)

### Bug Fixes

- update package groups ([6546943](https://gitlab.com/rxap/packages/commit/654694331b7c43dd1fa7f20622739f93bafdb145))

## [19.0.7-dev.14](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.13...rxap@19.0.7-dev.14) (2024-06-11)

### Bug Fixes

- update package groups ([fc18898](https://gitlab.com/rxap/packages/commit/fc18898baeeb53fff253631883562f36c6a2ec2d))

## [19.0.7-dev.13](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.12...rxap@19.0.7-dev.13) (2024-06-11)

### Bug Fixes

- update package groups ([8c6f13f](https://gitlab.com/rxap/packages/commit/8c6f13f5ad376965c187e21948565e1e9995504a))

## [19.0.7-dev.12](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.11...rxap@19.0.7-dev.12) (2024-06-11)

### Bug Fixes

- update package groups ([8ecb8f6](https://gitlab.com/rxap/packages/commit/8ecb8f6c2d95316b2c5c4c1accdedda0bb17241d))

## [19.0.7-dev.11](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.10...rxap@19.0.7-dev.11) (2024-06-10)

### Bug Fixes

- update package groups ([87cd96a](https://gitlab.com/rxap/packages/commit/87cd96a9c04d036dc4aec4194cbdcbf33fdf09ea))

## [19.0.7-dev.10](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.9...rxap@19.0.7-dev.10) (2024-06-06)

### Bug Fixes

- update package groups ([02684fc](https://gitlab.com/rxap/packages/commit/02684fc3c891e92f655b8a49742fc5a9e419908a))

## [19.0.7-dev.9](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.8...rxap@19.0.7-dev.9) (2024-06-05)

### Bug Fixes

- update package groups ([e602dcb](https://gitlab.com/rxap/packages/commit/e602dcb288f59ebe5e7538f16af937dc347599eb))

## [19.0.7-dev.8](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.7...rxap@19.0.7-dev.8) (2024-06-04)

### Bug Fixes

- update package groups ([28ad720](https://gitlab.com/rxap/packages/commit/28ad72025c9d5223af09f7f415d43fb120152c50))

## [19.0.7-dev.7](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.6...rxap@19.0.7-dev.7) (2024-06-04)

### Bug Fixes

- update package groups ([a54c5a3](https://gitlab.com/rxap/packages/commit/a54c5a3c8f1f32eb1eacb871bffe6537dafcf19c))

## [19.0.7-dev.6](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.5...rxap@19.0.7-dev.6) (2024-06-04)

### Bug Fixes

- update package groups ([838557c](https://gitlab.com/rxap/packages/commit/838557c374aa501c77ceb0b28fc14a625bad22a8))

## [19.0.7-dev.5](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.4...rxap@19.0.7-dev.5) (2024-06-03)

### Bug Fixes

- update package groups ([edc8ff5](https://gitlab.com/rxap/packages/commit/edc8ff5156e1cd55b72824e3872298e758c821b4))

## [19.0.7-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.3...rxap@19.0.7-dev.4) (2024-06-03)

### Bug Fixes

- update package groups ([94d029d](https://gitlab.com/rxap/packages/commit/94d029d6c580683072031408b4039c173c721d65))

## [19.0.7-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.2...rxap@19.0.7-dev.3) (2024-06-02)

### Bug Fixes

- update package groups ([2610cc0](https://gitlab.com/rxap/packages/commit/2610cc001bc57b765c8cd26ef47682b721adbc54))

## [19.0.7-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.0.7-dev.1...rxap@19.0.7-dev.2) (2024-06-02)

### Bug Fixes

- update package groups ([e734d2a](https://gitlab.com/rxap/packages/commit/e734d2a1af26bc48357f1ebc09b2b8e206485085))

## [19.0.7-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.0.6...rxap@19.0.7-dev.1) (2024-06-02)

### Bug Fixes

- update package groups ([4b4d132](https://gitlab.com/rxap/packages/commit/4b4d132025bcc86068cb52ea7e6dcd8fc83f1a4c))

## [19.0.7-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.0.6...rxap@19.0.7-dev.0) (2024-06-02)

### Bug Fixes

- update package groups ([e32bf7e](https://gitlab.com/rxap/packages/commit/e32bf7e667ebf12113bff34e48ce5a6ab5fc9e55))

## [19.0.6](https://gitlab.com/rxap/packages/compare/rxap@19.0.5...rxap@19.0.6) (2024-06-02)

### Bug Fixes

- update package groups ([0509897](https://gitlab.com/rxap/packages/commit/05098975cc90acf340aa8257f911e66d36d4db3e))

## [19.0.5](https://gitlab.com/rxap/packages/compare/rxap@19.0.5-dev.4...rxap@19.0.5) (2024-06-02)

**Note:** Version bump only for package rxap

## [19.0.5-dev.4](https://gitlab.com/rxap/packages/compare/rxap@19.0.5-dev.3...rxap@19.0.5-dev.4) (2024-06-02)

### Bug Fixes

- update package groups ([d32cf7e](https://gitlab.com/rxap/packages/commit/d32cf7e526845656e80071abc489518a9932e8b7))

## [19.0.5-dev.3](https://gitlab.com/rxap/packages/compare/rxap@19.0.4...rxap@19.0.5-dev.3) (2024-06-02)

### Bug Fixes

- update package groups ([3ebb393](https://gitlab.com/rxap/packages/commit/3ebb39369d022403c0f84cce8b4cb38629796f9c))
- update package groups ([533f78d](https://gitlab.com/rxap/packages/commit/533f78d855397dcfd94afe8c21511da5d273c6a9))
- update package groups ([cb79671](https://gitlab.com/rxap/packages/commit/cb796715225c885fa3816f33f19b6c719d39ed4e))

## [19.0.5-dev.2](https://gitlab.com/rxap/packages/compare/rxap@19.0.5-dev.1...rxap@19.0.5-dev.2) (2024-05-31)

### Bug Fixes

- update package groups ([71fccf6](https://gitlab.com/rxap/packages/commit/71fccf66044ddb3439098c164fa0166227eaaa5c))

## [19.0.5-dev.1](https://gitlab.com/rxap/packages/compare/rxap@19.0.5-dev.0...rxap@19.0.5-dev.1) (2024-05-31)

### Bug Fixes

- update package groups ([1fa4f52](https://gitlab.com/rxap/packages/commit/1fa4f52e25859a40d955aa40346101b7805bbd9e))

## [19.0.5-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.0.4...rxap@19.0.5-dev.0) (2024-05-31)

### Bug Fixes

- update package groups ([38017a7](https://gitlab.com/rxap/packages/commit/38017a76a2e4d8a2886485060e4ad3cf14b86ea6))

## [19.0.4](https://gitlab.com/rxap/packages/compare/rxap@19.0.2...rxap@19.0.4) (2024-05-30)

**Note:** Version bump only for package rxap

## [19.0.2](https://gitlab.com/rxap/packages/compare/rxap@19.0.2-dev.0...rxap@19.0.2) (2024-05-30)

**Note:** Version bump only for package rxap

## [19.0.2-dev.0](https://gitlab.com/rxap/packages/compare/rxap@19.0.1...rxap@19.0.2-dev.0) (2024-05-30)

**Note:** Version bump only for package rxap

## [19.0.1](https://gitlab.com/rxap/packages/compare/rxap@19.0.1-dev.0...rxap@19.0.1) (2024-05-30)

**Note:** Version bump only for package rxap

## [19.0.1-dev.0](https://gitlab.com/rxap/packages/compare/rxap@18.0.1...rxap@19.0.1-dev.0) (2024-05-30)

**Note:** Version bump only for package rxap

## [18.0.1](https://gitlab.com/rxap/packages/compare/rxap@18.0.1-dev.0...rxap@18.0.1) (2024-05-29)

**Note:** Version bump only for package rxap

## [18.0.1-dev.0](https://gitlab.com/rxap/packages/compare/rxap@17.0.1...rxap@18.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package rxap

## [17.0.1](https://gitlab.com/rxap/packages/compare/rxap@17.0.1-dev.0...rxap@17.0.1) (2024-05-29)

**Note:** Version bump only for package rxap

## [17.0.1-dev.0](https://gitlab.com/rxap/packages/compare/rxap@16.0.4...rxap@17.0.1-dev.0) (2024-05-29)

**Note:** Version bump only for package rxap

## [16.0.4](https://gitlab.com/rxap/packages/compare/rxap@16.0.3...rxap@16.0.4) (2024-05-28)

### Bug Fixes

- set correct tslib version ([6bf89b5](https://gitlab.com/rxap/packages/commit/6bf89b5c1cdf82e478b27a58893fd5e5b70c7de1))

## [16.0.3](https://gitlab.com/rxap/packages/compare/rxap@16.0.3-dev.4...rxap@16.0.3) (2024-05-28)

**Note:** Version bump only for package rxap

## [16.0.3-dev.4](https://gitlab.com/rxap/packages/compare/rxap@16.0.3-dev.3...rxap@16.0.3-dev.4) (2024-05-28)

### Bug Fixes

- update package groups ([bedd5ea](https://gitlab.com/rxap/packages/commit/bedd5eacdd3b70c7c8e13e00afe8e90d3d7d336d))

## [16.0.3-dev.3](https://gitlab.com/rxap/packages/compare/rxap@16.0.3-dev.2...rxap@16.0.3-dev.3) (2024-05-28)

### Bug Fixes

- update package groups ([d8951c2](https://gitlab.com/rxap/packages/commit/d8951c2878e81423cee1f8a471563bd89fabcb38))

## [16.0.3-dev.2](https://gitlab.com/rxap/packages/compare/rxap@16.0.3-dev.1...rxap@16.0.3-dev.2) (2024-05-28)

### Bug Fixes

- update package groups ([60e45fe](https://gitlab.com/rxap/packages/commit/60e45fed11a6fb60d32d9f4c3f31dc1264c4023b))

## [16.0.3-dev.1](https://gitlab.com/rxap/packages/compare/rxap@16.0.3-dev.0...rxap@16.0.3-dev.1) (2024-05-28)

### Bug Fixes

- update package groups ([e4e8f6d](https://gitlab.com/rxap/packages/commit/e4e8f6d605da0728ef5342252832260b4aab984e))

## [16.0.3-dev.0](https://gitlab.com/rxap/packages/compare/rxap@16.0.2...rxap@16.0.3-dev.0) (2024-05-28)

### Bug Fixes

- update package groups ([958cc21](https://gitlab.com/rxap/packages/commit/958cc21259d0e3c4424a30aa2ee487d0b81de3e5))

## [16.0.2](https://gitlab.com/rxap/packages/compare/rxap@16.0.2-dev.3...rxap@16.0.2) (2024-05-27)

**Note:** Version bump only for package rxap

## [16.0.2-dev.3](https://gitlab.com/rxap/packages/compare/rxap@16.0.2-dev.2...rxap@16.0.2-dev.3) (2024-05-27)

### Bug Fixes

- update package groups ([dbdbf8d](https://gitlab.com/rxap/packages/commit/dbdbf8dc0b455a323d49fcfcebdf774e023d2836))

## [16.0.2-dev.2](https://gitlab.com/rxap/packages/compare/rxap@16.0.2-dev.1...rxap@16.0.2-dev.2) (2024-05-27)

### Bug Fixes

- update package groups ([8ac836f](https://gitlab.com/rxap/packages/commit/8ac836f4716094abf85b20adefa1dd196f37cc05))

## [16.0.2-dev.1](https://gitlab.com/rxap/packages/compare/rxap@16.0.2-dev.0...rxap@16.0.2-dev.1) (2024-05-27)

### Bug Fixes

- remove verbose logging ([da76f16](https://gitlab.com/rxap/packages/commit/da76f168d22f9aba29915fa7408eb78b42beefa0))

## [16.0.2-dev.0](https://gitlab.com/rxap/packages/compare/rxap@16.0.1...rxap@16.0.2-dev.0) (2024-05-27)

**Note:** Version bump only for package rxap

## [16.0.1](https://gitlab.com/rxap/packages/compare/rxap@16.0.1-dev.0...rxap@16.0.1) (2024-05-27)

**Note:** Version bump only for package rxap

## 16.0.1-dev.0 (2024-05-27)

**Note:** Version bump only for package rxap
