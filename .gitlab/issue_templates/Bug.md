## Bug Report

### Summary
Provide a concise description of the issue.

### Steps to Reproduce
1. **Step 1:** Describe the first step
2. **Step 2:** Describe the second step
3. **Step 3:** Continue as needed

### Expected Behavior
Describe what you expected to happen.

### Actual Behavior
Describe what actually happened.

### Repository Link
Provide a link to a repository where the issue can be reproduced:
[Repository Link](https://example.com/repository)

### Environment
- **Operating System:** (e.g., Ubuntu 20.04, Windows 10)
- **Browser:** (e.g., Chrome 91, Firefox 89)
- **Application Version:** (e.g., v1.2.3)

### Additional Context
Add any other context about the problem here.

### Screenshots
If applicable, add screenshots to help explain your problem.

### Labels
<!-- Automatically assign labels like ~bug using quick actions -->
/label ~bug
