export interface SettingsControllerRemoveAtPropertyParameter {
  propertyPath: string;
  index: number;
}
